classdef Intensity < MeasurementModule
    %To measure object intensities over a given channel.
    
    properties        
        imageChannel
        %The ID of the channel on which the measurements will be carried
        %out. This will be used later on by the data (ECP) to retrieve the
        %corresponding image.
                
        measurementList
        %The name of the measurements in a field (the basic ones without any dynamic extention)
        
        dynamicSettings
        %Dynamic settings cellarray for distributions.
    end
    
    methods
        %Constrcutor
        function obj = Intensity(paramcell,~)
            obj.imageChannel = paramcell{1};
            
            %initialize in dynamicSettings                        
            obj.measurementList = obj.providedMeasurements();
            obj.dynamicSettings = cell(1,length(obj.measurementList));
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 1.01; 
            % Version history
            % 1     including the dynamic field: Intensity distribution
            %  .01  bugfix in the underlying CellProfiler module for mass
            %       displacement calculation
        end                                        
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            %Calling own providedMeasurements and measurementList is
            %ambiguous
            uniqueID = obj.uniqueID();
            if ~(length(obj.measurementList{selectedFeature})>=21 && strcmp(obj.measurementList{selectedFeature}(1:21),'IntensityDistribution'))
                fieldNames = obj.providedMeasurements();
                featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID];
            else
                nofBins = length(obj.dynamicSettings{selectedFeature})-1;
                featureNames = cell(1,nofBins);
                for i=1:nofBins
                    featureNames{i} = [obj.measurementList{selectedFeature} '_' num2str(obj.dynamicSettings{selectedFeature}(i)) '-' num2str(obj.dynamicSettings{selectedFeature}(i+1)) '_' uniqueID];
                end
            end
        end
        
        function data = configureDynamicColumns(obj,data,~,selectedFeature)
            function [bool,msg] = checkFunc(params)                
            %local function to check correctnesss of the dist GUI                
                [bool,msg] = checkNumber(params{1},0,0,[-Inf Inf],'Separators');                
            end
            
            if (length(obj.measurementList{selectedFeature})>=21 && strcmp(obj.measurementList{selectedFeature}(1:21),'IntensityDistribution'))
                fieldList = obj.providedMeasurements();
                paramarray{1}.name = 'Separators:';
                paramarray{1}.type = 'int';     
                infoText = sprintf(['You asked for distribution readouts in module:\n %s \n In feature: \n %s \n'...
                    'Please specify the bounds for the intensity distribution. (separate values with commas)']...
                    ,obj.uniqueID(),fieldList{selectedFeature});
                params = Settings_GUI(paramarray,'infoText',infoText,'checkFunction',@checkFunc);
                                
                obj.dynamicSettings{selectedFeature} = params{1};
                
            end
            
        end
        
        function id = uniqueID(obj)
            id = [class(obj) '_' obj.imageChannel];
        end
        
    end
    
    methods (Static)
        function [paramarray,checkVisibility] = getParameters(data)
            possibleChannels = listImageChannels(data);
            paramarray{1}.name = 'On which channel?';
            paramarray{1}.type = 'enum';
            paramarray{1}.values = possibleChannels;            
            checkVisibility = @(a,b)(defaultCheckVisibility(a,b,true));
        end                                        
    end
    
end

