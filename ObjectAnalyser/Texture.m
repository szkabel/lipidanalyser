classdef Texture < MeasurementModule
    %To measure object intensities over a given channel.
    
    properties
        %The ID of the channel on which the measurements will be carried
        %out. This will be used later on by the data (ECP) to retrieve the
        %corresponding image.
        imageChannel
        
        %The scale of the texture
        textureScale;
    end
    
    methods
        %Constrcutor
        function obj = Texture(paramcell,~)
            obj.imageChannel = paramcell{1};
            obj.textureScale = paramcell{2};
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 0;
        end                        
        
        function featureColumn = fetchBlockMeasurement(obj,data,imageID,objectName,selectedColumn)
            featureColumn = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
        end
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            fieldNames = obj.providedMeasurements();
            uniqueID = obj.uniqueID();
            featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID];
        end
        
        function data = configureDynamicColumns(~,data,~,~)
        end
        
        function id = uniqueID(obj)
            id = [class(obj) '_' obj.imageChannel '_' num2str(obj.textureScale)];
        end
        
    end
    
    methods (Static)
        function [paramarray,checkVisibility] = getParameters(data)
            possibleChannels = listImageChannels(data);
            paramarray{1}.name = 'On which channel?';
            paramarray{1}.type = 'enum';
            paramarray{1}.values = possibleChannels; 
            paramarray{2}.name = 'Texture scale';
            paramarray{2}.type = 'int';
            checkVisibility = @(a,b)(defaultCheckVisibility(a,b,true));
        end                                        
        
        function [bool,msg] = checkParameters(paramcell)
            [bool,msg] = checkNumber(paramcell{2},1,1,[1 Inf],'Texture scale');
        end
    end
    
end

