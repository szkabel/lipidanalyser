classdef RelatedMeasurement < MeasurementModule        
    %The module is used to provide aggregated measurements of objects that
    %are in some way related to the actually examined one. The
    % relation can be defined in many ways resulting in subclasses of
    %this class. This also means that this class is abstract and the
    %assignParents method must be implemented in the subclasses.
    %
    %   In all cases when we assign objects to others it is the
    %   case that for 1 examined object we might want to store several
    %   related object measurement however this is not possible. (e.g.
    %   store to lipid droplets the related mitochondria intensity then
    %   which mitochondrium's intensity should we relate? (assuming
    %   that we have many to choose from.) To overcome this problem one
    %   can apply several aggregation rules and assign aggregated
    %   values to the examined object. (continuing the example: assign
    %   the mean intensity of the mitochondria to the lipid droplets).
    %
    %   This module provides the aggregations as separate features. The
    %   possible aggregation methods are listed with the static
    %   aggregateFunctions method.        
    %
    % Rule of matching:
    %   In general an n to m typed relation is implemented here so arbitrary number of
    %   objects can be assigned together. (The inspiring idea here is to
    %   make it possible to relate subcellular particles to each other within
    %   a cell, so a single cell naturally forms a parent relation).
    %   However the idea of aggregating objects can not be restricted only 
    %   for cellular relations. E.g. neighbourhood relation needs a really
    %   similar approach (as we wish to aggregate objects together bases on
    %   some relation which is in this case the neighbourhood relation).
    %   Therefore the following specification comes:
    %   
    %   The parent relation is implemented via unique index (ID) arrays.
    %   The unique IDs form a group. Each object can be a part of multiple
    %   groups (including the case when it is is member of no group ie
    %   parentless). This approach provides high flexibility but also
    %   causes difficulties in the relation. 
    %   
    %   Which objects should be
    %   related to a specific one? Is it enough to have one single shared groupID or do we
    %   need an object which must be member of all of the current object's
    %   group? Or should the related object share at least a specific
    %   number of groups? This must be controlled in a programmable
    %   parameter here --> ownMatchingCriterion (numerical: 1->Inf, where Inf
    %   stands for that all group must match).
    %   
    %   NOTE: Currently if the related object meets to have at least the
    %   specified amount of matching groupIDs then it is considered as
    %   matching (i.e. there can be an arbitrary number of extra groupIDs
    %   in the related object if the criterion is met) However it is a
    %   possibility for extention that we also stand conditions that the
    %   related object cannot have other groupIDs (exact match).
    %                  
    %   Parentless (parent empty) secondary objects are equipped with 0
    %   related measurements.
    %      
    
    properties        
        otherObject
        %The name of the related object.
                
        relatedMeasureModule
        %The measurement module that is able to perform the related
        %measurement                
        
        selectedAggregationFunctions
        %Contains the user selected aggregation functions
                
        nofOriginalFeatures
        origFeatureIndices
        origFeatureNames
        origFeatureDynamic
        %Information regarding the related measurement
        %Number of features provided by the related measurement (non-aggregated)
                                
        filter
        %Filter similar to the filter in the 'main' process. This a
        %sturcture with 4 fields:
        %   index:  Array of the indices to which the condition is
        %           connected. In this special case the index is a subset
        %           of the array of 1:nofOriginalFeatures.
        %   condition: This is a string that represents a logical value. In
        %           the representation of the logical formula can use
        %           variables with the form #N#. During evaluation the #1#
        %           changes with same datas filter.index(1) element.
        %   true,false: values to give back based on the condition.
        %   
        %   See evalFilterOnRow function.
                
        measModuleToFilterOn
        %The module of which measurements are used for filtering. This is
        %also run during the measure method.
                
        dynamicSettings
        %Dynamic settings cellarray for distributions.
                
        ownMatchingCriterion = 1; 
        %A number specifying how many matches do we need so that an object
        %is actually related to another one (where the number referes to
        %the number of the groups that are actually shared) Default is one,
        %so that one shared group is enough for the match. The default
        %implements the any(OR) relation.
    end
    
    methods
        %% INTERFACE METHODS
        
        %Constrcutor which is unique in many ways. The paramcell's first
        %couple of entries are actual parameters for this module the others
        %are parameters of the RELATED Measurement Module.
        %
        % Additionally there is a hidden parameter that is coming from the
        % static 'data' (hidden parameter is the selected related
        % measurement module name and the other object's name)
        %
        %Moreover this constructor creates its own GUI to fetch more
        %parameters for itself, namely to define filters on the
        %aggregation.
        function obj = RelatedMeasurement(paramcell,data)
            disp('RelatedMeasurementConstructor run');
            obj.otherObject = paramcell{1};
            uiwait(msgbox('In the following you''ll be asked to provide the related measurement!','Select related module'));                
            %Note: it might be possible to improve the user experience by
            %using only the selected features for relations (1st and 2nd outputs)
            %this includes the constructor of related measurement, hopefully the multiple instance will not conflict                        
            [obj.origFeatureIndices,obj.origFeatureNames,obj.relatedMeasureModule,obj.origFeatureDynamic] = AddMeasurement_GUI(data);
            if isempty(obj.origFeatureIndices) || (length(obj.origFeatureIndices) == 1 && obj.origFeatureIndices(1) == -1)
                error('LipidAnalyser:ModuleConstruct','You must specify the related measurement before going on.');
            end
            
            %get the selected aggregation functions
            obj.selectedAggregationFunctions = find(ismember(RelatedMeasurement.aggregateFunctions(),paramcell{2}));
                        
            obj.nofOriginalFeatures = length(obj.relatedMeasureModule.providedMeasurements());
            
            %initialize in dynamicSettings
            thisFeatures = obj.providedMeasurements();
            obj.dynamicSettings = cell(1,length(thisFeatures));                        
                        
            obj.getFiltersFromUser(paramcell{3},data,obj.origFeatureNames,obj.origFeatureDynamic,obj.relatedMeasureModule);            
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 0;
        end
        
        function [fieldList,dynamicFields,aggregateFunc,originalFeatureNumber] = providedMeasurements(obj)                                                
            fieldList = obj.origFeatureNames;
            originalDynamic = obj.origFeatureDynamic;
            %get the selected aggregation functions
            [aggrFuncs,dynamic] = obj.getSelectedAggregatedFunctions();            
            
            nofFu = length(aggrFuncs);
            fieldLength = nofFu*length(fieldList);
            newFieldList = cell(1,fieldLength);
            dynamicFields = zeros(1,fieldLength);
            aggregateFunc = cell(1,fieldLength);
            originalFeatureNumber = zeros(1,fieldLength);            
            for i=1:fieldLength
                aggFunIndex = mod(i-1,nofFu)+1;
                currentOriginalFieldIndex = floor((i-1)/nofFu)+1;
                newFieldList{i} = [fieldList{currentOriginalFieldIndex} '-' aggrFuncs{aggFunIndex}];
                aggregateFunc{i}.number = aggFunIndex;
                aggregateFunc{i}.name = aggrFuncs{aggFunIndex};
                originalFeatureNumber(i) = obj.origFeatureIndices(currentOriginalFieldIndex);
                if originalDynamic(currentOriginalFieldIndex)                                            
                    dynamicFields(i) = 1;                    
                else
                    dynamicFields(i) = dynamic(aggFunIndex);
                end
            end           
            fieldList = newFieldList;
            fieldList{end+1} = 'NofRelatedElements';   
            dynamicFields(end+1) = 0;
            aggregateFunc{end+1} = []; % empty aggregation
            originalFeatureNumber(end+1) = Inf; %the original feature should be big to make it invalid
        end
                
        function [featureStrs] = measure(obj,data,imageID,~)            
            simpleRelatedFeatureStrs = calculateRequestedMeasurement( data,obj.relatedMeasureModule,imageID,obj.otherObject); 
            %Ask for the related measurement to run
            %The result can be a long array of measurements given we have
            %nested related modules
            if ~isempty(simpleRelatedFeatureStrs)
                featureStrs = simpleRelatedFeatureStrs;
            else
                featureStrs = {};
            end
            if ~isempty(obj.measModuleToFilterOn)
                filterFeatureStrs = calculateRequestedMeasurement( data,obj.measModuleToFilterOn,imageID,obj.otherObject);
                if ~isempty(filterFeatureStrs)
                    featureStrs(end+1) = filterFeatureStrs;
                end
            end
            %Relate modules doesn not need any own calculations during
            %measurement to avoid data redundancy in the anyway huge data
            %sets.       
        end                
                 
        function oneBlock = fetchBlockMeasurement(obj,data,imageID,selfObjectName,selectedFeature)        
        %This is the soul function of this class.
        %Warning: before calling this function one should ensure that the
        %corresponding storeRequestedMeasurement was called.         
            [~,dynamic,aggregateFuncs,origFeatures] = obj.providedMeasurements();
            
            selectedOriginalFeature = origFeatures(selectedFeature);
            
            %Mind the fact that the results here are cellarrays, where each
            %entry is another array specifying the groupIDs to which the
            %object is assigned
            [ownParents,relatedParents] = obj.getObjectRelationForImg(data,imageID,selfObjectName);            
            
            filterIndices = obj.filteredIndexArray(data,imageID,length(relatedParents)); %gets the filters for the actual block
            
            if ~isempty(obj.dynamicSettings{selectedFeature})
                oneBlock = zeros(length(ownParents),sum(cellfun('length',obj.dynamicSettings{selectedFeature}.edges)-1)); %reserve space for result
            elseif dynamic(selectedFeature)
                resolvedFeatureNames = obj.relatedMeasureModule.resolveDynamicFeatures(selectedOriginalFeature);
                oneBlock = zeros(length(ownParents),length(resolvedFeatureNames));
            else
                oneBlock = zeros(length(ownParents),1); %reserve space for result
            end
            
            maxParentID = max([...
                    cellfun(@max,relatedParents(~cellfun(@isempty,relatedParents)));...
                    cellfun(@max,    ownParents(~cellfun(@isempty,    ownParents)));...
                    0 ... % 0 in case both of those are completely empty
                    ]);
            transposedRelatedParents = transposeNeighbourArray(relatedParents,maxParentID);
                        
            if selectedOriginalFeature<=obj.nofOriginalFeatures %if we ask for regular features
                
                oneRelatedBlock = obj.relatedMeasureModule.fetchBlockMeasurement(data,imageID,obj.otherObject,selectedOriginalFeature);
                
                selectedAggregateFunc = aggregateFuncs{selectedFeature}.name;
                                
                                
                for i=1:length(ownParents)
                    if ~isempty(ownParents{i})
                        currMatchBool = obj.calcMatchCriterion(transposedRelatedParents,ownParents{i},length(relatedParents));
                        relValues = oneRelatedBlock(currMatchBool & filterIndices,:);
                        oneBlock(i,:) = obj.evalAggregateFunc(selectedAggregateFunc,relValues,obj.dynamicSettings{selectedFeature});
                    end                        
                end                
                
            else % if we ask for the number of aggregated elements after filtering                
                for i=1:size(oneBlock,1)
                    if ~isempty(ownParents{i})
                        currMatchBool = obj.calcMatchCriterion(transposedRelatedParents,ownParents{i},length(relatedParents));
                        oneBlock(i) = sum(currMatchBool & filterIndices);
                    end
                end                    
            end %Nof features if-else end
            
        end
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            [fieldNames,~,aggregateFuncs,origFeatures] = obj.providedMeasurements();
            uniqueID = obj.uniqueID();                        
            
            selectedOriginalFeature = origFeatures(selectedFeature);
            
            if selectedOriginalFeature <= obj.nofOriginalFeatures % if the feature is not the aggregated number
                selectedAggregateFunc = aggregateFuncs{selectedFeature}.name;
                if strcmp(selectedAggregateFunc,'distribution')
                    currEdges = obj.dynamicSettings{selectedFeature}.edges;
                    individualLengths = cellfun('length',currEdges);
                    featureNames = cell(1,sum(individualLengths-1));
                    %TODO here the nesting is missing, or not?
                    relatedFeatureNames = obj.relatedMeasureModule.resolveDynamicFeatures(selectedOriginalFeature);
                    kk = 1;
                    for j=1:length(currEdges)
                        for k = 1:(individualLengths(j)-1)
                            featureNames{kk} = ...
                                [relatedFeatureNames{j} '-' aggregateFuncs{selectedFeature}.name  ...
                                '_'...
                                num2str(currEdges{j}(k)) '-' num2str(currEdges{j}(k+1)) '_' uniqueID];
                            kk = kk+1;
                        end
                    end
                else
                    relatedFeatureNames = obj.relatedMeasureModule.resolveDynamicFeatures(selectedOriginalFeature);
                    featureNames = cell(1,length(relatedFeatureNames));
                    for j=1:length(featureNames)
                        featureNames{j} = [ relatedFeatureNames{j} '-' aggregateFuncs{selectedFeature}.name '_' uniqueID];
                    end
                end
            else            
                featureNames = {[ fieldNames{selectedFeature} '_' uniqueID]};
            end
        end
        
        function data = configureDynamicColumns(obj,data,imageSet,selectedFeature)   
            function [bool,msg] = checkFunc(params)
                if strcmp(params{1},'Automatic')
                    bool = 1; msg = '';
                elseif strcmp(params{1},'Step size')
                    [bool,msg] = checkNumber(params{2},0,1,[0 Inf],'Step size');
                elseif strcmp(params{1},'Number of bags')
                    [bool,msg] = checkNumber(params{2},1,1,[0 Inf],'Number of bags');
                elseif strcmp(params{1},'Separators')
                    [bool,msg] = checkNumber(params{2},0,0,[-Inf Inf],'Separators');
                end
            end
            [fieldList,~,aggregateFuncs,origFeatures] = obj.providedMeasurements();
            
            selectedOriginalFeature = origFeatures(selectedFeature);
            
            if selectedOriginalFeature <= obj.nofOriginalFeatures % if the feature is not the aggregated number                
                if strcmp(aggregateFuncs{selectedFeature}.name,'distribution')
                    %ask for distribution info with a custom GUI                    
                    paramarray{1}.name = 'Binning method';
                    paramarray{1}.type = 'enum';
                    paramarray{1}.values = {'Automatic','Step size','Number of bags','Separators'};
                    paramarray{2}.name = 'Parameter if not automatic:';
                    paramarray{2}.type = 'int';     
                    infoText = sprintf('You asked for distribution readouts in module:\n %s \n In feature: \n %s ',obj.uniqueID(),fieldList{selectedFeature});
                    params = Settings_GUI(paramarray,'infoText',infoText,'checkFunction',@checkFunc);
                    
                    %THOUGHT: Create a bar plot automatically (?)
                    
                    %In case of separators there is no need to go through
                    %data
                    if strcmp(params{1},'Separators')
                        data = obj.relatedMeasureModule.configureDynamicColumns(data,imageSet,selectedOriginalFeature);
                        featureNamesOfTheRelatedModule = obj.relatedMeasureModule.resolveDynamicFeatures(selectedOriginalFeature);
                        for j=1:length(featureNamesOfTheRelatedModule)
                            obj.dynamicSettings{selectedFeature}.edges{j} = params{2};
                        end
                    else
                        %Big for loop, parallel if possible
                        obj.relatedMeasureModule.configureDynamicColumns(data,imageSet,selectedOriginalFeature);
                        featureStr = cell(length(imageSet),1);
                        if checkToolboxByName('Parallel Computing Toolbox')                            
                            parfor i=1:length(imageSet)
                                imageID = getImageID(data,imageSet(i));                                                     
                                featureStr{i} = calculateRequestedMeasurement(data,obj.relatedMeasureModule,imageID,obj.otherObject); %Calculate
                            end
                        else
                            for i=1:length(imageSet)
                                imageID = getImageID(data,imageSet(i));                                                     
                                featureStr{i} = calculateRequestedMeasurement(data,obj.relatedMeasureModule,imageID,obj.otherObject); %Calculate
                            end
                        end
                        completeData = cell(length(imageSet),1);
                        for i=1:length(imageSet)
                            imageID = getImageID(data,imageSet(i));  
                            [ data ] = storeRequestedMeasurement( data,featureStr{i}); %Store
                            completeData{i} = obj.relatedMeasureModule.fetchBlockMeasurement(data,imageID,obj.otherObject,selectedOriginalFeature);                                                                            
                        end
                        completeData = cell2mat(completeData);

                        %Parameter switch:
                        for j=1:size(completeData,2) % in a hope that during 'compilation' it is recognized that the if's are independent from the for...
                            actData = completeData(:,j);
                            if strcmp(params{1},'Automatic')                        
                                    [~,edges] = histcounts(actData);
                                    obj.dynamicSettings{selectedFeature}.edges{j} = edges;                        
                            elseif strcmp(params{1},'Step size')                                         
                                    [~,edges] = histcounts(actData,min(actData):params{2}:max(actData));
                                    obj.dynamicSettings{selectedFeature}.edges{j} = edges;
                            elseif strcmp(params{1},'Number of bags')                        
                                    [~,edges] = histcounts(actData,params{2});
                                    obj.dynamicSettings{selectedFeature}.edges{j} = edges;                        
                            end
                        end
                    end
                    
                    %Configure also the filters
                    if ~isempty(obj.measModuleToFilterOn) && ~isempty(obj.filter)
                        for i=1:length(obj.filter.index)
                            obj.measModuleToFilterOn.configureDynamicColumns(data,imageSet,obj.filter.index(i));
                        end
                    end
                    
                else % configure other related measurements
                    obj.relatedMeasureModule.configureDynamicColumns(data,imageSet,selectedOriginalFeature);
                end
            end
        end
        
        function id = uniqueID(obj)            
            if ~isempty(obj.filter)
                %filter out most of the 'bad' characters to ensure the
                %uniqueness of the measurement but meanwhile producing a
                %valid structure field.
                filterText = obj.filter.condition;
                %filterText = transformToValidFieldName(filterText);
                filterModuleUniqueID = obj.measModuleToFilterOn.uniqueID();
                filterText = ['_' filterText '_' filterModuleUniqueID];
            else
                filterText = [];
            end
            id = [class(obj) '_' obj.otherObject '_Filter::[' filterText ']'];
        end
        
        %% CLASS SPECIFIC FUNCTIONS
        
        function [aggrFuncs,dynamic] = getSelectedAggregatedFunctions(obj)
            [aggrFuncs,dynamic] = RelatedMeasurement.aggregateFunctions();
            aggrFuncs = aggrFuncs(obj.selectedAggregationFunctions);
            dynamic = dynamic(obj.selectedAggregationFunctions);
        end
        
        function getFiltersFromUser(obj,filterSpec,data,originalFeatures,originalDynamic,relatedMeasureModule)
            if (strcmp(filterSpec,'yes-itself'))
                SelectedFeatures = listdlg('PromptString','Select the features on which you whish to condition:','ListSize',[300,300], 'Name', 'Select measurements', 'SelectionMode','multiple','ListString',originalFeatures);
                if any(originalDynamic(SelectedFeatures))
                    warndlg('You are not allowed to condition on dynamic columns.');
                    SelectedFeatures(originalDynamic(SelectedFeatures)) = []; %delete the invalid filter columns,
                end
                obj.filter = AddFilter_GUI(SelectedFeatures,length(originalFeatures),originalFeatures(SelectedFeatures),0);
                obj.measModuleToFilterOn = relatedMeasureModule;
            elseif (strcmp(filterSpec,'yes-other module'))
                uiwait(msgbox('In the following you''ll be asked to provide the measurement on which you want to condition later on!','Select conditioning module'));                
                [~,~,module] = AddMeasurement_GUI(data);
                if ~isempty(module)                                                                
                    [originalFeatures,relatedDynamic] = module.providedMeasurements();
                    SelectedFeatures = listdlg('PromptString','Select the features on which you whish to condition:','ListSize',[300,300], 'Name', 'Select measurements', 'SelectionMode','multiple','ListString',originalFeatures);
                    if any(relatedDynamic(SelectedFeatures))
                        warndlg('You are not allowed to condition on dynamic columns.');
                        SelectedFeatures(relatedDynamic(SelectedFeatures)) = []; %delete the invalid filter columns,
                    end
                    obj.filter = AddFilter_GUI(SelectedFeatures,length(originalFeatures),originalFeatures(SelectedFeatures),0);                    
                    obj.measModuleToFilterOn = module;
                else
                    obj.filter = [];
                    obj.measModuleToFilterOn = [];
                end                
            elseif (strcmp(filterSpec,'yes-preconfigured'))
                obj.selectPreconfiguredFilter(data);
            else
                obj.filter = [];
                obj.measModuleToFilterOn = [];
            end            
        end
    
        function filterIndices = filteredIndexArray(obj,data,imageID,nofRelatedObjects)
        %Calculates which related objects fulfil the filter condition
            if ~isempty(obj.filter)
                filterSize = length(obj.filter.index);
                usedFilter = obj.filter; % create a copy for the reindexing.
                matrixToFilterOn = zeros(nofRelatedObjects,filterSize);
                filterIndices = zeros(nofRelatedObjects,1);
                for i=1:filterSize                    
                    matrixToFilterOn(:,i) = obj.measModuleToFilterOn.fetchBlockMeasurement(data,imageID,obj.otherObject,usedFilter.index(i));
                    usedFilter.index(i) = i;
                    usedFilter.true = '1';
                    usedFilter.false = '0';
                end
                for i=1:nofRelatedObjects
                    filterIndices(i) = evalFilterOnRow(matrixToFilterOn(i,:),usedFilter);
                end
            else
                filterIndices = ones(nofRelatedObjects,1);
            end
        
        end
        
        function currMatchBool = calcMatchCriterion(obj,transposedRelatedParents,toMatch,relatedLength)
        %Gives back a bool array as long as relatedParents
        %This implements the matching rule described above

            if size(transposedRelatedParents,2)>1, transposedRelatedParents = transposedRelatedParents'; end
            %This was crazy slow
            %nofMatches = cellfun(@sum,cellfun(@ismember,repmat({toMatch},length(relatedParents),1),relatedParents,'UniformOutput',false));
            nofMatches = zeros(relatedLength,1);            
            for j=1:length(toMatch)
                for i=1:length(transposedRelatedParents{toMatch(j)})
                    nofMatches(transposedRelatedParents{toMatch(j)}(i)) = nofMatches(transposedRelatedParents{toMatch(j)}(i)) + 1;
                end
            end            
            if isinf(obj.ownMatchingCriterion)
                minMatch = length(toMatch);
            else
                minMatch = obj.ownMatchingCriterion;
            end
            currMatchBool = nofMatches >= minMatch;        
        end
        
        function aggregatedValue = evalAggregateFunc(~,selectedAggregateFunc,relValues,dynSet)
            if strcmp(selectedAggregateFunc,'distribution')
                %rel values must have length(dynamicSettings) number of colums
                individualLengths = cellfun('length',dynSet.edges);
                aggregatedValue = zeros(1,sum(individualLengths-1));
                kk = 1;
                for j=1:length(dynSet.edges)
                    bin = histcounts(relValues(:,j),dynSet.edges{j});
                    aggregatedValue(kk:kk+length(bin)-1) = bin;
                    kk = kk + length(bin);
                end
            else
                if ~isempty(relValues)
                    aggregatedValue = feval(selectedAggregateFunc,relValues);
                else
                    aggregatedValue = 0;
                end
            end
        end
        
        function selectPreconfiguredFilter(obj,data) %#ok<INUSD> used in eval
            preconFilterList = {'Overlap','overlapFunc';'Non-overlap','nonOverlapFunc'};
            [selectedPreconFilter,isOK] = listdlg('PromptString','Select a preconfigured filter:', 'Name', 'Select built-in filters', 'SelectionMode','single','ListString', preconFilterList(:,1),'ListSize',[300,300]);
            if isOK
                eval(['obj.' preconFilterList{selectedPreconFilter,2} '(data)']);
            else
                obj.filter = [];
                obj.measModuleToFilterOn = [];
            end
        end
        
        %% PRECONFIGURED FILTER FUNCTIONS
        function overlapFunc(obj,data)
            objectNames = getObjectNames(data);
            [selectedObject,isOK] = listdlg('PromptString','Select the object with which examine overlap:', 'Name', 'Select object for overlap', 'SelectionMode','single','ListString', objectNames,'ListSize',[300,300]);
            [selectedParentObject,isOKParent] = listdlg('PromptString','Select the object used as parent for overlap.', 'Name', 'Select parent object for overlap', 'SelectionMode','single','ListString', objectNames,'ListSize',[300,300]);
            if isOK && isOKParent
                paramcell = {objectNames{selectedObject},1,1,objectNames{selectedParentObject},'no',0,[]};
                obj.measModuleToFilterOn = ObjectRelation(paramcell,data);
                filt.index = 1; %WARNING BURNT IN! The overlap is the 1st index in the object relation.
                filt.condition = '#1#>0';
                filt.true = '1';
                filt.false = '0';
                obj.filter = filt;                
            else
                obj.filter = [];
                obj.measModuleToFilterOn = [];
            end            
        end
        
        function nonOverlapFunc(obj,data)
            objectNames = getObjectNames(data);
            [selectedObject,isOK] = listdlg('PromptString','Select the object with which examine non-overlap:', 'Name', 'Select object for overlap', 'SelectionMode','single','ListString', objectNames,'ListSize',[300,300]);
            [selectedParentObject,isOKParent] = listdlg('PromptString','Select the object used as parent for overlap.', 'Name', 'Select parent object for overlap', 'SelectionMode','single','ListString', objectNames,'ListSize',[300,300]);
            if isOK && isOKParent
                paramcell = {objectNames{selectedObject},1,1,objectNames{selectedParentObject},'no',0,[]};
                obj.measModuleToFilterOn = ObjectRelation(paramcell,data);
                filt.index = 1; %WARNING BURNT IN! The overlap is the 1st index in the object relation.
                filt.condition = '#1# == 0';
                filt.true = '1';
                filt.false = '0';
                obj.filter = filt;                
            else
                obj.filter = [];
                obj.measModuleToFilterOn = [];
            end            
        end
        
    end
%% Abstract method for selecting parent
    methods (Abstract)  
        [ownParents,relatedParents] = getObjectRelationForImg(obj,data,imageID,selfObjectName);
    end
    
%% STATIC METHODS
    
    methods (Static)
        function paramarray = getParameters(data)                                    
            paramarray{1}.type = 'enum';
            paramarray{1}.name = 'Related object:';
            paramarray{1}.values = getObjectNames(data);
            
            paramarray{2}.type = 'multiple-enum';
            paramarray{2}.name = 'Choose aggregation functions:';
            paramarray{2}.values = RelatedMeasurement.aggregateFunctions();

            paramarray{3}.type = 'enum';
            paramarray{3}.name = 'Apply filters:';
            paramarray{3}.values = {'no','yes-itself','yes-other module','yes-preconfigured'};
        end                               
        
        function [SelectedModule, isOK] = askForRelatedMeasurementModuleName()
        %the input condition if given then the text is changed in the
        %dialog
        %gives back module by name
            listOfMeasurementModules = listMeasurementModules('root'); %only simple modules can be used for relation
            
            [SelectedModule,isOK] = listdlg('PromptString','Select a related measurement module:', 'Name', 'Select modules', 'SelectionMode','single','ListString', listOfMeasurementModules,'ListSize',[300,300]);            
            
            if isOK
                SelectedModule = listOfMeasurementModules{SelectedModule};
            else
                SelectedModule = [];
            end
        end
        
        function [aggFuncs,dynamic] = aggregateFunctions()
            aggFuncs = {'mean','std','median','min','max','sum','distribution'};
            dynamic = [0,       0,      0,      0,     0,     0,      1];
        end                                    
    end
    
end

