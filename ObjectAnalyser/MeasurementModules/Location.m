classdef Location < MeasurementModule
    %Location: to get the absolute location of the center of an object and
    %its relative position to its parent's center. For objects without
    %parent their relative position is the same as their absolute.
    % This is the COORDINATE SYSTEM of CellProfiler = regionprops etc.
        
    properties        
        parentObject;
    end
    
    methods
        function obj = Location(paramcell,~)
            obj.parentObject = paramcell{1};
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 0;
        end
        
        function [fieldList,dynamicFields] = providedMeasurements(~)
            fieldList = {'AbsoluteX','AbsoluteY','RelativeX','RelativeY'};
            dynamicFields = zeros(size(fieldList));
        end
        
        function [featureStrs] = measure(obj,data,imageID,objectName)            
            %get out absolute positions
            mask = retrieveMaskImage(data,objectName,imageID);
            R = regionprops(mask,'Centroid');
            objectCenters = cat(1,R.Centroid);
            if isempty(objectCenters), featureStrs = obj.createFeatureStructure([],objectName,imageID); return; end
            %objectCenters 1:2 is needed to omit the possible 3rd dimension
            %from overlapping representation
            features(:,1:2) = objectCenters(:,1:2);
                    
            %relative features to specified parent
            parents = retrieveParents(data,imageID,objectName,obj.parentObject);            
            parentMask = retrieveMaskImage( data, obj.parentObject, imageID);
            R = regionprops(parentMask,'Centroid');
            parentCenters = cat(1,R.Centroid);
            extendedParentCentersWithZeros = zeros(size(features,1),2);
            %same here: need to check for overlapping objects
            extendedParentCentersWithZeros(parents>0,:) = parentCenters(parents(parents>0),1:2);
            features(:,3:4) = features(:,1:2) - extendedParentCentersWithZeros;                        
            
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
            
        end
        
        function featureColumn = fetchBlockMeasurement(obj,data,imageID,objectName,selectedColumn)
            featureColumn = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
        end
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            fieldNames = obj.providedMeasurements();
            uniqueID = obj.uniqueID();
            featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID]; %#ok<USENS> set in eval
        end
        
        function data = configureDynamicColumns(~,data,~,~)
        end
        
        function id = uniqueID(obj)
            id = [class(obj) '_Parent::' obj.parentObject];
        end
    end
    
    methods (Static)                
        function [paramarray,checkVisibility] = getParameters(data)
            i = 1;
            paramarray{i}.name = 'Object for relative position';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = getObjectNames(data);
            checkVisibility = @(a,b)(defaultCheckVisibility(a,b,true));
        end
    end
    
end

