classdef IntensityGray < Intensity
    %To measure object intensities over a given channel.            
    methods
        
        function obj = IntensityGray(paramcell,~)
            obj = obj@Intensity(paramcell);
        end
        
        function [fieldList,dynamicFields] = providedMeasurements(~)
            %fake measure to get available fields
           [~,fieldList] = performCPMeasurement(zeros(2),zeros(2),'MeasureObjectIntensityMLGOC');
           fieldList{end+1} = 'IntensityDistribution';
           dynamicFields = zeros(size(fieldList));
           dynamicFields(end) = 1;
        end
        
        function [featureStrs] = measure(obj,data,imageID,objectName)
            mask = retrieveMaskImage(data,objectName,imageID);
            originalImage = retrieveOriginalImage(data,imageID,obj.imageChannel);
            [features,~] = performCPMeasurement(mask,originalImage,'MeasureObjectIntensityMLGOC');
            
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
        end
        
        function oneBlock = fetchBlockMeasurement(obj, data,imageID,objectName,selectedColumn)
            if strcmp(obj.measurementList{selectedColumn},'IntensityDistribution')
                mask = retrieveMaskImage(data,objectName,imageID);
                nofObjects = max(mask(:));
                originalImage = retrieveOriginalImage(data,imageID,obj.imageChannel);
                oneBlock = zeros(nofObjects,length(obj.dynamicSettings{selectedColumn})-1);                
                for i=1:nofObjects
                    oneBlock(i,:) = histcounts(originalImage(mask == i),obj.dynamicSettings{selectedColumn});
                end
            else
                oneBlock = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
            end
        end                        
    end
         
end

