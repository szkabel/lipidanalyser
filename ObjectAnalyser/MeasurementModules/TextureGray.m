classdef TextureGray < Texture
    %To measure object texture over a given channel for grayscale images        
    methods                                
        function obj = TextureGray(paramcell,~)
            obj = obj@Texture(paramcell);
        end
         
        function [fieldList,dynamicFields] = providedMeasurements(~)
           %fake measure to get available fields
           [~,fieldList] = performCPMeasurement(zeros(2),zeros(2),'MeasureTextureMLGOC',{struct('index',8,'value',3)});           
           dynamicFields = zeros(size(fieldList));
        end  
        
        function [featureStrs] = measure(obj,data,imageID,objectName)
            originalImage = retrieveOriginalImage(data,imageID,obj.imageChannel);
            mask = retrieveMaskImage(data,objectName,imageID);
            paramArrayStruct{1} = struct('index',8,'value',num2str(obj.textureScale)); %The texture scale is the 8th parameter of the MeasureTextureMLGOC module
            [features,~] = performCPMeasurement(mask,originalImage,'MeasureTextureMLGOC',paramArrayStruct);
            
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
        end                                                               
        
    end            
    
end

