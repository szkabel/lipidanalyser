classdef ObjectRelation < MeasurementModule
    %Object relation module. This module does measurements which require to
    %compare the selected object to another type of object. As a special
    %case it is also possible to compare an object to itself.
    %   
    %   The measurement module provides several features see function
    %   measureObjectRelation.
    %   
    %   Behaviour based on the parameter: group by parent:
    %       If we group by parent then based on a specific image
    %       all the obj1 and obj2 typed objects are collected and grouped
    %       according to their parent value. Warning: for
    %       'objects compared to themselves and even groupping by
    %       themselves' option is not meaningful,
    %       the result will be comparison to an empty set. 
    %       In other words each object1 element are grouped by their parent and they are
    %       compared to the type2 objects which has the same parent. (like
    %       comparing subcellular particles within the same cell)
    %       This results that in prim-seco comparisons everything is
    %       compared to its natural related objects.           
    %       
    %       Still remaining special case: what happens with objects that
    %       has no parent? They will be compared to an empty set.
    %
    %   Self relation rule: if the selected other object is the same as the
    %   currently examined object the class performs the so called
    %   'self-relation' measurements. These are exactly the same as the
    %   non-self relations except for some special cases. By definition of
    %   overlap index in the non-self relation case the overlap index would
    %   be always 1 as an object completely overlaps with itself, therefore
    %   instead we only see how much an object overlaps with the same type
    %   of object excluding itself. It works similarly for the other
    %   measurements. In the implementation we extract as a single object
    %   the current object to mask1 and delete it from the mask2.
    %       
    
    properties 
        %The name of the other object to which we measure the relation
        otherObjectName
        %The parameter k for the knn in object relation
        k
        %The parameter l for the lth closest object
        l
        %Group by parent, the name of the object to be used as parent for
        %grouping
        groupByParent
        %Provided features (mapped directly from measureObjectRelationFunction)
        providedFeatures
        
        %Restrict the neighbours: values can be
        %'no','yes-ring','yes-k-closest'
        restrictNghbrs;
        %if above starts with yes, then a parameter is stored
        nghbrsParam;
    end
    
    methods
        %Constrcutor
        function obj = ObjectRelation(paramcell,~)
            i = 1;
            obj.otherObjectName = paramcell{i};
            i = i+1;
            obj.k = paramcell{i};
            i = i+1;
            obj.l = paramcell{i};
            i = i+1;            
           obj.groupByParent = paramcell{i};
            i = i+1;            
            obj.restrictNghbrs = paramcell{i};
            if strcmp(obj.restrictNghbrs,'yes-ring') || strcmp(obj.restrictNghbrs,'yes-k-closest')
                obj.nghbrsParam = paramcell{i+1};            
            end
            
            %fake measure to get available fields
            obj.providedFeatures = obj.providedMeasurements();
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 0.11;
        end
        
        function [fieldList,dynamicFields] = providedMeasurements(~)
           %fake measure to get available fields
           m1.Mask = zeros(2);
           m1.Centers = [];
           m1.MaxRadius = [];
           m1.PixelChains = [];
           m2 = m1;
           [~,fieldList] = measureObjectRelation(m1,m2,0,0);
           dynamicFields = zeros(size(fieldList));
        end               
        
        function [featureStrs] = measure(obj,data,imageID,objectName)
            %get the mask image in general.
            mask1 = retrieveMaskImage(data,objectName,imageID);
            mask2 = retrieveMaskImage(data, obj.otherObjectName, imageID);
                                       
            nofObj1 = max(mask1(:));            
            
            if nofObj1 == 0
                features = NaN(1,length(obj.providedFeatures));            
            else                
                
                mask1Struct = createMaskStructure(mask1);
                mask2Struct = createMaskStructure(mask2);
                selfBool = strcmp(objectName,obj.otherObjectName);
                               
                parents1 = retrieveParents(data,imageID,objectName,obj.groupByParent);
                parents2 = retrieveParents(data,imageID,obj.otherObjectName,obj.groupByParent);                
                features = zeros(length(parents1),length(obj.providedFeatures));
                for i = convert2RowVector(unique(parents1))
                    currentGroup1 = find(parents1 == i);                                                                
                    if i~=0 % if object 1 has no parents then compare it to an empty set
                        currentGroup2 = find(parents2 == i);                    
                    else
                        currentGroup2 = [];
                    end
                    mask1Sub = subsetMaskStructure(mask1Struct,currentGroup1);
                    mask2Sub = subsetMaskStructure(mask2Struct,currentGroup2);
                    if strcmp(obj.restrictNghbrs,'no')
                        [mask1Array,mask2Array,mask1Indices] = prepareMasksForSelfRelation(mask1Sub,mask2Sub,selfBool);
                        for j=1:length(mask1Array)
                            features(currentGroup1(mask1Indices{j}),:) = measureObjectRelation(mask1Array{j},mask2Array{j},obj.k, obj.l);
                        end                    
                    else
                        neighParamStruct.type = obj.restrictNghbrs(5:end); %remove yes- from the beginning
                        neighParamStruct.parameter = obj.nghbrsParam;
                        neighboursForObj1 = calculateNeighbours(mask1Sub,mask2Sub,selfBool,neighParamStruct);
                        for j=1:length(neighboursForObj1)
                            singleMask1 = subsetMaskStructure(mask1Sub,j);
                            singleMaskNeighbours = subsetMaskStructure(mask2Sub,neighboursForObj1{j});
                            features(currentGroup1(j),:) = measureObjectRelation(singleMask1,singleMaskNeighbours,obj.k, obj.l);
                        end
                    end
                end
            end
           
            featureStrs = obj.createFeatureStructure(features,objectName,imageID); 
        end
        
        function featureColumn = fetchBlockMeasurement(obj,data,imageID,objectName,selectedColumn)
            featureColumn = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
        end
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            fieldNames = obj.providedMeasurements();
            uniqueID = obj.uniqueID();
            featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID];
        end
        
        function data = configureDynamicColumns(~,data,~,~)
        end
        
        function id = uniqueID(obj)
           id = [class(obj) '_' obj.otherObjectName '_k' num2str(obj.k) '_l' num2str(obj.l) '_' num2str(obj.groupByParent) '_' obj.restrictNghbrs num2str(obj.nghbrsParam)];
        end               
        
    end
    
    methods (Static)
        function [paramarray,checkVisibility] = getParameters(data)
            objectNames = getObjectNames(data);
            i = 1;
            paramarray{i}.name = 'Compare to:';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = objectNames;
            i = i+1;
            paramarray{i}.name = 'k (for MixtureIndex KNN):';
            paramarray{i}.type = 'int';
            paramarray{i}.default = 3;
            i = i+1;
            paramarray{i}.name = 'l (for l. closest object):';
            paramarray{i}.type = 'int';
            paramarray{i}.default = 1;
            i = i+1;
            paramarray{i}.name = 'Group by parent:';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = getObjectNames(data);
            i = i+1;
            paramarray{i}.name = 'Restrict for neighbours?';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = {'no','yes-ring','yes-k-closest'};
            paramarray{i}.dynamicVis = true;
            i = i+1;
            paramarray{i}.name = 'Neighbour parameter';
            paramarray{i}.type = 'int';
            paramarray{i}.default = 3;
            
            function vB = checkVis(paramarray,paramcell)                                
                neighbourRestrictParamIdx = find(...
                    cellfun(@strcmp,...
                        cellfun(@getfield,paramarray,repmat({'name'},1,length(paramarray)),'UniformOutput',false),...
                        repmat({'Restrict for neighbours?'},1,length(paramarray))...
                    ));
                vB = repmat({true},1,length(paramarray));
                if strcmp(paramcell{neighbourRestrictParamIdx},'no')
                    vB{neighbourRestrictParamIdx+1} = false;                
                end
            end
            
            checkVisibility = @checkVis;
        end                                        
                
        function [m1,m2] = prepareMasks(currentGroup1, mask1, mask1Centers, object1MaxRadius, mask1PixelChains, currentGroup2, mask2, mask2Centers, object2MaxRadius, mask2PixelChains) 
            m1.Mask = subsetMask(mask1,currentGroup1);
            m1.Centers = mask1Centers(currentGroup1,:);
            m1.MaxRadius = object1MaxRadius(currentGroup1);
            m1.PixelChains = mask1PixelChains(currentGroup1);
            if ~isempty(currentGroup2)                
                m2.Centers = mask2Centers(currentGroup2,:);
                m2.MaxRadius = object2MaxRadius(currentGroup2);
                m2.PixelChains = mask2PixelChains(currentGroup2);
                m2.Mask = subsetMask(mask2,currentGroup2);
            else
                m2.Centers = [];
                m2.MaxRadius = [];
                m2.PixelChains = [];
                m2.Mask = zeros(size(mask2));
            end            
        end
        
         function [bool,msg] = checkParameters(paramcell)
             %TODO: check also for l and also for the neighbourhood
             %parameters
            [bool,msg] = checkNumber(paramcell{2},1,1,[1 Inf],'k');            
        end
                
    end
    
end

