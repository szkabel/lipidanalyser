classdef IntensityColor < Intensity
    %To measure object intensities over a given channel.            
    methods      
        
        function obj = IntensityColor(paramcell,~)
            obj = obj@Intensity(paramcell);
        end
        
        function [colFieldList,dynamicFields] = providedMeasurements(~)
            %fake measure to get available fields
           [~,fieldList] = performCPMeasurement(zeros(2),zeros(2),'MeasureObjectIntensityMLGOC');
           fieldList{end+1} = 'IntensityDistribution';                                 
           colFieldList = cell(3*length(fieldList),1);
           colID = {'red','green','blue'};
           dynamicFields = zeros(size(colFieldList));           
           for i=1:3
               for j=1:length(fieldList)
                   colFieldList{(i-1)*length(fieldList)+j} = [fieldList{j} '_' colID{i}];
               end
               dynamicFields((i-1)*length(fieldList)+j) = 1; %always the last is the dynamic
           end                  
        end
                
        function [featureStrs] = measure(obj,data,imageID,objectName)
            mask = retrieveMaskImage(data,objectName,imageID);            
            originalImage = retrieveOriginalImage(data,imageID,obj.imageChannel);
            if size(originalImage,3)~=3
                error('LipidAnalyser:IntensityColor','IntensityColor measurement module requires a 3 channel image to measure.');
            end
            featuresByColor = cell(1,3);
            for i=1:3
                [featuresByColor{i},~] = performCPMeasurement(mask,originalImage(:,:,i),'MeasureObjectIntensityMLGOC');
            end
            features = [featuresByColor{1} featuresByColor{2} featuresByColor{3}]; %horzcat
            
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
        end
        
        function oneBlock = fetchBlockMeasurement(obj, data,imageID,objectName,selectedColumn)
            if (length(obj.measurementList{selectedColumn})>=21 && strcmp(obj.measurementList{selectedColumn}(1:21),'IntensityDistribution'))
                mask = retrieveMaskImage(data,objectName,imageID);
                nofObjects = max(mask(:));
                originalImage = retrieveOriginalImage(data,imageID,obj.imageChannel);
                chanName = obj.measurementList{selectedColumn}(23:end);
                switch chanName
                    case 'red'
                        originalImage = originalImage(:,:,1);
                    case 'green'
                        originalImage = originalImage(:,:,2);
                    case 'blue'
                        originalImage = originalImage(:,:,3);
                end
                oneBlock = zeros(nofObjects,length(obj.dynamicSettings{selectedColumn})-1);                
                for i=1:nofObjects
                    oneBlock(i,:) = histcounts(originalImage(mask == i),obj.dynamicSettings{selectedColumn});
                end
            else
                shift = floor(selectedColumn/(length(obj.measurementList)/3));
                oneBlock = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn-shift);
            end
        end                        
    end
        
end

