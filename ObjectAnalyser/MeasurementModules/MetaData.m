classdef MetaData < MeasurementModule
    %To measure object intensities over a given channel.
    
    properties        
        parentObject;
    end
    
    methods
        %Constrcutor
        function obj = MetaData(paramcell,~)
             obj.parentObject = paramcell{1};
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 0.1;
        end
        
        function [fieldList,dynamicFields] = providedMeasurements(~)            
           fieldList = {'Object index','Parent'};
           dynamicFields = zeros(size(fieldList));
        end
                
        function [featureStrs] = measure(obj,data,imageID,objectName)            
            features = retrieveParents(data,imageID,objectName,obj.parentObject);            
            if isempty(features)
                features = zeros(0,2);
            else
                features = [features convert2ColumnVector(1:size(features,1))];
            end
            
            featureStrs = obj.createFeatureStructure(features(:,[2 1]),objectName,imageID); %change order
        end
        
        function featureColumn = fetchBlockMeasurement(obj,data,imageID,objectName,selectedColumn)
            featureColumn = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
        end
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            fieldNames = obj.providedMeasurements();
            uniqueID = obj.uniqueID();
            featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID];
        end
        
        function data = configureDynamicColumns(~,data,~,~)
        end
        
        function id = uniqueID(obj)
            id = [class(obj) '_Parent::' obj.parentObject];
        end
        
    end
    
    methods (Static)
        function [paramarray,checkVisibility] = getParameters(data)
            i = 1;
            paramarray{i}.name = 'Parent object';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = getObjectNames(data);
            checkVisibility = @(a,b)(defaultCheckVisibility(a,b,true));
        end                                        
    end
    
end


