classdef AreaShape < MeasurementModule
    %To measure object intensities over a given channel.
    
    properties
        %fake metric exchange value to indicate if the results are required
        %in pixel
        metric = -1;
    end
    
    methods
        %Constrcutor
        function obj = AreaShape(paramcell,~)
            if strcmp(paramcell{1},'Metric')
                obj.metric = paramcell{2};
            end            
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 0;
        end
        
        function [fieldList,dynamicFields] = providedMeasurements(~)
            %fake measure to get available fields
           [~,fieldList] = performCPMeasurement(zeros(2),zeros(2),'MeasureObjectAreaShapeMLGOC');
           dynamicFields = zeros(size(fieldList));
        end
                
        function [featureStrs] = measure(obj,data,imageID,objectName)          
            fakeOrigImage = zeros(1);          
            mask = retrieveMaskImage(data,objectName,imageID);
            [features,~] = performCPMeasurement(mask,fakeOrigImage,'MeasureObjectAreaShapeMLGOC');
            powerMap = [2,0,0,0,0,1,0,1,1,0];            
            if obj.metric ~=-1
                factors = repmat(obj.metric,1,length(powerMap)).^powerMap;
                features = features .* repmat(factors,size(features,1),1);
            end
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
        end
        
        function featureColumn = fetchBlockMeasurement(obj,data,imageID,objectName,selectedColumn)
            featureColumn = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
        end
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            fieldNames = obj.providedMeasurements();
            uniqueID = obj.uniqueID();
            featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID];
        end
        
        function data = configureDynamicColumns(~,data,~,~)
        end
        
        function id = uniqueID(obj)
            id = [class(obj) '_1px' strrep(strrep(num2str(obj.metric),'.','p'),'-','M') 'm'];
        end
        
    end
    
    methods (Static)
        function [paramarray,checkVisibility] = getParameters(~)            
            paramarray{1}.name = 'Units:';
            paramarray{1}.type = 'enum';
            paramarray{1}.values = {'Pixel','Metric'};
            paramarray{1}.dynamicVis = true;
            
            paramarray{2}.name = 'Pixel size:';
            paramarray{2}.type = 'int';
            
            function vB = checkVis(~,paramcell)
                if strcmp(paramcell{1},'Metric')
                    vB = {true,true};
                else
                    vB = {true,false};
                end
            end
            checkVisibility = @checkVis;
        end                                        

        function [bool,msg] = checkParameters(paramcell)
            bool = 1;
            msg = '';
            if strcmp(paramcell{1},'Metric')
                [bool,msg] = checkNumber(paramcell{2},0,1,[0 Inf],'The metric factor');
            end
        end
    end
    
end


