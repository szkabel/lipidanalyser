classdef SpatialIntensity < MeasurementModule
    %SpatialIntensity measurement module. It calculates various statistics
    %of spatial distribution of the mass of intensity signal (i.e. sum of
    %intensities) over the specificed object.
    %   The spatial characteristic of the module means that it calculates
    %   how far on average the signal on a given channel is from a
    %   specified reference of interest (e.g. the center of an object or
    %   the full object itself - in this latter case distance is measured
    %   from the contour)
    
    
    properties
        intChan
        refObj
        parentObj
        refType
    end
    
    methods        
        function obj = SpatialIntensity(paramcell,~)
            i = 1;
            obj.intChan = paramcell{i}; 
            i = i+1;
            obj.refObj = paramcell{i};
            i = i+1;
            obj.parentObj = paramcell{i};
            i = i+1;
            obj.refType = paramcell{i};                                   
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 0.01;
        end
                        
        function [fieldList,dynamicFields] = providedMeasurements(~)
            fieldList = {'absDistAbsInt',...
                         'normDistAbsInt',...
                         'absDistNormInt',...
                         'normDistNormInt',...
                         };
             dynamicFields = zeros(size(fieldList));
        end
                        
        function [featureStrs] = measure(obj,data,imageID,objectName)        
            selfMask = retrieveMaskImage(data,objectName,imageID);
            refMask  = retrieveMaskImage(data, obj.refObj, imageID);
            
            parentsSelf = retrieveParents(data,imageID,objectName,obj.parentObj);
            parentsRef = retrieveParents(data,imageID,obj.refObj,obj.parentObj);
            intensityImg = retrieveOriginalImage(data,imageID,obj.intChan);
            
            if strcmp(obj.refType,'center')
                R = regionprops(refMask);
            end
            
            nofObj = length(parentsSelf);
            features = zeros(nofObj,4);
            
            for i = 1:max(selfMask(:)) % iterate over the objects                
                currRefIdx = find(parentsRef == parentsSelf(i));
                
                currSelfMask = selfMask;
                % zero out the non-relevant masks in this iteration
                currSelfMask(selfMask~=i) = 0;
                
                boolMask = currSelfMask > 0;
                intValues = intensityImg(boolMask);
                
                if strcmp(obj.refType,'full-body')
                    currRefMask = refMask;
                    currRefMask(~ismember(refMask,currRefIdx)) = 0;
                    distImg = bwdist(currRefMask);
                    tmpImg = distImg.*double(intensityImg);
                    intensityDistances = tmpImg(boolMask);
                    distM = distImg(boolMask); %there can be extra 0s here
                elseif strcmp(obj.refType,'center')
                     centers = cell2mat({R(currRefIdx).Centroid}');                     
                     imgSize = size(refMask);
%                     centers = min(center,imgSize); % max cut, this would
%                     centers = max(center,[0 0]);                    
                    [coordsX,coordsY] = meshgrid(1:imgSize(2),1:imgSize(1));
                    coordsX = coordsX(boolMask);
                    coordsY = coordsY(boolMask);
                    distM = pdist2([coordsX coordsY],centers);
                    distM = min(distM,[],2);
                    intensityDistances = distM.*double(intValues);
                end    
                absSum = sum(intensityDistances.*distM);
                
                features(i,:) = [...
                    absSum;...
                    absSum / max(distM);...
                    absSum / sum(intensityDistances);...
                    absSum / (max(distM)*sum(intensityDistances));...
                    ]';           
            end

            %inherited from superclass
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
        end
                        
        function featureColumn = fetchBlockMeasurement(obj,data,imageID,objectName,selectedColumn)
            featureColumn = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
        end
                        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            fieldNames = obj.providedMeasurements();
            uniqueID = obj.uniqueID();
            featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID];
        end
        
        function data = configureDynamicColumns(~,data,~,~)
        end
        
        %Provides a unique ID for this module including its parameters
        %based on which it is possible to recognize the measurements of
        %this module.
        %   OUTPUT:
        %       id  the unique ID string. It is the module name before the
        %       first underscore later on it contains the parameters if
        %       there is any.
        function id = uniqueID(obj)
            id = [class(obj) '_' obj.refObj '_p' obj.parentObj '_' obj.refType '_' obj.intChan];
        end
                
    end        
    
    methods (Static)        
                
        function [paramarray,checkVisibility] = getParameters(data)
            objectNames = getObjectNames(data);
            possibleChannels = listImageChannels(data);
            
            i = 1;
            paramarray{i}.name = 'Channel';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = possibleChannels;
            paramarray{i}.doc = 'The channel to be used for the intensity quantification.';
            i = i + 1;
            paramarray{i}.name = 'Reference object';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = objectNames;
            paramarray{i}.doc = 'The module calculates the intensity weighted average of distances between each pixel of the object and a reference object. Specify here this reference object.';
            i = i + 1;
            paramarray{i}.name = 'Parent object';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = objectNames;            
            paramarray{i}.doc = 'Usually one only wants to take into account objects (e.g. subcellular organelles) within the same grouping category (e.g. cells). Specify here this grouping object (if grouping is not desired then select image)';
            i = i + 1;
            paramarray{i}.name = 'Reference type';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = {'center','full-body'};
            paramarray{i}.doc = 'Center means that the origo for distance calculation is just the center of the reference object(s). Full-body means that every pixel of the reference object is considered to have 0 distance, other distances are calculated to the closest reference point.';            
            
            checkVisibility = @(a,b)(defaultCheckVisibility(a,b,true));
        end
    end
    
end

