classdef Cluster < MeasurementModule
    %To measure object intensities over a given channel.
    
    properties
        %The distance within which we consider 2 object as neighbouring       
        distanceThreshold
        
        %Indicates if we cluster within some object defined as parent OR in the whole image
        %Both the cases are handled now by possibility to parent by the whole image.
        groupByParent
        
        %Selects from spatial or radiometric distance
        distanceMetric                
        
        %For spatial method a flag to set if the estimated radius of the
        %object is counted in the distance threshold or not
        distanceCenter                 
        
        %The ID of the channel on which the measurements will be carried
        %out. This will be used later on by the data (ECP) to retrieve the
        %corresponding image.
        imageChannel        
        
        %makePlot boolean to indicate if we want to make a plot about the
        %clusters.
        makePlot
    end
    
    methods
        %Constrcutor
        function obj = Cluster(paramcell,~)
            i = 1;
            obj.distanceThreshold = paramcell{i};
            i = i+1;            
            obj.groupByParent = paramcell{i};
            i = i+1;
            obj.distanceMetric = paramcell{i};
            i = i+1;
            obj.distanceCenter = paramcell{i};
            i = i+1;
            obj.imageChannel = paramcell{i};
            i = i+1;            
            if strcmp(paramcell{i},'yes')
                obj.makePlot = 1;
            elseif strcmp(paramcell{i},'no')
                obj.makePlot = 0;
            end
        end
        
        function versionNumber = getCurrentVersion(~)
            versionNumber = 2;
        end
        
        function [fieldList,dynamicFields] = providedMeasurements(~)
            %fake measure to get available fields
           fieldList = {'Parent','ClusterID','ClusterSize'};
           dynamicFields = zeros(size(fieldList));
        end
                
        function [featureStrs] = measure(obj,data,imageID,objectName)
            mask = retrieveMaskImage(data,objectName,imageID);
            nofObjects = max(mask(:));
            features = zeros(nofObjects,3);
            parents = retrieveParents(data,imageID,objectName,obj.groupByParent);
            features(:,1) = parents;
            
            if obj.makePlot
                distMtx = zeros(nofObjects);
                maskC = zeros(nofObjects,2);
                clusterIDs = zeros(nofObjects,1);
            end
                        
            cummulativeObjectCount = 0;
            for i=convert2RowVector(unique(parents))
                childrenIndices = find(parents == i);

                currMask = subsetMask(mask,childrenIndices);                    

                [features(childrenIndices,2),distanceMatrix,maskCenters] = obj.processCurrentMask(currMask,data,imageID);

                if obj.makePlot
                    distMtx(cummulativeObjectCount+1:cummulativeObjectCount+length(childrenIndices),cummulativeObjectCount+1:cummulativeObjectCount+length(childrenIndices)) = distanceMatrix;
                    maskC(cummulativeObjectCount+1:cummulativeObjectCount+length(childrenIndices),:) = maskCenters;
                    clusterIDs(cummulativeObjectCount+1:cummulativeObjectCount+length(childrenIndices)) = features(childrenIndices,2)+cummulativeObjectCount;
                end
                cummulativeObjectCount = cummulativeObjectCount + length(childrenIndices);
            end            
            
            if obj.makePlot
                figure(); % make figure for plot
                if strcmp(obj.distanceMetric,'radiometrial')
                    img = retrieveOriginalImage(data,imageID,obj.imageChannel);
                    visualizeClusters(img,maskC,clusterIDs,'plot',distMtx);
                elseif strcmp(obj.distanceMetric,'spatial')
                    visualizeClusters(mask>0,maskC,clusterIDs,'plot',distMtx);
                end                
            end
            
            %add the size of each cluster. If group by parents then the
            %size is only within the cell
            for i=1:size(features,1)
                if obj.groupByParent
                    features(i,3) = sum(features(:,2) == features(i,2) & features(:,1) == features(i,1) );
                else
                    features(i,3) = sum(features(:,2) == features(i,2));
                end
            end
           
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
        end
        
        function featureColumn = fetchBlockMeasurement(obj,data,imageID,objectName,selectedColumn)
            featureColumn = loadRequestedMeasurement(data,obj,imageID,objectName,selectedColumn);
        end
        
        function featureNames = resolveDynamicFeatures(obj,selectedFeature)
            fieldNames = obj.providedMeasurements();
            uniqueID = obj.uniqueID();
            featureNames{1} = [ fieldNames{selectedFeature} '_' uniqueID]; %#ok<USENS> set in eval
        end
        
        function data = configureDynamicColumns(~,data,~,~)
        end
        
        function id = uniqueID(obj)
            if strcmp(obj.distanceMetric,'spatial')
                id = [class(obj) '_' obj.distanceMetric '_' strrep(num2str(obj.distanceThreshold),'.','p') '_GroupBy::' obj.groupByParent '_' obj.distanceCenter];
            elseif strcmp(obj.distanceMetric,'radiometrial')
                id = [class(obj) '_' obj.distanceMetric '_' strrep(num2str(obj.distanceThreshold),'.','p') '_GroupBy::' obj.groupByParent '_' obj.imageChannel];
            end
        end
        
        function [clusterID,distanceMatrix,maskCenters] = processCurrentMask(obj,currMask,data,imageID)
            if strcmp(obj.distanceMetric,'radiometrial')
                originalImage = retrieveOriginalImage(data,imageID,obj.imageChannel);
                [clusterID,distanceMatrix,maskCenters] = identifyClusters(currMask,obj.distanceMetric,obj.distanceThreshold,originalImage);
            elseif strcmp(obj.distanceMetric,'spatial')
                [clusterID,distanceMatrix,maskCenters] = identifyClusters(currMask,obj.distanceMetric,obj.distanceThreshold,obj.distanceCenter);
            end
        end
        
    end
    
    methods (Static)
        function [paramarray,checkVisibility] = getParameters(data)            
            % WARNING: if modifying the order of parameters, remember to
            % update the burnt-in values for the parameters' dynamic
            % visualization
            possibleChannels = listImageChannels(data);
            i = 1;
            paramarray{i}.name = 'Distance threshold:';
            paramarray{i}.type = 'int';
            i = i + 1;
            paramarray{i}.name = 'Group by parent?';
            paramarray{i}.type = 'enum';            
            paramarray{i}.values = getObjectNames(data);
            i = i + 1;
            paramarray{i}.name = 'Distance metric:';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = {'spatial','radiometrial'};
            paramarray{i}.dynamicVis = true;
            i = i + 1;
            paramarray{i}.name = 'Distance center';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = {'center','contour'};
            i = i + 1;
            paramarray{i}.name = 'On which channel?';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = possibleChannels;
            i = i + 1;
            paramarray{i}.name = 'Do you want to plot the result?';
            paramarray{i}.type = 'enum';
            paramarray{i}.values = {'no','yes'};                        
            
            function vB = checkVis(paramarray,paramcell)
                
                %This below is a rather complicated way to write
                %distanceMetricParamIdx = 3 just to avoid the problem if by
                %any chance the order above of parameters above is changed.
                distanceMetricParamIdx = find(...
                    cellfun(@strcmp,...
                        cellfun(@getfield,paramarray,repmat({'name'},1,length(paramarray)),'UniformOutput',false),...
                        repmat({'Distance metric:'},1,length(paramarray))...
                    ));
                vB = repmat({true},1,length(paramarray));
                if strcmp(paramcell{distanceMetricParamIdx},'spatial')
                    vB{distanceMetricParamIdx+2} = false;
                elseif strcmp(paramcell{distanceMetricParamIdx},'radiometrial')
                    vB{distanceMetricParamIdx+1} = false;
                end
            end
            checkVisibility = @checkVis;
        end                                        
        
        function [bool,msg] = checkParameters(paramcell)
            [bool,msg] = checkNumber(paramcell{1},0,1,[0 Inf],'Distance threshold');
        end
        
    end
    
end

