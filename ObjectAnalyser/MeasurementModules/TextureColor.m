classdef TextureColor < Texture
    %To measure object texture over a given channel for RGB images        
    methods    
        function obj = TextureColor(paramcell,~)
            obj = obj@Texture(paramcell);
        end
        
        function [colFieldList,dynamicFields] = providedMeasurements(~)
           %fake measure to get available fields
           [~,fieldList] = performCPMeasurement(zeros(2),zeros(2),'MeasureTextureMLGOC',{struct('index',8,'value',3)});
           %Add colors
           colFieldList = cell(3*length(fieldList),1);
           colID = {'red','green','blue'};
           for i=1:3
               for j=1:length(fieldList)
                   colFieldList{(i-1)*length(fieldList)+j} = [fieldList{j} '_' colID{i}];
               end
           end
           dynamicFields = zeros(size(colFieldList));
        end                
        
        function [featureStrs] = measure(obj,data,imageID,objectName)
            originalImage = retrieveOriginalImage(data,imageID,obj.imageChannel);
            if size(originalImage,3)~=3
                error('LipidAnalyser:TextureColor','TextureColor measurement module requires a 3 channel image to measure.');
            end
            mask = retrieveMaskImage(data,objectName,imageID);
            paramArrayStruct{1} = struct('index',8,'value',num2str(obj.textureScale)); %The texture scale is the 8th parameter of the MeasureTextureMLGOC module
            featuresByColor = cell(1,3);
            for i=1:3
                [featuresByColor{i},~] = performCPMeasurement(mask,originalImage(:,:,i),'MeasureTextureMLGOC',paramArrayStruct);
            end
            features = [featuresByColor{1} featuresByColor{2} featuresByColor{3}]; %horzcat
            
            featureStrs = obj.createFeatureStructure(features,objectName,imageID);
        end                                                               
        
    end   
end

