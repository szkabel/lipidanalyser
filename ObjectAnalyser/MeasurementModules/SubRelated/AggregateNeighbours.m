classdef AggregateNeighbours< RelatedMeasurement
    %Aggregate neighbours of a specific type
    %NOTE: think about extending it with the possibility for selecting multiple
    %types of objects to be considered as neighbours.
    
    properties
        %above the related measurement properties
        
        neighbourType;
        % a structure defining the neighbourhood parameters. See in
        % calculateNeighbours
        
    end
        
    methods
        function obj = AggregateNeighbours(paramcell,data)
            %modified constructor
            oldParamcell = [paramcell(1) paramcell(4:end)];
            obj = obj@RelatedMeasurement(oldParamcell,data);
            obj.neighbourType.type = paramcell{2};            
            obj.neighbourType.parameter = paramcell{3};            
        end
        
        function [ownIndicesCell,relatedNeighboursCell] = getObjectRelationForImg(obj,data,imageID,selfObjectName)
            %Neighbourhood calculations:
            
            ownMask = retrieveMaskImage(data,selfObjectName,imageID); %retreieves the mask image
            relatedMask = retrieveMaskImage(data,obj.otherObject,imageID);
            
            [ownNeighbours] = calculateNeighbours(ownMask,relatedMask,strcmp(selfObjectName,obj.otherObject),obj.neighbourType);            
            ownIndicesCell = num2cell( (1:max(ownMask(:)))' );
                        
            [relatedNeighboursCell] = transposeNeighbourArray(ownNeighbours,max(relatedMask(:)));
        end
    end
    
    methods (Static)
        
        
        function [paramarray,checkVisibility] = getParameters(data)
            oldParamarray = RelatedMeasurement.getParameters(data);   %static function                     
                        
            paramarray(1) = oldParamarray(1);
            paramarray{1}.name = 'Neighbour object:';
            
            paramarray{2}.type = 'enum';
            paramarray{2}.name = 'Neighbourhood calculation:';
            paramarray{2}.values = {'k-closest','ring'};
            
            paramarray{3}.type = 'int'; %int refers to number not integer
            paramarray{3}.name = 'parameter (k or ring radius)';            
                        
            paramarray(4:4+length(oldParamarray)-2) = oldParamarray(2:end);
            
            checkVisibility = @(a,b)(defaultCheckVisibility(a,b,true));
        end
        
        
        function [bool,msg] = checkParameters(paramcell)
            if strcmp(paramcell{2},'k-closest')
                [bool,msg] = checkNumber(paramcell{3},1,1,[1 Inf],'k for k closest');
            elseif strcmp(paramcell{2},'ring')
                [bool,msg] = checkNumber(paramcell{3},0,1,[0 Inf],'Ring radius');
            end
        end
        
    end
    
end


