classdef AggregateByParent < RelatedMeasurement        
    %Aggregate by parent, see detailed instructions in RelatedMeasurement    
    %Constructor is overloaded here because of needed modifications
    
    properties       
       parentObject; 
       %The object based on which the relation is done
       
       parentRelationModule
       %The module which measures the parent-self object relation
              
    end
        
    methods
        function obj = AggregateByParent(paramcell,data)
            obj = obj@RelatedMeasurement(paramcell([1 3:end]),data);
            disp('AggregateByParentConstructor run');
                                   
            obj.parentObject = paramcell{2};
        end
        
         function [featureStrs] = measure(obj,data,imageID,objectName)
            featureStrs = measure@RelatedMeasurement(obj,data,imageID,objectName);
            %MetaData trick for parent calculation
            paramcell = {obj.parentObject}; %currently metadata only requires the parentObject
            obj.parentRelationModule = MetaData(paramcell);
            ownParentFeatureStrs = calculateRequestedMeasurement( data,obj.parentRelationModule,imageID,objectName);
            relatedParentFeatureStrs = calculateRequestedMeasurement( data,obj.parentRelationModule,imageID,obj.otherObject);
            %Extent the feature structures
            featureStrs = [featureStrs ownParentFeatureStrs relatedParentFeatureStrs];             
        end                
        
        function [ownParentsCell,relatedParentsCell] = getObjectRelationForImg(obj,data,imageID,selfObjectName)                                   
            ownParents = obj.parentRelationModule.fetchBlockMeasurement(data,imageID,selfObjectName,...
                find(strcmp('Parent',obj.parentRelationModule.providedMeasurements())));             %#ok<FNDSB> it is not variable but a function
            ownParentsCell = num2cell(ownParents);
            ownParentsCell(ownParents == 0)= {[]};
            
            relatedParents = obj.parentRelationModule.fetchBlockMeasurement(data,imageID,obj.otherObject,...
                find(strcmp('Parent',obj.parentRelationModule.providedMeasurements())));             %#ok<FNDSB> it is not variable but a function
            relatedParentsCell = num2cell(relatedParents);
            relatedParentsCell(relatedParents == 0) = {[]};
        end
        
        function id = uniqueID(obj)
            id = uniqueID@RelatedMeasurement(obj);
            spltID = strsplit(id,'_');
            spltID(4:length(spltID)+1) = spltID(3:end);
            spltID{3} = ['P::' obj.parentObject];
            id = strjoin(spltID,'_');
        end
    end
    
    %TODO: Overwrite uniqueID now it can be easier to read
    
    methods (Static)                                     
        function [paramarray,checkVisibility] = getParameters(data)
            %Overwrite            
                        
            paramarray{1}.type = 'enum';
            paramarray{1}.name = 'Related object:';
            paramarray{1}.values = getObjectNames(data);
            
            paramarray{2}.type = 'enum';
            paramarray{2}.name = 'Parent object:';
            paramarray{2}.values = getObjectNames(data);
            
            paramarray{3}.type = 'multiple-enum';
            paramarray{3}.name = 'Choose aggregation functions:';
            paramarray{3}.values = RelatedMeasurement.aggregateFunctions();

            paramarray{4}.type = 'enum';
            paramarray{4}.name = 'Apply filters:';
            paramarray{4}.values = {'no','yes-itself','yes-other module','yes-preconfigured'};                                   
            
            checkVisibility = @(a,b)(defaultCheckVisibility(a,b,true));
        end
    end
    
end

