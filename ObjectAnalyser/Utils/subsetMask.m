 function currentMask = subsetMask(mask, currentGroup)           
 %Makes a subset mask out of a given mask. (reindex with consecutive
 %indices).
 %  INPUTS:
 %      mask            An input mask image
 %      currentGroup    Subset of the mask indices which should be kept in
 %                      the subset mask (optional). If not given then it is
 %                      simply reindexing the objects to be consecutive.
 
    if nargin<2
        currentGroup = unique(mask(:));
    end
    currentGroup(currentGroup == 0) = [];
    
    currentMask = zeros(size(mask));
    r = regionprops(mask,'PixelIdx');
    for i=1:length(currentGroup)                       
        currentMask(r(currentGroup(i)).PixelIdxList) = i;
    end
end

