function [maskStruct] = createMaskStructure(maskImg)

maskCenters = regionprops(maskImg,'Centroid');
maskCenters = cat(1,maskCenters.Centroid);

nofObj = size(maskCenters,1);

[objectMaxRadius,maskPixelChains] = extractPixelChainsFromMask(maskImg,maskCenters,nofObj);

maskStruct.Mask = maskImg;
maskStruct.Centers = maskCenters;
maskStruct.MaxRadius = objectMaxRadius;
maskStruct.PixelChains = {maskPixelChains(:).PixelList};

end

