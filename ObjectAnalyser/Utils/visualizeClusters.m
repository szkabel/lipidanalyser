function [ imageWithClusters ] = visualizeClusters( imageToDrawOn, coordinates, clusterIndices, mode,distMtx)
% AUTHOR:   Abel Szkalisity
% DATE:     Dec 1, 2016
% NAME:     visualizeCircles
%
% Draws spanning trees over sets of points to an image. The sets are
% identified with indices.
%
% INPUT:
%   imageToDrawOn  This is the base image to which we're drawing.
%   coordinates    An N by 2 array for identifying the points' location.
%                  The coordinates in 'regionprops coordinate' system.
%   clusterIndices An N length vector. The ith element shows the cluster ID
%                  of the ith point from in coordinates.
%   mode           'draw' or 'plot'. Draw draws on the image, plot plots it
%   distMtx        The distance matrix between the objects. The values in
%                  there are used to determine the Minimum spanning tree
%                  for the cluster visualization and also the color of it.
%
%
% OUTPUT:
%   imageWithClusters   The new image with the drawn trees.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


imageWithClusters = imageToDrawOn;

if strcmp(mode,'plot')    
    imshow(imageWithClusters);
    hold on;
end

possibleIndices = convert2RowVector(unique(clusterIndices));
for k = possibleIndices(1:end)
    currentElements = find(clusterIndices == k);
    %currentDistMtx = squareform(pdist(coordinates(currentElements,:)));
    currentDistMtx = distMtx(currentElements,currentElements);
    %
    if ~isempty(currentDistMtx)
        minGraph  = graphminspantree(sparse(currentDistMtx));
    else
        minGraph = sparse([]);
    end    
    currentDistMtx(minGraph == 0) = 0;
    %
    maxD = max(currentDistMtx(:));
    for i=1:size(currentDistMtx,1)
        for j=1:size(currentDistMtx,1)
            if (currentDistMtx(i,j))
                if strcmp(mode,'draw')
                    imageWithClusters = drawLineToPicture( imageWithClusters, round(coordinates(currentElements(i),[2,1])), round(coordinates(currentElements(j),[2,1])), [0 currentDistMtx(i,j)/maxD 0]);
                else
                    line([coordinates(currentElements(i),1) coordinates(currentElements(j),1)],[coordinates(currentElements(i),2) coordinates(currentElements(j),2)] ,'Color',[0 currentDistMtx(i,j)/maxD 0], 'LineWidth', 0.3);
                end
            end
        end
    end
end

if strcmp(mode,'plot')        
    hold off;
end

end

