function [neighbourIndices] = calculateNeighbours(mask1,mask2,self,neighbourType)
%The function calculates for each object in mask1 what are the neighbours
%of it in mask 2.
%
%   INPUTS:
%       mask1,mask2     Grayscale mask images, or the structure created by
%                       createMaskStructure.
%       self            Bool indicating wheather mask1 and mask2 are the
%                       same. This is actually really important to avoid
%                       the object itself calculated as its neighbour.
%       neighbourType   A structure indicating the neighbour type. The type
%                       field can be 'ring' or 'k-closest'. In both case we
%                       have a parameter field also. In the former case 
%                       the neighbour objects will be those within a
%                       parameter wide ring around the object.
%                       In the latter case the k closest objects measured
%                       in contour distance will be identified as
%                       neighbours. In this case parameter is k.
%
%   OUTPUTS:
%       neighbourIndices A cellarray with the length equal to the number of
%                       mask1 objects. Each entry is an array of the
%                       neighbour indices.
%
%   NOTE: as measureDistanceBetweenChains is an expensive function it might
%   be relevant to output the results in this function. Another possibility
%   to make it as an ObjectAnalyser module and then utilize it's avoid
%   recalculation functionality.

if isstruct(mask1)
    mask1Struct = mask1;
else
    mask1Struct = createMaskStructure(mask1);
end
if isstruct(mask2)
    mask2Struct = mask2;
else
    mask2Struct = createMaskStructure(mask2);
end

nofObj1 = length(mask1Struct.PixelChains);

neighbourIndices = cell(1,nofObj1);

[mask1Array,mask2Array,mask1Indices,mask2Indices] = prepareMasksForSelfRelation(mask1Struct,mask2Struct,self);

for i=1:length(mask1Array)
    if strcmp(neighbourType.type,'ring')
        [closestDistance,closestNeighbourIdx,~] = measureDistanceBetweenChains(mask1Array{i},mask2Array{i},'distLimit',neighbourType.parameter);
        %filtering only for those distances that are relevant for us
        if ~isempty(closestNeighbourIdx) && ~isempty(closestDistance)
            for j=1:length(mask1Indices{i})
                neighbourIndices{mask1Indices{i}(j)} = mask2Indices{i}(closestNeighbourIdx(j,closestDistance(j,:)<neighbourType.parameter));
            end
        end
    elseif strcmp(neighbourType.type,'k-closest')
        [~,closestNeighbourIdx,~] = measureDistanceBetweenChains(mask1Array{i},mask2Array{i},'numberLimit',neighbourType.parameter);
        %filtering only if there were enough neighbours
        if ~isempty(closestNeighbourIdx)
            for j=1:length(mask1Indices{i})
                neighbourIndices{mask1Indices{i}(j)} = mask2Indices{i}(   closestNeighbourIdx(j,( 1:neighbourType.parameter & ~isnan(closestNeighbourIdx(j,1:neighbourType.parameter)) ))   );
            end
        end
    end
end


end