function [mask1Array,mask2Array,mask1Indices,mask2Indices] = prepareMasksForSelfRelation(maskStruct1,maskStruct2,self)
%This function prepares the mask structures for relation. The main point
%here is that if self relation is needed, then the relation should be run
%one-by-one on each object so that the current one is deleted from the
%related mask.
%In the OUTPUT the 4 cell arrays are identically long. The first 2 contains
%the maskStructures to be compared at their matching indices, whilst the
%3rd and 4th one identifies which are the indices included in the first and
%second set respectively

    %Init
    mask1Orig = maskStruct1.Mask;            
    nofObject1 = max(mask1Orig(:));
    mask2Orig = maskStruct2.Mask;
    nofObject2 = max(mask2Orig(:));
    %if we ask for self relation
    if self                                    
        mask1Array = cell(1,nofObject1);
        mask2Array = cell(1,nofObject1);
        mask1Indices = cell(1,nofObject1);
        mask2Indices = cell(1,nofObject1);
        for i=1:nofObject1            
            currentMask2 = maskStruct2;
            %in mask1 retain only the object i and set its value to
            %1.
            maskedMask1 = zeros(size(mask1Orig));
            maskedMask1(mask1Orig == i) = 1;
            %delete object i from mask2
            currentMask2.Mask(mask1Orig == i) = 0;
            %shift the indices one down to fill the gap caused by
            %deletion of object i.
            currentMask2.Mask(mask1Orig > i) = mask1Orig(mask1Orig > i) - 1;
            %update other fields of m2 as well
            currentMask2.Centers     =     currentMask2.Centers([1:i-1 i+1:end],:);
            currentMask2.MaxRadius   =   currentMask2.MaxRadius([1:i-1 i+1:end]);
            currentMask2.PixelChains = currentMask2.PixelChains([1:i-1 i+1:end]);
            %create mm1 the current new mask for the single object
            currentMask1.Mask = maskedMask1;
            currentMask1.Centers = maskStruct1.Centers(i,:);
            currentMask1.MaxRadius = maskStruct1.MaxRadius(i);
            currentMask1.PixelChains = maskStruct1.PixelChains(i);

            mask1Array{i} = currentMask1;
            mask2Array{i} = currentMask2;
            mask1Indices{i} = i;
            mask2Indices{i} = [1:i-1 i+1:nofObject2];
        end        
    else                
        mask1Array = {maskStruct1};
        mask2Array = {maskStruct2};
        mask1Indices = {1:nofObject1};
    end
end