function [maskStruct] = subsetMaskStructure(maskStruct,currentGroup)
%Creates a subset from the full mask structure

if ~isempty(currentGroup)                
    maskStruct.Centers = maskStruct.Centers(currentGroup,:);
    maskStruct.MaxRadius = maskStruct.MaxRadius(currentGroup);
    maskStruct.PixelChains = maskStruct.PixelChains(currentGroup);
    maskStruct.Mask = subsetMask(maskStruct.Mask,currentGroup);
else
    maskStruct.Centers = [];
    maskStruct.MaxRadius = [];
    maskStruct.PixelChains = [];
    maskStruct.Mask = zeros(size(maskStruct.Mask));
end            

end

