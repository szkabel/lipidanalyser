function [ featureVector, featureNames] = performCPMeasurement( mask, originalImg, moduleName, params )
% AUTHOR:   Abel Szkalisity
% DATE:     Nov 11, 2016
% NAME:     performCPMeasurement
%
% To wrap to more generic form a measurement performed by cell profiler.
% (get rid of handles dependency). This function is to provide a general
% interface for other program components to perform measurements.
%
% NOTE: this function is now strictly connected to CellProfiler, however
% it's functionality is more general. Later on it can turn out that instead
% of wrapping around the CP functions it is more convenient to write the
% measurement functions separately (maybe call those via CP too). 
%
% INPUT:
%   mask            A mask image that identifies the objects to measure.
%                   The pixels with the same mask image value form a region
%                   and the measurements are done on that region of the
%                   originalImg. The values of this image goes from 1 to N.
%                   To handle overlapping objects it is allowed that the
%                   mask has a 3rd dimension which are layers of the mask.
%                   The layered masks can only be used with the measurement
%                   functions MLGOC.
%   originalImg     The image on which the measurements are performed
%   moduleName      The name of the measurement module that we want to run.
%   params          Module parameters (if any). Module parameters must be a
%                   cell array with all the required parameters for that
%                   module. Each entry is a struct with two fields: index
%                   and value. The index is the index of the parameter in
%                   the original CP module and the value was it's value.
%                   NOTE: Each CP module takes in parameters as text!
%
% OUTPUT:
%   featureVector   An N by k matrix with the measured values for each
%                   object. The i th row of the matrix corresponds to the
%                   object with index i in the mask image. k is varying
%                   according to the specified module.
%   featureNames    A cellarray with length k. Each entry is a string
%                   describing the meaning of the values in the kth column
%                   of featureVector.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% Collect here the neccessary parameters:
%   handles.Current.CurrentModuleNumber # string
%   handles.Settings.ModuleNames(CurrentModuleNum) # string as well
%
%   handles.Pipeline.(ImageName) # original image
%   handles.Settings.VariableValues{CurrentModuleNum,1} # ImageName string
%   handles.Settings.VariableValues{CurrentModuleNum,2} # ObjectName string
%   handles.Pipeline.(['Segmented', ObjectName]);

%disgusting code: as in CellProfiler each measurement module has it's own
%parameter list, which is not standardized (they don't follow the abstract
%interface that we defined with this function). Therefore here we specify a
%mapping from this abstract parameter list to EACH cellprofiler
%measurement. parameterMapping is a cellarray with structs. Each struct has
%a name field with the name of the module and a mapping. Mapping is an
%array. The first element of mapping is the index of the mask name
%parameter in the given module, the second is the originalImage IF needed.
%Otherwise it is -1.

parameterMapping = {...
    struct('name','MeasureObjectIntensity',...
           'mapping',[2 1]...
        )...    
    struct('name','MeasureObjectAreaShape',...
           'mapping',[1 -1]...
        )...    
    struct('name','MeasureTexture',...
           'mapping',[2 1]...
        )...    
    struct('name','MeasureObjectIntensityMLGOC',...
           'mapping',[2 1]...
        )...    
    struct('name','MeasureObjectAreaShapeMLGOC',...
           'mapping',[1 -1]...
        )...    
    struct('name','MeasureTextureMLGOC',...
           'mapping',[2 1]...
        )...    
    };

for i=1:length(parameterMapping)
    if strcmp(parameterMapping{i}.name,moduleName)
        moduleIndex = i;
        break;
    end
end

if nargin<4
    params = cell(0);
end

originalImg = im2double(originalImg);

maxParametersForAnyModule = 100; %estimating that none of the modules will have more than 100 parameters

%WELL DEFINED FAKE THINGS
fakeModuleNumberStr = '01';
fakeModuleNumber = str2double(fakeModuleNumberStr);

handles.Current.CurrentModuleNumber = fakeModuleNumberStr;
handles.Current.NumberOfModules = fakeModuleNumber;
handles.Settings.ModuleNames{fakeModuleNumber} = moduleName;
handles.Current.SetBeingAnalyzed = 1;

fakeImageName = 'originalImg';
fakeObjectName = 'thisIsAnObject';
handles.Settings.VariableValues = cell(fakeModuleNumber,maxParametersForAnyModule);
for i=1:maxParametersForAnyModule
    handles.Settings.VariableValues{fakeModuleNumber,i} = 'Do not use';
end
if (parameterMapping{moduleIndex}.mapping(2) ~=-1)
    handles.Pipeline.(fakeImageName) = originalImg;
    handles.Settings.VariableValues{fakeModuleNumber,parameterMapping{moduleIndex}.mapping(2)} = fakeImageName;
end
if (parameterMapping{moduleIndex}.mapping(1) ~=-1)
    handles.Pipeline.(['Segmented', fakeObjectName]) = double(mask); %CP works with double images inside
    handles.Settings.VariableValues{fakeModuleNumber,parameterMapping{moduleIndex}.mapping(1)} = fakeObjectName;
end
for i=1:length(params)
    handles.Settings.VariableValues{fakeModuleNumber,params{i}.index} = params{i}.value;
end
    

%LOOSELY DEFINED FAKE THINGS
% fake the figure handles
for i=1:handles.Current.NumberOfModules
    handles.Current.(['FigureNumberForModule' num2str(i,'%02d')]) = 111;
end
handles.Settings.PixelSize = '1';
handles.Preferences.FontSize = 10;

handles = feval(moduleName,handles);

fieldNames = fieldnames(handles.Measurements.(fakeObjectName));
featureVector = handles.Measurements.(fakeObjectName).(fieldNames{2}){1};
featureNames = handles.Measurements.(fakeObjectName).(fieldNames{1});



end

