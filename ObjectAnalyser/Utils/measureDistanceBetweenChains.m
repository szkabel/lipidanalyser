function [closestDistance,closestNeighbourIdx,meanClosestDistance] = measureDistanceBetweenChains(mask1Struct,mask2Struct,varargin)
%MeasureDistanceBetweenChains:
%   This functions computes the distance between 2 contours (pixel chains).
%   For each contour in set1 (mask1PixelChains) it outputs the first lth
%   closest distance from any contour points in set2. (mask2PixelChains).
%   To optimize the process we take in a matrix which should give a lower
%   bound for the actual distance.
%   
%   INPUTS:
%       mask1Struct: a mask structure array with the most important mask
%               properties to be used for fastening. It can be created from a
%               grayscale mask image by createMaskStructure
%       mask2Struct: same as above just for mask2
%       numberLimit: Specifying how many contour neighbours do we want to
%               retrieve at most.
%       distLimit: The calculation of the distances are stopped if the next
%               possible closest objects are further away then this limit.       
%       crossDistM: optional cross distance matrix between the object
%               centers
%   NOTE: the numberLimit and the distLimit are connected with an "and"
%         relation. So if the first numberLimit object is reached but
%         the distance is not, then we still stop. It is advised to
%         use this function with either distLimit or numberLimit
%         parameter but not both. Btw this functioning can be changed
%         in the init section.
%
%   OUTPUTS:
%       closestDistance: a matrix with N1 rows, for each contour in set1.
%               Each row says how far is the first k closest point from any
%               contours in set2, such that k>=lth (so the at least the
%               first l closest object's distance is there). 
%       closestNeighbourIdx: a matrix with the same size as closestDistance
%               above. In each row it contains the index of mask2PixelChains
%               objects that are the closest to the given row's object.
%       meanClosestDistance: a vector with length N1. For each contour in
%               set1 for each individual contour point measure what is the closest
%               set2 type contour point and take the mean of this distance.
%               (meanContourDistance)

%subtract the maximal possible radii from the cross distance matrix. In
%the updated form the distances are lower bounds for the possible
%minimal distances.

%Old inputs, now made it internal
%       updatedCrossDistM: this is a distance matrix which should give a
%               lower bound for the actual distances between the pixelchains. It
%               has N1 rows and N2 columns. (N1 x N2 matrix)
%       mask1PixelChains: a cellarray with length N1 each entry contains an
%               n by 2 array which is the set of contour coordinates.
%       mask2PixelChains: same for set 2 its length is N2

%% Parameters
    p = inputParser();
    addParameter(p,'numberLimit',Inf);
    addParameter(p,'distLimit',Inf);
    addParameter(p,'crossDistM',[]);
    
    %Specifies how the distLimit and numberLimit params are connected
    %regarding the stop criterium.
    connectionType = @and;

    parse(p,varargin{:});

    numberLimit = p.Results.numberLimit;
    distLimit = p.Results.distLimit;
    crossDistM = p.Results.crossDistM;
    
%% handle special cases
    if isempty(mask1Struct.Centers) || isempty(mask2Struct.Centers)
        closestDistance = [];
        closestNeighbourIdx = []; 
        meanClosestDistance = [];
        return;
    end   
%% Init
    if isempty(crossDistM)
        mask1Centers = mask1Struct.Centers(:,1:2); %remove 3rd layer
        mask2Centers = mask2Struct.Centers(:,1:2); %remove 3rd layer
        crossDistM = pdist2(mask1Centers,mask2Centers);
    end
    
    object1MaxRadius = mask1Struct.MaxRadius;
    mask1PixelChains = mask1Struct.PixelChains;
    object2MaxRadius = mask2Struct.MaxRadius;
    mask2PixelChains = mask2Struct.PixelChains;    

    updatedCrossDistM = crossDistM - repmat(convert2ColumnVector(object1MaxRadius),1,size(crossDistM,2));
    updatedCrossDistM = updatedCrossDistM - repmat(convert2RowVector(object2MaxRadius),size(crossDistM,1),1);

    nofObj1 = length(mask1PixelChains);
    nofObj2 = length(mask2PixelChains);
    closestDistance = NaN(nofObj1,nofObj2);
    closestNeighbourIdx = NaN(nofObj1,nofObj2);
    meanClosestDistance = zeros(1,nofObj1);    
    
%% Calculate object by object
    for i=1:nofObj1
        currentIndex = 1;
        [sortedDistValues,sortedDistIndices] = sort(updatedCrossDistM(i,:));          
        %% Do while init
        bigDistanceMatrix = pdist2(mask1PixelChains{i},mask2PixelChains{sortedDistIndices(currentIndex)});
        %CurrentClosestDistances stores for each obj1 contour point the
        %closest distance to any of the 2nd object contour points
        currentClosestDistances = min(bigDistanceMatrix,[],2);
        minDistsFromThisObj = currentClosestDistances;
        %closestContourDistByObj stores the minimum contour distance by
        %objects (not for each contour point). The indexing of this array
        %is based on the sorted values of the center distances. i.e. the
        %first element of closestContourDistByObj refers to that object's
        %closest distance, which can be the closest to it based on the
        %updatedCrossDistance matrix.
        closestContourDistByObj = zeros(1,nofObj2);
        closestContourDistByObj(currentIndex) = min(minDistsFromThisObj);        
        %The while condition is composed of the followings: 
        %First is to check if we are not out of bounds. Then
        %   1) to make sure that the contour closest distances are really
        %   the closest ones for each contour point we have to go on if the
        %   estimations (in sortedDistValues) shows that there is potential
        %   having an object closer then so far (stored in currentClosestDistances)
        %   2) Check if we have reached already the numberLimit object (l-th).
        %   This contains 2 cases: if we haven't reached yet the l-th object then we
        %   have to go on, as the question is the l-th closest object. But
        %   even if we have reached the l-th closest object we should go on if
        %   there are better candidates in the following objects (as the
        %   sortedDistValues are only estimates).                           
        %   3) We go on if the next estimate
        %   (sortedDistValues(currentIndex+1)) is still better then the
        %   distance limit.
        while currentIndex < nofObj2 && ...
                (...
                    ( sortedDistValues(currentIndex+1) < max(currentClosestDistances) ) || ...
                    (...
                        connectionType(...
                            ( currentIndex < numberLimit ||  sortedDistValues(currentIndex+1) < max(closestContourDistByObj(1:currentIndex)) ) ...
                            ,...
                            ( sortedDistValues(currentIndex+1) < distLimit)...
                        )...
                     )...
                )
            currentIndex = currentIndex + 1;
            bigDistanceMatrix = pdist2(mask1PixelChains{i},mask2PixelChains{sortedDistIndices(currentIndex)});
            minDistsFromThisObj = min(bigDistanceMatrix,[],2);
            %ideally the currentClosestDist doesn't change from this point
            currentClosestDistances = min(currentClosestDistances,minDistsFromThisObj);            
            closestContourDistByObj(currentIndex) = min(minDistsFromThisObj);
        end
        [sortedContourObjectDists,sortedSortedObjectIdx] = sort(closestContourDistByObj(1:currentIndex));
        closestDistance(i,1:currentIndex) = sortedContourObjectDists;        
        
        closestNeighbourIdx(i,1:currentIndex) = sortedDistIndices(sortedSortedObjectIdx);
        %SANITY CHECK
        if numberLimit == 1
            if min(currentClosestDistances) == closestDistance(i,numberLimit)                
            else
                disp('Something really wrong');
            end
        end
        meanClosestDistance(i) = mean(currentClosestDistances);
    end
end
