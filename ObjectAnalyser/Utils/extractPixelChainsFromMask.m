function [object1MaxRadius,mask1PixelChains] = extractPixelChainsFromMask(mask1,mask1Centers,nofObj1)
%this function extracts the pixel chains (with their absolute coordinate in
%the image coordinate system. (axes according to regionprops)). It also
%calculates how far is the furthest perimeter point from the centroid

    object1MaxRadius = zeros(nofObj1,1);    
    
    labelledContours = CPlabelperim(mask1);
    mask1PixelChains = regionprops(labelledContours,'PixelList');
   
    for i=1:nofObj1        
        object1MaxRadius(i) = max(pdist2(mask1Centers(i,1:2),mask1PixelChains(i).PixelList(:,1:2)));
    end
end

%The old code here to extract contour pixels were anyway crap. Seen much
%better solution from CellProfiler.