function [ clusterID, distanceMatrix,maskCenters] = identifyClusters( mask, distanceMethod, maxDistance, varargin)
% AUTHOR:   Abel Szkalisity
% DATE:     Dec 1, 2016
% NAME:     identifyClusters
%
% Identify clusters of objects. The function builds up a graph from the
% objects in the mask image. There is edge between all objects which are
% within a specified distance. This distance can be specified as constant
% pixel distance between the centers OR the approximated contours.
% Alternatively based on an original image the distance can be weighted by
% the pixel intensities connecting the 2 center points. Therefore objects
% connected with brighter pixels are considered to be more closer to each
% other. The clusters are formed from the connected components of that
% graph.
%
% INPUT:
%   mask            Mask image identifying the objects to be processed.
%                   It can be a multi-layer mask image as well. Indices
%                   must go consecutively from 1 to N. (otherwise
%                   regrionprops gives back undefined results)
%   distanceMethod  'spatial' or 'radiometrial'. If the method is spatial
%                   then only the object positions count, if the method is
%                   radiometrial then also the intensities on the line
%                   connecting the 2 object centers.
%   maxDistance     The distance limit within 2 object is considered to be
%                   connected in the graph. It's value depends on
%                   distanceMethod.
%   varargin        distanceMethod dependent parameters.
%
%                       spatial:
%                       1. distanceEndPoints: can be either 'center' or
%                       'contour'. In the contour case the distances are
%                       calculated between estimated contours (the objects
%                       are replaced by a circle).
%
%                       radiometrial:
%                       1. OriginalImage: we need an image with the same
%                       size as mask, on which the radiometrial distance
%                       can be calculated.
%
% OUTPUT:
%   clusterID      An array with as many rows as many objects we provided
%                  through the mask image. Each entry provides a clusterID
%                  to that object determined by the connected components of
%                  the graph.
%   distanceMatrix The distanceMatrix on which the graph building is based.
%   maskCenters    Coordinates in 'regionprops' coordinate system for the
%                  input objects.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

maskRegionProperties = regionprops(mask);
maskCenters = cat(1,maskRegionProperties.Centroid); %concat to one array
maskAreas = cat(1,maskRegionProperties.Area);
maskCenters = maskCenters(:,1:2); %get rid of the 3rd dimension

if strcmp(distanceMethod,'spatial')    
    if strcmp(varargin{1},'center')
        distanceMatrix = squareform(pdist(maskCenters));
    elseif strcmp(varargin{1},'contour')    
        distanceMatrix = squareform(pdist(maskCenters)); 
        distanceMatrix = distanceMatrix - repmat(sqrt(convert2ColumnVector(maskAreas)./ pi),1,size(distanceMatrix,2)); %subtract one object estimated radius
        distanceMatrix = distanceMatrix - repmat(sqrt(convert2RowVector(maskAreas)./ pi),size(distanceMatrix,1),1); %subtract the other object's estimated radius
        distanceMatrix(distanceMatrix<0) = 0;
    else
        warning(['''' varargin{1} ''' is not valid argument for method ''' distanceMethod '''.']);
        return;
    end     
elseif strcmp(distanceMethod,'radiometrial')
    origImg = im2double(varargin{1}); %read in and normalize to 0-1 the input image.
    distanceMatrix = squareform(pdist(maskCenters));
    for i=1:size(distanceMatrix,1)-1
        for j=i+1:size(distanceMatrix,1)
            if distanceMatrix(i,j)>maxDistance
                newDistance = 0;
                distanceMatrix(i,j) = newDistance;
                distanceMatrix(j,i) = newDistance;
            else
                %connectionProfile = improfile(varargin{1},[maskCenters(i,1),maskCenters(j,1)],[maskCenters(i,2),maskCenters(j,2)]);
                connectionProfile = lineIntensitiesFromPicture( origImg, round(maskCenters(i,[2 1])),round(maskCenters(j,[2 1])));
                if mean(connectionProfile) == 0
                    newDistance = 0; %don't put edge if there isn't any intensity between
                else
                    newDistance = distanceMatrix(i,j)./mean(connectionProfile);
                end
                distanceMatrix(i,j) = newDistance;
                distanceMatrix(j,i) = newDistance;
            end
        end
    end
end

%threshold distances locally
dM = distanceMatrix;
dM(dM>maxDistance) = 0;
dM(dM<0) = 0;

if isempty(dM)    
    dM = [];
end

[~,componentVector] = graphconncomp(sparse(dM),'Weak','True');

clusterID = convert2ColumnVector(componentVector);
%Case of single point with single cluster
if isempty(clusterID) && size(maskCenters,1) == 1
    clusterID = 1;
    distanceMatrix = 0;
end

end

