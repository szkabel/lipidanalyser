function [ relationFeatures, featureNames] = measureObjectRelation(mask1Struct,mask2Struct,k,lth)
% AUTHOR:   Abel Szkalisity
% DATE:     Nov 28, 2016
% NAME:     measureObjectRelation
%
% A general function to measure relations between two type of object. By
% relation we mean their relative localization. This function computes some
% relation measurements for each object in mask1.
%
% INPUTS:
%  m1,m2            The first 2 input is the same in type, they describe
%                   the objects of interest. This is a structure with
%                   several fields (not only the actual mask image is
%                   passed as input but several other properties to fasten
%                   computation.)
%                   Fields for m1 and m2:
%                       Mask: Image that identifies the objects to measure.
%                       The pixels with the same mask image value form an
%                       object. The mask image can be provided in a layered
%                       form (3D mask image). Still in this case the indices
%                       identifying the objects are unique within the complete
%                       image. The indices identifies the objects, going from 1
%                       to N1 consecutively. m1.Mask and m2.Mask must have
%                       the same dimension.
%                       Centers: an N1 by 2 matrix for the centroid
%                       positions of the objects.
%                       MaxRadius: an N1 long array, providing the maximal
%                       distance between any contour point and the centre
%                       of the object.
%                       PixelChains: an N1 long cellarray. For each object
%                       it contains a matrix where each row is one pixel of
%                       the contour chain.
%  k                Parameter for k nearest neighbours for the mixture
%                   index
%  lth              Parameter for the index of the closest object
%  
% OUTPUT:
%  relationFeatures An N1 by m matrix where m is the number of relation
%                   describing features defined by this function.
%                   Currently calculated features:
%                       1. Overlap ratio: intersectionArea/object1Area
%                       2. Absolute overlap area: the size of the
%                       intersection area in pixels
%                       3. mixture index: |kNN = 2|/k (2 stands for object type 2 to which we compare)
%                       4. Closest distance: How far is the closest l-th other
%                       typed object to this one (distances measured from center)
%                       5. MeanDistance: The average distance from all
%                       other object measured from centers.
%                       6. Closest contour distance: how far is the l-th closest
%                       object if the distances are measured from the
%                       contours?
%                       7. Mean contour distance: for each contour point of
%                       the object1 calculate the closest contour point of
%                       any type2 object and take the mean of these.
%                   The rows of this matrix corresponds to the index of
%                   mask1.
%                   Description terminology: object# is the currently
%                   measured object of type# which is coming from mask#.
%  featureNames     A cellarray with m entry describing the columns of
%                   relationFeatures.
%                   
%

mask1 = mask1Struct.Mask;
mask1Centers = mask1Struct.Centers;

mask2 = mask2Struct.Mask;
mask2Centers = mask2Struct.Centers;

%generate the reference image (to which we'll compare the objects on the
%other mask)
mergedMask2 = logical(sum(mask2,3));

nofObj1 = size(mask1Centers,1);
nofObj2 = size(mask2Centers,1);

nofOutputs = 7;

featureNames = cell(1,nofOutputs);
%init variables
relationFeatures = zeros(nofObj1,nofOutputs);
currentFeature = 0;

%1. OVERLAP INDEX
currentFeature = currentFeature + 1;
featureNames{currentFeature} = 'OverlapIndex';
featureNames{currentFeature+1} = 'AbsoluteOverlap';
%iterate through all layers
for i=1:size(mask1,3)          
   %if this is a single layer image we take advantage about the consecutive
   %numbering
   if size(mask1,3) ~= 1
       thisLayer = mask1(:,:,i);
       indicesInThisLayer = unique(thisLayer);
   else
       thisLayer = mask1;
       indicesInThisLayer = 1:nofObj1;
   end   
   
   if ~isempty(indicesInThisLayer)
       
        if (indicesInThisLayer(1) == 0) %get rid of non-object regions.
           indicesInThisLayer = indicesInThisLayer(2:end);
        end
       
        rExamined = regionprops(thisLayer,'Area');
        rExamined = cat(1,rExamined.Area);
        rOverlap = regionprops(thisLayer .* mergedMask2,'Area');
        rOverlapMatrix = zeros(size(rExamined));
        rOverlap = cat(1,rOverlap.Area);        
        rOverlapMatrix(1:length(rOverlap)) = rOverlap; %fill in the first elements.
        relationFeatures(indicesInThisLayer,currentFeature) = rOverlapMatrix ./ rExamined;
        relationFeatures(indicesInThisLayer,currentFeature+1) = rOverlapMatrix;
   end   
end
%Increase feature number because of the absolute area
currentFeature = currentFeature + 1;

%
%CUSTOM REMOVE FOR FASTENING

%2. MIXTURE INDEX
currentFeature = currentFeature + 1;
featureNames{currentFeature} = 'MixtureIndex';
featureNames{currentFeature+1} = 'ClosestDistance';
featureNames{currentFeature+2} = 'MeanDistance';
featureNames{currentFeature+3} = 'ClosestContourDistance';
featureNames{currentFeature+4} = 'MeanContourDistance';

if ~isempty(mask1Centers) && ~isempty(mask2Centers)
    %remove the layer information (if exist)
    mask1Centers = mask1Centers(:,1:2);
    mask2Centers = mask2Centers(:,1:2);

    distM = squareform(pdist([mask1Centers;mask2Centers]));
    %check for too big k
    if k>size(distM,1)-1
        k = size(distM,1)-1;
        disp(['Warning: k exceeds the number of objects available. k is truncated to: ' num2str(k)]);
    end        

    %if there are at least 2 object.
    if k>0
        for i=1:nofObj1
            [~,nearestNeighbours] = sort(distM(i,:));
            nearestNeighbours = nearestNeighbours(2:end);
            relationFeatures(i,currentFeature) = sum(  nearestNeighbours(1:k)>nofObj1  )/k;
        end
    end    
    
    %Calc. the closest distance to the related object
    %Do this inside, we can only calculate distances if we have at least
    %one object    
    
    %cross distance matrix row stands for the mask1
    crossDistM = distM(1:nofObj1,nofObj1+1:end);
    
    if lth>nofObj2
        lth = nofObj2;
        disp(['Warning: l exceeds the number of objects available. l is truncated to: ' num2str(lth)]);
    end        
    
    %
    currentFeature = currentFeature + 1;    
    
    [~,closestCentreIndices] = sort(crossDistM,2);    
    relationFeatures(:,currentFeature) = crossDistM(sub2ind(size(crossDistM),(1:nofObj1)',closestCentreIndices(:,lth)));
    
    %SANITY CHECK
    if lth == 1 
        if ~all(relationFeatures(:,currentFeature) == min(crossDistM,[],2))        
            disp('something wrong');
        end        
    end
    
    %The average distance
    currentFeature = currentFeature + 1;    
    relationFeatures(:,currentFeature) = mean(crossDistM,2);       
    
    [closestDistances,~,meanClosestDistances] = measureDistanceBetweenChains(mask1Struct,mask2Struct,'numberLimit',lth);
    
    currentFeature = currentFeature + 1;    
    relationFeatures(:,currentFeature) = closestDistances(:,lth);
    
    currentFeature = currentFeature + 1;    
    relationFeatures(:,currentFeature) = meanClosestDistances;
    
else %SPECIAL CASES
    %if we came to the else branch therefore either mask1 or mask2 is empty
    %mixture index
    relationFeatures(1:nofObj1,currentFeature) = 0; % as if mask1 is empty then nofObjects = 0 so that it doesn't matter. If mask2 is empty then there isn't any 'other typed' object therefore mixture index is 0.    
    %ClosestDistance to other typed object
    currentFeature = currentFeature + 1;
    relationFeatures(1:nofObj1,currentFeature) = Inf; %ifs similarly to above, if there isn't other typed object then distance from it is infinite.
    %MeanDistance
    currentFeature = currentFeature + 1;
    relationFeatures(1:nofObj1,currentFeature) = Inf;
    %ClosestContourDistance
    currentFeature = currentFeature + 1;
    relationFeatures(1:nofObj1,currentFeature) = Inf;
    %MeanContourDistance
    currentFeature = currentFeature + 1;
    relationFeatures(1:nofObj1,currentFeature) = Inf;
end

%}

currentFeature = currentFeature + 1;
featureNames{currentFeature} = 'NofRelatedObjects';
relationFeatures(:,currentFeature) = nofObj2;



end

