function [backRelation] = transposeNeighbourArray(neighbourIndices,maxPossNeighbour)
%To calculate the inverse of the calculateNeighbours result. In that
%function for each object its neighbours are retrieved. However in some
%cases the inverse information might be also useful i.e. for each neighbour
%object retrieve all those to which it was assigned as neighbour.
%
%   INPUTS: 
%       neighbourIndices    A cellarray. Each entry refers to one normal
%                           object and contains the indices of its neighbours
%       maxPossNeighbour    An index indicating how long the output should
%                           be
%
%   OUPUT:
%       backRelation        A cellarray containing maxPossNeighbour
%                           elements. Each one is an array indicating all
%                           those objects to which the actual index was
%                           assigned

backRelation = cell(maxPossNeighbour,1);

for i=1:length(neighbourIndices)
    for j=1:length(neighbourIndices{i})
        backRelation{neighbourIndices{i}(j)}(end+1) = i;
    end
end


end

