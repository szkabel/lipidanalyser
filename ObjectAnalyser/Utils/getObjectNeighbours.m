function [neighbourList] = getObjectNeighbours(nghbrType,param,mask1,mask2)
%nghbrType can be yes-ring,yes-k-closest referring to the ring specified or
%the k-closest neighbours. Param correspondingly is either a pixel value,
%or the number of closest neighbors to consider
%m1 and m2 are grayscale mask images
%
%The neighbourList is a cellarray with as many elements as many gray levels
%mask1 has (and each entry refers to a single object there)
%The entries are structures with the following fields:
%   index arrays which encode the neighbours of the specific
%   object from mask2

mask1Centers = regionprops(mask1,'Centroid');
mask2Centers = regionprops(mask2,'Centroid');

mask1Centers = cat(1,mask1Centers.Centroid);
mask2Centers = cat(1,mask2Centers.Centroid);

nofObj1 = size(mask1Centers,1);
nofObj2 = size(mask2Centers,1);

switch (nghbrType)
    case 'yes-k-closest'
        %here we assume that mask2Centers are consecutive
        crossDistM = pdist2(mask1Centers,mask2Centers);
        for i=1:nofObj1
            [~,neighbourIdx] = crossDistM(i,:);
        end
    case 'yes-ring'
        
end



end