function deleteUIControls(textHandles,editHandles)
%To delete all param and text edit handles

    for i=1:length(textHandles)
        if ishandle(textHandles{i})
            delete(textHandles{i});
        end
    end
    for i=1:length(editHandles)
        if ishandle(editHandles{i})
            delete(editHandles{i});
        end
    end
end

