function varargout = AddFilter_GUI(varargin)
% ADDFILTER_GUI MATLAB code for AddFilter_GUI.fig
%      ADDFILTER_GUI, by itself, creates a new ADDFILTER_GUI or raises the existing
%      singleton*.
%
%      H = ADDFILTER_GUI returns the handle to a new ADDFILTER_GUI or the handle to
%      the existing singleton*.
%
%      ADDFILTER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADDFILTER_GUI.M with the given input arguments.
%
%      ADDFILTER_GUI('Property','Value',...) creates a new ADDFILTER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AddFilter_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AddFilter_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AddFilter_GUI

% Last Modified by GUIDE v2.5 25-Jan-2017 22:54:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AddFilter_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @AddFilter_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AddFilter_GUI is made visible.
function AddFilter_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AddFilter_GUI (see VARARGIN)

% UIWAIT makes AddFilter_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

uData.selectedIndex = varargin{1}; %the first input is the array of the selected indices in the measurement listbox (List_Features).
uData.maxIndex = varargin{2}; %the second input argument is the number of actual requested features (this is needed to distinguish between the filter fields and measurement fields)

conditionInfoText = cell(length(varargin{3})*2);
for i=1:length(varargin{3})
    conditionInfoText{i*2} = ['#' num2str(i) '#: ' varargin{3}{i}];
end
set(handles.Text_VariablesInfo,'String',conditionInfoText);

%the 4th input indicates to display or not the true/false values.
if varargin{4} == 0
%hide all fields
    set(handles.textTrue,'Visible','off');
    set(handles.textFalse,'Visible','off');
    set(handles.Edit_True,'Visible','off');
    set(handles.Edit_False,'Visible','off');
end

%This is a double check this is already done in the call.
if uData.maxIndex < max(uData.selectedIndex)
    uData.closeFigure = 1;
else    
    uData.closeFigure = 0;    
end

set(handles.figure1,'UserData',uData);

uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = AddFilter_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if ~isempty(handles)    
    uData = get(handles.figure1,'UserData');
    if uData.closeFigure
        varargout{1} = [];
        figure1_CloseRequestFcn(handles.figure1,eventdata,handles);
    else
        res.index = uData.selectedIndex;
        res.condition = uData.condition;
        res.true = uData.true;
        res.false = uData.false;
        varargout{1} = res;
        figure1_CloseRequestFcn(handles.figure1,eventdata,handles);
    end
else
    varargout{1} = [];
    figure1_CloseRequestFcn(hObject,eventdata,handles);
end

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_Condition_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_Condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Edit_Condition as text
%        str2double(get(hObject,'String')) returns contents of Edit_Condition as a double


% --- Executes during object creation, after setting all properties.
function Edit_Condition_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_Condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_True_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_True (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Edit_True as text
%        str2double(get(hObject,'String')) returns contents of Edit_True as a double


% --- Executes during object creation, after setting all properties.
function Edit_True_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_True (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Edit_False_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_False (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Edit_False as text
%        str2double(get(hObject,'String')) returns contents of Edit_False as a double


% --- Executes during object creation, after setting all properties.
function Edit_False_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_False (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Button_OK.
function Button_OK_Callback(hObject, eventdata, handles)
% hObject    handle to Button_OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
uData.condition = get(handles.Edit_Condition,'String');
uData.true = get(handles.Edit_True,'String');
uData.false = get(handles.Edit_False,'String');
set(handles.figure1,'UserData',uData);

uiresume(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
