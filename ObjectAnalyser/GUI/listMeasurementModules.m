function measList = listMeasurementModules(subFolder)    
    fullp = mfilename('fullpath');
    splittedFullP = strsplit(fullp,{'/','\'});
    splittedFullP(end-1:end) = []; % ..
    splittedFullP{end+1} = 'MeasurementModules';
    dirPath = fullfile(splittedFullP{:});
    if (fullp(1) == '/') % handling the linux root case
        dirPath = ['/' dirPath];
    end
    
    if nargin==0 % default is everything        
        measList = listRecursive(dirPath);        
    else %specifications
        if strcmp(subFolder,'root')
            measList = listMFromFolder(dirPath);
        else
        end
    end
end

function measList = listMFromFolder(dirToList)
    wS = what(dirToList);
    w = struct2cell(wS);
    m = w{2};
    % for only the math files
    for i = 1:length(m)
        %see its name without .m
        splitted = strsplit(m{i},'.');
        %rename
        m{i} = splitted{1};
    end
    %return the cellarray
    measList = m;
end

function measList = listRecursive(folder)
    %root
    measList = listMFromFolder(folder);
    d = dir(folder);
    for i=3:length(d)
        if d(i).isdir
            measList = [measList; listRecursive(fullfile(folder,d(i).name))];
        end
    end
end