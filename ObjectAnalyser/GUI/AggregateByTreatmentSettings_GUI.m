function varargout = AggregateByTreatmentSettings_GUI(varargin)
% AGGREGATEBYTREATMENTSETTINGS_GUI MATLAB code for AggregateByTreatmentSettings_GUI.fig
%      AGGREGATEBYTREATMENTSETTINGS_GUI, by itself, creates a new AGGREGATEBYTREATMENTSETTINGS_GUI or raises the existing
%      singleton*.
%
%      H = AGGREGATEBYTREATMENTSETTINGS_GUI returns the handle to a new AGGREGATEBYTREATMENTSETTINGS_GUI or the handle to
%      the existing singleton*.
%
%      AGGREGATEBYTREATMENTSETTINGS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AGGREGATEBYTREATMENTSETTINGS_GUI.M with the given input arguments.
%
%      AGGREGATEBYTREATMENTSETTINGS_GUI('Property','Value',...) creates a new AGGREGATEBYTREATMENTSETTINGS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AggregateByTreatmentSettings_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AggregateByTreatmentSettings_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AggregateByTreatmentSettings_GUI

% Last Modified by GUIDE v2.5 20-Dec-2018 12:00:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AggregateByTreatmentSettings_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @AggregateByTreatmentSettings_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AggregateByTreatmentSettings_GUI is made visible.
function AggregateByTreatmentSettings_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AggregateByTreatmentSettings_GUI (see VARARGIN)

% Choose default command line output for AggregateByTreatmentSettings_GUI
handles.output = hObject;

uData.options = {0,'No treatment export';...
                 1,'Specify by text';...
                 2,'CSV file'};
             
handles.UserData = uData;             

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AggregateByTreatmentSettings_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = AggregateByTreatmentSettings_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit_TextArray_Callback(hObject, eventdata, handles)
% hObject    handle to edit_TextArray (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_TextArray as text
%        str2double(get(hObject,'String')) returns contents of edit_TextArray as a double


% --- Executes during object creation, after setting all properties.
function edit_TextArray_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_TextArray (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_CSV_fileAndPath_Callback(hObject, eventdata, handles)
% hObject    handle to edit_CSV_fileAndPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_CSV_fileAndPath as text
%        str2double(get(hObject,'String')) returns contents of edit_CSV_fileAndPath as a double


% --- Executes during object creation, after setting all properties.
function edit_CSV_fileAndPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_CSV_fileAndPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button_browse.
function button_browse_Callback(hObject, eventdata, handles)
% hObject    handle to button_browse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected object is changed in optionsPanel.
function optionsPanel_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in optionsPanel 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = handles.UserData;
switch hObject.String
    case uData.options{1,2}
        disp(1)
    case uData.options{2,2}
        disp(2)
    case uData.options{3,2}
        disp(3)
end
