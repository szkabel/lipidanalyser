function varargout = AddMeasurement_GUI(varargin)
% ADDMEASUREMENT_GUI MATLAB code for AddMeasurement_GUI.fig
%      ADDMEASUREMENT_GUI, by itself, creates a new ADDMEASUREMENT_GUI or raises the existing
%      singleton*.
%
%      H = ADDMEASUREMENT_GUI returns the handle to a new ADDMEASUREMENT_GUI or the handle to
%      the existing singleton*.
%
%      ADDMEASUREMENT_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADDMEASUREMENT_GUI.M with the given input arguments.
%
%      ADDMEASUREMENT_GUI('Property','Value',...) creates a new ADDMEASUREMENT_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AddMeasurement_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AddMeasurement_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AddMeasurement_GUI

% Last Modified by GUIDE v2.5 08-Apr-2019 14:48:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AddMeasurement_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @AddMeasurement_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AddMeasurement_GUI is made visible.
function AddMeasurement_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AddMeasurement_GUI (see VARARGIN)

% Choose default command line output for AddMeasurement_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
uData = get(handles.figure1,'UserData');

% init uData
uData.page = 1;
uData.maxPage = 3;
ECP = varargin{1};
uData.ECP = ECP;
uData.prevMeasModuleName = '';
uData.prevFieldList = {};
uData.module = [];

set(handles.figure1,'UserData',uData);
placeItemsOnGUI(handles);
uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = AddMeasurement_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% {
% Get default command line output from handles structure
if isempty(handles) || isempty(handles.output.featureIndices)
    varargout{1} = -1; %fake index to indicate to the calling code that the user didn't select any new measurements.
    varargout{2} = [];
    varargout{3} = [];
    varargout{4} = [];
    if ~isempty(handles)
        figure1_CloseRequestFcn(hObject, eventdata, handles)
    end
else     
    varargout{1} = handles.output.featureIndices;
    varargout{2} = handles.output.featureNames;
    varargout{3} = handles.output.module;
    varargout{4} = handles.output.dynamicFields;

    figure1_CloseRequestFcn(hObject, eventdata, handles)
end
%}

% --- Executes on button press in Button_AddMeasurements.
function Button_AddMeasurements_Callback(hObject, eventdata, handles)
% hObject    handle to Button_AddMeasurements (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');

handles.output = [];
handles.output.featureIndices = get(handles.Listbox_ProvidedFeatures,'Value');
handles.output.dynamicFields = uData.dynamicFields(handles.output.featureIndices);
handles.output.module = uData.module;

uniqueID = uData.module.uniqueID();

stringArray = get(handles.Listbox_ProvidedFeatures,'String');
handles.output.featureNames = stringArray(handles.output.featureIndices);
for i=1:length(handles.output.featureNames)
    handles.output.featureNames{i} =  [handles.output.featureNames{i} '_' uniqueID];
end

guidata(hObject,handles);
uiresume(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

placeItemsOnGUI(handles);

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

handles = createGUI(hObject);

guidata(hObject, handles);

%% Figure specific internal functions

function placeItemsOnGUI(handles)
%as this will be a "wizard" like object we need to know on which page we
%are, as the drawing depends on it

%First get the figure size

figSize = get(handles.figure1,'Position');

buttonWidth = 200;
buttonHeight = 30;
margin = 20;
buttonIdx = 1;
buttonPosition = [(buttonIdx-1)*buttonWidth+margin*(buttonIdx) 10 buttonWidth buttonHeight];
set(handles.PageBackwardButton,'Position',buttonPosition);
buttonIdx = buttonIdx+1;
buttonPosition = [(buttonIdx-1)*buttonWidth+margin*(buttonIdx) 10 buttonWidth buttonHeight];
set(handles.PageForwardButton,'Position',buttonPosition);
buttonIdx = buttonIdx+1;
buttonPosition = [(buttonIdx-1)*buttonWidth+margin*(buttonIdx) 10 buttonWidth buttonHeight];
set(handles.Button_AddMeasurements,'Position',buttonPosition);

uData = get(handles.figure1,'UserData');

pageStates = cell(1,uData.maxPage);
for i=1:length(pageStates)
    if uData.page == i
        pageStates{i} = 'On';
    else
        pageStates{i} = 'Off';
    end
end

if uData.page == 1
    set(handles.PageBackwardButton,'Enable','Off');
else
    set(handles.PageBackwardButton,'Enable','On');
end
if uData.page == uData.maxPage
    set(handles.PageForwardButton,'Enable','Off');
else
    set(handles.PageForwardButton,'Enable','On');
end
    
%Page number
pgNum = 1;
set(handles.Listbox_AvailableModules,'Visible',pageStates{pgNum});

pgNum = 2;
set(handles.Uipanel_Parameters,'Visible',pageStates{pgNum});

pgNum = 3;
set(handles.Button_AddMeasurements,'Enable',pageStates{pgNum});
set(handles.Listbox_ProvidedFeatures,'Visible',pageStates{pgNum});

greatPanelPosition = [margin margin*2 + buttonHeight figSize(3)-2*margin figSize(4) - 3*margin - buttonHeight];
set(handles.Listbox_AvailableModules,'Position',greatPanelPosition);
set(handles.Uipanel_Parameters,'Position',greatPanelPosition);
set(handles.Listbox_ProvidedFeatures,'Position',greatPanelPosition);

if isfield(uData,'textHandles') && isfield(uData,'editHandles')
    %resizeUIControls(uData.textHandles,uData.editHandles,greatPanelPosition,20, [10 50]); %magic consts same as below for nice look
      
    placeUIControls(...
    uData.paramarray, uData.textHandles, uData.editHandles, greatPanelPosition, 20, [10 50],...        
    'checkVisibility',uData.checkVisibility);    
end


function handles = createGUI(hObject)

handles.PageForwardButton = uicontrol(...
    'Parent',hObject,...
    'Style','pushbutton',...
    'Units','pixels',...
    'String','>>',...    
    'Callback',@(hObject,eventdata)AddMeasurement_GUI('pageForwardCallback',hObject,eventdata,guidata(hObject)));

handles.PageBackwardButton = uicontrol(...
    'Parent',hObject,...
    'Style','pushbutton',...
    'Units','pixels',...
    'String','<<',...    
    'Callback',@(hObject,eventdata)AddMeasurement_GUI('pageBackwardCallback',hObject,eventdata,guidata(hObject)));

handles.Button_AddMeasurements = uicontrol(...
    'Parent',hObject,...
    'Style','pushbutton',...
    'Units','pixels',...
    'String','Add measurements',...    
    'Callback',@(hObject,eventdata)AddMeasurement_GUI('Button_AddMeasurements_Callback',hObject,eventdata,guidata(hObject)));

handles.Listbox_AvailableModules = uicontrol(...
    'Parent',hObject,...
    'Style','listbox',...
    'Units','pixels',...
    'String',listMeasurementModules());

handles.Uipanel_Parameters = uipanel(...
    'Parent',hObject,...   
    'Units','pixels',...
    'Title','Module parameters');

handles.Listbox_ProvidedFeatures = uicontrol(...
    'Parent',hObject,...
    'Style','listbox',...
    'Units','pixels',...
    'String','Something wrong');


function pageForwardCallback(~,~,handles)    
    uData = get(handles.figure1,'UserData');    
    uData.page = uData.page + 1;    
    
    %% Handle transition between pages
    if uData.page == 2        
        moduleString = handles.Listbox_AvailableModules.String{get(handles.Listbox_AvailableModules,'Value')};
        if ~strcmp(moduleString,uData.prevMeasModuleName)
            ECP = uData.ECP; %#ok<NASGU> used in eval
            [paramarray,checkVisibility] = eval([moduleString '.getParameters(ECP);']);
            if isfield(uData,'textHandles') && isfield(uData,'editHandles'), deleteUIControls(uData.textHandles,uData.editHandles); end
            [uData.textHandles,uData.editHandles] = generateUIControls(paramarray,handles.Uipanel_Parameters,20,[10,50],'checkVisibility',checkVisibility);
            uData.paramarray = paramarray;
            uData.checkVisibility = checkVisibility;
            uData.prevMeasModuleName = moduleString;
        end
    end
    
    if uData.page == 3        
        try
            paramcell = fetchUIControlValues(uData.paramarray,uData.editHandles,[uData.prevMeasModuleName '.checkParameters']); %#ok<NASGU> used in eval
            uData.module = eval([uData.prevMeasModuleName '(paramcell,uData.ECP);']); %constructor
            [fieldList,uData.dynamicFields] = uData.module.providedMeasurements(); %the newly created object's provided measurements 
            if length(uData.prevFieldList) ~= length(fieldList) || ~all(cellfun(@strcmp,uData.prevFieldList,fieldList))
                set(handles.Listbox_ProvidedFeatures,'string',fieldList);
                set(handles.Listbox_ProvidedFeatures,'Max',length(fieldList));
                set(handles.Listbox_ProvidedFeatures,'Value',1);
                uData.prevFieldList = fieldList;
            end
        catch ME
            %reset page
            uData.page = uData.page - 1;    
            %if error was thrown because user input violates the constraints
            if strcmp(ME.identifier,'LipidAnalyser:errorConstraintFault')
                %user notification is already handled in the check function
            elseif strcmp(ME.identifier,'LipidAnalyser:ModuleConstruct')
                errordlg(ME.message);
            else
                %otherwise throw the exc on.
                rethrow(ME);
            end        
        end               
                
    end

    set(handles.figure1,'UserData',uData);
    placeItemsOnGUI(handles);        
        
    
function pageBackwardCallback(~,~,handles)    
    uData = get(handles.figure1,'UserData');  
    uData.page = uData.page - 1;
    set(handles.figure1,'UserData',uData);
    placeItemsOnGUI(handles);
