function varargout = ByTreatmentOptions_GUI(varargin)
% BYTREATMENTOPTIONS_GUI MATLAB code for ByTreatmentOptions_GUI.fig
%      BYTREATMENTOPTIONS_GUI, by itself, creates a new BYTREATMENTOPTIONS_GUI or raises the existing
%      singleton*.
%
%      H = BYTREATMENTOPTIONS_GUI returns the handle to a new BYTREATMENTOPTIONS_GUI or the handle to
%      the existing singleton*.
%
%      BYTREATMENTOPTIONS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BYTREATMENTOPTIONS_GUI.M with the given input arguments.
%
%      BYTREATMENTOPTIONS_GUI('Property','Value',...) creates a new BYTREATMENTOPTIONS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ByTreatmentOptions_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ByTreatmentOptions_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ByTreatmentOptions_GUI

% Last Modified by GUIDE v2.5 29-Jan-2019 17:46:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ByTreatmentOptions_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ByTreatmentOptions_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ByTreatmentOptions_GUI is made visible.
function ByTreatmentOptions_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ByTreatmentOptions_GUI (see VARARGIN)

% Choose default command line output for ByTreatmentOptions_GUI
%handles.output = hObject;

handles.table_custom.Data = cell(50,20);

if ~isempty(varargin)
    restoreData = varargin{1};
    switch restoreData.type
        case 'none'
            handles.radio_none.Value = 1;            
        case 'file'
            handles.radio_file.Value = 1;
            handles.edit_treatmentPath.String = restoreData.savedData;
        case 'localTable'
            handles.radio_custom.Value = 1;
            handles.table_custom.Data = restoreData.savedData;
    end
    set(handles.listbox_aggregationFunctions,'Value',restoreData.selectedFuncs);                
end

uibuttongroup1_SelectionChangedFcn(handles.uibuttongroup1, [], handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ByTreatmentOptions_GUI wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ByTreatmentOptions_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


if isfield(handles,'output')
    varargout{1} = handles.output{1};
    varargout{2} = handles.output{2};    
    varargout{3} = handles.output{3};
    varargout{4} = 'ok';
else
    varargout{1} = [];
    varargout{2} = [];
    varargout{3} = [];
    varargout{4} = 'cancel';
end
if isfield(handles,'figure1')
    figure1_CloseRequestFcn(hObject, eventdata, handles);
end


function edit_treatmentPath_Callback(hObject, eventdata, handles)
% hObject    handle to edit_treatmentPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_treatmentPath as text
%        str2double(get(hObject,'String')) returns contents of edit_treatmentPath as a double


% --- Executes during object creation, after setting all properties.
function edit_treatmentPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_treatmentPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[treatmentFile,treatmentPath] = uigetfile('*.csv','Specify a treatment file',pwd);
if ~(isempty(treatmentFile) || (length(treatmentFile)==1 && treatmentFile == 0))
    set(handles.edit_treatmentPath,'String',fullfile(treatmentPath,treatmentFile));
end


% --- Executes when selected object is changed in uibuttongroup1.
function uibuttongroup1_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uibuttongroup1 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.radio_none.Value
    handles.listbox_aggregationFunctions.Enable = 'off';
else
    handles.listbox_aggregationFunctions.Enable = 'on';
end

if handles.radio_file.Value ~= 1
    handles.edit_treatmentPath.Enable = 'off';
    handles.pushbutton1.Enable = 'off';    
else
    handles.edit_treatmentPath.Enable = 'on';
    handles.pushbutton1.Enable = 'on';     
end

if handles.radio_custom.Value ~= 1
    handles.table_custom.Enable = 'off';
else
    handles.table_custom.Enable = 'on';    
end


% --- Executes on button press in buttonSaveSettings.
function buttonSaveSettings_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSaveSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%   Ask the user to provide the image name regular expressions and the
%   possible replacement name. The regular expressions are according to
%   MATLAB: see
%   https://se.mathworks.com/help/matlab/ref/regexp.html#inputarg_expression

%This should create the treatmentBasedStructure which is a cellarray of
%structures, where the structure has the following fields:
% .name the name of the treatment
% .regex The regular expressions for the images to match
% .data This is empty initially, generate report collects the data for this

%default values

contents = cellstr(get(handles.listbox_aggregationFunctions,'String'));
createByTreatmentFile = contents(get(handles.listbox_aggregationFunctions,'Value'));
byTreatmentStructure = [];
typeOfTreatment = 'none';
savedData = [];

if handles.radio_none.Value
    createByTreatmentFile = []; %empty all functions and therefore ban any file to be created   
end

try
    if handles.radio_file.Value
        %check if everything is ok
        filePathAndName = handles.edit_treatmentPath.String;
        if isempty(filePathAndName), error('No treatment file was specified'); end
        if exist(filePathAndName,'file')
            fid = fopen(filePathAndName);

            i = 1;
            oneLine = fgetl(fid);
            while ischar(oneLine)
                if ~isempty(oneLine)
                    currTreatment = strsplit(oneLine,';');
                    inputStructure.name = strtrim(currTreatment{1});
                    for j=2:length(currTreatment), currTreatment{j} = strtrim(currTreatment{j});  end
                    inputStructure.regex = currTreatment(2:end);
                    inputStructure.data = [];    
                    inputStructure.nofElements = 0;

                    byTreatmentStructure{i} = inputStructure; %#ok<*AGROW> According to Matlab forums the line numbers can't be estimated beforehand.
                    i = i+1;
                end

                oneLine = fgetl(fid);            
            end

            fclose(fid);
            typeOfTreatment = 'file';
            savedData = handles.edit_treatmentPath.String;
        else
            error(['Treatment file: ' filePathAndName ' doesn''t exist']);
        end
    end

    if handles.radio_custom.Value
        tableData = handles.table_custom.Data;
        nonEmptyRowIdx = find(sum(~cellfun(@isempty,tableData),2)>0);
        byTreatmentStructure = cell(1,length(nonEmptyRowIdx));
        for i=1:length(nonEmptyRowIdx)
            %If there is data in the line and %If there is a proper treatment name
            if ~isempty(tableData{nonEmptyRowIdx(i),1})
                inputStructure.name = strtrim(tableData{nonEmptyRowIdx(i),1});
                reducedRow = tableData(nonEmptyRowIdx(i),2:end);
                inputStructure.regex = reducedRow(~cellfun(@isempty,reducedRow));
                inputStructure.data = [];
                inputStructure.nofElements = 0;
                
                byTreatmentStructure{i} = inputStructure;
            end
        end            
        typeOfTreatment = 'localTable';
        savedData = handles.table_custom.Data;
    end
catch e
    errordlg(e.message,'Settings error');
    %createByTreatmentFile = []; %empty all functions and therefore ban any file to be created
    return;
end

% Get default command line output from handles structure
handles.output{1} = createByTreatmentFile;
handles.output{2} = byTreatmentStructure;
handles.output{3}.type = typeOfTreatment;
handles.output{3}.savedData = savedData;
handles.output{3}.selectedFuncs = get(handles.listbox_aggregationFunctions,'Value');

guidata(hObject,handles);

uiresume(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(handles.figure1);

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on selection change in listbox_aggregationFunctions.
function listbox_aggregationFunctions_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_aggregationFunctions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_aggregationFunctions contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_aggregationFunctions


% --- Executes during object creation, after setting all properties.
function listbox_aggregationFunctions_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_aggregationFunctions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
