function varargout = ObjectAnalyser_GUI(varargin)
% OBJECTANALYSER_GUI MATLAB code for ObjectAnalyser_GUI.fig
%      OBJECTANALYSER_GUI, by itself, creates a new OBJECTANALYSER_GUI or raises the existing
%      singleton*.
%
%      H = OBJECTANALYSER_GUI returns the handle to a new OBJECTANALYSER_GUI or the handle to
%      the existing singleton*.
%
%      OBJECTANALYSER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OBJECTANALYSER_GUI.M with the given input arguments.
%
%      OBJECTANALYSER_GUI('Property','Value',...) creates a new OBJECTANALYSER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ObjectAnalyser_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ObjectAnalyser_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ObjectAnalyser_GUI

% Last Modified by GUIDE v2.5 24-Mar-2019 12:03:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ObjectAnalyser_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ObjectAnalyser_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ObjectAnalyser_GUI is made visible.
function ObjectAnalyser_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ObjectAnalyser_GUI (see VARARGIN)

% Choose default command line output for ObjectAnalyser_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ObjectAnalyser_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

welcomeText = sprintf('Welcome to the Object analyser!\nTo start to use the program select a dataset to work on. (ECP.mat exported by CellProfiler)');

global Project;

if nargin == 4
    ECP = varargin{1};
    ECPPath = ECP.getWorkFolder();
else

    choice = questdlg(welcomeText,'Welcome! Tervetuola! Willkommen! Udvozoljuk!','Select data set','Exit','Don''t touch','Select data set');

    switch choice
        case 'Select data set'
            [ECPName,ECPPath] = uigetfile('.mat','Specify the location of ECP');            
        case 'Don''t touch' %burnt in fastenings for easier development
            ECPName = 'ECP.mat';
            %ECPPath = 'D:\�bel\SZBK\Projects\LipidAnalyzer\WorkingFolder\';
            %ECPPath = 'D:\�bel\SZBK\Projects\Veijo\Experiment247';
            %ECPPath = 'D:\�bel\SZBK\Projects\LipidAnalyzer\ArtificialImages\GameImagesToTest';
            ECPPath = 'D:\Abel\SZBK\Trainings\181207_MikroAnalPractical\Images';
            %set(handles.Edit_OutputPath,'String','D:\�bel\SZBK\Projects\LipidAnalyzer\WorkingFolder\FakeFolder');
            set(handles.Edit_OutputPath,'String','D:\Abel\SZBK\Trainings\181207_MikroAnalPractical\Images\Results');
        case 'Exit'
            uData.closeFigure = 1;
            set(handles.figure1,'UserData',uData);
            return;
    end    
    load(fullfile(ECPPath,ECPName),'ECP');
    
end

if checkToolboxByName('Parallel Computing Toolbox')  
    handles.checkbox_parallel.Enable = 'on';
else
    handles.checkbox_parallel.Enable = 'off';
end

% Set FocusGain callback

% CODE FROM: https://undocumentedmatlab.com/blog/detecting-window-focus-events
% Get the underlying Java reference
warning off MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame
jFig = get(handles.figure1, 'JavaFrame');
jAxis = jFig.getAxisComponent;
 
% Set the focus event callback
set(jAxis.getComponent(0),'FocusGainedCallback',{@updateObjectAnalyserGUI,handles.figure1});
% perhaps also set the FocusLostCallback here

if exist('ECP','var')     
    ECP.afterReOpen();
    
    [ok,ECP] = checkFolders(ECP);
    if ~ok
        uData.closeFigure = 1;
        set(handles.figure1,'UserData',uData);
        return;
    end
    fillGUIwithData(ECP,handles);

    %load uData here from Project if exists
    if ~isempty(Project) && isFieldRec(Project,{'OAWin','uData'}) && isFieldRec(Project,{'OAWin','gui'})
        uData = Project.OAWin.uData;        
        set(handles.figure1,'UserData',uData);    
        storeObjectAnalyserGUIstate(handles,'load',Project.OAWin.gui);
        drawFeatureList(handles);
    else
    %Default settings then
        uData.requestedMeasurements = cell(0);
        uData.requestedFieldList = cell(0);
        uData.requestedFieldMapping = zeros(0,3);
        uData.aggregateFields = [];
        uData.filterNameList = cell(0);
        uData.filters = cell(0);
        uData.ECPPath = ECPPath; %to store the work dir in principle
        [uData.byTreatment.bool,uData.byTreatment.struct] = createDefaultByTreatmentOptions();
    end
    
    
    uData.ECP = ECP;
    set(handles.figure1,'UserData',uData);    
    setConfigureMode(2,handles); %turn off config stuff.
else
    uData.closeFigure = 1;
    set(handles.figure1,'UserData',uData);
end

% --- Outputs from this function are returned to the command line.
function varargout = ObjectAnalyser_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
uData = get(handles.figure1,'UserData');
if isfield(uData,'closeFigure')
    figure1_CloseRequestFcn(handles.figure1, [], handles);
end
varargout{1} = handles.output;


% --- Executes on selection change in Popup_SelectObject.
function Popup_SelectObject_Callback(hObject, eventdata, handles)
% hObject    handle to Popup_SelectObject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Popup_SelectObject contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Popup_SelectObject


% --- Executes during object creation, after setting all properties.
function Popup_SelectObject_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Popup_SelectObject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Listbox_SelectImage.
function Listbox_SelectImage_Callback(hObject, eventdata, handles)
% hObject    handle to Listbox_SelectImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Listbox_SelectImage contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Listbox_SelectImage


% --- Executes during object creation, after setting all properties.
function Listbox_SelectImage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Listbox_SelectImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button_SelectAll.
function button_SelectAll_Callback(hObject, eventdata, handles)
% hObject    handle to button_SelectAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

nofImgs = length(get(handles.Listbox_SelectImage,'String'));
set(handles.Listbox_SelectImage,'Value',1:nofImgs);

% --- Executes on button press in button_InvertSelection.
function button_InvertSelection_Callback(hObject, eventdata, handles)
% hObject    handle to button_InvertSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
selectedIndices = get(handles.Listbox_SelectImage,'Value');
nofImgs = length(get(handles.Listbox_SelectImage,'String'));
newIndices = setdiff(1:nofImgs,selectedIndices);
set(handles.Listbox_SelectImage,'Value',newIndices);


% --- Executes on selection change in Listbox_Features.
function Listbox_Features_Callback(hObject, eventdata, handles)
% hObject    handle to Listbox_Features (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Listbox_Features contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Listbox_Features


% --- Executes during object creation, after setting all properties.
function Listbox_Features_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Listbox_Features (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Button_AddFeature.
function Button_AddFeature_Callback(~, ~, handles)
% hObject    handle to Button_AddFeature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uidata = get(handles.figure1,'UserData');
if get(handles.checkbox_FixRelatedMeasurements,'Value')
    [selectedIndices,featureNames,module,dynamicFields] = AddMeasurement_GUI(uidata.ECP,'AggregateByParent');
else
    [selectedIndices,featureNames,module,dynamicFields] = AddMeasurement_GUI(uidata.ECP);
end
if selectedIndices ~= -1
    uidata.requestedMeasurements{end+1} = module;
    for i=1:length(selectedIndices)
        uidata.requestedFieldList{end+1} = featureNames{i};
        uidata.requestedFieldMapping(end+1,:) = [length(uidata.requestedMeasurements) selectedIndices(i) dynamicFields(i)];
    end    
    
    set(handles.figure1,'UserData',uidata);
    drawFeatureList(handles);
end
%

% --- Executes on button press in Button_RemoveFeature.
function Button_RemoveFeature_Callback(~, ~, handles)
% hObject    handle to Button_RemoveFeature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
indicesToDelete = get(handles.Listbox_Features,'Value');
[indicesToDeleteFromFeatures,indicesToDeleteFromFilters] = retainMeasurementIndices(indicesToDelete,uData);
%The ORDER of following 3 call (for is counted as 1) cannot be switched. First we must delete
%from the filters and then from the measurements, because the measurement
%delete deletes also those filters which are connected to the deleted
%measurement. However in the filter delete we must sustain the really
%requested delete (otherwise the indces would slide away)
uData = deleteFromFilters(uData,indicesToDeleteFromFilters);

for i=1:length(uData.filters)
    if ismember(uData.filters{i}.index,indicesToDeleteFromFeatures)
        uData = deleteFromFilters(uData,i+length(uData.requestedFieldList));
    end
end

uData = deleteFromFeatures(uData,indicesToDeleteFromFeatures);

set(handles.figure1,'UserData',uData);
set(handles.Listbox_Features,'Value',1);
drawFeatureList(handles);


% --- Executes on button press in Button_Aggregate.
function Button_Aggregate_Callback(~, ~, handles) %#ok<DEFNU> Called from GUI
% hObject    handle to Button_Aggregate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
uData.aggregateFields = get(handles.Listbox_Features,'Value');
set(handles.figure1,'UserData',uData);
updateAggregateColors(handles);

% --- Executes on button press in Button_disaggregate.
function Button_disaggregate_Callback(~, ~, handles) %#ok<DEFNU> Called from GUI
% hObject    handle to Button_disaggregate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
selected = get(handles.Listbox_Features,'Value');
uData.aggregateFields = setdiff(uData.aggregateFields,selected);
set(handles.figure1,'UserData',uData);
updateAggregateColors(handles);


% --- Executes on button press in Button_GenerateReport.
function Button_GenerateReport_Callback(hObject, eventdata, handles)
% hObject    handle to Button_GenerateReport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
data = uData.ECP;
selectedImageIndices = get(handles.Listbox_SelectImage,'Value');
contents = cellstr(get(handles.Popup_SelectObject,'String'));
selectedObject = contents{get(handles.Popup_SelectObject,'Value')};
requestedMeasurements = uData.requestedMeasurements;
requestedFieldMapping = uData.requestedFieldMapping;
requestedFieldList = uData.requestedFieldList; %This it the outputFieldList here already if we had dynamic columns.
filter = uData.filters;
filterNames = uData.filterNameList;
aggregateFields = uData.aggregateFields;
outputDir = get(handles.Edit_OutputPath,'String');
singleFileBool = get(handles.Checkbox_SingleFile,'Value');
separateFileBool = get(handles.Checkbox_SeparateFile,'Value');
averageByTreatmentBool = uData.byTreatment.bool;
averageByTreatmentStruct = uData.byTreatment.struct;
includeHeadersBool = get(handles.checkbox_Headers,'Value');
emptyLine0Bool = get(handles.checkbox_empty0,'Value');
contents = cellstr(get(handles.popup_extention,'String'));
fileExt = contents{get(handles.popup_extention,'Value')};
parallelBool = get(handles.checkbox_parallel,'Value');


%postProcessScript = get(handles.Edit_PostProcessScript,'String');

data = generateReport(data,selectedImageIndices,selectedObject,...
    requestedMeasurements,requestedFieldMapping,requestedFieldList,...
    filter,filterNames,...
    aggregateFields,outputDir,singleFileBool,separateFileBool,averageByTreatmentBool,averageByTreatmentStruct,...
    includeHeadersBool,emptyLine0Bool,fileExt,parallelBool...
    );

uData.ECP = data;
set(handles.figure1,'UserData',uData);


function Edit_OutputPath_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_OutputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Edit_OutputPath as text
%        str2double(get(hObject,'String')) returns contents of Edit_OutputPath as a double


% --- Executes during object creation, after setting all properties.
function Edit_OutputPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_OutputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Button_BrowseOutputDir.
function Button_BrowseOutputDir_Callback(hObject, eventdata, handles)
% hObject    handle to Button_BrowseOutputDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
folder = uigetdir(uData.ECP.getWorkFolder,'Specify a folder for the output');
if ~(isempty(folder) || (length(folder)==1 && folder == 0))
    set(handles.Edit_OutputPath,'String',folder);
end

% --- Executes on button press in Button_MoveUp.
function Button_MoveUp_Callback(~, ~, handles)
% hObject    handle to Button_MoveUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
selectedIndices = get(handles.Listbox_Features,'value');
selectedIndices = retainMeasurementIndices(selectedIndices,uData);
if min(selectedIndices)>1
    moveInFeatureList(handles,selectedIndices,-1);
    set(handles.Listbox_Features,'value',selectedIndices-1);
end


% --- Executes on button press in Button_MoveDown.
function Button_MoveDown_Callback(~, ~, handles)
% hObject    handle to Button_MoveDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
selectedIndices = get(handles.Listbox_Features,'value');
selectedIndices = retainMeasurementIndices(selectedIndices,uData);

if max(selectedIndices)<length(uData.requestedFieldList)
    moveInFeatureList(handles,selectedIndices,+1);
    set(handles.Listbox_Features,'value',selectedIndices+1);
end


function updateAggregateColors(handles)
    uData = get(handles.figure1,'UserData');
        currentList = [uData.requestedFieldList, uData.filterNameList];
    aggregateFields = uData.aggregateFields;
    if ~isempty(aggregateFields)
        for i=convert2RowVector(aggregateFields)
            currentList{i} = [ '<HTML><BODY color = #' reshape(dec2hex([0 1 0].*255,2)',1,6) '>' currentList{i}];
        end
    end
    set(handles.Listbox_Features,'String',currentList);
    
function drawFeatureList(handles)
    uData = get(handles.figure1,'UserData');    
    set(handles.Listbox_Features,'Value',min(1,length(uData.requestedFieldList)));
    set(handles.Listbox_Features,'Max',length(uData.requestedFieldList)+length(uData.filterNameList));
    configureSetup(handles);
    updateAggregateColors(handles);
   
function [measurementIndices,filterIndices] = retainMeasurementIndices(indices,uData)    
    measurementIndices = indices(indices<=length(uData.requestedFieldList));
    filterIndices = indices(indices>length(uData.requestedFieldList));
    
function uData = deleteFromFeatures(uData,indicesToDelete)
    for j=length(indicesToDelete):-1:1
        i = indicesToDelete(j);
        uData.requestedFieldList(i) = [];
        uData.requestedFieldMapping(i,:) = [];
        uData = deleteFromAggregateFields(uData,i);
    end
    
    %if all fields from a measurement are deleted then delete the full measurement
    for i=length(uData.requestedMeasurements):-1:1
        if ~any(uData.requestedFieldMapping(:,1) == i)
            uData.requestedMeasurements(i) = [];
            uData.requestedFieldMapping(uData.requestedFieldMapping(:,1)>i,1) = uData.requestedFieldMapping(uData.requestedFieldMapping(:,1)>i,1)-1;
        end
    end
    
    %check and delete also the corresponding filters
    %here those measurements are already deleted which are in the indices
    %to delete
    for j = length(uData.filters):-1:1
        if any(ismember(uData.filters{j}.index,indicesToDelete))
            uData = deleteFromFilters(uData,j+length(uData.requestedFieldList));
        end
    end
    
    %finally update the remaining filters indices to be correct
    for j=1:length(uData.filters)
        %subtract from the index entry the number of preceding deleted indices
        for i=1:length(uData.filters{j}.index)
            uData.filters{j}.index(i) = uData.filters{j}.index(i) - sum(indicesToDelete < uData.filters{j}.index(i)); 
        end
        uData.filterNameList{j} = transformFilterCondition(uData.filters{j});
    end
        
function uData = deleteFromFilters(uData,indicesToDelete)
    indicesWithinFilters = indicesToDelete - length(uData.requestedFieldList);
    for j=length(indicesWithinFilters):-1:1
        i = indicesWithinFilters(j);
        iOld = indicesToDelete(j);
        uData.filters(i) = [];
        uData.filterNameList(i) = [];
        uData = deleteFromAggregateFields(uData,iOld);
    end
    
%to delete only one index from aggregate fields
function uData = deleteFromAggregateFields(uData,i)
    uData.aggregateFields(uData.aggregateFields == i) = [];
    uData.aggregateFields(uData.aggregateFields>i) = uData.aggregateFields(uData.aggregateFields>i)-1;                   
    
    
function moveInFeatureList(handles,selectedIndices,direction)
%handles for GUI elements
% selectedIndices: which features to move
% direction: +1 or -1

uData = get(handles.figure1,'UserData');

newRequestedFieldList = uData.requestedFieldList;
newRequestedFieldMapping = uData.requestedFieldMapping;
newAggregateFields = uData.aggregateFields;
newFilters = uData.filters;

currentMovedObject = 1;
notMovedElements = setdiff(1:length(uData.requestedFieldList),selectedIndices);
nextNotMovedElement = 1;
for i=1:length(uData.requestedFieldList)
    if (currentMovedObject <= length(selectedIndices) && (selectedIndices(currentMovedObject)+direction == i) )
        oldIndex = selectedIndices(currentMovedObject);
        newRequestedFieldList{i} = uData.requestedFieldList{oldIndex};        
        newRequestedFieldMapping(i,:) = uData.requestedFieldMapping(oldIndex,:);
        currentMovedObject = currentMovedObject + 1;
    else        
        oldIndex = notMovedElements(nextNotMovedElement);
        newRequestedFieldList{i} = uData.requestedFieldList{oldIndex};        
        newRequestedFieldMapping(i,:) = uData.requestedFieldMapping(oldIndex,:);
        nextNotMovedElement = nextNotMovedElement+1;
    end
    loc = find(oldIndex == uData.aggregateFields);
    if ~isempty(loc)
        newAggregateFields(loc) = i;
    end
    
    %update filter indices
    for j=1:length(uData.filters)
        if (ismember(oldIndex,uData.filters{j}.index))
            newFilters{j}.index( uData.filters{j}.index == oldIndex ) = i;
        end        
    end

end
uData.requestedFieldList = newRequestedFieldList;
uData.requestedFieldMapping = newRequestedFieldMapping;
uData.aggregateFields = newAggregateFields;
uData.filters = newFilters;

for j=1:length(uData.filters)
    uData.filterNameList{j} = transformFilterCondition(uData.filters{j});
end
set(handles.figure1,'UserData',uData);
drawFeatureList(handles);

function transformed = transformFilterCondition(filter)
%the input is one structure of which condition field is translated
%according to the index array.

transformed = [filter.condition ' ? ' filter.true ' : ' filter.false];
for i=1:length(filter.index)
    transformed = strrep(transformed,['#', num2str(i) ,'#'],['%',num2str(filter.index(i)),'%']);
end
transformed = strrep(transformed,'%','#');


function uData = loadAspectToUData(uData,aspect)
    fn = fieldnames(aspect);
    for i=1:length(fn)
        uData.(fn{i}) = aspect.(fn{i});
    end
    
    
function configureSetup(handles)
%This function handles the on/off state of the advanced dynamic column mode
uData = get(handles.figure1,'UserData');

dynamicFields = uData.requestedFieldMapping(:,3);

if any(dynamicFields)
    if strcmp(get(handles.Checkbox_ConfigColumns,'Enable'),'off')
        setConfigureMode(0,handles);
        set(handles.Checkbox_ConfigColumns,'Enable','on');
    end
else
    setConfigureMode(2,handles);
    set(handles.Checkbox_ConfigColumns,'Enable','off');
end
        

% --------------------------------------------------------------------
function Pushtool_Save_ClickedCallback(~, ~, handles) %#ok<DEFNU> called from GUI
% hObject    handle to Pushtool_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[SaveFile, SavePath] = uiputfile('*.fig','Save object analyser project as');
if ~isequal(SaveFile,0)
    savefig(handles.figure1,fullfile(SavePath,SaveFile));
end


% --------------------------------------------------------------------
function Pushtool_Open_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Pushtool_Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileOpen,pathOpen] = uigetfile('*.fig','Select saved project');
if ~isequal(fileOpen,0)
    figure1_CloseRequestFcn(handles.figure1, eventdata, handles);
    openfig(fullfile(pathOpen,fileOpen));
end

function figure1_HideRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Project;

if ~isempty(Project)
    Project.figHandles.ModuleFigHandles.button_OA.Value = 0;
    ModuleHandler_GUI('button_IA_Callback',Project.figHandles.ModuleFigHandles.button_OA,[],Project.figHandles.ModuleFigHandles);
else
    hObject.Visible = 'off';
end



% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in Checkbox_SingleFile.
function Checkbox_SingleFile_Callback(hObject, eventdata, handles)
% hObject    handle to Checkbox_SingleFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Checkbox_SingleFile


% --- Executes on button press in Checkbox_SeparateFile.
function Checkbox_SeparateFile_Callback(hObject, eventdata, handles)
% hObject    handle to Checkbox_SeparateFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Checkbox_SeparateFile


% --- Executes on button press in Button_AddFilters.
function Button_AddFilters_Callback(hObject, eventdata, handles)
% hObject    handle to Button_AddFilters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
selectedIndices = get(handles.Listbox_Features,'Value');
allElements = get(handles.Listbox_Features,'String');
selectedElements = allElements(selectedIndices);

if ~isempty(selectedIndices) && max(selectedIndices)<=length(uData.requestedFieldList)
    
    filter = AddFilter_GUI(selectedIndices,length(uData.requestedFieldList),selectedElements,1);  

    if ~isempty(filter)
        uData.filterNameList{end+1} = transformFilterCondition(filter);
        uData.filters{end+1} = filter;
    end
end

set(handles.figure1,'UserData',uData);

drawFeatureList(handles);

function Edit_PostProcessScript_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_PostProcessScript (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Edit_PostProcessScript as text
%        str2double(get(hObject,'String')) returns contents of Edit_PostProcessScript as a double


% --- Executes during object creation, after setting all properties.
function Edit_PostProcessScript_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_PostProcessScript (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Button_BrowsePostProcess.
function Button_BrowsePostProcess_Callback(hObject, eventdata, handles)
% hObject    handle to Button_BrowsePostProcess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileName,folder] = uigetfile(pwd,'Specify the postprocess function');
if ~(isempty(folder) || (length(folder)==1 && folder == 0))
    addpath(folder);
    set(handles.Edit_PostProcessScript,'String',fileName);
end


% --------------------------------------------------------------------
function Pushtool_SaveECP_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Pushtool_SaveECP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
ECP = uData.ECP; %#ok<NASGU>Used in the saveVarScript

[SaveFile, SavePath] = uiputfile('*.mat','Save data as'); %#ok<ASGLU> Used in he saveVarScript

if ~isequal(SaveFile,0)
    varToSave = 'ECP'; %#ok<NASGU>Used in the saveVarScript
    saveVarScript;
end


% --------------------------------------------------------------------
function Pushtool_SaveAspect_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Pushtool_SaveAspect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');

aspect = rmfield(uData,'ECP'); %#ok<NASGU> used in the saveVarScript. Delete the data

[SaveFile, SavePath] = uiputfile([handles.figure1.UserData.ECP.getWorkFolder() '\*.mat'],'Save aspect as'); %#ok<ASGLU> Used in he saveVarScript

if ~isequal(SaveFile,0)
    varToSave = 'aspect'; %#ok<NASGU>Used in the saveVarScript
    saveVarScript;
end


% --------------------------------------------------------------------
function Pushtool_LoadAspect_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Pushtool_LoadAspect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileOpen,pathOpen] = uigetfile([handles.figure1.UserData.ECP.getWorkFolder() '\*.mat'],'Select saved aspect');
uData = get(handles.figure1,'UserData');
if ~isequal(fileOpen,0)
    load(fullfile(pathOpen,fileOpen));
    uData = loadAspectToUData(uData,aspect);
end

set(handles.figure1,'UserData',uData);
drawFeatureList(handles);


% --------------------------------------------------------------------
function Pushtool_LoadECP_ClickedCallback(~, ~, handles) %#ok<*DEFNU> Called in GUI
% hObject    handle to Pushtool_LoadECP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileOpen,pathOpen] = uigetfile('*.mat','Select data');
uData = get(handles.figure1,'UserData');
if ~isequal(fileOpen,0)
    load(fullfile(pathOpen,fileOpen));
    [ok,ECP] = checkFolders(ECP);  %#ok<NODEF> Comes from load
    uData.ECP = ECP;
end

if ok
    fillGUIwithData(uData.ECP,handles);

    set(handles.figure1,'UserData',uData);
    drawFeatureList(handles);
end


% --- Executes on button press in checkbox_FixRelatedMeasurements.
function checkbox_FixRelatedMeasurements_Callback(hObject, ~, handles)
% hObject    handle to checkbox_FixRelatedMeasurements (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
    set(handles.Popup_SelectObject,'Enable','off');
else
    set(handles.Popup_SelectObject,'Enable','on');
end


% --- Executes on button press in Checkbox_ConfigColumns.
function Checkbox_ConfigColumns_Callback(hObject, eventdata, handles)
% hObject    handle to Checkbox_ConfigColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Checkbox_ConfigColumns

uData = get(handles.figure1,'UserData');
requestedFieldList = uData.requestedFieldList;
requestedMeasurements = uData.requestedMeasurements;
requestedFieldMapping = uData.requestedFieldMapping;
data = uData.ECP;
selectedImageIndices = get(handles.Listbox_SelectImage,'Value');

if get(hObject,'Value')
    %disable any further change in modification of image set and move of
    %features
    setConfigureMode(1,handles);
    
    %configure modules for this image set
    h = waitbar(0,'Configuring dynamic columns...');
    try
        for i=1:length(requestedFieldList)    
            if ishandle(h), waitbar((i-1)/length(requestedFieldList),h,['Configuring dynamic columns on field: ' num2str(i) '...']); end
            data = requestedMeasurements{requestedFieldMapping(i,1)}.configureDynamicColumns(data,selectedImageIndices,requestedFieldMapping(i,2));
            if ishandle(h), waitbar(i/length(requestedFieldList),h,['Configuring dynamic columns on field: ' num2str(i) '...']); end
        end
    catch ME        
        if strcmp(ME.identifier,'Settings_GUI:noParameterProvided')
            %rollback config mode
            setConfigureMode(0,handles); 
            set(handles.Checkbox_ConfigColumns,'Value',0);
            if ishandle(h), close(h); end
            return;
        else
            rethrow(ME);
        end
    end
    
    if ishandle(h), close(h); end

    outputFieldList = cell(0);
    for i=1:length(requestedFieldList)    
        currentFeatureNames = requestedMeasurements{requestedFieldMapping(i,1)}.resolveDynamicFeatures(requestedFieldMapping(i,2));
        outputFieldList = [outputFieldList currentFeatureNames]; %#ok<AGROW> TODO: calculate beforehand if turns out to be slow    
    end
    
    uData.oldRequestedFieldList = requestedFieldList;    
    uData.requestedFieldList = outputFieldList;
    
else
    setConfigureMode(0,handles);
    if isfield(uData,'oldRequestedFieldList')
        uData.requestedFieldList = uData.oldRequestedFieldList;
    end
    uData.filters = cell(0);
    uData.filterNameList = cell(0);
    uData.aggregateFields = [];
    
end

set(handles.figure1,'UserData',uData);
drawFeatureList(handles);

% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(~, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

if strcmp(eventdata.Key,'delete') && strcmp(get(handles.Button_RemoveFeature,'Enable'),'on')
    Button_RemoveFeature_Callback([], [], handles);
end


%% Additional functions
%if set is 1 then we set the GUI to configured mode. With 0 we switch back
%to normal mode.
function setConfigureMode(set,handles)
    if set == 0
        setString = 'on';
        unsetString = 'off';
    elseif set == 1
        setString = 'off';
        unsetString = 'on';
    elseif set == 2 %Turn off the configure stuff
        setString = 'on';
        unsetString = 'on';
        handles.Checkbox_ConfigColumns.Enable = 'off';
    end
    handles.Listbox_SelectImage.Enable = setString;
    handles.Button_MoveUp.Enable = setString;
    handles.Button_MoveDown.Enable = setString;
    handles.Button_AddFeature.Enable = setString;
    handles.Button_RemoveFeature.Enable = setString;
    handles.button_SelectAll.Enable = setString;
    handles.button_InvertSelection.Enable = setString;    
    %opposite
    handles.Button_Aggregate.Enable = unsetString;
    handles.Button_disaggregate.Enable = unsetString;
    handles.Button_AddFilters.Enable = unsetString;
    handles.Button_GenerateReport.Enable = unsetString;
    
function fillGUIwithData(ECP,handles)
    imageNameList = getImageNames(ECP);
    %set the images
    set(handles.Listbox_SelectImage,'String',imageNameList);
    %make it possible to select all
    set(handles.Listbox_SelectImage,'Max',length(imageNameList));
    set(handles.Popup_SelectObject,'String',getObjectNames(ECP));
    
function updateObjectAnalyserGUI(~,~,hFig)

%TODO: THERE ARE EXTREME CASES WHEN THIS FILL OUT GUI IS DEFINITELY NOT
%ENOUGH: E.G. WHEN DYNAMIC COLUMNS ARE CONFIGURED AND MEANWHILE THE IMAGES
%WHERE UPDATED. Maybe in configured state the step back should be
%forbidden?
if ishandle(hFig)
    ECP = hFig.UserData.ECP;
    handles = guidata(hFig);
    fillGUIwithData(ECP,handles);
end

       
function [ok,ECP] = checkFolders(ECP)
%returns an indicator if the folders are ok, and the new ECP if the folders
%are modified correctly
ok = 1;
    workFolder = getWorkFolder(ECP);
    while ~(exist(workFolder,'dir') && checkMaskFolders(ECP, workFolder))
        workFolder = uigetdir(pwd,'Please specify the location of the work folder (where the masks are saved)');
        if workFolder
            ECP = setWorkFolder(ECP,workFolder);
        else
            ok = 0;
            return;
        end
    end
    
    channelNames = getChannelDirectories(ECP);
    CellProfilerNames = getCellProfilerRawImageNames(ECP);
    
    for i=1:length(channelNames)        
        while ~exist(channelNames{i},'dir')            
            %here the work folder is definitely ok.
            curDir = uigetdir(getWorkFolder(ECP),['Please specify a new location for the ' CellProfilerNames{i} ' images']);
            if curDir                
                button = questdlg('Do you want to set this directory for all remaining channels?','Do for all','Yes','No','Yes');
                if strcmp(button,'Yes')
                    for j=i:length(channelNames)
                        channelNames{j} = curDir;
                    end                    
                else
                    channelNames{i} = curDir;
                end
            else
                ok = 0;
                return;
            end
        end
    end
        
    ECP.setChannelDirectories(channelNames);
    
    


% --- Executes on button press in button_averageByTreatment.
function button_averageByTreatment_Callback(hObject, eventdata, handles)
% hObject    handle to button_averageByTreatment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
if isfield(uData.byTreatment,'storedGUI') && ~isempty(uData.byTreatment.storedGUI)
    [resBool,resStruct,resStoredGUI,stat] = ByTreatmentOptions_GUI(uData.byTreatment.storedGUI);
    if strcmp(stat,'ok')
        uData.byTreatment.bool = resBool;
        uData.byTreatment.struct = resStruct;
        uData.byTreatment.storedGUI = resStoredGUI;
    end
else
    [uData.byTreatment.bool,uData.byTreatment.struct,uData.byTreatment.storedGUI] = ByTreatmentOptions_GUI();
end
set(handles.figure1,'UserData',uData);


% --- Executes on button press in checkbox_Headers.
function checkbox_Headers_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_Headers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_Headers


% --- Executes on selection change in popup_extention.
function popup_extention_Callback(hObject, eventdata, handles)
% hObject    handle to popup_extention (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_extention contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_extention


% --- Executes during object creation, after setting all properties.
function popup_extention_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_extention (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_empty0.
function checkbox_empty0_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_empty0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_empty0


% --- Executes on button press in checkbox_parallel.
function checkbox_parallel_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_parallel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_parallel

if get(hObject,'Value')
    handles.Checkbox_SingleFile.Value = 0;
    handles.Checkbox_SeparateFile.Value = 1;
end



% --------------------------------------------------------------------
function pushTool_accFormat_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to pushTool_accFormat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.Checkbox_SingleFile.Value = 0;
handles.Checkbox_SeparateFile.Value = 1;
handles.checkbox_Headers.Value = 0;
handles.checkbox_empty0.Value = 1;
handles.popup_extention.Value = 2;

