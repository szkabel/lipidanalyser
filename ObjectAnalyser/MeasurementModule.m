classdef MeasurementModule < handle
    %MeasurementModule Abstract class (interface) for different measurement
    %modules available in ECP and Object analyser.
    %   The measurement modules can make in general measurements on image
    %   regions called objects identified by mask images. The more common
    %   name of measurements in image analysis is features, and the process
    %   of measuring is feature extraction. We can consider them as
    %   properties of the objects. 
    %   Each measurement module can have parameters (such as the intensity
    %   is measured on an image which is a parameter to the module, however
    %   the object mask is always an input to the measure function of the module)
    %
    %   Each measurement module must encode itself in a unique way to a
    %   string which will be the name of the structure for the
    %   measurements. The encoded string must start with the name of the
    %   module and may continue with _ . (underscore separates the module
    %   name from its encoded parameters).  
    %
    %   The main function in each measurement module is the measure
    %   function which actually performs the operations on tha data.
    
    
    properties
        
    end
    
    methods (Abstract)  
        %Each Meas.Module needs a constructor with two parameters. The
        %first is the parameter array the second is the data
        %   INPUT: paramcell: a cellarray whith the neccessary parameters described in getParameters.
        %          data:      the data used in the project (ECP)
                
        %Version number of the given measurement. Used to track if there
        %were changes in the measurements provided by the module, to
        %indicate that recalculation of values should be done.
        % OUTPUT:
        %   versionNumber   a double number
        versionNumber = getCurrentVersion(obj);                                
                
        %Each module can provide various outputs (e.g. intensity
        %measurement can include mean, integrated intensity etc.). This
        %function provides a description for the given measurement.        
        % OUTPUT: 
        %   fieldList       A cellarray with strings.
        %   dynamicFields   A logical array exactly as big as fieldList.
        %                   The ith entry indicates if the ith field is
        %                   really dynamic (i.e. it resolves to more than
        %                   one column).
        [fieldList,dynamicFields] = providedMeasurements(obj);        
                
        %Measure function.
        %   INPUT: 
        %       data    ECP structure
        %       imageID To retrieve the appropriate data we might need to
        %               know on which cycle do we operate currently. (From
        %               which image does the mask image is coming from).
        %               This imageID should identify the image within the
        %               data structure.
        %       objectName Some measurement module might need the
        %               identifier of the object to which to mask is
        %               assigned. This is the name of the object as string.
        %   OUTPUT:
        %       featureStrs This is a cellarray (each entry is a structure
        %       describing a measurement. This is needed as some meas.
        %       modules might require other meas. modules to run). The
        %       structure entries are the following:
        %           Version: the module's own version number see above
        %           Data: the actual features that were measured. An N by k
        %           matrix where N is the number of objects
        %           in the mask. k has exactly the same length as the return
        %           array (fieldList) of providedMeasurements function.
        %           uniqueID: the module's own uniqueID
        %           ObjectName: the name of the object measured (this is
        %           not necessarily the same as the objectName coming
        %           inside)     
        %           imageID: the ID of the image field where the meas was
        %           done
        [featureStrs] = measure(obj,data,imageID,objectName)        
                
        %fetch measurements in the desired format. The measure function
        %does the native measurement which is then must be stored to the
        %database from where it can be still accessed by it's native (load
        %measurement) function. This function wraps around that native
        %function for most of the modules, however it opens the possibility
        %to do this process in another way as well, and produce various
        %aspects of the same data. It enables the handling of dynamic
        %column ranges. (i.e. variable width of the result table)
        %   INPUTS:
        %       data:    ECP structure. (or later if changed other data
        %               structure)
        %       imageID: The unique ID of the image within data
        %       objectName: identifier name of the object
        %       selectedFeature: The index of the feature that we want to
        %                   fetch. The index is in correspondance with the
        %                   output of providedMeasurements function
        %  
        %   OUTPUT:
        %       featureColumns A matrix with the feautures. Number of rows
        %               is the number of objectName objects in imageID
        %               image, width is VARYING according to the output of
        %               resolveDynamicFeatures function's output.        
        featureColumns = fetchBlockMeasurement(obj,data,imageID,objectName,selectedFeature)
                
        %Resolves possible dynamic feature names. If the module produces
        %dynamic features (variable length feature outputs such as
        %distributions) then after determining the dynamic feature size the
        %new featureNames are provided here. Before calling this function
        %it is required to call the configureDynamicColumns.
        %   INPUT:
        %       selectedFeature: Index of the required resolve. Corresponds
        %           to the output of the providedMeasurements
        %   OUTPUT:
        %       featureNames: cellarray
        featureNames = resolveDynamicFeatures(obj,selectedFeature);        
                
        %Configure the dynamicColumns for the given image set. This is used
        %to determine the width of the dynamic features. Meanwhile one
        %might need to perform some measurements that can be restored to
        %data that's why data is given back.
        %   INPUTS:
        %       data:   Data structure
        %       imageSet: an array of indices (corresponding to data's
        %               getImageNames function)
        %       selectedFeature: Index of the required feature. Based on
        %               that the module should determine if it needs to
        %               configure anything.
        %       selectedObject: the name of the selected object in the
        %               current run.
        %   OUTPUT: 
        %       data:   Data structure for reusing calc.
        data = configureDynamicColumns(obj,data,imageSet,selectedFeature);        
        
        %Provides a unique ID for this module including its parameters
        %based on which it is possible to recognize the measurements of
        %this module.
        %   OUTPUT:
        %       id  the unique ID string. It is the module name before the
        %       first underscore later on it contains the parameters if
        %       there is any.
        id = uniqueID(obj)        
                
    end
    
    methods
        %A function which creates a structure from the actual features for
        %easier handling
        function featureStrs = createFeatureStructure(obj,features,objectName,imageID)        
            featureStrs = {struct(...
                'Version',obj.getCurrentVersion(),...
                'Data',features,...
                'uniqueID',obj.uniqueID(),...
                'ObjectName',objectName,...
                'imageID',imageID....
                    )};
        end
    end
    
    methods (Abstract, Static)        
        
        %Each measurement method might need some parameters. This function
        %must give back the description of all the necessary parameters.
        % INPUT:
        %   data        So far ECP structure, but we'll use only functions
        %               to access it so that later on it can be changed to
        %               data class.
        % OUTPUT:
        %   paramarray: see generateUIControls function in ACC/Utils
        %   checkVisibility: the function handle if needed for dynamic
        %               input field visualization. Otherwise please use the
        %               defaultCheckVisibility function handle with true
        %               bool value (e.g. @(a,b)(defaultCheckVisibility(a,b,true))
        [paramarray,checkVisibility] = getParameters(data)
    end
    
    methods (Static)
        %For stability reasons the modules must be able to verify that a
        %given parameter list is valid or not. This function is only
        %static and provides a general true implementation but this can be
        %(and suggested to be) overwritten by the specific measurement
        %modules.
        % INPUT:
        %   paramcell   A cellarray corresponding to getParameters.
        %
        % OUTPUT:
        %   bool        Boolean value indicating if the parameters are
        %               CORRECT or not.
        %   msg         If the parameters are not correct then in the msg
        %               string the function should provide information
        %               about the problem
        function [bool,msg] = checkParameters(~)
            bool = 1;
            msg = '';
        end
    end
end

