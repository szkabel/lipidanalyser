function imageBasedAggregations = runSingleImage(...
    imgIdx,data,selectedObject,regularFeatureSize,...
    emptyLineBool,requestedMeasurements,requestedFieldMapping,...
    filters,aggregateFields,fileExt,singleOutputFileName,tgtFolder,treatDir,...
    outputFieldList,byTreatmentOutput,treatmentStructure,...
    singleFileOutput,separateFileOutput)
%Run the measurements completely on a single image

    %% Compute and store all the measurements into the DB    
    imageID = getImageID(data,imgIdx);
    
    for j=1:length(requestedMeasurements)                                
        %This function is the kinda replacement for the old store requested meas
        imgFeatureStrs = data.calculateRequestedMeasurement(requestedMeasurements{j},imageID,selectedObject);
        %And store immediately, if needed. This is the new call type for data classes        
        for k=1:length(imgFeatureStrs)
            data.storeRequestedMeasurement(imgFeatureStrs{k});
        end
    end

    %% Handle empty images and ADD FILTERS
    savedMatrix = {};    
    parents = retrieveParents(data,imageID,selectedObject,selectedObject);

    %handle empty images (as it is not specified what the modules output, therefore avoid putting anything to file)    
    if isempty(parents)
        if emptyLineBool            
            %Put proper amount of 0
            matrixToSave = zeros(1,regularFeatureSize);
        else
            %Default: skip all empty lines
            fprintf('The following image was empty:\n%s\n',imageID);
            imageBasedAggregations = [];
            return;
        end
    else
        %init size
        matrixToSave = zeros(length(parents),regularFeatureSize);      

        %load all the requested fields one by one
        j = 1;           
        for k = 1:size(requestedFieldMapping,1)
            featureColumns = requestedMeasurements{requestedFieldMapping(k,1)}.fetchBlockMeasurement(data,imageID,selectedObject,requestedFieldMapping(k,2));
            matrixToSave(1:size(featureColumns,1),j:j+size(featureColumns,2)-1) = featureColumns;
            j = j + size(featureColumns,2);                
        end
        matrixToSave(size(featureColumns,1)+1:end,:) = []; %delete remaining if needed because of grouping

        %Add filters as last columns
        if ~isempty(filters)
            matrixToSave = addFilter(matrixToSave,filters);    
        end    

    end

    %% Aggregations (within image)
    if ~isempty(aggregateFields)                
        [matrixToSave,nofGroupBy] = aggregateMatrix(matrixToSave,aggregateFields,{'mean','std'});
        matrixToSave(:,end+1) = convert2ColumnVector(nofGroupBy);
    end

    %% Postproces
    %if ~isempty(postprocessFunction)
    %    [~,postprocessFunction,~] = fileparts(postprocessFunction);
    %    [matrixToSave,outputFieldList] = feval(postprocessFunction,matrixToSave,outputFieldList);
    %end        

    %% Write single files to disk
    if singleFileOutput        
        flushData(matrixToSave,outputFieldList,tgtFolder,singleOutputFileName,fileExt,imageID,1);
    end
    if separateFileOutput
        singleTgtFolder = fullfile(tgtFolder,'SingleFiles');
        if ~exist(singleTgtFolder,'dir'), mkdir(singleTgtFolder); end
        flushData(matrixToSave,outputFieldList,singleTgtFolder,imageID,fileExt,[],0);
    end
    %Prepare for saving in case of by treatment
    if ~isempty(byTreatmentOutput)
        savedMatrix = matrixToSave;        
    end
    
    %% Treatment based aggregations
    if ~isempty(byTreatmentOutput)
        imageBasedAggregations.imageID = imageID;  
        if ~isempty(savedMatrix)
            imageBasedAggregations.data = minDataForAggregation(savedMatrix);
            imageBasedAggregations.nofElements = size(savedMatrix,1);
        else
            imageBasedAggregations.data = [];
            imageBasedAggregations.nofElements = 0;
        end            

        %Also flush out by treatment if sort is needed
        if ismember('sort',byTreatmentOutput)
            treatIdx = getTreatmentID(treatmentStructure,imageID);
            for j=1:length(treatIdx)
                flushData(savedMatrix,outputFieldList,treatDir,['.' treatmentStructure{treatIdx(j)}.name],fileExt, [treatmentStructure{treatIdx(j)}.name ' - ' imageID], 1);
            end
        end
    else
        imageBasedAggregations = [];
    end
end

function aggMatrix = minDataForAggregation(data)
    aggMatrix = zeros(4,size(data,2));
    aggMatrix(1,:) = sum(data,1);
    aggMatrix(2,:) = sum(data.^2,1);
    aggMatrix(3,:) = min(data,[],1);
    aggMatrix(4,:) = max(data,[],1);
end

function resArray = getTreatmentID(treatmentStructure,imageID)
    resArray = [];
    for i=1:length(treatmentStructure)        
        for j=1:length(treatmentStructure{i}.regex)
            if regexp(imageID,treatmentStructure{i}.regex{j},'ONCE')                
                resArray = [resArray i];
                break;
            end
        end        
    end    
end

