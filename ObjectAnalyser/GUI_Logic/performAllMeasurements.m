function [allFeatureStrs] = performAllMeasurements(requestedMeasurements,selectedImageIndices,data,h,selectedObject,parBool)
    allFeatureStrs = cell(length(selectedImageIndices),1);

    %% Loop on the selectedImages first time, performing the actual measurement,
    %most time consuming
    nofImages = length(selectedImageIndices);
    if parBool
        if ishandle(h), waitbar(0.1665 ,...
                h,'Parallel measurement run, see terminal for progress');
        end
        parfor i=1:length(selectedImageIndices)
            imageID = getImageID(data,selectedImageIndices(i));    
            perImgFeatureStrs = cell(length(requestedMeasurements),1);    
            %store all the requested measurement
            for j=1:length(requestedMeasurements)                
                disp(['Analysing objects on image ' num2str(i) '(/' num2str(nofImages) ') by module: ' class(requestedMeasurements{j})]);
                %This function is the kinda replacement for the old store requested meas
                perImgFeatureStrs{j} = calculateRequestedMeasurement(data,requestedMeasurements{j},imageID,selectedObject);
            end              
            allFeatureStrs{i} = perImgFeatureStrs;
        end        
    else
        for i=1:length(selectedImageIndices)
            imageID = getImageID(data,selectedImageIndices(i));    
            perImgFeatureStrs = cell(length(requestedMeasurements),1);    
            %store all the requested measurement
            for j=1:length(requestedMeasurements)
                if ishandle(h), waitbar( ((i-1)*length(requestedMeasurements)+j-1)/(length(requestedMeasurements)*nofImages)*0.33 ,...
                        h,['Analysing objects on image ' num2str(i) '(/' num2str(nofImages) ') by module: ' class(requestedMeasurements{j})]); 
                end

                %This function is the kinda replacement for the old store requested meas
                perImgFeatureStrs{j} = calculateRequestedMeasurement(data,requestedMeasurements{j},imageID,selectedObject);
            end              
            allFeatureStrs{i} = perImgFeatureStrs;
        end
    end
end

