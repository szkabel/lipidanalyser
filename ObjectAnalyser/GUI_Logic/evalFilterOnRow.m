function filterValue = evalFilterOnRow(rowData,filter)
%Evaluate a filter on a row and returns the filter value.
%A filter is a structure with 3 fields: condition, true and false, all of
%them are strings, that are evaluated in this script.
%
%If one wish to use variable values during evaluating the strings then they
%can be provided in the rowData input which is a vector. In the string you
%can refer to the ith index of the vector with the string #i#.
    for i=1:length(filter.index)
        filter.condition = strrep(filter.condition,['#',num2str(i),'#'],num2str(rowData(filter.index(i))));
        filter.true = strrep(filter.true,['#',num2str(i),'#'],num2str(rowData(filter.index(i))));
        filter.false = strrep(filter.false,['#',num2str(i),'#'],num2str(rowData(filter.index(i))));
    end
    if (eval(filter.condition))
        filterValue = eval(filter.true);
    else
        filterValue = eval(filter.false);
    end
end