function [groupedMatrix,nofGroupedObjects,outputFieldList] = aggregateMatrix(origMatrix,groupby,aggregateFuncs,outputFieldList)
%very simple and straight-forward not optimized implementation for group-by
%operation.
%Origmatrix is the matrix. Groupby is an array of indices to use as key for
%the aggregation
%It produces a bigger grouped matrix, as it expands all the non-key columns
%with the functions listed in aggregateFuncs.
%aggregateFuncs is a cellarray of functions that compute a single value
%from a set of numbers such as: mean, std, max, min, median etc. If the
%function is provided with a matrix it should operate over the first
%dimension (as all the examples do).
%If given an outputFieldList it also updates those with appropriate
%postfixes
%It sorts the results according to the first group by key

%sort by the first key
    [~,idx] = sort(origMatrix(1:end,groupby(1)));
    origMatrix = origMatrix(idx,:);
    
    nofKeys = length(groupby);
    nofFunc = length(aggregateFuncs);
    
    newWidth = nofKeys+nofFunc*(size(origMatrix,2)-nofKeys);    
    
    %Expansion calculations
    newKeyIndices = zeros(1,nofKeys);
    newOtherIndices = zeros(1,newWidth-nofKeys);    
    if nargin>3
        newOutputFieldList = cell(1,newWidth);
    end
    k = 1;
    keyCounter = 1;
    otherCounter = 1;
    for i=1:size(origMatrix,2)
        if ismember(i,groupby)           
            if nargin>3
                newOutputFieldList{k} = [outputFieldList{i} '_AGGREGATE_KEY'];
            end
            newKeyIndices(keyCounter) = k;
            keyCounter = keyCounter + 1;
            k = k+1;
        else
            if nargin>3
                for j = 1:nofFunc
                    newOutputFieldList{k+j-1} = [outputFieldList{i} '_' aggregateFuncs{j}];
                end
            end
            newOtherIndices(otherCounter:otherCounter+nofFunc-1) = k:k+nofFunc-1;
            otherCounter = otherCounter + nofFunc;
            k = k + nofFunc;
        end
    end
    if nargin>3
        outputFieldList = newOutputFieldList;   
    end
    
    %Matrix aggregation
    [ groupedMatrix,nofGroupedObjects ] = groupMatrix( origMatrix,groupby, aggregateFuncs,newOtherIndices,newKeyIndices);
    
end

