function data = generateReport(data,selectedImageIndices,selectedObject,...
    requestedMeasurements,requestedFieldMapping,outputFieldList,...
    filters,filterNames,...
    aggregateFields,...
    outputDir,singleFileOutput,separateFileOutput,byTreatmentOutput,treatmentStructure,...
    includeHeadersBool,emptyLineBool,fileExt,parallelBool)
%
% Who knows?
%
% Generate report is the main function that controls the flow of generating
% report, and measuring the required properties of the objects. The
% function first runs all the measurements modules that are needed to put
% together the final file (each unique measurement run only once) and
% stores the results in data. (storeRequestedMeasurement). Then by the
% provided mapping it extracts the now available data, does the
% postprocessing if needed (aggregation) and flushes it out in the requested
% format.
%
% INPUTS:
%   data        This object represents our database. Currently it is
%               implemented as a structure (ECP) but to maintain
%               portability the data input variable can be only manipulated
%               with functions. However concrete interface does not exist
%               yet for this 'class'.
%   selectedImageIndices: Inner format for indexing images of the data
%               class. This is the subset of possible indices given back by
%               the getImageNames function of the data.
%   selectedObject: The name of the selected object type to analyse.
%   requestedMeasurements: Cellarray that contains the modules to be run
%               (or which measurements are requested in this case.)
%   requestedFieldMapping: An n by 2 array. Each row identifies one output
%               field in the report file. The first column identifies the
%               index of the measurement module in the
%               requestedMeasurements cellarray, the second one identifies
%               the index of the feature in this module for this output
%               field.
%   outputFieldList: A cellarray with the requested (preconfigured) field names.
%   filters     A cellarray with structures that describes the filters (pre
%               aggregation filters) to be applied on the data matrix.
%   filterNames A cellarray with the filter names so it can be included in
%               the header.
%   aggregateFields: an array with the field indices among which we
%               aggregate (group) the result file
%   outputDir   The output directory for the analysis files.
%   singleFileOutput: Boolean indicates if you ask for a single file output
%   separateFileOutput: Boolean indicates if you ask for separate files as
%               output
%   byTreatmentOutput: Cellarray with the function names, used for
%               grouping the data by treatment, can be empty.
%   treatmentStructure: The structure describing how to aggregate by
%               treatments. For further documentation see:
%               ByTreatmentOptions_GUI buttonSaveSettings_Callback
%               function.
%   
%   postprocessFunction (Optional - and removed from the script currently) the name of a postprocess function that is
%               run just before flushing the data out. This function must
%               wait for 2 input arguments and output 2 output arguments.
%               (inputs: matrix and headers, outputs postprocessed matrix
%               and headers)
%   includeHeadersBool If it is true, then in each file a header will be
%               written that specifies the meaning of each column. If it is
%               false, then it writes the headers to a separate file called
%               measurementNames.txt
%   emptyLineBool If this is true then a zero line is created for empty
%               images and the complete 0 line stored and calculated for
%               any other merge option like average by treatment etc.
%   fileExt     A string with starting dot specyfing the export format
%   parallelBool A bool indicating to use parallel compupting toolbox if
%              possible. This function doesn't check for PCT it must be
%              done beforehand.
% OUTPUT:
%   data        To save computation time we give back to the GUI the
%               already calculated measurements.

%return if output is not required at all.
if (singleFileOutput + separateFileOutput + length(byTreatmentOutput) == 0)    
    return;
end

if isempty(selectedImageIndices) || isempty(requestedMeasurements)
    return;
end

if parallelBool && singleFileOutput
    warndlg('In case of parallel analysis, the results in the single file may be unreliable.');
end

if ~isempty(byTreatmentOutput)
    if isempty(treatmentStructure)
        byTreatmentOutput = []; % if the regex is badly defined then the file is not created
    end
end

singleOutputFileName = [timeString(5) '_' selectedObject];
tgtFolder = fullfile(outputDir,singleOutputFileName);
mkdir(tgtFolder);

outputFieldList = [outputFieldList,filterNames];

try
    runMeasuring(...
        selectedImageIndices,data,selectedObject,emptyLineBool,includeHeadersBool,outputFieldList,...
        requestedMeasurements,requestedFieldMapping,filters,aggregateFields,...
        singleFileOutput,separateFileOutput,byTreatmentOutput,treatmentStructure,...
        parallelBool,fileExt,singleOutputFileName,...
        tgtFolder);

    msgbox('Object analysis is done!');

catch e
    %TMP error log
    disp(getReport(e));
    
    errordlg(sprintf('Error during data analysis:\n%s',e.message));
end

end