function runMeasuring(...
    selectedImageIndices,data,selectedObject,emptyLineBool,includeHeadersBool,outputFieldList,...
    requestedMeasurements,requestedFieldMapping,filters,aggregateFields,...
    singleFileOutput,separateFileOutput,byTreatmentOutput,treatmentStructure,...
    parallelBool,fileExt,singleOutputFileName,...
    tgtFolder)
    %If this goes well, it's gonna be copied into the main function...
    
    %% Preparation
    
    if ~isempty(aggregateFields)
        matrixToSave = zeros(1,length(outputFieldList));
        [~,~,outputFieldList] = aggregateMatrix(matrixToSave,aggregateFields,{'mean','std'},outputFieldList);
        outputFieldList{end+1} = 'Number of aggregated elements';
    end
    
    %Save the original size before the possible removal
    regularFeatureSize = length(outputFieldList);
    
    %Process for different headers
    if ~includeHeadersBool
        featureNamesFile = fopen(fullfile(tgtFolder,['measurementNames' fileExt]),'w');
        for j=1:length(outputFieldList)
            fprintf(featureNamesFile,'%s\n',outputFieldList{j});            
        end
        fclose(featureNamesFile);
        outputFieldList = [];
    end
    
    %Process for treatment based outputs
    if ~isempty(byTreatmentOutput)
        imageBasedAggregations = cell(length(selectedImageIndices),1);
        treatDir = fullfile(tgtFolder,'TreatmentAggregations');
        if ~exist(treatDir,'dir'), mkdir(treatDir); end                
    else
        treatDir = '';
    end        

    %% Loop over the selectedImages performing AND storing the measurement into the DB    
    nofImages = length(selectedImageIndices);    
    
    global myParallelWaitbarHandle;
    infoStr = 'Analysing objects...';
    clear updateWaitBar; %'0-ing the counter'    
    D = parallel.pool.DataQueue;
    afterEach(D, @updateWaitBar);    
    
    waitHandle = waitbar(0,infoStr);
    myParallelWaitbarHandle = waitHandle;
    
    if parallelBool
        data.prepareForSave();
        maxIter = length(selectedImageIndices);
        parfor i=1:maxIter
            data.afterReOpen(); %#ok<PFBNS>
            imageID = getImageID(data,selectedImageIndices(i));  
            disp(['Analysing objects on image ' num2str(i) '(/' num2str(nofImages) ') - ' imageID]);                                    
                                    
            % Export the single image in the requested format
            % The output is only used for the treatment based aggregations.
            imageBasedAggregations{i} = runSingleImage(selectedImageIndices(i),...
                data,selectedObject,regularFeatureSize,...
                emptyLineBool,requestedMeasurements,requestedFieldMapping,...
                filters,aggregateFields,fileExt,singleOutputFileName,tgtFolder,treatDir,...
                outputFieldList,byTreatmentOutput,treatmentStructure,...
                singleFileOutput,separateFileOutput);
            send(D, struct('str',infoStr,'maxIter',maxIter));
        end
        data.afterReOpen();
    else        
        for i=1:nofImages
            imageID = getImageID(data,selectedImageIndices(i));  
            disp(['Analysing objects on image ' num2str(i) '(/' num2str(nofImages) ') - ' imageID]);
            if ishandle(waitHandle), waitbar( i/nofImages,waitHandle,['Analysing objects on image ' num2str(i) '(/' num2str(nofImages) ')']); end
                                    
            % Export the single image in the requested format
            % The output is only used for the treatment based aggregations.
            imageBasedAggregations{i} = runSingleImage(selectedImageIndices(i),...
                data,selectedObject,regularFeatureSize,...
                emptyLineBool,requestedMeasurements,requestedFieldMapping,...
                filters,aggregateFields,fileExt,singleOutputFileName,tgtFolder,treatDir,...
                outputFieldList,byTreatmentOutput,treatmentStructure,...
                singleFileOutput,separateFileOutput);                                                                    
        end
                
    end
           
    if ishandle(waitHandle)
        waitbar( 1,waitHandle,'Creating treatment based files...');     
    end
    
    %% Final processing by treatment
    if ~isempty(byTreatmentOutput)
        for i = 1:length(imageBasedAggregations)
            if ~isempty(imageBasedAggregations{i})
                treatmentStructure = updateTreatmentBasedStructure(imageBasedAggregations{i},treatmentStructure);
            end
        end

        for i=1:length(byTreatmentOutput)
            flushByTreatment( treatmentStructure,outputFieldList,treatDir,[singleOutputFileName '_byTreatment'], byTreatmentOutput{i},fileExt);
        end
    end
    
    if ishandle(waitHandle), close(waitHandle); end

end

