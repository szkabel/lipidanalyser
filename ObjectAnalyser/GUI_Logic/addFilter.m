function [ filterMatrix ] = addFilter( origMatrix, filters )
%addFilter Adds columns to a matrix with value based on previos columns.
%   To each row of a matrix some new column added based on values in the
%   row. One can specify the new values as conditional statements on
%   previous columns. The condition will be evaluated and according to that
%   one can specify a true and a false related value which the extra column
%   will take.
%   
%   INPUTS:
%       origMatrix  A big original matrix (N x M) only with numbers
%       filters     cellarray with K entry. Each entry is a filter
%                   structure. See filter structures in AddFilterGUI.
%
%   OUTPUT:
%       filterMatrix The extended matrix (N x M+K).

[N,M] = size(origMatrix);
K = length(filters);

filterMatrix = zeros(N,M+K);
filterMatrix(1:N,1:M) = origMatrix;
%by each row
for i=1:N
    for j=1:K
        filterMatrix(i,M+j) = evalFilterOnRow(origMatrix(i,1:M),filters{j});
    end
end

end



