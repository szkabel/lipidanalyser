function  treatmentStructure = updateTreatmentBasedStructure(treatmentAggregation,treatmentStructure)
%updateTreatmentBasedStructure To store the data to the appropriate array
%of the structure
%   matrixToSave The matrix that we will save
%   imageID      To search for the regex in treatmentStructure
%   treatmentStructure cellarray of strctures

imageID = treatmentAggregation.imageID;
matrixToSave = treatmentAggregation.data;

for i=1:length(treatmentStructure)
    found = 0;
    for j=1:length(treatmentStructure{i}.regex)
        if regexp(imageID,treatmentStructure{i}.regex{j},'ONCE')
            found = 1;
            break;
        end
    end
    if found && ~isempty(matrixToSave)
        treatmentStructure{i}.data = cat(3,treatmentStructure{i}.data, matrixToSave);
        treatmentStructure{i}.nofElements = treatmentStructure{i}.nofElements + treatmentAggregation.nofElements;
    end
end


end

