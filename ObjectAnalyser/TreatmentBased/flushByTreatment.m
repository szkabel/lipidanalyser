function flushByTreatment( tS,headers,filePath,fileName, aggregationMethod,fileExt)
%flushByTreatment Flush out the data treatment based that we collected in
%the treatmentStructure
%Aggregation method is a function name like mean, std etc that is used to
%aggregate the treatment based data

if strcmp(aggregationMethod,'sort')        
    %This will be done from disk
    fTarg = fullfile(filePath,[fileName '_' aggregationMethod fileExt]);
    if ~ispc, system(['touch "' fTarg '"']); end
    for i=1:length(tS)
        headerColumn = tS{i}.name;
        fSrc = fullfile(filePath,['.' headerColumn fileExt]);
        if ispc
            if i==1
                system(['type "' fSrc '" > "' fTarg '"']);
            else
                system(['type "' fSrc '" >> "' fTarg '"']);
            end
        else            
            system(['cat "' fSrc '" >> "' fTarg '"']);            
        end
    end
    
else
    
    if ~isempty(tS)
        if ~isempty(headers), headers{end+1} = '# aggregated elements'; end
        i = 1;
        while i<=length(tS) && isempty(tS{i}.data), i = i+1; end
        if i<=length(tS)
            matrixToSave = zeros(1,size(tS{i}.data,2)+1);        
        end
    end

    for i=1:length(tS)    
        if ~isempty(tS{i}.data)
            headerColumn = tS{i}.name;
            N = tS{i}.nofElements;
            sums = sum(tS{i}.data(1,:,:),3);
            ssums = sum(tS{i}.data(2,:,:),3);
            minV = min(tS{i}.data(3,:,:),[],3);
            maxV = max(tS{i}.data(4,:,:),[],3);
            switch aggregationMethod
                case 'mean'
                    matrixToSave(1,1:end-1) = sums./N;
                case 'std'                    
                    matrixToSave(1,1:end-1) = sqrt(ssums./N - (sums./N).^2);
                case 'min'
                    matrixToSave(1,1:end-1) = minV;                    
                case 'max'
                    matrixToSave(1,1:end-1) = maxV;
            end            
            matrixToSave(1,end) = N;
            flushData(matrixToSave,headers,filePath,[fileName '_' aggregationMethod],fileExt, headerColumn, 1);
        end
    end

end

end

