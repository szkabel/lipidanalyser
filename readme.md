# Download

## A) Using git
Install the git version control system for the simplest way to download the software. Download the repository with the following git command:
```
git clone --recurse-submodules https://bitbucket.org/szkabel/lipidanalyser.git
```

## B) Manual download
Download the software code from: https://bitbucket.org/szkabel/lipidanalyser/downloads/ and unpack it.

Download the settings GUI code from: https://github.com/szkabel/matlab_settings_gui/archive/refs/heads/master.zip Unpack this code and place it under the lipid analyzer folder to Utils/matlab_settings_gui.

# Installation

Currently only source version is available, so a Matlab license and the Image Processing Toolbox is required to run the tool. No other separate installation is needed, the Matlab environment is capable of running the tool.
The program was used and tested on versions 2019b - 2023b.

# Startup

1) Run Matlab, and navigate to the downloaded repository.

2) Type `startupLipidAnalyzer` to the command line and hit enter.

