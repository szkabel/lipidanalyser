function handles = putSettingsToCPGUI(Settings,hObject,eventdata,handles)
%Extracted from CP by Abel Szkalisity at 2019.11.19.

try
    [NumberOfModules, MaxNumberVariables] = size(Settings.VariableValues); %#ok Ignore MLint
    if (size(Settings.ModuleNames,2) ~= NumberOfModules)||(size(Settings.NumbersOfVariables,2) ~= NumberOfModules)
        CPerrordlg(['The file ' SettingsPathname SettingsFileName ' is not a valid settings or output file. Settings can be extracted from an output file created when analyzing images with CellProfiler or from a small settings file saved using the "Save Settings" button.']);
        errFlg = 1;
        return
    end
catch
    CPerrordlg(['The file ' SettingsPathname SettingsFileName ' is not a valid settings or output file. Settings can be extracted from an output file created when analyzing images with CellProfiler or from a small settings file saved using the "Save Settings" button.']);
    errFlg = 1;
    return
end

%%% Hide stuff in the background, but keep old values in case of errors.
OldValue = get(handles.ModulePipelineListBox,'Value');
OldString = get(handles.ModulePipelineListBox,'String');
set(handles.ModulePipelineListBox,'Value',1);
set(handles.ModulePipelineListBox,'String','Loading...');
set(get(handles.variablepanel,'children'),'visible','off');
set(handles.slider1,'visible','off');

%%% Check to make sure that the module files can be found and get paths
ModuleNames = Settings.ModuleNames;
Skipped = 0;
for k = 1:NumberOfModules
    if ~isdeployed
        CurrentModuleNamedotm = [char(ModuleNames{k}) '.m'];
        
        %% Smooth.m was changed to SmoothOrEnhance.m since Tophat Filter
        %% was added to the Smooth Module
        if strcmp(CurrentModuleNamedotm,'Smooth.m')
            CurrentModuleNamedotm  = 'SmoothOrEnhance.m'; %% 
            Filename = 'SmoothOrEnhance';
            Pathname = handles.Preferences.DefaultModuleDirectory;
            pause(.1);
            figure(handles.figure1);
            Pathnames{k-Skipped} = Pathname;
            Settings.ModuleNames{k-Skipped} = Filename;
            CPwarndlg('Note: The module ''Smooth'' has been replaced with ''SmoothOrEnhance''.  The settings have been transferred for your convenience')
        end
        
        if exist(CurrentModuleNamedotm,'file')
            Pathnames{k-Skipped} = fileparts(which(CurrentModuleNamedotm)); %#ok Ignore MLint
        else
            %%% If the module.m file is not on the path, it won't be
            %%% found, so ask the user where the modules are.
            Choice = CPquestdlg(['The module ', CurrentModuleNamedotm, ' cannot be found. Either its name has changed or it was moved or deleted. What do you want to do? Note: You can also choose another module to replace ' CurrentModuleNamedotm ' if you select Search Module. It will be loaded with its default settings and you will also be able to see the saved settings of ' CurrentModuleNamedotm '.'],'Module not found','Skip Module','Search Module','Abort','Skip Module');
            switch Choice
                case 'Skip Module'
                    %%% Check if this was the only module in the pipeline or if
                    %%% all previous modules have been skipped too
                    if Skipped+1 == NumberOfModules
                        CPerrordlg('All modules in this pipeline were skipped. Loading will be canceled.','Loading Pipeline Error')
                        Abort = 1;
                    else
                        %%% Remove module info from the settings
                        View = CPquestdlg(['The pipeline will be loaded without ' CurrentModuleNamedotm ', but keep in mind that it might not work properly. Would you like to see the saved settings ' CurrentModuleNamedotm ' had?'], 'Module Skipped', 'Yes', 'No', 'Yes');
                        if strcmp(View,'Yes')
                            FailedModule(handles,Settings.VariableValues(k-Skipped,:),'Sorry, variable descriptions could not be retrieved from this file',CurrentModuleNamedotm,k-Skipped);
                        end
                        %%% Notice that if the skipped module is the one that
                        %%% had the most variables, then the VariableValues
                        %%% will have some empty columns at the end. I guess it
                        %%% doesn't matter, but it could be fixed if necessary.
                        Settings.VariableValues(k-Skipped,:) = [];
                        Settings.VariableInfoTypes(k-Skipped,:) = [];
                        Settings.ModuleNames(k-Skipped) = [];
                        Settings.NumbersOfVariables(k-Skipped) = [];
                        Settings.VariableRevisionNumbers(k-Skipped) = [];
                        Settings.ModuleRevisionNumbers(k-Skipped) = [];
                        Skipped = Skipped+1;
                        Abort = 0;
                    end
                case 'Search Module'
                    %% Why is this 'if' needed?  An outer 'if' has already
                    %% checked for this.  David 2008.02.08
                    if ~isdeployed
                        filter = '*.m';
                    else
                        filter = '*.txt';
                    end
                    [Filename Pathname] = CPuigetfile(filter, ['Find ' CurrentModuleNamedotm ' or Choose Another Module'], handles.Preferences.DefaultModuleDirectory);
                    pause(.1);
                    figure(handles.figure1);
                    if Filename == 0
                        Abort = 1;
                    else
                        Pathnames{k-Skipped} = Pathname;
                        %% Why is this 'if' needed?  An outer 'if' has already
                        %% checked for this.  David 2008.02.08
                        if ~isdeployed
                            Settings.ModuleNames{k-Skipped} = Filename(1:end-2);
                        else
                            Settings.ModuleNames{k-Skipped} = Filename(1:end-4);
                        end
                        Abort = 0;
                    end
                otherwise
                    Abort = 1;
            end
            if Abort
                %%% Restore whatever the user had before attempting to load
                set(handles.ModulePipelineListBox,'String',OldString);
                set(handles.ModulePipelineListBox,'Value',OldValue);
                ModulePipelineListBox_Callback(hObject,[],handles);
                errFlg = 1;
                return
            end
        end
    else
        Pathnames{k-Skipped} = handles.Preferences.DefaultModuleDirectory;
    end
end

%%% Save old settings in case of error
OldValue = get(handles.ModulePipelineListBox,'Value');
OldString = get(handles.ModulePipelineListBox,'String');
OldSettings = handles.Settings;
try
    OldVariableBox = handles.VariableBox;
    OldVariableDescription = handles.VariableDescription;
catch
    OldVariableBox = {};
    OldVariableDescription = {};
end

%%% Update handles structure
handles.Settings.ModuleNames = Settings.ModuleNames;
%Modified by szkabel 2019.01.18.
if isfield(Settings,'ObjectExport')
    handles.Settings.ObjectExport = Settings.ObjectExport;
end
handles.Settings.VariableValues = {};
handles.Settings.VariableInfoTypes = {};
handles.Settings.VariableRevisionNumbers = [];
handles.Settings.ModuleRevisionNumbers = [];
handles.Settings.NumbersOfVariables = [];
handles.VariableBox = {};
handles.VariableDescription = {};
handles.OriginalVariableDescriptionPositions = {};
handles.OriginalVariableEditPositions = {};

%%% For each module, extract its settings and check if they seem alright
revisionConfirm = 0;
Skipped = 0;
for ModuleNum=1:length(handles.Settings.ModuleNames)
    CurrentModuleName = handles.Settings.ModuleNames{ModuleNum-Skipped};
    %%% Replace names of modules whose name changed
    if strcmp('CreateBatchScripts',CurrentModuleName) || strcmp('CreateClusterFiles',CurrentModuleName)
        handles.Settings.ModuleNames(ModuleNum-Skipped) = {'CreateBatchFiles'};
    elseif strcmp('WriteSQLFiles',CurrentModuleName)
        handles.Settings.ModuleNames(ModuleNum-Skipped) = {'ExportToDatabase'};
    end
    
    %%% Load the module's settings

    try
        %%% First load the module with its default settings
        [defVariableValues defVariableInfoTypes defDescriptions handles.Settings.NumbersOfVariables(ModuleNum-Skipped) DefVarRevNum ModuleRevNum] = LoadSettings_Helper(Pathnames{ModuleNum-Skipped}, CurrentModuleName);
        %%% If no VariableRevisionNumber was extracted, default it to 0
        if isfield(Settings,'VariableRevisionNumbers')
            SavedVarRevNum = Settings.VariableRevisionNumbers(ModuleNum-Skipped);
        else
            SavedVarRevNum = 0;
        end
        
        %%% Adjust old 'LoadImages' variables to new ones. This is applied to 
        %%% the pipelines saved with the LoadImages variable revision number less than 2
        %%% VariableValues is a cell structure, please use {} rather than ().
        if strcmp('LoadImages',CurrentModuleName) && (SavedVarRevNum < 2)
            ImageOrMovie = Settings.VariableValues{ModuleNum-Skipped,11};
            if strcmp(ImageOrMovie,'Image')
                new_variablevalue = 'individual images';
            else
                if strcmp(Settings.VariableValues{ModuleNum-Skipped,12},'avi')
                    new_variablevalue = 'avi movies';
                elseif strcmp(Settings.VariableValues{ModuleNum-Skipped,12},'stk')
                    new_variablevalue = 'stk movies';
                end
            end
            Settings.VariableValues{ModuleNum-Skipped,11} = new_variablevalue;
            Settings.VariableValues{ModuleNum-Skipped,12} = Settings.VariableValues{ModuleNum-Skipped,13};
            Settings.VariableValues{ModuleNum-Skipped,13} = Settings.VariableValues{ModuleNum-Skipped,14};   
            SavedVarRevNum = 2;
        end

        %%% Using the VariableRevisionNumber and the number of variables,
        %%% check if the loaded module and the module the user is trying to
        %%% load is the same
        if SavedVarRevNum == DefVarRevNum && handles.Settings.NumbersOfVariables(ModuleNum-Skipped) == Settings.NumbersOfVariables(ModuleNum-Skipped)
            %%% If so, replace the default settings with the saved ones            
            handles.Settings.VariableValues(ModuleNum-Skipped,1:Settings.NumbersOfVariables(ModuleNum-Skipped)) = Settings.VariableValues(ModuleNum-Skipped,1:Settings.NumbersOfVariables(ModuleNum-Skipped));
            %%% save module revision number
            handles.Settings.ModuleRevisionNumbers(ModuleNum-Skipped) = ModuleRevNum;
        else
            %%% If not, show the saved settings. Note: This will always
            %%% appear if user selects another module when they search for
            %%% the missing module, but the user is appropriately warned
            savedVariableValues = Settings.VariableValues(ModuleNum-Skipped,1:Settings.NumbersOfVariables(ModuleNum-Skipped));
            FailedModule(handles, savedVariableValues, defDescriptions, char(handles.Settings.ModuleNames(ModuleNum-Skipped)),ModuleNum-Skipped);
            %%% Go over each variable
            for k = 1:handles.Settings.NumbersOfVariables(ModuleNum-Skipped)
                if strcmp(defVariableValues(k),'Pipeline Value')
                    %%% Create FixList, which will later be used to replace
                    %%% pipeline-dependent variable values in the loaded modules
                    handles.Settings.VariableValues(ModuleNum-Skipped,k) = {''};
                    if exist('FixList','var')
                        FixList(end+1,1) = ModuleNum-Skipped;
                        FixList(end,2) = k;
                    else
                        FixList(1,1) = ModuleNum-Skipped;
                        FixList(1,2) = k;
                    end
                else
                    %%% If no need to change, save the default loaded variables
                    handles.Settings.VariableValues(ModuleNum-Skipped,k) = defVariableValues(k);
                end
            end
            %%% Save the infotypes and VariableRevisionNumber
             handles.Settings.VariableInfoTypes(ModuleNum-Skipped,1:numel(defVariableInfoTypes)) = defVariableInfoTypes;
             handles.Settings.VariableRevisionNumbers(ModuleNum-Skipped) = DefVarRevNum;
             handles.Settings.ModuleNames{ModuleNum-Skipped} = CurrentModuleName;
             handles.Settings.ModuleRevisionNumbers(ModuleNum-Skipped) = ModuleRevNum;
            revisionConfirm = 1;
        end
        clear defVariableInfoTypes;
    catch e
        disp(getReport(e))
        %%% It is very unlikely to get here, because this means the
        %%% pathname was incorrect, but we had checked this before
        Choice = CPquestdlg(['The ' CurrentModuleName ' module could not be found in the directory specified or an error occured while extracting its variable settings. This error is not common; the module might be corrupt or, if running on the non-developers version of CellProfiler, your preferences may not be set properly. To check your preferences, click on File >> Set Preferences.  The module will be skipped and the rest of the pipeline will be loaded. Would you like to see the module''s saved settings? (' lasterr ')'],'Error','Yes','No','Abort','Yes');
        switch Choice
            case 'Yes'
                FailedModule(handles,Settings.VariableValues(ModuleNum-Skipped,:),'Sorry, variable descriptions could not be retrieved from this file',CurrentModuleName,ModuleNum-Skipped);
                Abort = 0;
            case 'No'
                Abort = 0;
            otherwise
                Abort = 1;
        end
        if Skipped+1 == length(handles.Settings.ModuleNames)
            CPerrordlg('All modules in this pipeline were skipped. Loading will be canceled.  Your preferences may not be set correctly.  Click File >> Set Preferences to be sure that the module path is correct.','Loading Pipeline Error')
            Abort = 1;
        else
            %%% Remove module info from the settings and handles
            handles.Settings.ModuleNames(ModuleNum-Skipped) = [];
            Pathnames(ModuleNum-Skipped) = [];
            Settings.VariableValues(ModuleNum-Skipped,:) = [];
            Settings.VariableInfoTypes(ModuleNum-Skipped,:) = [];
            Settings.ModuleNames(ModuleNum-Skipped) = [];
            Settings.NumbersOfVariables(ModuleNum-Skipped) = [];
            try Settings.VariableRevisionNumbers(ModuleNum-Skipped) = []; end
            try Settings.ModuleRevisionNumbers(ModuleNum-Skipped) = []; end
            Skipped = Skipped+1;
        end
        if Abort
            %%% Reset initial handles settings
            handles.Settings = OldSettings;
            handles.VariableBox = OldVariableBox;
            handles.VariableDescription = OldVariableDescription;
            set(handles.ModulePipelineListBox,'String',OldString);
            set(handles.ModulePipelineListBox,'Value',OldValue);
            guidata(hObject,handles);
            ModulePipelineListBox_Callback(hObject,[],handles);
            errFlg = 1;
            return
        end
    end
end

delete(get(handles.variablepanel,'children'));
try
    handles.Settings.PixelSize = Settings.PixelSize;
    handles.Preferences.PixelSize = Settings.PixelSize;
    set(handles.PixelSizeEditBox,'String',handles.Preferences.PixelSize)
end
handles.Current.NumberOfModules = 0;
contents = handles.Settings.ModuleNames;
guidata(hObject,handles);

WaitBarHandle = CPwaitbar(0,'Loading Pipeline...');
for i=1:length(handles.Settings.ModuleNames)
    if isdeployed
        PutModuleInListBox([contents{i} '.txt'], Pathnames{i}, handles, 1);
    else
        PutModuleInListBox([contents{i} '.m'], Pathnames{i}, handles, 1);
    end
    handles=guidata(handles.figure1);
    handles.Current.NumberOfModules = i;
    CPwaitbar(i/length(handles.Settings.ModuleNames),WaitBarHandle,'Loading Pipeline...');
end

if exist('FixList','var')
    for k = 1:size(FixList,1)
        PipeList = get(handles.VariableBox{FixList(k,1)}(FixList(k,2)),'string');
        FirstValue = PipeList(1);
        handles.Settings.VariableValues(FixList(k,1),FixList(k,2)) = FirstValue;
    end
end

guidata(hObject,handles);
set(handles.ModulePipelineListBox,'String',contents);
set(handles.ModulePipelineListBox,'Value',1);
eventdata = [];
ModulePipelineListBox_Callback(hObject, eventdata, handles);
close(WaitBarHandle);

%%% If the user loaded settings from an output file, prompt them to
%%% save it as a separate Settings file for future use.
if exist('LoadedSettings','var') && isfield(LoadedSettings, 'handles')
    Answer = CPquestdlg('The settings have been extracted from the output file you selected.  Would you also like to save these settings in a separate, smaller, settings-only file?','','Yes','No','Yes');
    if strcmp(Answer, 'Yes') == 1
        tempSettings = handles.Settings;
        if(revisionConfirm == 1)
            VersionAnswer = CPquestdlg('How should the settings file be saved?', 'Save Settings File', 'Exactly as found in output', 'As Loaded into CellProfiler window', 'Exactly as found in output');
            if strcmp(VersionAnswer, 'Exactly as found in output')
                handles.Settings = Settings;
            end
        end
        SavePipeline_Callback(hObject, eventdata, handles);
        handles.Settings = tempSettings;
    end
end

%%% SUBFUNCTION %%%
function [VariableValues VariableInfoTypes VariableDescriptions NumbersOfVariables VarRevNum ModuleRevNum] = LoadSettings_Helper(Pathname, ModuleName)

VariableValues = {[]};
VariableInfoTypes = {[]};
VariableDescriptions = {[]};
VarRevNum = 0;
ModuleRevNum = 0;
NumbersOfVariables = 0;
if isdeployed
    ModuleNamedotm = [ModuleName '.txt'];
else
    ModuleNamedotm = [ModuleName '.m'];
end
fid=fopen(fullfile(Pathname,ModuleNamedotm));
while 1
    output = fgetl(fid);
    if ~ischar(output)
        break
    end
    if strncmp(output,'%defaultVAR',11)
        displayval = output(17:end);
        istr = output(12:13);
        i = str2double(istr);
        VariableValues(i) = {displayval};
    elseif strncmp(output,'%choiceVAR',10)
        if ~iscellstr(VariableValues(i))
            displayval = output(16:end);
            istr = output(11:12);
            i = str2double(istr);
            VariableValues(i) = {displayval};
        end
    elseif strncmp(output,'%textVAR',8)
        displayval = output(13:end);
        istr = output(9:10);
        i = str2double(istr);
        VariableDescriptions(i) = {displayval};
        VariableValues(i) = {[]};
        NumbersOfVariables = i;
    elseif strncmp(output,'%pathnametextVAR',16)
        displayval = output(21:end);
        istr = output(17:18);
        i = str2double(istr);
        VariableDescriptions(i) = {displayval};
        VariableValues(i) = {[]};
        NumbersOfVariables = i;
    elseif strncmp(output,'%filenametextVAR',16)
        displayval = output(21:end);
        istr = output(17:18);
        i = str2double(istr);
        VariableDescriptions(i) = {displayval};
        VariableValues(i) = {[]};
        NumbersOfVariables = i;
    elseif strncmp(output,'%infotypeVAR',12)
        displayval = output(18:end);
        istr = output(13:14);
        i = str2double(istr);
        VariableInfoTypes(i) = {displayval};
        if ~strcmp(output((length(output)-4):end),'indep') && isempty(VariableValues{i})
            VariableValues(i) = {'Pipeline Value'};
        end
    elseif strncmp(output,'%%%VariableRevisionNumber',25)
        try
            VarRevNum = str2double(output(29:30));
        catch
            VarRevNum = str2double(output(29:29));
        end
    elseif strncmp(output,'% $Revision:', 12)
        try
            ModuleRevNum = str2double(output(14:17));
        catch
            ModuleRevNum = str2double(output(14:18));
        end
    end
end
fclose(fid);

%%% SUBFUNCTION %%%
function FailedModule(handles, savedVariables, defaultDescriptions, ModuleName, ModuleNum)
helpText = ['The settings contained within the selected file are based on an old version of the ',ModuleName,...
    ' module. As a result, it is possible that your old settings are no longer reasonable. '...
    'Displayed below are the settings retrieved from your file. You can use the saved settings '...
    'to attempt to set up the module again. Sorry for the inconvenience.'];

%%% Creates the dialog box and its text, buttons, and edit boxes.
MainWinPos = get(handles.figure1,'Position');

[ScreenWidth,ScreenHeight] = CPscreensize;
FigWidth = MainWinPos(3)*4/5;
FigHeight = MainWinPos(4);
LeftPos = .5*(ScreenWidth-FigWidth);
BottomPos = .5*(ScreenHeight-FigHeight);
FigPosition = [LeftPos BottomPos FigWidth FigHeight];
Color = [0.7 0.99 0.7];

%%% Label we attach to figures (as UserData) so we know they are ours
userData.Application = 'CellProfiler';
LoadSavedWindowHandle = figure(...
    'Units','pixels',...
    'Color',Color,...
    'DockControls','off',...
    'MenuBar','none',...
    'Name',['Saved Variables for Module ',num2str(ModuleNum)],...
    'NumberTitle','off',...
    'Position',FigPosition,...
    'Resize','off',...
    'HandleVisibility','on',...
    'Tag','savedwindow',...
    'UserData',userData);

informtext = uicontrol(...
    'Parent',LoadSavedWindowHandle,...
    'BackgroundColor',Color',...
    'Units','normalized',...
    'Position',[0.05 0.70 0.9 0.25],...
    'String',helpText,...
    'Style','text',...
    'FontName','helvetica',...
    'HorizontalAlignment','left',...
    'FontSize',handles.Preferences.FontSize,...
    'Tag','informtext'); %#ok Ignore MLint

savedbox = uicontrol(...
    'Parent',LoadSavedWindowHandle,...
    'BackgroundColor', Color,...
    'Units','normalized',...
    'Position',[0.7 0.1 0.25 0.55],...
    'String',savedVariables,...
    'Style','listbox',...
    'Value',1,...
    'FontName','helvetica',...
    'FontSize',handles.Preferences.FontSize,...
    'Tag','savedbox'); %#ok Ignore MLint

descriptionbox = uicontrol(...
    'Parent',LoadSavedWindowHandle,...
    'BackgroundColor',Color,...
    'Units','normalized',...
    'Position',[0.05 0.1 0.6 0.55],...
    'String',defaultDescriptions,...
    'Style','listbox',...
    'Value',1,...
    'FontName','helvetica',...
    'FontSize',handles.Preferences.FontSize,...
    'Tag','descriptionbox'); %#ok Ignore MLint

savedtext = uicontrol(...
    'Parent',LoadSavedWindowHandle,...
    'BackgroundColor',Color,...
    'Units','normalized',...
    'Position',[0.665 0.65 0.2 0.05],...
    'String','Saved Variables:',...
    'Style','text',...
    'FontName','helvetica',...
    'FontSize',handles.Preferences.FontSize,...
    'Tag','descriptiontext'); %#ok Ignore MLint

descriptiontext = uicontrol(...
    'Parent',LoadSavedWindowHandle,...
    'BackgroundColor',Color,...
    'Units','normalized',...
    'Position',[0.015 0.65 0.25 0.05],...
    'String','Variable Descriptions:',...
    'Style','text',...
    'FontName','helvetica',...
    'FontSize',handles.Preferences.FontSize,...
    'Tag','descriptiontext'); %#ok Ignore MLint
