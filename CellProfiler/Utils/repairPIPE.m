function pipe = repairPIPE(pipe)
% It seems some pipeline saves loose the Number of Variables and the
% VariableRevisionNumbers fields. Let's try to fix this dirty here.

fullp = mfilename('fullpath');
splittedFullP = strsplit(fullp,{'/','\'});
splittedFullP(end-1:end) = []; % ..
splittedFullP{end+1} = 'Modules';
dirPath = fullfile(splittedFullP{:});
if (fullp(1) == '/') % handling the linux root case
    dirPath = ['/' dirPath];
end

nofModules = length(pipe.ModuleNames);
if isempty(pipe.NumbersOfVariables)
    pipe.NumbersOfVariables = sum(~cellfun(@isempty,pipe.VariableValues),2)';
end
if isempty(pipe.VariableRevisionNumbers)
    pipe.VariableRevisionNumbers = zeros(1,nofModules);
    for i=1:nofModules
        % read in the current newest as a good guess   
        if exist(fullfile(dirPath,[pipe.ModuleNames{i} '.m']),'file')
            f = fopen(fullfile(dirPath,[pipe.ModuleNames{i} '.m']));
            line = fgetl(f);
            while ischar(line)
                line = fgetl(f);
                prefix = '%%%VariableRevisionNumber =';
                if startsWith(line,prefix)
                    pipe.VariableRevisionNumbers(i) = str2double(line(length(prefix)+1:end));
                    break; % if we found the revision number then stop
                end
            end
            fclose(f);
        end
    end    
end



end
