function channelArray =  getCPchannels(handles)
%to extract typed in channel names
channelArray = cell(0);
for i=1:length(handles.Settings.ModuleNames)    
    if strcmp(handles.Settings.ModuleNames(i),'LoadImages')
        j = 2;
        while (~strcmp(handles.Settings.VariableValues{i,j},'/') && j<10)
            channelArray{end+1} = handles.Settings.VariableValues{i,j}; %#ok<AGROW>Size can't be estimated before, but not many usually.
            j = j + 2;            
        end
    end
end
