%CUSTOM EXTRA FUNCTIONS from ACC
function fieldInDeep = isfieldRecursive(CommonHandles,string) %#ok<INUSL> it is used in eval.
% AUTHOR:   Abel Szkalisity
% DATE:     August 16, 2016
% NAME:     isfieldRecursive
%
% Function to check fields recursively.
%
% INPUT:
%       CommonHandles  usual CommonHandles
%       string         A string which will be parsed by the dots
%
% OUTPUT:
%       fieldInDeep    Boolean value, true iff the field described by
%                      string is really field of CommonHandles
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


splittedString = strsplit(string,'.');
fieldInDeep = 1;

for i=1:length(splittedString)
    if i>1
        baseString = 'CommonHandles.';
    else
        baseString = 'CommonHandles';
    end
    if eval(['~isfield(' baseString strjoin(splittedString(1:i-1),'.') ', ''' splittedString{i} ''')'])
        fieldInDeep = 0;
        break;
    end
end

%Implementation of strsplit and strjoin
function joinedString = strjoin(inputStringCell,borderString)
    if isempty(inputStringCell)
        joinedString = [];
        return;
    end
    
    joinedString = '';
    for i=1:length(inputStringCell)-1
        joinedString = [joinedString inputStringCell{i} borderString];
    end
    joinedString = [joinedString inputStringCell{end}];    
    
    
function splittedCellArray = strsplit(inputStr,delimStr)
    pos = strfind(inputStr,delimStr);
    if ~isempty(pos)
        splittedCellArray = cell(1,length(pos)+1);
        splittedCellArray{1} = inputStr(1:pos(1)-1);        
        for i=2:length(pos)
            splittedCellArray{i} = inputStr(pos(i-1)+1:pos(i)-1);
        end
        if isempty(i), i = 1; end
        splittedCellArray{length(pos)+1} = inputStr(pos(i)+1:end);
    else
        splittedCellArray = {inputStr};
    end
