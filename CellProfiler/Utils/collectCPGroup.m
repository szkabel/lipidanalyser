function [indepGroupString, creatingModule] = collectCPGroup(pipeStruct,strSpec)
%CODE to collect independent objects for asking the user for
%strSpec is the variable info type
%return a cellarray of strings
%We don't collect \ and Image as it is, as Image is a reserved name
    indepGroupString = {};
    creatingModule = [];
    for i=1:numel(pipeStruct.VariableInfoTypes)
        [ii,jj] = ind2sub(size(pipeStruct.VariableInfoTypes),i);
        % NOTE: Cell Profiler's VariableInfoType and VariableValues array
        % is not neccesssarily the same size! (Variable Info Type is empty
        % for several fields and in that case the array is not complete!
        if strcmp(pipeStruct.VariableInfoTypes{i},strSpec) &&...
                 ~strcmp(pipeStruct.VariableValues{ii,jj},'\') &&...
                 ~strcmp(pipeStruct.VariableValues{ii,jj},'Image')
            indepGroupString{end+1} = pipeStruct.VariableValues{ii,jj};
            creatingModule(end+1) = ii;
        end
    end
end

