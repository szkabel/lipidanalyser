function morphedAllMasks = fitMaskToMultiStack(currentMask,morphedAllMasks,objectIdx,initMatrix2D)    

    fits = false;
    for j=1:size(morphedAllMasks,3)
        if ~any(currentMask & morphedAllMasks(:,:,j)) %it fits
            fits = true;
            break;
        end           
    end
    if fits
        currStack = morphedAllMasks(:,:,j);
        currStack(currentMask) = objectIdx;
        morphedAllMasks(:,:,j) = currStack;
    else
        currStack = initMatrix2D;
        currStack(currentMask) = objectIdx;
        morphedAllMasks(:,:,end+1) = currStack;
    end
    
end