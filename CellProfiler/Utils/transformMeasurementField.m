function [wdata,header,structure] = transformMeasurementField(measurementField,omitFields,currentSet,objectName)
%transformMeasurementField
% The purpose of this function is to help in the process of
% transforming the CP measurement fields to ACC feature vectors. CP makes
% measurements for each objects and stores them in
% handles.Measurements.(ObjectName).xxx fields. xxx can end to 'Features'
% in which case that field stores the interpretation of the columns in the
% next field.
% In one run this function extracts features corresponding to one image
% this should be specified in the currentSet input. (This value identifies
% which cell will be used from the xxx cellarray)
%
%   INPUTS:
%       measurementField        the CP measurement field
%       omitFields              measurement fields containing any of the
%                               strings listed in this cellarray will be
%                               excluded from the output.
%       currentSet              Which image to use.
%       objectName              This will be included as a prefix in
%                               headers.
%       
%   OUTPUT:
%       wdata                   Matrix based output for regular ACC use
%       header                 Feature names of wdata in a cellarray.
%       structure               The restructured form of CP for ACC-OA.

measurementFieldNames = fieldnames(measurementField);
ftext = 'Features';
measurementFieldNamesDescrPos = strfind(measurementFieldNames, ftext);

% avoid the omit fields
indicators = zeros(length(measurementFieldNames),length(omitFields));

for i=1:length(omitFields)
    for k=1:length(measurementFieldNames)
        indicators(k,i) = isempty(strfind(measurementFieldNames{k}, omitFields{i}));
    end
end

header = {};
wdata = [];
structure = [];

for k=find(all(indicators'))
    %if this IS a Feature descibing position
    if ~isempty(measurementFieldNamesDescrPos{k})
        % put feature names
        featureNameList = measurementField.(measurementFieldNames{k});
        %split up according to underscores the part before the Features
        measType = measurementFieldNames{k}(1:measurementFieldNamesDescrPos{k}(1)-1);        
        % put data
        for l=1:length(featureNameList)
            header = [header [objectName '.' measurementFieldNames{k} '.' featureNameList{l}]]; %#ok<AGROW> not much ~3-400 features max.
        end
        featureValues = measurementField.(measurementFieldNames{k+1});
        currentData = featureValues{currentSet};
        %TODO: compute the number of non omitted feature beforehand
        wdata = [wdata (currentData)];  %#ok<AGROW> It is much hassle but can be done.
        structure.(measType).Data = currentData; %store the data
        structure.(measType).Version = 0; %native CP is version 0.
    end
end