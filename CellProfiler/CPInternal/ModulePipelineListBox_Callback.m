% --- Executes on selection change in ModulePipelineListBox.
function ModulePipelineListBox_Callback(hObject, eventdata, handles) %#ok We want to ignore MLint error checking for this line.
ModuleHighlighted = get(handles.ModulePipelineListBox,'Value');
%Clean up the original positions if the list selection is changed
if (length(ModuleHighlighted) > 0)
    ModuleNumber = ModuleHighlighted(1);
    if( handles.Current.NumberOfModules > 0 )
        %%% 2. Sets all VariableBox edit boxes and all
        %%% VariableDescriptions to be invisible.
        warning('off','MATLAB:ui:javaframe:PropertyToBeRemoved');
        set(findobj('Parent',handles.variablepanel,'Visible','On'),'Visible','Off');
        warning('on','MATLAB:ui:javaframe:PropertyToBeRemoved');
        set(handles.VariableDescription{ModuleNumber},'Visible','On');

        if length(handles.VariableBox) == 1
            set(handles.VariableBox{ModuleNumber}(~strcmp({get(handles.VariableBox{ModuleNumber},'string')},'n/a')),'Visible','On'); %only makes the boxes without n/a as the string visible
        else
            set(handles.VariableBox{ModuleNumber}(~strcmp(get(handles.VariableBox{ModuleNumber},'string'),'n/a')),'Visible','On'); %only makes the boxes without n/a as the string visible
        end
        try
            set(handles.BrowseButton{ModuleNumber},'Visible','On')
        end
        %%% 2.25 Removes slider and moves panel back to original
        %%% position.
        %%% If panel location gets changed in GUIDE, must change the
        %%% position values here as well.
        set(handles.variablepanel, 'position', [238 0 563 346]);
        MatlabVersion = version;
        MatlabVersion = str2double(MatlabVersion(1:3));
        if ispc || (MatlabVersion >= 7.1)
            set(handles.slider1,'value',get(handles.slider1,'max'));
        else
            set(handles.slider1,'value',get(handles.slider1,'min'));
        end

        set(handles.slider1,'visible','off');
        %%% 2.5 Checks whether a module is loaded in this slot.
        % contents = get(handles.ModulePipelineListBox,'String');
        % ModuleName = contents{ModuleNumber};

        %%% 5.  Sets the slider
        MaxInfo = get(handles.slider1,'UserData');
        MaxInfo = MaxInfo(ModuleNumber);
        if(MaxInfo > 0)
            set(handles.slider1,'visible','on');
            set(handles.slider1,'max',MaxInfo);
            if ispc || (MatlabVersion >= 7.1)
                set(handles.slider1,'value',get(handles.slider1,'max'));
            else
                set(handles.slider1,'value',get(handles.slider1,'min'));
            end
            shortStep = max(.2,1/MaxInfo);
            bigStep = min(1,5/MaxInfo);
            if shortStep>bigStep
                bigStep = shortStep;
            end
            set(handles.slider1,'SliderStep',[shortStep bigStep ]);
        end
        slider1_Callback(handles.slider1,0,handles);
    else
        % Anne 7/11/06 Nice idea to have a confirmation, but I commented out the
        % dialog, because I think almost always
        % the user will have clicked intentionally, and the consequences of opening
        % up the Add module window are pretty minimal (that is, it is an easily
        % reversible choice). Also, because this can only happen right when the
        % user starts up CellProfiler, the chances for random clicking are fairly
        % minimal as well.
        % Rodrigo 7/13/06 - Maybe we can check the selection type, and open the Add
        % module window only if the user double-clicked. Nothing will happen
        % otherwise. What do you prefer?
        % Rodrigo 7/20/06 - We may have another problem with this. When
        % ModulePipelineListBox_Callback gets called by ClearPipeline_Callback, it
        % opens the AddModule window because ClearPipeline sets
        % handles.Current.NumberOfModules to 0 right before making the call. I
        % removed the call from ClearPipeline, but the same happens when you use
        % RemoveModule when you only have 1 module in the listbox. Obviously I
        % can't remove the call from RemoveModule_Callback because it's necessary,
        % so we might need to add some kind of test here. I checked every other
        % call to ModulePipelineListBox_Callback and I think they're all ok.
        % Rodrigo 7/21/06 - I just added another call to this function. I needed to
        % call it in the LoadPipeline_Callback function to refresh the variable
        % panel if the loading was aborted. It also opens the AddModule window.

        %        Answer = CPquestdlg('No modules are loaded. Do you want to add one?','No modules are loaded','Yes','No','Yes');
        %       if strcmp(Answer,'Yes')
        %%%%%  if strcmp(get(gcf,'SelectionType'),'open') %% these two lines should make it work
        %
        % Mike 9/7/06 I think opening the add module window should not
        % be done here, since it happens on occasions such as trying to
        % plot a histogram. Commented out.
        % AddModule_Callback(findobj('tag','AddModule'),[],handles);
        %%%%%  end
        %      end
    end
else
    % Mike 9/7/06 This is also very annoying. Feel free to discuss with me.
    %CPhelpdlg('No module highlighted.');
end

%%%% On double-click of a Module in the ModulePipelineListBox,
%%%%  bring its corresponding figure window (if it exists) to the foreground
if strcmp(get(gcf,'SelectionType'),'open')
    SelectedFigs = sort(findobj('-regexp','Name',handles.Settings.ModuleNames{ModuleHighlighted},'-and','NumberTitle','on','-and','-property','UserData'));
    for iFig = 1:length(SelectedFigs)
        CPfigure(SelectedFigs(iFig));
    end
end

