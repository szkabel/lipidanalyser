function storevariable(ModuleNumber, VariableNumber, UserEntry, handles)
%%% This function stores a variable's value in the handles structure,
%%% when given the Module Number, the Variable Number,
%%% the UserEntry (from the Edit box), and the initial handles
%%% structure.

InfoType = get(handles.VariableBox{ModuleNumber}(str2double(VariableNumber)),'UserData');
StrSet = get(handles.VariableBox{ModuleNumber}(str2double(VariableNumber)),'string');
% Type = get(handles.VariableBox{ModuleNumber}(str2double(VariableNumber)),'Style');

if length(InfoType) >= 5 && strcmp(InfoType(end-4:end),'indep')
    PrevValue = handles.Settings.VariableValues(ModuleNumber, str2double(VariableNumber));
    ModList = findobj('UserData',InfoType(1:end-6));
    %Filter out objects that are over this one
    ModList2 = findobj('UserData',InfoType(1:end));
    ModList2 = ModList2(ModList2 ~= handles.VariableBox{ModuleNumber}(str2double(VariableNumber)));
    %ModList3 = nonzeros(ModList2(strcmp(get(ModList2,'String'),PrevValue)));
    for i = 1:length(ModList2)
        Values = get(ModList2(i),'value');
        PrevStrSet = get(ModList2(i),'string');
        if Values == 0
            if strcmp(PrevStrSet,PrevValue)
                if exist('ModList3','var')
                    ModList3(end+1) = ModList2(i); %#ok Ignore MLint
                else
                    ModList3 = ModList2(i);
                end
            end
        else
            if strcmp(PrevStrSet(Values),PrevValue)
                if exist('ModList3','var')
                    ModList3(end+1) = ModList2(i); %#ok
                else
                    ModList3 = ModList2(i);
                end
            end
        end
    end
    if ischar(UserEntry)
        if size(StrSet,1) == 1
            for i = 1:length(ModList2)
                Values = get(ModList2(i),'value');
                PrevStrSet = get(ModList2(i),'string');
                if Values == 0
                    if strcmp(PrevStrSet,StrSet)
                        if exist('ModList4','var')
                            ModList4(end+1) = ModList2(i); %#ok Ignore MLint
                        else
                            ModList4 = ModList2(i);
                        end
                    end
                else
                    if strcmp(PrevStrSet(Values),StrSet)
                        if exist('ModList4','var')
                            ModList4(end+1) = ModList2(i); %#ok
                        else
                            ModList4 = ModList2(i);
                        end
                    end
                end
            end
        else
            OrigValues = get(handles.VariableBox{ModuleNumber}(str2double(VariableNumber)),'value');
            for i = 1:length(ModList2)
                Values = get(ModList2(i),'value');
                PrevStrSet = get(ModList2(i),'string');
                if Values == 0
                    if strcmp(PrevStrSet,StrSet(OrigValues))
                        if exist('ModList4','var')
                            ModList4(end+1) = ModList2(i); %#ok
                        else
                            ModList4 = ModList2(i);
                        end
                    end
                else
                    if strcmp(PrevStrSet(Values),StrSet(OrigValues))
                        if exist('ModList4','var')
                            ModList4(end+1) = ModList2(i); %#ok
                        else
                            ModList4 = ModList2(i);
                        end
                    end
                end
            end
        end
    else
        for i = 1:length(ModList2)
            Values = get(ModList2(i),'value');
            PrevStrSet = get(ModList2(i),'string');
            if Values == 0
                if strcmp(PrevStrSet,StrSet(UserEntry))
                    if exist('ModList4','var')
                        ModList4(end+1) = ModList2(i); %#ok
                    else
                        ModList4 = ModList2(i);
                    end
                end
            else
                if strcmp(PrevStrSet(Values),StrSet(UserEntry))
                    if exist('ModList4','var')
                        ModList4(end+1) = ModList2(i); %#ok
                    else
                        ModList4 = ModList2(i);
                    end
                end
            end
        end
    end

    if ~exist('ModList4','var')
        ModList4 = [];
    end
    if ~exist('ModList3','var')
        ModList3 = [];
    end

    for i=1:numel(ModList)
        BoxTag = get(ModList(i),'tag');
        BoxNum = str2double(BoxTag((length(BoxTag)-1):end));
        ModNum = [];
        for m = 1:handles.Current.NumberOfModules
            if length(handles.VariableBox{m}) >= BoxNum
                if ModList(i) == handles.VariableBox{m}(BoxNum)
                    ModNum = m;
                end
            end
        end
        if isempty(ModNum)
            m = handles.Current.NumberOfModules + 1;
            if ModList(i) == handles.VariableBox{m}(BoxNum)
                ModNum = m;
            end
        end
        CurrentString = get(ModList(i),'String');
        try
            if isempty(CurrentString{1})
                CurrentString = StrSet;
                set(ModList(i),'Enable','on');
            end
        catch
            if isempty(CurrentString)
                CurrentString = StrSet;
                set(ModList(i),'Enable','on');
            end
        end
        MatchedIndice = strmatch(PrevValue,CurrentString);
        if ~isempty(MatchedIndice) && isempty(ModList3)
            if isempty(ModList4)
                if ~iscell(CurrentString)
                    set(ModList(i),'String',{UserEntry});
                else
                    if length(CurrentString) == 1
                        set(ModList(i),'String',cat(1,CurrentString,{UserEntry}));
                    else
                        if ischar(UserEntry)
                            set(ModList(i),'String',cat(1,CurrentString(1:(MatchedIndice-1)),{UserEntry},CurrentString((MatchedIndice+1):end)));
                            if isempty(get(ModList(i), 'value')),
                               set(ModList(i), 'Value', MatchedIndice);
                            end
                            VarVal = get(ModList(i),'value');
                            SetStr = get(ModList(i),'string');
                            handles.Settings.VariableValues(ModNum,BoxNum) = SetStr(VarVal);
                            clear VarVal SetStr
                        else
                            set(ModList(i),'String',cat(1,CurrentString(1:(MatchedIndice-1)),StrSet(UserEntry),CurrentString((MatchedIndice+1):end)));
                        end
                    end
                end
            else
                set(ModList(i),'String',cat(1,CurrentString(1:(MatchedIndice-1)),CurrentString((MatchedIndice+1):end)));
                if get(ModList(i),'Value')==MatchedIndice
                    set(ModList(i),'Value',1);
                    VarVals = get(ModList(i),'string');
                    if iscell(VarVals)
                        handles.Settings.VariableValues(ModNum, BoxNum) = VarVals(1);
                    else
                        handles.Settings.VariableValues(ModNum, BoxNum) = VarVals;
                    end
                else
                    OldVal = get(ModList(i),'Value');
                    if (OldVal ~= 0) && OldVal > MatchedIndice
                        set(ModList(i),'Value',(OldVal-1));
                    end
                end
            end
        elseif isempty(ModList4)
            if numel(CurrentString) == 0
                CurrentString = {UserEntry};
                set(ModList(i),'String',CurrentString);
            elseif ~iscell(CurrentString)
                CurrentString = {CurrentString};
                set(ModList(i),'String',CurrentString);
            else
                if ischar(UserEntry)
                    if size(StrSet,1) == 1
                        if ~strcmp(StrSet,'n/a') && ~strcmp(StrSet,'/')
                            CurrentString(numel(CurrentString)+1) = {StrSet};
                        end
                        set(ModList(i),'String',CurrentString);
                    else
                        OrigValues = get(handles.VariableBox{ModuleNumber}(str2double(VariableNumber)),'value');
                        if ~strcmp(StrSet{OrigValues},'n/a') && ~strcmp(StrSet{OrigValues},'/')
                            CurrentString(numel(CurrentString)+1) = {StrSet{OrigValues}};
                        end
                        set(ModList(i),'String',CurrentString);
                    end
                else
                    if ~strcmp(StrSet(UserEntry),'n/a') && ~strcmp(StrSet(UserEntry),'/')
                        CurrentString(numel(CurrentString)+1) = StrSet(UserEntry);
                    end
                    set(ModList(i),'String',CurrentString);
                end
            end
        end
    end
end

if strcmp(get(handles.VariableBox{ModuleNumber}(str2double(VariableNumber)),'style'),'edit')
    handles.Settings.VariableValues(ModuleNumber, str2double(VariableNumber)) = {UserEntry};
else
    if ischar(UserEntry)
        handles.Settings.VariableValues(ModuleNumber, str2double(VariableNumber)) = {UserEntry};
    else
        handles.Settings.VariableValues(ModuleNumber, str2double(VariableNumber)) = StrSet(UserEntry);
    end
end
guidata(handles.figure1, handles);
