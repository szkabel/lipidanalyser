function CPHidingFunction(hObject,~)
%Written by szkabel at 2019.11.19.

global Project;

if ~isempty(Project)
    Project.figHandles.ModuleFigHandles.button_CP.Value = 0;
    ModuleHandler_GUI('button_CP_Callback',Project.figHandles.ModuleFigHandles.button_CP,[],Project.figHandles.ModuleFigHandles);
else
    hObject.Visible = 'off';
end




