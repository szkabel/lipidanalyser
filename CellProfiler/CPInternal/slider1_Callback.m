% --- Executes on slider movement.
function slider1_Callback(hObject, ~, handles)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range
%        of slider
ModuleHighlighted = get(handles.ModulePipelineListBox,'Value');
ModuleNumber = ModuleHighlighted(1);

if isempty(handles.Settings.NumbersOfVariables)
    set(handles.slider1,'visible','off');
    guidata(handles.figure1,handles);
    return
end
% Note:  The yPosition is 0 + scrollPos because 0 is the original Y
% Position of the variablePanel.  If the original location of the
% variablePanel gets changed, then the constant offset must be changed as
% well.

%IF REMOVED: we do not support MATLABs from before 2012.
    Ypos = get(handles.slider1,'max') - get(handles.slider1,'Value');
    set(handles.variablepanel,'BackgroundColor',[.7 .99 .7]);    
    %disp(get(handles.variablepanel,'Clipping'));    
    %disp([variablepanelPos(1) Ypos variablepanelPos(3) variablepanelPos(4)]);
    %set(handles.variablepanel, 'position', [variablepanelPos(1) Ypos variablepanelPos(3) variablepanelPos(4)]);
    for i=1:handles.Settings.NumbersOfVariables(ModuleNumber)        
        if (~isfield(handles,'OriginalVariableDescriptionPositions') ||...
            length(handles.OriginalVariableDescriptionPositions)<ModuleNumber ||...
            size(handles.OriginalVariableDescriptionPositions{ModuleNumber},1)<i)
        
                handles.OriginalVariableDescriptionPositions{ModuleNumber}(i,:) = get(handles.VariableDescription{ModuleNumber}(i),'Position');
        end
        tempPos=handles.OriginalVariableDescriptionPositions{ModuleNumber}(i,:);
        tempPos(2) = tempPos(2) + Ypos;
        set(handles.VariableDescription{ModuleNumber}(i),'Position',tempPos);        
        if(tempPos(2))>-20
            set(handles.VariableDescription{ModuleNumber}(i),'visible','on');
            VarDesOn=1;
        else
            set(handles.VariableDescription{ModuleNumber}(i),'visible','off');
            VarDesOn=0;
        end
        %Variable input fields (EDIT boxes)
        if (~isfield(handles,'OriginalVariableEditPositions') ||...
            length(handles.OriginalVariableEditPositions)<ModuleNumber ||...
            size(handles.OriginalVariableEditPositions{ModuleNumber},1)<i)
        
                handles.OriginalVariableEditPositions{ModuleNumber}(i,:) = get(handles.VariableBox{ModuleNumber}(i),'Position');
        end
        tempPos = handles.OriginalVariableEditPositions{ModuleNumber}(i,:);
        tempPos(2) = tempPos(2) + Ypos;
        set(handles.VariableBox{ModuleNumber}(i),'Position',tempPos);
        if ((tempPos(2))>-20) && VarDesOn  && (size(get(handles.VariableBox{ModuleNumber}(i),'String'),1)~=1 || ~strcmp(get(handles.VariableBox{ModuleNumber}(i),'String'),'n/a'))
            set(handles.VariableBox{ModuleNumber}(i),'visible','on');
        else
            set(handles.VariableBox{ModuleNumber}(i),'visible','off');
        end
        try
            if (~isfield(handles,'OriginalVariableBrowsePositions') ||...
                length(handles.OriginalVariableBrowsePositions)<ModuleNumber ||...
                size(handles.OriginalVariableBrowsePositions{ModuleNumber},1)<i)

                handles.OriginalVariableBrowsePositions{ModuleNumber}(i,:) = get(handles.BrowseButton{ModuleNumber}(i),'Position');
            end
            tempPos = handles.OriginalVariableBrowsePositions{ModuleNumber}(i,:);
            tempPos(2) = tempPos(2) + Ypos;
            set(handles.BrowseButton{ModuleNumber}(i),'Position',tempPos);
            if ((tempPos(2)+Ypos)>-20) && VarDesOn
                set(handles.BrowseButton{ModuleNumber}(i),'visible','on');
            else
                set(handles.BrowseButton{ModuleNumber}(i),'visible','off');
            end
        end
    end
    guidata(handles.figure1,handles);
