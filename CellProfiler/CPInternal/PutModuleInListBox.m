function PutModuleInListBox(ModuleNamedotm, Pathname, handles, RunInBG)
%Organized from CP by Abel Szkalisity on 2019.11.19.

if ModuleNamedotm ~= 0,
    %%% The folder containing the desired .m file is added to Matlab's search path.
    if ~isdeployed
        addpath(Pathname);
        differentPaths = which(ModuleNamedotm, '-all');
        if isempty(differentPaths)
            %%% If the module's .m file is not found on the search path, the result
            %%% of exist is zero, and the user is warned.
            CPerrordlg(['Something is wrong; The .m file ', ModuleNamedotm, ' was not initially found by Matlab, so the folder containing it was added to the Matlab search path. But, Matlab still cannot find the .m file for the analysis module you selected. The module will not be added to the image analysis pipeline.'],'Error');
            return
        elseif length(differentPaths) > 1
            CPwarndlg(['More than one file with this same module name exists in the Matlab search path.  The pathname from ' char(differentPaths{1}) ' will likely be used, but this is unpredictable.  Modules should have unique names that are not the same as already existing Matlab functions to avoid confusion.'],ModuleNamedotm,'modal');
        end
    end
    if(exist(ModuleNamedotm(1:end-2),'builtin') ~= 0)
        warningString = 'Your module has the same name as a builtin Matlab function.  Perhaps you should consider renaming your module.';
        warndlg(warningString);
    end

    %%% 3. The last two characters (=.m) are removed from the
    %%% ModuleName.m and called ModuleName. If we are using the compiled
    %%% version (isdeployed), we must remove 4 characters (=.txt) instead.
    if isdeployed
        ModuleName = ModuleNamedotm(1:end-4);
	if ~ strcmp(ModuleNamedotm(end-3:end), '.txt'),			       
	        CPwarndlg('Only compiled modules (.txt files) can be added to the pipeline in the compiled version of CellProfiler. If you load .m files, your pipeline will not function correctly.');
	end
    else
        ModuleName = ModuleNamedotm(1:end-2);
    end
    %%% The name of the module is shown in a text box in the GUI (the text
    %%% box is called ModuleName1.) and in a text box in the GUI which
    %%% displays the current module (whose settings are shown).

    %%% 4. The text description for each variable for the chosen module is
    %%% extracted from the module's .m file and displayed.
    if handles.Current.NumberOfModules == 0
        ModuleNums = 1;
    elseif RunInBG
        ModuleNums = handles.Current.NumberOfModules+1;
    else
        ModuleNums = get(handles.ModulePipelineListBox,'value')+1;
    end
    ModuleNumber = TwoDigitString(ModuleNums);

    % This is where the "shift" of modules happen
    for ModuleCurrent = handles.Current.NumberOfModules:-1:ModuleNums;
        %%% 1. Switches ModuleNames
        handles.Settings.ModuleNames{ModuleCurrent+1} = handles.Settings.ModuleNames{ModuleCurrent};
        contents = get(handles.ModulePipelineListBox,'String');
        contents{ModuleCurrent+1} = handles.Settings.ModuleNames{ModuleCurrent};
        set(handles.ModulePipelineListBox,'String',contents);
        %%% 2. Copy then clear the variable values in the handles
        %%% structure.
        handles.Settings.VariableValues(ModuleCurrent+1,:) = handles.Settings.VariableValues(ModuleCurrent,:);
        %%% 3. Copy then clear the num of variables in the handles
        %%% structure.
        handles.Settings.NumbersOfVariables(ModuleCurrent+1) = handles.Settings.NumbersOfVariables(ModuleCurrent);
        %%% 4. Copy then clear the variable revision numbers in the handles
        %%% structure.
        handles.Settings.VariableRevisionNumbers(ModuleCurrent+1) = handles.Settings.VariableRevisionNumbers(ModuleCurrent);
        %%% 5. Copy then clear the module revision numbers in the handles
        %%% structure.
        handles.Settings.ModuleRevisionNumbers(ModuleCurrent+1) = handles.Settings.ModuleRevisionNumbers(ModuleCurrent);
        %%% 6. Copy then clear the variable infotypes in the handles
        %%% structure.
        if size(handles.Settings.VariableInfoTypes,1) >= ModuleCurrent
            handles.Settings.VariableInfoTypes(ModuleCurrent+1,:) = handles.Settings.VariableInfoTypes(ModuleCurrent,:);
            % THIS IS NOT ENOUGH! THE VariableInfoTypes variable is not
            % filled fully all the time, hence it needs to be emptied too
            handles.Settings.VariableInfoTypes(ModuleCurrent,:) = cell(1,size(handles.Settings.VariableInfoTypes,2));
        end
        contents = get(handles.ModulePipelineListBox,'String');
        contents{ModuleCurrent+1} = handles.Settings.ModuleNames{ModuleCurrent};
        set(handles.ModulePipelineListBox,'String',contents);
                
    end

    if ModuleNums <= handles.Current.NumberOfModules
        handles.VariableDescription = [handles.VariableDescription(1:ModuleNums-1) {[]} handles.VariableDescription(ModuleNums:end)];
        handles.OriginalVariableDescriptionPositions = [handles.OriginalVariableDescriptionPositions(1:ModuleNums-1) {[]} handles.OriginalVariableDescriptionPositions(ModuleNums:end)];
        handles.OriginalVariableEditPositions = [handles.OriginalVariableEditPositions(1:ModuleNums-1) {[]} handles.OriginalVariableEditPositions(ModuleNums:end)];
        handles.VariableBox = [handles.VariableBox(1:ModuleNums-1) {[]} handles.VariableBox(ModuleNums:end)];
        if isfield(handles,'BrowseButton')
            if length(handles.BrowseButton) >= ModuleNums
                handles.BrowseButton = [handles.BrowseButton(1:ModuleNums-1) {[]} handles.BrowseButton(ModuleNums:end)];
            end
        end
    end

    fid=fopen(fullfile(Pathname,ModuleNamedotm));
    lastVariableCheck = 0;

    numberExtraLinesOfDescription = 0;
    varSpacing = 25;
    firstBoxLoc = 345; firstDesLoc = 343; normBoxHeight = 23; normDesHeight = 20;
    normBoxLength = 94;
    pixelSpacing = 2;

    while 1;
        output = fgetl(fid);
        if ~ischar(output)
            break
        end
        if strncmp(output,'%defaultVAR',11)
            displayval = output(17:end);
            istr = output(12:13);
            lastVariableCheck = str2double(istr);
            handles.Settings.NumbersOfVariables(str2double(ModuleNumber)) = lastVariableCheck;
            set(handles.VariableBox{ModuleNums}(lastVariableCheck),'String',displayval);
            CheckVal = handles.Settings.VariableValues(ModuleNums,lastVariableCheck);
            if isempty(CheckVal{1})
                handles.Settings.VariableValues(ModuleNums,lastVariableCheck) = {displayval};
            end
        elseif strncmp(output,'%textVAR',8)
            lastVariableCheck = str2double(output(9:10));
            if ~RunInBG
                handles.Settings.VariableValues(ModuleNums, lastVariableCheck) = {''};
            end
            handles.Settings.NumbersOfVariables(str2double(ModuleNumber)) = lastVariableCheck;
            descriptionString = output(14:end);

            handles.VariableBox{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[1 1 1],...
                'Callback','CellProfiler(''VariableBox_Callback'',gcbo,[],guidata(gcbo))',...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'Position',[470 295-25*lastVariableCheck 94 23],...
                'String','n/a',...
                'Style','edit',...
                'CreateFcn', 'CellProfiler(''VariableBox_CreateFcn'',gcbo,[],guidata(gcbo))',...
                'Tag',['VariableBox' TwoDigitString(lastVariableCheck)],...                
                'UserData','undefined',...
                'Visible','off');

            handles.VariableDescription{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[.7 .99 .7],...
                'CData',[],...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'FontWeight','bold',...
                'HorizontalAlignment','right',...
                'Position',[2 291-25*lastVariableCheck 465 23],...
                'String','',...
                'Style','text',...
                'Tag',['VariableDescription' TwoDigitString(lastVariableCheck)],...
                'UserData',[],...               
                'Visible','off',...
                'CreateFcn', '');

            set(handles.VariableDescription{ModuleNums}(lastVariableCheck),'string',descriptionString);

            linesVarDes = length(textwrap(handles.VariableDescription{ModuleNums}(lastVariableCheck),{descriptionString}));
            numberExtraLinesOfDescription = numberExtraLinesOfDescription + linesVarDes - 1;
            VarDesPosition = get(handles.VariableDescription{ModuleNums}(lastVariableCheck), 'Position');
            varXPos = VarDesPosition(1);
            varYPos = firstDesLoc+pixelSpacing*numberExtraLinesOfDescription-varSpacing*(lastVariableCheck+numberExtraLinesOfDescription);
            varXSize = VarDesPosition(3);
            varYSize = normDesHeight*linesVarDes + pixelSpacing*(linesVarDes-1);
            set(handles.VariableDescription{ModuleNums}(lastVariableCheck),'Position', [varXPos varYPos varXSize varYSize]);
            varXPos = 470;
            varYPos = firstBoxLoc+pixelSpacing*numberExtraLinesOfDescription-varSpacing*(lastVariableCheck+numberExtraLinesOfDescription-(linesVarDes-1)/2.0);
            varXSize = normBoxLength;
            varYSize = normBoxHeight;
            set(handles.VariableBox{ModuleNums}(lastVariableCheck), 'Position', [varXPos varYPos varXSize varYSize]);

        elseif strncmp(output,'%filenametextVAR',16)

            lastVariableCheck = str2double(output(17:18));
            if ~RunInBG
                handles.Settings.VariableValues(ModuleNums, lastVariableCheck) = {''};
            end
            handles.Settings.NumbersOfVariables(str2double(ModuleNumber)) = lastVariableCheck;
            descriptionString = output(22:end);

            handles.VariableBox{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[1 1 1],...
                'Callback','CellProfiler(''VariableBox_Callback'',gcbo,[],guidata(gcbo))',...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'Position',[305 295-25*lastVariableCheck 195 23],...
                'String','NO FILE LOADED',...
                'Style','edit',...
                'CreateFcn', 'CellProfiler(''VariableBox_CreateFcn'',gcbo,[],guidata(gcbo))',...
                'Tag',['VariableBox' TwoDigitString(lastVariableCheck)],...                
                'UserData','undefined',...
                'Visible','off');

            handles.BrowseButton{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[.7 .99 .7],...
                'Callback','handles = guidata(findobj(''tag'',''figure1'')); VariableBoxHandle = get(gco,''UserData''); CurrentChoice = get(VariableBoxHandle,''String''); if exist(CurrentChoice,''file''), if ~isdeployed, Pathname = fileparts(which(CurrentChoice)); end; else, Pathname = handles.Current.DefaultImageDirectory; end; [Filename Pathname] = CPuigetfile([],''Choose a file'',Pathname); pause(.1); figure(handles.figure1); if Pathname == 0, else, set(VariableBoxHandle,''String'',Filename); ModuleHighlighted = get(handles.ModulePipelineListBox,''Value''); ModuleNumber = ModuleHighlighted(1); VariableName = get(VariableBoxHandle,''Tag''); VariableNumberStr = VariableName(12:13); VarNum = str2num(VariableNumberStr); handles.Settings.VariableValues(ModuleNumber,VarNum) = {Filename}; guidata(handles.figure1,handles); end; clear handles VariableBoxHandle CurrentChoice Pathname Filename ModuleHighlighted ModuleNumber VariableName VariableNumberStr VarNum;',...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'FontWeight','bold',...
                'Position',[501 296-25*lastVariableCheck 63 20],...
                'String','Browse...',...
                'Style','pushbutton',...
                'CreateFcn', 'CellProfiler(''VariableBox_CreateFcn'',gcbo,[],guidata(gcbo))',...
                'Tag',['BrowseButton' TwoDigitString(lastVariableCheck)],...                
                'UserData',handles.VariableBox{ModuleNums}(lastVariableCheck),...
                'Visible','off');
            %%% In the callback function, if the user inputs an invalid filename, uigetfile goes to the default image directory instead of the current one as in other browse button callbacks because as of now (7/06), all modules use filenametextVAR only to get input files that should probably be there, so it's more convenient this way. In the future, code could be replaced to look more like the one for the pathnametextVAR browse button.

            handles.VariableDescription{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[.7 .99 .7],...
                'CData',[],...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'FontWeight','bold',...
                'HorizontalAlignment','right',...
                'Position',[2 295-25*lastVariableCheck 300 28],...
                'String','',...
                'Style','text',...
                'Tag',['VariableDescription' TwoDigitString(lastVariableCheck)],...
                'UserData',[],...                
                'Visible','off',...
                'CreateFcn', '');

            set(handles.VariableDescription{ModuleNums}(lastVariableCheck),'string',descriptionString);

            linesVarDes = length(textwrap(handles.VariableDescription{ModuleNums}(lastVariableCheck),{descriptionString}));
            numberExtraLinesOfDescription = numberExtraLinesOfDescription + linesVarDes - 1;
            VarDesPosition = get(handles.VariableDescription{ModuleNums}(lastVariableCheck), 'Position');
            varXPos = VarDesPosition(1);
            varYPos = firstDesLoc+pixelSpacing*numberExtraLinesOfDescription-varSpacing*(lastVariableCheck+numberExtraLinesOfDescription);
            varXSize = VarDesPosition(3);
            varYSize = normDesHeight*linesVarDes + pixelSpacing*(linesVarDes-1);
            set(handles.VariableDescription{ModuleNums}(lastVariableCheck),'Position', [varXPos varYPos varXSize varYSize]);
            varYPos = firstBoxLoc+pixelSpacing*numberExtraLinesOfDescription-varSpacing*(lastVariableCheck+numberExtraLinesOfDescription-(linesVarDes-1)/2.0);
            set(handles.VariableBox{ModuleNums}(lastVariableCheck), 'Position', [305 varYPos 195 23]);
            set(handles.BrowseButton{ModuleNums}(lastVariableCheck), 'Position', [501 varYPos 63 20]);

        elseif strncmp(output,'%pathnametextVAR',16)

            lastVariableCheck = str2double(output(17:18));
            if ~RunInBG
                handles.Settings.VariableValues(ModuleNums, lastVariableCheck) = {''};
            end
            handles.Settings.NumbersOfVariables(str2double(ModuleNumber)) = lastVariableCheck;
            descriptionString = output(22:end);

            handles.VariableBox{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[1 1 1],...
                'Callback','CellProfiler(''VariableBox_Callback'',gcbo,[],guidata(gcbo))',...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'Position',[305 295-25*lastVariableCheck 195 23],...
                'String','.',...
                'Style','edit',...
                'CreateFcn', 'CellProfiler(''VariableBox_CreateFcn'',gcbo,[],guidata(gcbo))',...
                'Tag',['VariableBox' TwoDigitString(lastVariableCheck)],...                
                'UserData','undefined',...
                'Visible','off');

            handles.BrowseButton{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[.7 .99 .7],...
                'Callback','handles = guidata(findobj(''tag'',''figure1'')); VariableBoxHandle = get(gco,''UserData''); CurrentChoice = get(VariableBoxHandle,''String''); if ~exist(CurrentChoice, ''dir''), CurrentChoice = pwd; end; Pathname = uigetdir(CurrentChoice,''Pick the directory you want.''); pause(.1); figure(handles.figure1); if Pathname == 0, else, set(VariableBoxHandle,''String'',Pathname); ModuleHighlighted = get(handles.ModulePipelineListBox,''Value''); ModuleNumber = ModuleHighlighted(1); VariableName = get(VariableBoxHandle,''Tag''); VariableNumberStr = VariableName(12:13); VarNum = str2num(VariableNumberStr); handles.Settings.VariableValues(ModuleNumber,VarNum) = {Pathname}; guidata(handles.figure1,handles); end; clear handles VariableBoxHandle CurrentChoice Pathname ModuleHighlighted ModuleNumber VariableName VariableNumberStr VarNum;',...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'FontWeight','bold',...
                'Position',[501 295-25*lastVariableCheck 63 20],...
                'String','Browse...',...
                'Style','pushbutton',...
                'CreateFcn', 'CellProfiler(''VariableBox_CreateFcn'',gcbo,[],guidata(gcbo))',...
                'Tag',['BrowseButton' TwoDigitString(lastVariableCheck)],...                
                'UserData',handles.VariableBox{ModuleNums}(lastVariableCheck),...
                'Visible','off');

            handles.VariableDescription{ModuleNums}(lastVariableCheck) = uicontrol(...
                'Parent',handles.variablepanel,...
                'Units','pixels',...
                'BackgroundColor',[.7 .99 .7],...
                'CData',[],...
                'FontName','helvetica',...
                'FontSize',handles.Preferences.FontSize,...
                'FontWeight','bold',...
                'HorizontalAlignment','right',...
                'Position',[2 295-25*lastVariableCheck 300 28],...
                'String','',...
                'Style','text',...
                'Tag',['VariableDescription' TwoDigitString(lastVariableCheck)],...
                'UserData',[],...                
                'Visible','off',...
                'CreateFcn', '');

            set(handles.VariableDescription{ModuleNums}(lastVariableCheck),'string',descriptionString);

            linesVarDes = length(textwrap(handles.VariableDescription{ModuleNums}(lastVariableCheck),{descriptionString}));
            numberExtraLinesOfDescription = numberExtraLinesOfDescription + linesVarDes - 1;
            VarDesPosition = get(handles.VariableDescription{ModuleNums}(lastVariableCheck), 'Position');
            varXPos = VarDesPosition(1);
            varYPos = firstDesLoc+pixelSpacing*numberExtraLinesOfDescription-varSpacing*(lastVariableCheck+numberExtraLinesOfDescription);
            varXSize = VarDesPosition(3);
            varYSize = normDesHeight*linesVarDes + pixelSpacing*(linesVarDes-1);
            set(handles.VariableDescription{ModuleNums}(lastVariableCheck),'Position', [varXPos varYPos varXSize varYSize]);
            varYPos = firstBoxLoc+pixelSpacing*numberExtraLinesOfDescription-varSpacing*(lastVariableCheck+numberExtraLinesOfDescription-(linesVarDes-1)/2.0);
            set(handles.VariableBox{ModuleNums}(lastVariableCheck), 'Position', [305 varYPos 195 23]);
            set(handles.BrowseButton{ModuleNums}(lastVariableCheck), 'Position', [501 varYPos 63 20]);

        elseif strncmp(output,'%choiceVAR',10)
            if ~(exist('StrSet','var'))
                StrSet = cell(1);
                StrSet{1} = output(16:end);
            else
                StrSet{numel(StrSet)+1} = output(16:end);
            end
            if isempty(handles.Settings.VariableValues(ModuleNums,lastVariableCheck))
                handles.Settings.VariableValues(ModuleNums,lastVariableCheck) = StrSet(1);
            end
        elseif strncmp(output,'%infotypeVAR',12)
            lastVariableCheck = str2double(output(13:14));
            try
                set(handles.VariableBox{ModuleNums}(lastVariableCheck),'UserData', output(18:end));
            catch
                keyboard;
            end
            handles.Settings.VariableInfoTypes(ModuleNums,lastVariableCheck) = {output(18:end)};

            if strcmp(output((length(output)-4):end),'indep')
                UserEntry = char(handles.Settings.VariableValues(ModuleNums,lastVariableCheck));
                if ~strcmp(UserEntry,'n/a') && ~strcmp(UserEntry,'/') && ~isempty(UserEntry)
                    storevariable(ModuleNums,output(13:14),UserEntry,handles);
                end
            end
            guidata(handles.figure1,handles);
        elseif strncmp(output,'%inputtypeVAR',13)
            lastVariableCheck = str2double(output(14:15));
            set(handles.VariableBox{ModuleNums}(lastVariableCheck),'style', output(19:27));
            VersionCheck = version;
            if strcmp(output(19:27),'popupmenu') && ~ispc && str2double(VersionCheck(1:3)) >= 7.1
                set(handles.VariableBox{ModuleNums}(lastVariableCheck),'BackgroundColor',[.7 .99 .7]);
            end
            if ~(exist('StrSet','var'))
                StrSet = cell(1);
                Count = 1;
            else
                Count = size(StrSet,2)+1;
            end
            for i=1:handles.Current.NumberOfModules
                for j=1:size(handles.Settings.VariableInfoTypes,2)
                    if size(handles.Settings.VariableInfoTypes,1) >= i
                        if ~strcmp(get(handles.VariableBox{ModuleNums}(lastVariableCheck),'UserData'),'undefined') && strcmp(handles.Settings.VariableInfoTypes{i,j},[get(handles.VariableBox{ModuleNums}(lastVariableCheck),'UserData'),' indep'])
                            if  (~isempty(handles.Settings.VariableValues{i,j})) && ( Count == 1 || (ischar(handles.Settings.VariableValues{i,j}) && isempty(strmatch(handles.Settings.VariableValues{i,j}, StrSet, 'exact')))) && ~strcmp(handles.Settings.VariableValues{i,j},'/') && ~strcmp(handles.Settings.VariableValues{i,j},'Do not save') && ~strcmp(handles.Settings.VariableValues{i,j},'n/a')
                                TestStr = 0;
                                for m=1:length(StrSet)
                                    if strcmp(StrSet(m),handles.Settings.VariableValues(i,j))
                                        TestStr = TestStr + 1;
                                    end
                                end
                                if TestStr == 0
                                    StrSet(Count) = handles.Settings.VariableValues(i,j);
                                    Count = Count + 1;
                                end
                            end
                        end
                    end
                end
            end

            if strcmp(output(29:end),'custom')
                if  (~isempty(handles.Settings.VariableValues{ModuleNums,lastVariableCheck})) && ( Count == 1 || (ischar(handles.Settings.VariableValues{ModuleNums,lastVariableCheck}) && isempty(strmatch(handles.Settings.VariableValues{ModuleNums,lastVariableCheck}, StrSet, 'exact'))))
                    StrSet(Count) = handles.Settings.VariableValues(ModuleNums,lastVariableCheck);
                    Count = Count + 1;
                end
                StrSet(Count) = {'Other..'};
                Count = Count + 1;
            end

            set(handles.VariableBox{ModuleNums}(lastVariableCheck),'string',StrSet);
            guidata(handles.figure1,handles);

            if Count == 1
                set(handles.VariableBox{ModuleNums}(lastVariableCheck),'enable','off')
                guidata(handles.figure1,handles);
            end

            clear StrSet
        elseif strncmp(output,'% $Revision:', 12)
            try
                handles.Settings.ModuleRevisionNumbers(ModuleNums) = str2double(output(14:17));
            catch
                handles.Settings.ModuleRevisionNumbers(ModuleNums) = str2double(output(14:18));
            end
        elseif strncmp(output,'%%%VariableRevisionNumber',25)
            try
                handles.Settings.VariableRevisionNumbers(ModuleNums) = str2double(output(29:30));
            catch
                handles.Settings.VariableRevisionNumbers(ModuleNums) = str2double(output(29:29));
            end
            break;
        end
    end

    fclose(fid);
    if ~isfield(handles.Settings,'VariableInfoTypes')||size(handles.Settings.VariableInfoTypes,1)==size(handles.Settings.VariableValues,1)-1
        handles.Settings.VariableInfoTypes(size(handles.Settings.VariableValues,1),:)={[]};
    end
    
    for i=1:lastVariableCheck
        if strcmp(get(handles.VariableBox{ModuleNums}(i),'style'),'edit')
            if ~RunInBG
                handles.Settings.VariableValues{ModuleNums, i} = get(handles.VariableBox{ModuleNums}(i),'String');
            else
                set(handles.VariableBox{ModuleNums}(i),'String',handles.Settings.VariableValues{ModuleNums,i});
            end
        else
            OptList = get(handles.VariableBox{ModuleNums}(i),'String');
            if ~RunInBG
                handles.Settings.VariableValues{ModuleNums, i} = OptList{1};
            else
                PPos = find(strcmp(handles.Settings.VariableValues{ModuleNums,i},OptList));
                if isempty(PPos)
                    if ~strcmp(handles.Settings.VariableValues{ModuleNums,i},'Pipeline Value')
                        %%% [Kyungnam & Anne, Aug-08-2007]
                        %%% Here is the place where the values of VariableBoxes are set
                        %%% based on the Settings of the pipeline loaded by the user.
                        %%% We are adding a catch to help deal with old versions of the Modules using Median Filtering
                        %%% e.g., CorrectIllumination_Calculate, Smooth. Median Filtering is automatically converted to
                        %%% Gaussian Filter.
                        if strcmp(handles.Settings.VariableValues{ModuleNums,i}, 'Median Filtering')
                            handles.Settings.VariableValues{ModuleNums,i} = 'Gaussian Filter';
                            CPwarndlg('Your pipeline uses Modules(s) including old ''Median Filtering'' which is actually ''Gaussian Filter''. We automatically convert Median Filtering to Gaussian Filter for your convenience');
                        else
                            set(handles.VariableBox{ModuleNums}(i),'String',[OptList;handles.Settings.VariableValues(ModuleNums,i)]);                            
                        end                                            
                        PPos = find(strcmp(handles.Settings.VariableValues{ModuleNums,i},OptList));
                        if (isempty(PPos)),
                            PPos = 1;
                        end
                        set(handles.VariableBox{ModuleNums}(i),'Value',PPos);
                    end
                else
                    set(handles.VariableBox{ModuleNums}(i),'Value',PPos);
                end
            end
        end
    end
    
    if lastVariableCheck == 0
        CPerrordlg(['The module you attempted to add, ', ModuleNamedotm,', is not a valid CellProfiler module because it does not appear to have any variables.  Sometimes this error occurs when you try to load a module that has the same name as a built-in Matlab function and the built in function is located in a directory higher up on the Matlab search path.']);
        return
    end

    try Contents = handles.Settings.VariableRevisionNumbers(str2double(ModuleNumber));
    catch
        handles.Settings.VariableRevisionNumbers(str2double(ModuleNumber)) = 0;
    end

    %blah
    try ModuleRevContents = handles.Settings.ModuleRevisionNumbers(str2double(ModuleNumber));
    catch
        handles.Settings.ModuleRevisionNumbers(str2double(ModuleNumber)) = 0;
    end

    %%% 5. Saves the ModuleName to the handles structure.
    % Find which module slot number this callback was called for.

    handles.Settings.ModuleNames{ModuleNums} = ModuleName;
    if ~RunInBG
        contents = get(handles.ModulePipelineListBox,'String');
        if iscell(contents)
            contents{ModuleNums} = ModuleName;
        else
            contents = {ModuleName};
        end
        set(handles.ModulePipelineListBox,'String',contents);
    end

    handles.Current.NumberOfModules = numel(handles.Settings.ModuleNames);

    %%% 6. Choose Loaded Module in Listbox
    if ~RunInBG
        set(handles.ModulePipelineListBox,'Value',ModuleNums);
    else
        warning('off','MATLAB:ui:javaframe:PropertyToBeRemoved');
        set(findobj('Parent',handles.variablepanel,'Visible','On'),'Visible','Off');
        warning('on','MATLAB:ui:javaframe:PropertyToBeRemoved');
    end

    MaxInfo = get(handles.slider1,'UserData');
    MaxInfo = [MaxInfo(1:ModuleNums-1) ((handles.Settings.NumbersOfVariables(ModuleNums)-12+numberExtraLinesOfDescription)*25) MaxInfo(ModuleNums:end)];
    set(handles.slider1,'UserData',MaxInfo);

    %%% Updates the handles structure to incorporate all the changes.
    guidata(handles.figure1,handles);

    if ~RunInBG
        ModulePipelineListBox_Callback(gcbo, [], handles);
        slider1_Callback(handles.slider1,0,handles);
    end
end
