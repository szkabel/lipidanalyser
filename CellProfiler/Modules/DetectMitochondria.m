function handles = DetectMitochondria(handles)

% Help for the DetectMitochondria module
% Category: Object Processing
%
% SHORT DESCRIPTION:
% Adaptive thresholding by matlab and then connecting the regions and
% define them as objects
% *************************************************************************
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Abel Szkalisity 2017.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images you want to process?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What do you want to call the objects identified by this module?
%defaultVAR02 = Mitochondria
%infotypeVAR02 = objectgroup indep
ObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = What is the sensitivity for the adaptive thresholding? (bigger value means more identified object)
%defaultVAR03 = 0.4
sensitivity = str2double(handles.Settings.VariableValues{CurrentModuleNum,3});

%textVAR04 = What is the minimum average intensity for objects? [0-1]
%defaultVAR04 = 0.4
minIntensity = str2double(handles.Settings.VariableValues{CurrentModuleNum,4});

%textVAR05 = What is the size of the structuring element for morphological filtering?
%defaultVAR05 = 3
strelSize = str2double(handles.Settings.VariableValues{CurrentModuleNum,5});

%textVAR06 = Specify a name for the outlines of the identified objects! (optional)
%defaultVAR06 = Do not save
%infotypeVAR06 = outlinegroup indep
SaveOutlines = char(handles.Settings.VariableValues{CurrentModuleNum,6});

%%%VariableRevisionNumber = 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                   CODE                   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%drawnow

tic

OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

T = adaptthresh(OrigImage,sensitivity);

BW = imbinarize(OrigImage,T);

se = strel('square',strelSize);

morphFiltered = imclose(imopen(BW,se),se);

R = regionprops(morphFiltered,'PixelIdxList','BoundingBox');

FinalLabelMatrixImage = zeros(size(OrigImage));

%{ 
OPTION 1
k = 1;
for i=1:length(R)
    intensitySet = OrigImage(R(i).PixelIdxList);
    if mean(intensitySet)>minIntensity
        maskedImage = zeros(size(FinalLabelMatrixImage));        
        maskedImage(R(i).PixelIdxList) = 1;        
        innerThreshLevel = graythresh(intensitySet);        
        maskedImage(maskedImage == 1 & OrigImage<innerThreshLevel) = 0;                                        
        Rinner = regionprops(logical(maskedImage),'PixelIdxList');
        for j = 1:length(Rinner)
            FinalLabelMatrixImage(Rinner(j).PixelIdxList) = k;
            k = k+1;            
        end        
    end
end
%}

k = 1;
for i=1:length(R)
    intensitySet = OrigImage(R(i).PixelIdxList);
    if mean(intensitySet)>minIntensity
        fprintf('%d / %d\n',i,length(R));
        maskedImage = zeros(size(FinalLabelMatrixImage));        
        skeletonLabels = maskedImage;
        maskedImage(R(i).PixelIdxList) = 1;        
        smallerMask = maskedImage;
        innerThreshLevel = graythresh(intensitySet);        
        smallerMask(maskedImage == 1 & OrigImage<innerThreshLevel) = 0;
        skeleton = bwmorph(maskedImage,'skel',inf);
        Rinner = regionprops(skeleton,'PixelIdxList');
        for j = 1:length(Rinner)
            skeletonLabels(Rinner(j).PixelIdxList) = j;
        end        
        [newRegionLabels,~] = IdentifySecPropagateSubfunction(skeletonLabels,OrigImage,logical(smallerMask),0.05);        
        %[~,~,newRegionLabels] = plotVarianceChange(tempLabels,distances,OrigImage,skeletonLabels,0);
        for j=1:max(newRegionLabels(:))
            FinalLabelMatrixImage(newRegionLabels == j) = k;
            k = k+1;
        end
    end
end

% Story END and CP variable savings

StructuringElement = strel('square',3);
bitBiggerObjects = imdilate(FinalLabelMatrixImage,StructuringElement);
outlines = bitBiggerObjects - FinalLabelMatrixImage;
LogicalOutlines = outlines > 0;    

if ~isfield(handles.Measurements,ObjectName)
    handles.Measurements.(ObjectName) = {};
end

%%% Saves the final, segmented label matrix image of secondary objects to
%%% the handles structure so it can be used by subsequent modules.
fieldname = ['Segmented',ObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

handles = CPsaveObjectCount(handles, ObjectName, FinalLabelMatrixImage);
handles = CPsaveObjectLocations(handles, ObjectName, FinalLabelMatrixImage);

toc

%%% Saves images to the handles structure so they can be saved to the hard
%%% drive, if the user requested.
try
    if ~strcmpi(SaveOutlines,'Do not save')        
        handles.Pipeline.(SaveOutlines) = LogicalOutlines;
    end
catch e
    error(['The object outlines were not calculated by the ', ModuleName, ' module, so these images were not saved to the handles structure. The Save Images module will therefore not function on these images. This is just for your information - image processing is still in progress, but the Save Images module will fail if you attempted to save these images.'])
end


%FUNCTION DUPLICATION# FROM DetectAllSpots
function [vari,img,newRegionLabels] = plotVarianceChange(identifiedRegionLabels,distances,intensityImage,centerImage,toPlot,maxArea)
%script to plot out variance change inside the identified particles
    
    %for all identified subregion
    sorted = unique(centerImage(:));
    positives = sorted(sorted>0);
    vari = cell(1,length(positives));
    k = 1;
    img = zeros(size(intensityImage));
    newRegionLabels = zeros(size(intensityImage));
    for i=positives'
        origIdx = find(identifiedRegionLabels == i);
        dist = distances(origIdx);
        inten = intensityImage(origIdx);
        [~,sortedIdx] = sort(dist);
        vari{k} = zeros(1,length(sortedIdx));
        
        actMean = inten(sortedIdx(1));
        soFarDiv = 0;
        vari{k}(1) = 0;
        for j=2:length(vari{k})
            [soFarDiv,actMean] = onlineDiffFromMeanSquared(soFarDiv,actMean,inten(sortedIdx(j)),j);
            vari{k}(j) = soFarDiv / j;
            img(origIdx(sortedIdx(j))) = vari{k}(j);
        end
        
        %find the first local maxima
        %maxIndices = localMaxima(vari{k},50);
        %if isempty(maxIndices)
        %    maxIndices = length(vari{k});
        %end
        [~,maxIndices] = max(vari{k});
        if nargin>5
            endPoint = min(maxArea,maxIndices(1));
            newRegionLabels(origIdx(sortedIdx(1:endPoint))) = i;
        else
            newRegionLabels(origIdx(sortedIdx(1:maxIndices(1)))) = i;
        end
        
        %plot if requested
        if toPlot
            f = figure();
            mask = identifiedRegionLabels == i;
            imshow(mask);
            g = figure();
            plot(vari{k});
            s = input('Do you want to save this figure? [y/n]','s');
            if s=='y'
                saveas(gcf,['variPlot_' num2str(i,'%03d') '.png']);
            elseif s=='s'
                break;
            end
            close(g);
            close(f);
        end
        k = k+1;
    end

