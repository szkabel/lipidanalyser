function handles = ExportImgForAnalysis(handles)

% Help for the exportImgForAnalysis module:
% Category: Image Processing
%
% SHORT DESCRIPTION:
% By defult only original images without any modifications can be selected
% in the export results section. This module enables the addition of
% CellProfiler internal images (e.g. processed images) to this set. For
% further usage this module needs a directory where these images will be
% saved.
%

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Abel Szkalisity 2023.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images you want to export?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%pathnametextVAR02 = Where the exported images should be saved?
%defaultVAR02 = .
saveLocation = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = In what file format do you want to save images (figures must be saved as fig, which is only openable in Matlab)?
%choiceVAR03 = png
%choiceVAR03 = bmp
%choiceVAR03 = gif
%choiceVAR03 = jpg
%choiceVAR03 = jpeg
%choiceVAR03 = tif
%choiceVAR03 = tiff
%inputtypeVAR03 = popupmenu
FileFormat = char(handles.Settings.VariableValues{CurrentModuleNum,3});


%%%VariableRevisionNumber = 1

%%%%%%%%%%%%%%%%%%%%%%%
%%% RUNNING SECTION %%%
%%%%%%%%%%%%%%%%%%%%%%%
%drawnow

%%% Reads (opens) the image you want to analyze and assigns it to a
%%% variable.
OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

global Project;
[imgChannels,~,prim] = Project.ECP.listImageChannels;

primChan = imgChannels{prim};
primChan = strsplitN(primChan,'::',1);

imgList = Project.ECP.getImageNames;
imageID = imgList{handles.Current.SetBeingAnalyzed};
    
imageRightChannelID = strrep(imageID,primChan,ImageName);

imwrite(OrigImage,fullfile(saveLocation,[imageRightChannelID '.' FileFormat]));
