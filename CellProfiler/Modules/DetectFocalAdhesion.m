function handles = DetectFocalAdhesion(handles)

% Help for the DetectFocalAdhesion module
% Category: Object Processing
%
% SHORT DESCRIPTION:
% High pass filter and water segmentation.
% *************************************************************************
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Abel Szkalisity 2017.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images where the focal adhesions are located?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What did you call the cell mask object?
%infotypeVAR02 = objectgroup
CytoObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,2});
%inputtypeVAR02 = popupmenu

%textVAR03 = What do you want to call the identified objects?
%defaultVAR03 = FA
%infotypeVAR03 = objectgroup indep
ObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,3});


%textVAR04 = What is the size of smoothing for high-pass filtering?
%defaultVAR04 = 15
filterSize = str2double(handles.Settings.VariableValues{CurrentModuleNum,4});

%textVAR05 = How far from the cell edge you consider still local intensity maximas as focal adhesions?
%defaultVAR05 = 20
cellMaskPadding = str2double(handles.Settings.VariableValues{CurrentModuleNum,5});

%textVAR06 = What is the threshold for the relative gradient?
%defaultVAR06 = 0.3
gradThresh = str2double(handles.Settings.VariableValues{CurrentModuleNum,6});

%c%textVAR06 = Specify a name for the outlines of the identified objects! (optional)
%c%defaultVAR06 = Do not save
%c%infotypeVAR06 = outlinegroup indep
%cSaveOutlines =
%char(handles.Settings.VariableValues{CurrentModuleNum,6});

%%%VariableRevisionNumber = 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                   CODE                   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%drawnow

OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

cellmask = CPretrieveimage(handles,['Segmented', CytoObjectName],ModuleName,'DontCheckColor','DontCheckScale',size(OrigImage));

smoothed = imfilter(OrigImage,ones(filterSize)/(filterSize^2));

filtered = (OrigImage - smoothed);

filtered = medfilt2(filtered);

LabelMatrixImage = CPwater(filtered,graythresh(filtered),50,5);

%Draw in the edges between touching cell masks.
outlines = imopen(cellmask,strel('disk',3)) - cellmask;
cellmask(outlines > 0) = 0;

CellInsides = imerode(cellmask,strel('square',cellMaskPadding));
gradImage = imgradient(OrigImage);

i = 1;
while i<=max(LabelMatrixImage(:))
    logicalIndex = LabelMatrixImage == i;    
    if all(CellInsides(logicalIndex)) || sum(gradImage(logicalIndex))/sum(logicalIndex(:)) < gradThresh;
        LabelMatrixImage(logicalIndex) = 0;        
        LabelMatrixImage(LabelMatrixImage>i) = LabelMatrixImage(LabelMatrixImage>i)-1;
    else
        i = i+1;
    end
end

FinalLabelMatrixImage = imclose(LabelMatrixImage,strel('disk',3));


% Story END and CP variable savings

if ~isfield(handles.Measurements,ObjectName)
    handles.Measurements.(ObjectName) = {};
end

%%% Saves the final, segmented label matrix image of secondary objects to
%%% the handles structure so it can be used by subsequent modules.
fieldname = ['Segmented',ObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

handles = CPsaveObjectCount(handles, ObjectName, FinalLabelMatrixImage);
handles = CPsaveObjectLocations(handles, ObjectName, FinalLabelMatrixImage);

toc