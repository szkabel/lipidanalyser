function handles = IdentifyPrimaryAdaptThresh(handles)

% Help for the IdentifyPrimaryAdaptThresh module
% Category: Object Processing
%
% SHORT DESCRIPTION:
% Adaptive thresholding by matlab and then connecting the regions and
% define them as objects
% *************************************************************************
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Abel Szkalisity 2017.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images you want to process?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What do you want to call the objects identified by this module?
%defaultVAR02 = Mitochondria
%infotypeVAR02 = objectgroup indep
ObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = What is the sensitivity for the adaptive thresholding? (bigger value means more identified object)
%defaultVAR03 = 0.4
sensitivity = str2double(handles.Settings.VariableValues{CurrentModuleNum,3});

%textVAR04 = What is the minimum average intensity for objects? [0-1]
%defaultVAR04 = 0.4
minIntensity = str2double(handles.Settings.VariableValues{CurrentModuleNum,4});

%textVAR05 = What is the size of the structuring element for morphological filtering?
%defaultVAR05 = 3
strelSize = str2double(handles.Settings.VariableValues{CurrentModuleNum,5});

%textVAR06 = Specify a name for the outlines of the identified objects! (optional)
%defaultVAR06 = Do not save
%infotypeVAR06 = outlinegroup indep
SaveOutlines = char(handles.Settings.VariableValues{CurrentModuleNum,6});

%%%VariableRevisionNumber = 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                   CODE                   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%drawnow


OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

T = adaptthresh(OrigImage,sensitivity);

BW = imbinarize(OrigImage,T);

se = strel('square',strelSize);

morphFiltered = imclose(imopen(BW,se),se);

R = regionprops(morphFiltered,'PixelIdxList');

FinalLabelMatrixImage = zeros(size(OrigImage));

k = 1;
for i=1:length(R)
    if mean(OrigImage(R(i).PixelIdxList))>minIntensity
        FinalLabelMatrixImage(R(i).PixelIdxList) = k;
        k = k+1;
    end
end

StructuringElement = strel('square',3);
bitBiggerObjects = imdilate(FinalLabelMatrixImage,StructuringElement);
outlines = bitBiggerObjects - FinalLabelMatrixImage;
LogicalOutlines = outlines > 0;    

% Story END and CP variable savings

if ~isfield(handles.Measurements,ObjectName)
    handles.Measurements.(ObjectName) = {};
end

%%% Saves the final, segmented label matrix image of secondary objects to
%%% the handles structure so it can be used by subsequent modules.
fieldname = ['Segmented',ObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

handles = CPsaveObjectCount(handles, ObjectName, FinalLabelMatrixImage);
handles = CPsaveObjectLocations(handles, ObjectName, FinalLabelMatrixImage);

%%% Saves images to the handles structure so they can be saved to the hard
%%% drive, if the user requested.
try
    if ~strcmpi(SaveOutlines,'Do not save')        
        handles.Pipeline.(SaveOutlines) = LogicalOutlines;
    end
catch e
    error(['The object outlines were not calculated by the ', ModuleName, ' module, so these images were not saved to the handles structure. The Save Images module will therefore not function on these images. This is just for your information - image processing is still in progress, but the Save Images module will fail if you attempted to save these images.'])
end

toc    