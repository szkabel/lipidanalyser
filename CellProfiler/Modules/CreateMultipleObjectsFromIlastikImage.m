function handles = CreateMultipleObjectsFromIlastikImage(handles)

% Help for the Create Objects From Image Module
% Category: Object Processing
%
% SHORT DESCRIPTION:
% Creates a Cell Profiler based object from images that contain object
% masks. In this case the image should contain only a few possible indices
% and each stronglyconnected component from one pixel value will be
% considered as one object.
% *************************************************************************
%
% Settings:
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Peter Horvath 2010 and Abel Szkalisity 2016.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the mask images from which you are creaating the objects?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What neighborhood do you wish to use for connected components?
%choiceVAR02 = 8
%choiceVAR02 = 4
connectType = str2num(char(handles.Settings.VariableValues{CurrentModuleNum,2}));
%inputtypeVAR02 = popupmenu

%textVAR03 = What is the type of the original images?
%choiceVAR03 = uint8
%choiceVAR03 = uint16
origImgType = char(handles.Settings.VariableValues{CurrentModuleNum,3});
%inputtypeVAR03 = popupmenu


%textVAR04 = What do you want to call the objects created?
%defaultVAR04 = \
%infotypeVAR04 = objectgroup indep
ObjectName{1} = char(handles.Settings.VariableValues{CurrentModuleNum,4});

%textVAR05 = What is the corresponding pixel value?
%defaultVAR05 = \
PixelValue{1} = str2num(char(handles.Settings.VariableValues{CurrentModuleNum,5}));


%textVAR06 = What do you want to call the objects created?
%defaultVAR06 = \
%infotypeVAR06 = objectgroup indep
ObjectName{2} = char(handles.Settings.VariableValues{CurrentModuleNum,6});

%textVAR07 = What is the corresponding pixel value?
%defaultVAR07 = \
PixelValue{2} = str2num(char(handles.Settings.VariableValues{CurrentModuleNum,7}));


%textVAR08 = What do you want to call the objects created?
%defaultVAR08 = \
%infotypeVAR08 = objectgroup indep
ObjectName{3} = char(handles.Settings.VariableValues{CurrentModuleNum,8});

%textVAR09 = What is the corresponding pixel value?
%defaultVAR09 = \
PixelValue{3} = str2num(char(handles.Settings.VariableValues{CurrentModuleNum,9}));


%textVAR10 = What do you want to call the objects created?
%defaultVAR10 = \
%infotypeVAR10 = objectgroup indep
ObjectName{4} = char(handles.Settings.VariableValues{CurrentModuleNum,10});

%textVAR11 = What is the corresponding pixel value?
%defaultVAR11 = \
PixelValue{4} = str2num(char(handles.Settings.VariableValues{CurrentModuleNum,11}));


%textVAR12 = What do you want to call the objects created?
%defaultVAR12 = \
%infotypeVAR12 = objectgroup indep
ObjectName{5} = char(handles.Settings.VariableValues{CurrentModuleNum,12});

%textVAR13 = What is the corresponding pixel value?
%defaultVAR13 = \
PixelValue{5} = str2num(char(handles.Settings.VariableValues{CurrentModuleNum,13}));


%textVAR14 = What do you want to call the objects created?
%defaultVAR14 = \
%infotypeVAR14 = objectgroup indep
ObjectName{6} = char(handles.Settings.VariableValues{CurrentModuleNum,14});

%textVAR15 = What is the corresponding pixel value?
%defaultVAR15 = \
PixelValue{6} = str2num(char(handles.Settings.VariableValues{CurrentModuleNum,15}));
%%%VariableRevisionNumber = 1

OrigImage = CPretrieveimage(handles,ImageName,ModuleName);

for i=1:length(ObjectName)    
    if strcmp(ObjectName{i},'\') || isempty(PixelValue{i}), continue; end
    
    if ~isfield(handles.Measurements,ObjectName{i})
        handles.Measurements.(ObjectName{i}) = {};
    end
    
    switch (origImgType)
        case 'uint8'
            OrigImage = im2uint8(OrigImage);
        case 'uint16'
            OrigImage = im2uint16(OrigImage);                
    end
    currMaskImg = (OrigImage == PixelValue{i});
    FinalLabelMatrixImage = bwlabel(currMaskImg,connectType);

    %%% Saves the final, segmented label matrix image of identified objects to
    %%% the handles structure so it can be used by subsequent modules.
    fieldname = ['Segmented',ObjectName{i}];
    handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

    fieldname = ['SmallRemovedSegmented',ObjectName{i}];
    handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

    handles = CPsaveObjectCount(handles, ObjectName{i}, FinalLabelMatrixImage);
    handles = CPsaveObjectLocations(handles, ObjectName{i}, FinalLabelMatrixImage);
end