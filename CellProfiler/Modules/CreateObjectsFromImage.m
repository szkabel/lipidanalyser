function handles = CreateObjectsFromImage(handles)

% Help for the Create Objects From Image Module
% Category: Object Processing
%
% SHORT DESCRIPTION:
% Creates a Cell Profiler based object from images that contain object
% masks.
% *************************************************************************
%
%
%
% Settings:
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Peter Horvath 2010 and Abel Szkalisity 2016.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the mask images from which you are creaating the objects?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What do you want to call the objects created?
%defaultVAR02 = Object
%infotypeVAR02 = objectgroup indep
ObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = Min and max for the accepted object size
%defaultVAR03 = [1 Inf]
minMaxSizes = str2num(handles.Settings.VariableValues{CurrentModuleNum,3});

%%%VariableRevisionNumber = 2

OrigImage = CPretrieveimage(handles,ImageName,ModuleName);

if size(OrigImage,3) == 3
    OrigImage = rgb2gray(OrigImage);
end

%Possibly some image conversion should come here
uintImg = im2uint16(OrigImage);
if all(minMaxSizes == [1 Inf])
    idxToDel = [];
else
    R = regionprops(uintImg);
    idxToDel = find([R.Area] < minMaxSizes(1) | [R.Area] > minMaxSizes(2));
end
    idx = unique(uintImg);
    idx = setdiff(idx,[idxToDel,0]);
FinalLabelMatrixImage = subsetMask(uintImg,idx);

% Story END and CP variable savings

if ~isfield(handles.Measurements,ObjectName)
    handles.Measurements.(ObjectName) = {};
end

%%% Saves the final, segmented label matrix image of identified objects to
%%% the handles structure so it can be used by subsequent modules.
fieldname = ['Segmented',ObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

fieldname = ['SmallRemovedSegmented',ObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

handles = CPsaveObjectCount(handles, ObjectName, FinalLabelMatrixImage);
handles = CPsaveObjectLocations(handles, ObjectName, FinalLabelMatrixImage);
    