function handles = ExportToACC(handles)

% Save data into acc format
% Category: File Processing
%
% SHORT DESCRIPTION:
% Save the measurements into text files can be used by Advanced Cell
% Classifier
% *************************************************************************
%
% In ACC one can train Machine Learning algorithms to classify cells
% (feature vectors). The cells corresponds to a primary object (usually
% nuclei) which should be (for correct working) the first selected object
% to export in this module. The other objects selected to export must be
% related to this primary object either by implicit relation (each
% secondary object is implicitely related to primary objects) or by
% explicitly calling the relate module.
%
% In order to export objects to ACC which are not in 1-by-1
% relation with the primary object (e.g. usually nuclei is the primary
% object and to each cytoplasm there is only one nuclei), you MUST RUN A
% RELATE MODULE on the 'multiple' objects. Multiple here means that the
% object is NOT in 1-by-1 relation with the primary object but there are
% more of them. (e.g. each cell can have more than one subcellular
% particles such as lipid droplets). These objects must be signed as
% 'multiple' object providing the information that we should use the MEAN
% measurements for them. (In this case in each cell describing feature
% vector there will be entries which contains mean measurements from the
% multiple objects, e.g. the average area of lipid droplets within one
% cell).
%
% In order to use your project in ACC your saved images (such as the
% original coloured image, or the image with the contours) MUST HAVE THE
% SAME NAME. (e.g. you should select the same original channel name for
% both of them in the SaveImages module). In this module you should select
% the same channel as the base for the export to ACC.
%
% In this module we assumed that the similar channel images are coming from
% a similar channel folder.
%
% For correct working before running this module DELETE the ECP.mat file
% from the folder specified as Default Output folder.
%
% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Peter Horvath.
% Copyright 2010.
%
% Please see the AUTHORS file for credits.
%
% Website: http://acc.ethz.ch
%
% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
drawnow

[~, CurrentModuleNum, ~] = CPwhichmodule(handles);


%textVAR01 = Where do you want to save the measurements
%defaultVAR01 = anal2
OriginalFolder = char(handles.Settings.VariableValues{CurrentModuleNum,1});

%textVAR02 = Which images' original filenames do you want use as a base for the export? To make ACC work correctly you should set this to the same original filename with which you saved your images.
%infotypeVAR02 = imagegroup
CPInternalACCImageID = char(handles.Settings.VariableValues{CurrentModuleNum,2});
%inputtypeVAR02 = popupmenu custom

%textVAR03 = Which objects do you want to export? Cell location will be calculted from the first object. Normally use here nuclei. (Object #1 - PRIMARY)
%infotypeVAR03 = objectgroup
%choiceVAR03 = /
%choiceVAR03 = Image
%choiceVAR03 = Experiment
Object{1} = char(handles.Settings.VariableValues{CurrentModuleNum,3});
%inputtypeVAR03 = popupmenu

MultipleItem{1} = 'No';

%textVAR04 = Object #2
%infotypeVAR04 = objectgroup
%choiceVAR04 = /
%choiceVAR04 = Image
%choiceVAR04 = Experiment
Object{2} = char(handles.Settings.VariableValues{CurrentModuleNum,4});
%inputtypeVAR04 = popupmenu

%textVAR05 = Is this a multiple object?
%choiceVAR05 = No
%choiceVAR05 = Yes
MultipleItem{2} = char(handles.Settings.VariableValues{CurrentModuleNum,5});
%inputtypeVAR05 = popupmenu


%textVAR06 = Object #3
%infotypeVAR06 = objectgroup
%choiceVAR06 = /
%choiceVAR06 = Image
%choiceVAR06 = Experiment
Object{3} = char(handles.Settings.VariableValues{CurrentModuleNum,6});
%inputtypeVAR06 = popupmenu

%textVAR07 = Is this a multiple object?
%choiceVAR07 = No
%choiceVAR07 = Yes
MultipleItem{3} = char(handles.Settings.VariableValues{CurrentModuleNum,7});
%inputtypeVAR07 = popupmenu


%textVAR08 = Object #4
%infotypeVAR08 = objectgroup
%choiceVAR08 = /
%choiceVAR08 = Image
%choiceVAR08 = Experiment
Object{4} = char(handles.Settings.VariableValues{CurrentModuleNum,8});
%inputtypeVAR08 = popupmenu

%textVAR09 = Is this a multiple object?
%choiceVAR09 = No
%choiceVAR09 = Yes
MultipleItem{4} = char(handles.Settings.VariableValues{CurrentModuleNum,9});
%inputtypeVAR09 = popupmenu

%%%VariableRevisionNumber = 3

[Object,tempidx] = skipSlashFromCellArray(Object);
MultipleItem = MultipleItem(logical(tempidx));

drawnow

accIndex = 0;
%Identify the index of the ACCImageName base channel.
for j=1:length(handles.Measurements.Image.PathNamesText)
    if strcmp(handles.Measurements.Image.PathNamesText{j},CPInternalACCImageID)
        accIndex = j;
        break;
    end
end


if accIndex == 0
    error(['Image processing was canceled in the ', ModuleName, ' module because the specified image (',CPInternalACCImageID,') could not be found in the original image list.']);
end

% last cycle
%if handles.Current.SetBeingAnalyzed == handles.Current.NumberOfImageSets
% in the new version we do it for every cycle
if 1
    % operating system specific slash
    if ispc == 1
        OSSlash = '\';
    else
        OSSlash = '/';
    end

    % output folder
    outPutFolder = handles.Current.DefaultOutputDirectory;
    if outPutFolder(length(outPutFolder)) ~= OSSlash
        outPutFolder = [outPutFolder OSSlash];
    end
    
    %Check for duplicate entry
    [ObjectNames,indices] = unique(Object);
    MultipleItem = MultipleItem(indices);
        
    % for each image sets
    % Note: position of the objects comes from the first measured object
    % type (usually nuclei)
    
    %for i=1:handles.Current.NumberOfImageSets
    % current cycle
    i = handles.Current.SetBeingAnalyzed;    
     
        %Export to Object analyzer and to ECP format (startup) This should
        %never run in ObjectAnalyzer        
        
        ACCImageName = char(handles.Measurements.Image.FileNames{i}(accIndex));        
        outputFileName = [ACCImageName '.txt'];
    
        wdata = [];        
        
        % get xy positions
        PosField = handles.Measurements.(Object{1}); %here we should use the old Object because ObjectNames is sorted.
        wdata(:,1:2) = PosField.Location{i}(:,1:2); % nucleii location       
        header = {};
        header{1} = 'LocationX';
        header{2} = 'LocationY';        
        
        % get measured fileds:        
        for j=1:length(ObjectNames)                        
            
            % get measurements
            % if this is a simple object then just the ObjectNames
            if strcmp(MultipleItem{j},'No')          
               measurementField = handles.Measurements.(ObjectNames{j}); 
            else %otherwise we suppose there are mean measurements.
               measurementField = handles.Measurements.(['Mean' ObjectNames{j}]);               
            end            
            
            %%
            
            [locWData,locHeader,~] = transformMeasurementField(measurementField,{'Location','Parent','Children'},i,ObjectNames{j});
            wdata = [wdata locWData]; %#ok<AGROW> Only few (object number) times
            header = [header locHeader]; %#ok<AGROW> Only few times
            
            %%                        
                        
        end 
        %Check for folder existance
        if ~exist(fullfile(outPutFolder,OriginalFolder),'dir')
            mkdir(fullfile(outPutFolder,OriginalFolder));
        end
        % save output
        save(fullfile(outPutFolder,OriginalFolder,outputFileName), 'wdata', '-ascii');
        % save feature description for further analisys
        if ~exist(fullfile(outPutFolder,OriginalFolder,'featureNames.acc'),'file')
            outFile = fopen(fullfile(outPutFolder,OriginalFolder,'featureNames.acc'), 'w');
            for l=1:length(header)
                fprintf(outFile, '%s\n', header{l});
            end
            fclose(outFile);
        end                          
end    