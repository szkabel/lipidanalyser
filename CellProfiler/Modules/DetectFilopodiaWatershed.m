function handles = DetectFilopodiaWatershed(handles)

% Help for the DetectFilopodia module
% Category: Object Processing
%
% SHORT DESCRIPTION:
% Detect Filopodia based on the FiloDetect MATLAB tool.
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Abel Szkalisity 2017.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[CurrentModule, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images you want to process?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What do you want to call the objects identified by this module?
%defaultVAR02 = Filopodia
%infotypeVAR02 = objectgroup indep
ObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = What did you call the cell masks?
%infotypeVAR03 = objectgroup
CellMaskName = char(handles.Settings.VariableValues{CurrentModuleNum,3});
%inputtypeVAR03 = popupmenu

%textVAR04 = Specify a name for the outlines of the identified objects! (optional)
%defaultVAR04 = Do not save
%infotypeVAR04 = outlinegroup indep
SaveOutlines = char(handles.Settings.VariableValues{CurrentModuleNum,4});

%%%VariableRevisionNumber = 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                   CODE                   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%drawnow


OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

CellMaskImage = CPretrieveimage(handles,['Segmented' CellMaskName],ModuleName,0,0);

ThisModuleFigureNumber = handles.Current.(['FigureNumberForModule',CurrentModule]);

detachFilterSize = 20;
putBackFilterSize = ceil(detachFilterSize);
smoothFilterSize = 3;
edgeThreshold = 0.005;
iterationNumber = 7;

SmoothedImage = imgaussfilt(OrigImage,smoothFilterSize);

stBig = strel('disk',detachFilterSize);
Detached = imerode(CellMaskImage,stBig);
stMiddle = strel('disk',putBackFilterSize);
Detached = imdilate(Detached,stMiddle);

EdgeImage = edge(SmoothedImage,'Sobel',edgeThreshold);

FilteredEdgeImage = EdgeImage;
FilteredEdgeImage(Detached>0) = 0;

idx = find(FilteredEdgeImage);

newFiloEdges = FilteredEdgeImage;

for i=1:iterationNumber %while ~isempty(idx) %
    [idx,newFiloEdges] = waterShed(idx,OrigImage,newFiloEdges,Detached);
end

FinalLabelMatrixImage = zeros(size(newFiloEdges));
conncomp = bwconncomp(newFiloEdges);
for i=1:length(conncomp.PixelIdxList)
    FinalLabelMatrixImage(conncomp.PixelIdxList{i}) = i;    
end

ColoredLabelMatrixImage = CPlabel2rgb(handles,FinalLabelMatrixImage);

% Story END and CP variable savings

StructuringElement = strel('square',3);
bitBiggerObjects = imdilate(FinalLabelMatrixImage,StructuringElement);
outlines = bitBiggerObjects - FinalLabelMatrixImage;
LogicalOutlines = outlines > 0;    

if ~isfield(handles.Measurements,ObjectName)
    handles.Measurements.(ObjectName) = {};
end

%%% Saves the final, segmented label matrix image of secondary objects to
%%% the handles structure so it can be used by subsequent modules.
fieldname = ['Segmented',ObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

handles = CPsaveObjectCount(handles, ObjectName, FinalLabelMatrixImage);
handles = CPsaveObjectLocations(handles, ObjectName, FinalLabelMatrixImage);

%%% Saves images to the handles structure so they can be saved to the hard
%%% drive, if the user requested.
try
    if ~strcmpi(SaveOutlines,'Do not save')        
        handles.Pipeline.(SaveOutlines) = LogicalOutlines;
    end
catch e
    error(['The object outlines were not calculated by the ', ModuleName, ' module, so these images were not saved to the handles structure. The Save Images module will therefore not function on these images. This is just for your information - image processing is still in progress, but the Save Images module will fail if you attempted to save these images.'])
end

if any(findobj == ThisModuleFigureNumber)        
        imagesc(ColoredLabelMatrixImage);
        title('detected filopodia');
end


function [newIdx,newFiloEdges] = waterShed(idx,OrigImage,newFiloEdges,banned)

imgSize = size(OrigImage);
newIdx = [];
for i=1:length(idx)    
    [c1,c2] = ind2sub(imgSize,idx(i));
    if c1>1 && c2>2 && c1<imgSize(1) && c2<imgSize(2)
        candPos = zeros(1,8);
        candPos(1) = sub2ind(imgSize,c1-1,c2-1);
        candPos(2) = sub2ind(imgSize,c1+0,c2-1);
        candPos(3) = sub2ind(imgSize,c1+1,c2-1);
        candPos(4) = sub2ind(imgSize,c1-1,c2);    
        candPos(5) = sub2ind(imgSize,c1+1,c2);
        candPos(6) = sub2ind(imgSize,c1-1,c2+1);
        candPos(7) = sub2ind(imgSize,c1+0,c2+1);
        candPos(8) = sub2ind(imgSize,c1+1,c2+1);
        cand = OrigImage(candPos);
        [~,maxPlace] = sort(cand,'descend');
        j = 1;
        while j<9 && (newFiloEdges(candPos(maxPlace(j))) || banned(candPos(maxPlace(j))) )
            j = j + 1;
        end
        if j < 9
            newFiloEdges(candPos(maxPlace(j))) = 1;    
            if mod(i,100)==0
                %imagesc(newFiloEdges); pause(0.01);
            end
            newIdx(end+1) = candPos(maxPlace(j));            
        end
    end
end