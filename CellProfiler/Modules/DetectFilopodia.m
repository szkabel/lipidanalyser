function handles = DetectFilopodia(handles)

% Help for the DetectFilopodia module
% Category: Object Processing
%
% SHORT DESCRIPTION:
% Detect Filopodia based on the FiloDetect MATLAB tool.
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Abel Szkalisity 2017.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[CurrentModule, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images you want to process?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What do you want to call the objects identified by this module?
%defaultVAR02 = Filopodia
%infotypeVAR02 = objectgroup indep
ObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = What is the size of one pixel in microns?
%defaultVAR03 = 0.111
pixel = str2double(handles.Settings.VariableValues{CurrentModuleNum,3});

%textVAR04 = Specify a name for the outlines of the identified objects! (optional)
%defaultVAR04 = Do not save
%infotypeVAR04 = outlinegroup indep
SaveOutlines = char(handles.Settings.VariableValues{CurrentModuleNum,4});

%%%VariableRevisionNumber = 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                   CODE                   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%drawnow


OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

ThisModuleFigureNumber = handles.Current.(['FigureNumberForModule',CurrentModule]);
[FinalLabelMatrixImage,histo,endp,BW,BW1,BW2,BW3,BW4,BW6] = filopodia_detection(OrigImage,1/pixel);

ColoredLabelMatrixImage = CPlabel2rgb(handles,FinalLabelMatrixImage);

% Story END and CP variable savings

StructuringElement = strel('square',3);
bitBiggerObjects = imdilate(FinalLabelMatrixImage,StructuringElement);
outlines = bitBiggerObjects - FinalLabelMatrixImage;
LogicalOutlines = outlines > 0;    

if ~isfield(handles.Measurements,ObjectName)
    handles.Measurements.(ObjectName) = {};
end

%%% Saves the final, segmented label matrix image of secondary objects to
%%% the handles structure so it can be used by subsequent modules.
fieldname = ['Segmented',ObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

handles = CPsaveObjectCount(handles, ObjectName, FinalLabelMatrixImage);
handles = CPsaveObjectLocations(handles, ObjectName, FinalLabelMatrixImage);

%%% Saves images to the handles structure so they can be saved to the hard
%%% drive, if the user requested.
try
    if ~strcmpi(SaveOutlines,'Do not save')        
        handles.Pipeline.(SaveOutlines) = LogicalOutlines;
    end
catch e
    error(['The object outlines were not calculated by the ', ModuleName, ' module, so these images were not saved to the handles structure. The Save Images module will therefore not function on these images. This is just for your information - image processing is still in progress, but the Save Images module will fail if you attempted to save these images.'])
end

if any(findobj == ThisModuleFigureNumber)
        subplot(3,3,1), imagesc(OrigImage);colormap gray; axis off;
        title('input image');
        subplot(3,3,2);
        plot(histo);
        set(gca,'xlim',[ 0 max(OrigImage(:)) ] );
        set(gca,'yticklabel',[]);
        title('intensity histogram');
        hold on;
        plot(endp,0,'*g');
        hold off;
        subplot(3,3,3),imagesc(BW);colormap gray;axis off;
        title('thresholding');
        subplot(3,3,4),imagesc(BW1);colormap gray;axis off;
        title('segmented cell');
        subplot(3,3,5), imagesc(BW2);colormap gray; axis off;
        title('candidates');
        subplot(3,3,6), imagesc(BW3); axis off, colormap gray;
        title('ellipses');
        draw_ellipse(BW3);
        subplot(3,3,7), imagesc(BW4);axis off,colormap gray;
        title('filtering');
        Ia=detect_fused(OrigImage,BW6,1);
        subplot(3,3,8),imagesc(Ia); axis off; colormap gray;
        title(['yellow:fused'  ' '  'pink:single']);
        subplot(3,3,9), imagesc(ColoredLabelMatrixImage) ; axis off;
        title('detected filopodia');
end

%% Functions bit modified from FiloDetect by Sharmin Nilufar

function [FinalLabelMatrixImage,histo,endp,BW,BW1,BW2,BW3,BW4,BW6] = filopodia_detection(II,pixel)

I = im2uint8(II);
histo=imhist(I);

%***************micron to pixel conversion***************
size_thresh=pixel*0.6;
size_thresh1=pixel*0.4;

%*************Cell Segmentation******************* 
%Triangle method for thresholding
[lehisto ~] = imhist(I);
[level]=triangle_th(lehisto,256);
startp=level*256;
localr=lehisto(startp+1:startp+10);
[lmin]=min(localr);
x1 = find(localr == lmin,1,'last');
endp=startp+x1;
levelf=(endp/256);
BW=im2bw(I,levelf); 

% ******************Cell body selection*******************
BW1=imfill(BW,'holes'); 
ccc=bwconncomp(BW1);
stats = regionprops(ccc, 'Area');
idx = find([stats.Area] == max([stats.Area]));
BW1 = ismember(labelmatrix(ccc), idx);

% *****************Filopodia detection**********************
BW2=image_split(BW1,size_thresh);
cc1=bwconncomp(BW2);

stats_major=regionprops(cc1, 'MajorAxisLength');
stats_minor=regionprops(cc1, 'MinorAxisLength');

idx=find([stats_major.MajorAxisLength]>1.5*[stats_minor.MinorAxisLength]);
BW3=ismember(labelmatrix(cc1), idx);

cc2=bwconncomp(BW3);
stats_major1=regionprops(cc2, 'MajorAxisLength');
stats_minor1=regionprops(cc2, 'MinorAxisLength');
c_stats_minor=struct2cell(stats_minor1);
sum_minor=0;
for k = 1:numel(c_stats_minor)
    sum_minor=sum_minor+c_stats_minor{k};
end;
avg_minor=sum_minor/numel(c_stats_minor);

BW4=image_split(BW1,ceil(avg_minor));


BW5= bwmorph(BW4, 'thin',inf);  
cc = bwconncomp(BW5);
stats = regionprops(cc, 'Area');
idx1 = find([stats.Area] >(size_thresh1));
cc3 = bwconncomp(BW4);
BW6=ismember(labelmatrix(cc3), idx1);

%********************* Detect fused filopodia***********************

FinalLabelMatrixImage = zeros(size(II));
R = regionprops(BW6,'PixelIdxList');
for i=1:length(R)
    FinalLabelMatrixImage(R(i).PixelIdxList) = i;
end


function Ia=detect_fused(I,BW8,draw)

Ia=I;
Ibw = bwmorph(BW8, 'thin', Inf);
Ibw(1,:) = 0;
BW8label = bwlabel(BW8,8);

%fig,imagesc(I), colormap gray, axis off; hold on; drawnow;
stats_bbox=regionprops(Ibw, 'BoundingBox');


for iCount=1:length(stats_bbox),
    %[y_perim, x_perim] = find(bwperim(BW8label == iCount)==1);
     [x,y] = find((BW8label == iCount)==1);

    Bounding_box = stats_bbox(iCount).BoundingBox;
    y_min = Bounding_box(2);
    y_max = fix(Bounding_box(2)) + Bounding_box(4); 
    x_min = Bounding_box(1);
    x_max = fix(Bounding_box(1)) + Bounding_box(3); 
    I1 = Ibw(ceil(y_min):ceil(y_max),ceil(x_min):ceil(x_max));
    I_smallbw = I1;
    ccc=bwconncomp(I_smallbw);
    stats = regionprops(ccc, 'Area');
    idx = find([stats.Area] == max([stats.Area]));
    I_smallbw = ismember(labelmatrix(ccc), idx);
    bw = bwmorph(I_smallbw, 'thin', Inf);
    
    bw1 = bwmorph(bw, 'branchpoints');
    bw2 = bwmorph(bw, 'endpoints');
    if (draw==1)
        
    if  (sum(sum(bw1))>1 && sum(sum(bw2)) >=4), 
        %plot(x_perim, y_perim, '.y', 'markersize',2);
        for i=1:length(x)
            for j=1:length(y)
                Ia(x(i),y(i),1)=255;
                Ia(x(i),y(i),2)=255;
                Ia(x(i),y(i),3)=0;
            end
        end
              
    else
        %plot(x_perim, y_perim, '.r', 'markersize',2); 
        for i=1:length(x)
            for j=1:length(y)
                Ia(x(i),y(i),1)=255;
                Ia(x(i),y(i),2)=0;
                Ia(x(i),y(i),3)=255;
            end
        end
        
       
    end
    end
end



function draw_ellipse(BW)
    s = regionprops(BW, 'Orientation', 'MajorAxisLength', ...
        'MinorAxisLength', 'Eccentricity', 'Centroid','ConvexHull');
    phi = linspace(0,2*pi,50);
    cosphi = cos(phi);
    sinphi = sin(phi);
    
    for k = 1:length(s)
        xbar = s(k).Centroid(1);
        ybar = s(k).Centroid(2);
        
        a = s(k).MajorAxisLength/2;
        b = s(k).MinorAxisLength/2;
        
        theta = pi*s(k).Orientation/180;
        R = [ cos(theta)   sin(theta)
            -sin(theta)   cos(theta)];
        xy = [a*cosphi; b*sinphi];
        xy = R*xy;
        x = xy(1,:) + xbar;
        y = xy(2,:) + ybar;
        hold on;
        plot(x,y,'r','LineWidth',0.2);
        
    end
    
function RBW= image_split(BW1, size_thresh)
    size_se=ceil(size_thresh);
    se = strel('disk',size_se);

    BW4=imopen(BW1,se);
    BW5=(BW1-BW4);

    cc = bwconncomp(BW5);

    stats = regionprops(cc, 'Area');
    idx = find([stats.Area] > (size_thresh));
    RBW = ismember(labelmatrix(cc), idx);


function [level]=triangle_th(lehisto,num_bins)
%     Triangle algorithm
%     This technique is due to Zack (Zack GW, Rogers WE, Latt SA (1977), 
%     "Automatic measurement of sister chromatid exchange frequency", 
%     J. Histochem. Cytochem. 25 (7): 741�53, )
%     A line is constructed between the maximum of the histogram at 
%     (b) and the lowest (or highest depending on context) value (a) in the 
%     histogram. The distance L normal to the line and between the line and 
%     the histogram h[b] is computed for all values from a to b. The level
%     where the distance between the histogram and the line is maximal is the 
%     threshold value (level). This technique is particularly effective 
%     when the object pixels produce a weak peak in the histogram.

%     Use Triangle approach to compute threshold (level) based on a
%     1D histogram (lehisto). num_bins levels gray image. 

%     INPUTS
%         lehisto :   histogram of the gray level image
%         num_bins:   number of bins (e.g. gray levels)
%     OUTPUT
%         level   :   threshold value in the range [0 1];
% 
%     Dr B. Panneton, June, 2010
%     Agriculture and Agri-Food Canada
%     St-Jean-sur-Richelieu, Qc, Canad
%     bernard.panneton@agr.gc.ca


%   Find maximum of histogram and its location along the x axis
    [h,xmax]=max(lehisto);
    xmax=round(mean(xmax));   %can have more than a single value!
    h=lehisto(xmax);
    
%   Find location of first and last non-zero values.
%   Values<h/10000 are considered zeros.
    indi=find(lehisto>h/10000);
    fnz=indi(1);
    lnz=indi(end);

%   Pick side as side with longer tail. Assume one tail is longer.
    lspan=xmax-fnz;
    rspan=lnz-xmax;
    if rspan>lspan  % then flip lehisto
        lehisto=fliplr(lehisto');
        a=num_bins-lnz+1;
        b=num_bins-xmax+1;
        isflip=1;
    else
        lehisto=lehisto';
        isflip=0;
        a=fnz;
        b=xmax;
    end
    
%   Compute parameters of the straight line from first non-zero to peak
%   To simplify, shift x axis by a (bin number axis)
    m=h/(b-a);
    
%   Compute distances
    x1=0:(b-a);
    y1=lehisto(x1+a);
    beta=y1+x1/m;
    x2=beta/(m+1/m);
    y2=m*x2;
    L=((y2-y1).^2+(x2-x1).^2).^0.5;

%   Obtain threshold as the location of maximum L.    
    level=find(max(L)==L);
    level=a+mean(level);
    
%   Flip back if necessary
    if isflip
        level=num_bins-level+1;
    end
    
    level=level/num_bins;        

