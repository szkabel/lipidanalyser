function handles = RunIlastik(handles)

% Help for the Run Ilastik module:
% Category: Image Processing
%
% SHORT DESCRIPTION:
% Uses the headless operation of Ilastik to run a previously prepared
% Ilastik project on the given image.
%
% WORKS CURRENTLY ONLY FOR WINDOWS AND PIXEL CLASSIFICATION.
% *************************************************************************
%
%
% Settings:

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Abel Szkalisity 2023.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images you want to process?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What do you want to call the probability mask of object #1
%produced by Ilastik?
%defaultVAR02 = ProbabilityObject1
%infotypeVAR02 = imagegroup indep
predProbsName{1} = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = What do you want to call the probability mask of object #2
%produced by Ilastik?
%defaultVAR03 = /
%infotypeVAR03 = imagegroup indep
predProbsName{2} = char(handles.Settings.VariableValues{CurrentModuleNum,3});

%textVAR04 = What do you want to call the probability mask of object #3
%produced by Ilastik?
%defaultVAR04 = /
%infotypeVAR04 = imagegroup indep
predProbsName{3} = char(handles.Settings.VariableValues{CurrentModuleNum,4});

%textVAR05 = What do you want to call the probability mask of object #4
%produced by Ilastik?
%defaultVAR05 = /
%infotypeVAR05 = imagegroup indep
predProbsName{4} = char(handles.Settings.VariableValues{CurrentModuleNum,5});

%textVAR06 = Specify the Ilastik project with its full path
%defaultVAR06 = myPath\myIlastikProject.ilp
ilastikProjName = char(handles.Settings.VariableValues{CurrentModuleNum,6});

%pathnametextVAR07 = Where is ilastik located on your computer?
%defaultVAR07 = C:\Program Files\ilastik-1.3.2\
ilastikLocation = char(handles.Settings.VariableValues{CurrentModuleNum,7});

%textVAR08 = What was the original data type of this image?
%choiceVAR08 = uint16
%choiceVAR08 = uint8
%inputtypeVAR08 = popupmenu
dataType = char(handles.Settings.VariableValues{CurrentModuleNum,8});


%%%VariableRevisionNumber = 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PRELIMINARY CALCULATIONS & FILE HANDLING %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%drawnow

% The "fixed" area segmentation does a morphological dilatation on each
% a-trous wavelet level with the size of the convolution filter used on
% that level. (i.e. expands the detection with a circular corresponding to
% the circle to which the filter on that level is sensitive)

%%% Reads (opens) the image you want to analyze and assigns it to a
%%% variable.
OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

% check for the working directory of the OA Project
global Project;
if ~isempty(Project)
    wrkDir = Project.workDir;
else
    wrkDir = '.';
end

tmpLocation = fullfile(wrkDir,'ilastikTmp');
if ~exist(tmpLocation,'dir'), mkdir(tmpLocation); end
tmpFileName = 'ilastikInputImg';
inImgLoc = fullfile(tmpLocation,[tmpFileName '.png']);
switch dataType
    case 'uint16'
        imwrite(uint16(OrigImage.*2^16),inImgLoc);
    case 'uint8'
        imwrite(uint8(OrigImage.*2^8),inImgLoc);
end

% System call to Ilastik
[status,cmd] = system(...
    ['"' fullfile(ilastikLocation,'ilastik.exe') '" ' ...
    '--headless --project="' ilastikProjName '" ' ...
    '--output_format=tif ' ...
    '--export_source=Probabilities ' ...
	'--output_filename_format="' fullfile(tmpLocation,'{nickname}_results.tif') '" ' ...
    '"' inImgLoc '"' ...
    ]...
    );

if (status~=0)
    error(['A problem occured when trying to run Ilastik. Please double check the location of Ilastik and your project. Error message: ' cmd]);
end

ilastikResImg = imread(fullfile(tmpLocation,[tmpFileName '_results.tif']));

%%% Saves the final, segmented label matrix image of secondary objects to
%%% the handles structure so it can be used by subsequent modules.
for i=1:length(predProbsName)
    if ~strcmp(predProbsName{i},'/')
        fieldname = predProbsName{i};
        handles.Pipeline.(fieldname) = double(ilastikResImg(:,:,i))./getMaxImgValFromClass(ilastikResImg(:,:,i));
    end
end
