function [ regions ] = CPwater( OrigImage, pFmin, AmMin, ApMin )
% water algorithm for segmenting regions based on the paper:
% Eli Zamir et al. Molecular diversity of cell-matrix adhesions
% Journal of Cell Science 112. 1655-1669 (1999)
%
% INPUT:
%   OrigImage: the image to be segmented
%   pfMin: The minimum intensity value for thresholding
%   AmMin: The critical area for merging
%   ApMin: The threshold for minimum patch area (to exclude noise)
%
% Written by: Abel Szkalisity 2017.

regions = zeros(size(OrigImage));
 
[pixelList,pixelIndices] = sort(OrigImage(:),'descend');

lastPos = find(pixelList<pFmin,1,'first');
pixelIndices(1:lastPos+1:end) = [];

regionList = cell(1,lastPos);
lastRegion = 1;

for i=1:lastPos
    px = fetchNeighbours(regions,pixelIndices(i));
    if all(px == 0) %if it does not touch with any old patch
        regions(pixelIndices(i)) = lastRegion;
        regionList{lastRegion}.size = 1;
        regionList{lastRegion}.pxIdx = pixelIndices(i);
        lastRegion = lastRegion + 1;
    elseif sum(px ~= 0) == 1
        regionToPut = px(find(px,1,'First'));
        regions(pixelIndices(i)) = regionToPut;
        regionList{regionToPut}.size = regionList{regionToPut}.size+1;
        regionList{regionToPut}.pxIdx(end+1) = pixelIndices(i);
    else
        patches = px(px ~= 0);
        j = 1;
        while j <= length(patches)
            if regionList{patches(j)}.size>AmMin
                patches(j) = [];
            else
                j = j+1;
            end
        end         
        targetPatch = min(patches);
        for j = sort(patches,'descend')
            if j~=targetPatch && ~isempty(regionList{j})
                regions(regionList{j}.pxIdx) = targetPatch;
                regionList{targetPatch}.size = regionList{targetPatch}.size + regionList{j}.size;
                regionList{targetPatch}.pxIdx = [regionList{targetPatch}.pxIdx regionList{j}.pxIdx];
                regionList{j} = [];
            end
        end
        if isempty(targetPatch)
            targetPatch = px(find(px,1,'first'));
        end
        regions(pixelIndices(i)) = targetPatch;
        regionList{targetPatch}.size = regionList{targetPatch}.size + 1;
        regionList{targetPatch}.pxIdx = [regionList{targetPatch}.pxIdx pixelIndices(i)];
    end
        
end

counter = 1;
for i=1:length(regionList)
    if ~isempty(regionList{i})
        if regionList{i}.size>ApMin
            regions(regionList{i}.pxIdx) = counter;
            counter = counter + 1;
        else
            regions(regionList{i}.pxIdx) = 0;
        end
    end    
end


end

function px = fetchNeighbours(OrigImage,ind)
    [m,n] = size(OrigImage);    
    [i,j] = ind2sub([m,n],ind);
    c = 1;
    if i>1, px(c) = OrigImage(i-1,j); c = c+1; end
    if i<m, px(c) = OrigImage(i+1,j); c = c+1; end    
    if j>1, px(c) = OrigImage(i,j-1); c = c+1; end
    if j<n, px(c) = OrigImage(i,j+1); end    
end

