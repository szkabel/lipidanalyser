function varargout = ModuleHandler_GUI(varargin)
% MODULEHANDLER_GUI MATLAB code for ModuleHandler_GUI.fig
%      MODULEHANDLER_GUI, by itself, creates a new MODULEHANDLER_GUI or raises the existing
%      singleton*.
%
%      H = MODULEHANDLER_GUI returns the handle to a new MODULEHANDLER_GUI or the handle to
%      the existing singleton*.
%
%      MODULEHANDLER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MODULEHANDLER_GUI.M with the given input arguments.
%
%      MODULEHANDLER_GUI('Property','Value',...) creates a new MODULEHANDLER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ModuleHandler_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ModuleHandler_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
%  NOTES: There are several fields that are stored in the modules (windows)
%  separately and they are not fully synchronized with the Project variable
%  in all cases.
% 
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ModuleHandler_GUI

% Last Modified by GUIDE v2.5 18-Nov-2019 18:48:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ModuleHandler_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ModuleHandler_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ModuleHandler_GUI is made visible.
function ModuleHandler_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ModuleHandler_GUI (see VARARGIN)

% Choose default command line output for ModuleHandler_GUI
handles.output = hObject;

% A variable to store all Project related things in one place.

%initProject(handles)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ModuleHandler_GUI wait for user response (see UIRESUME)
% uiwait(handles.ModuleHandlerFig);


% --- Outputs from this function are returned to the command line.
function varargout = ModuleHandler_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in button_CP.
function button_CP_Callback(hObject, eventdata, handles)
% hObject    handle to button_CP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of button_CP

global Project;
[constructBool] = changeModuleWindow(hObject,handles);

if constructBool    
    Project.figHandles.CPHandle = CellProfiler;
    %TODO reload the settings from the CPHandle as it was before
    if isFieldRec(Project,{'IAWin','CPHandles','Settings'})
        putSettingsToCPGUI(Project.IAWin.CPHandles.Settings,Project.figHandles.CPHandle,[],guidata(Project.figHandles.CPHandle));
    end
    if isFieldRec(Project,{'CPWin','imgPath'})
        cphandles = guidata(Project.figHandles.CPHandle);
        cphandles.DefaultImageDirectoryEditBox.String = Project.CPWin.imgPath;
        CellProfiler('DefaultImageDirectoryEditBox_Callback',cphandles.DefaultImageDirectoryEditBox, [], cphandles);
    end
end




% --- Executes on button press in button_IA.
function button_IA_Callback(hObject, eventdata, handles)
% hObject    handle to button_IA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of button_IA

global Project;
[constructBool] = changeModuleWindow(hObject,handles);

if constructBool
    Project.figHandles.IAHandle = InteractiveWindow();
end

% --- Executes on button press in button_OA.
function button_OA_Callback(hObject, eventdata, handles)
% hObject    handle to button_OA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of button_OA

global Project;
[constructBool] = changeModuleWindow(hObject,handles);

if constructBool
    Project.figHandles.OAHandle = ObjectAnalyser_GUI(Project.ECP);
end



% --------------------------------------------------------------------
function toolbar_new_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbar_new (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

paramcell = {...
    struct('name','Project folder','type','dir')};
try
    paramarray = Settings_GUI(paramcell,'infoText','Please choose an empty directory for your project.','infoAlignment','center','checkFunction',@checkProjectSettings,'title','New project');
catch e
    if ~strcmp(e.identifier,'Settings_GUI:noParameterProvided')
        rethrow(e);
    end
    return;
end

%Defining the new Project, clearing up the previous one if exists.
global Project;
if ~isempty(Project)
    saveProjectToDisk();
    closeFigures();
    Project = [];
end
initProject(handles)
Project.workDir = paramarray{1};

updateModuleHandlerActivity(handles);

function [bool,msg] = checkProjectSettings(paramarray)
    bool = 1;
    msg = '';
    if ~exist(paramarray{1},'dir')
        bool = 0;
        msg = 'The project folder does not exist!';
    else
        d = dir(paramarray{1});
        if numel(d)>2
            bool = 0;
            msg = 'A new project folder must be empty!';
        end
    end


% --- Executes when user attempts to close ModuleHandlerFig.
function ModuleHandlerFig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to ModuleHandlerFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

saveProjectToDisk();
closeFigures();
global Project;
%If we delete then close the DB connection
if isfield(Project,'ECP'), Project.ECP.prepareForSave(); end
clear('global', 'Project');

delete(hObject);

% --------------------------------------------------------------------
function toolbar_open_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbar_open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

dirPath = uigetdir('Please choose a project directory');
% Exit in case the window was closed without selecting a directory
if isnumeric(dirPath) && dirPath == 0
    return;
end

global Project;

if ~isempty(Project)
    saveProjectToDisk();
    closeFigures();
    Project = []; %Clearing project
end

initProject(handles)

%TODO check if it is a proper object directory (i.e. has the hidden project file)

%TODO check that all instances of stored work dir is rewritten (e.g. in
%ECP) -> This should be solved as ECP is always a handle and not saved any
%more to disk
try
lP = load(fullfile(dirPath,Project.projFileName),'projToSave');
catch err
    if strcmp(err.identifier,'MATLAB:load:couldNotReadFile')
        errordlg('The directory selected is not a valid project directory');
        return;
    else
        rethrow(err)        
    end
end

Project = lP.projToSave;
Project.workDir = dirPath;
%In the saved projects figHandles are ripped --> re-init the project
initProject(handles)

if isfield(Project,'ECP')    
    Project.ECP.setWorkFolder(Project.workDir);
    Project.ECP.afterReOpen();
    
    %AAAnd here it should be also checked if the raw images were not moved
    %to somewhere else.
    %TODO: check also if in Project.IAWin.CPHandles the default image
    %directory is not existing, and replace also that one!
    try
        chanList = Project.ECP.listImageChannels();
        chanDirs = Project.ECP.getChannelDirectories();
        paramcell = cell(1,length(chanList));
        updateNeeded = false(1,length(chanList));
        for i=1:length(chanList)
            if ~exist(chanDirs{i},'dir')
                paramcell{i}.name = chanList{i};
                paramcell{i}.type = 'dir';
                paramcell{i}.default = chanDirs{i};
                updateNeeded(i) = true;
            end
        end
    catch err
        errordlg('Unexpected error occured')        
        disp(getReport(err));
        clear('global', 'Project');
        initProject(handles)
        return;
    end
    try
        if any(updateNeeded)
            newChans = Settings_GUI(paramcell(updateNeeded),'infoText',sprintf('The following image directories are missing from your computer.\nPlease specify their new location.'),'Title','');
            chanDirs(updateNeeded) = newChans;
            Project.ECP.setChannelDirectories(chanDirs);
        end
    catch err
        %Cleaning up
        if strcmp(err.identifier,'Settings_GUI:noParameterProvided')
            clear('global', 'Project');
            initProject(handles)
        else
            rethrow(err);
        end
    end
end

if isfieldRecursive(Project,'IAWin.CPHandles.Settings')
    Project.IAWin.CPHandles.Settings = repairPIPE(Project.IAWin.CPHandles.Settings);
end

% backward compatibility
if isfieldRecursive(Project,'IAWin.CPHandles.Settings') && isfieldRecursive(Project,'IAWin.up2Date')
    nofModules = length(Project.IAWin.CPHandles.Settings.ModuleNames);
    if size(Project.IAWin.up2Date,1) ~= nofModules, Project.IAWin.up2Date = repmat(Project.IAWin.up2Date,nofModules,1); end
end

updateModuleHandlerActivity(handles);


