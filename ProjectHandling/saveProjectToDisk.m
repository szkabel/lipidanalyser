function saveProjectToDisk()
%Saves data to disk, clears handles
%Note that the project variable needs to be initialized after calling this.

% TODO: notes for users:
% - the pipeline is not saved until it is submitted

global Project;

if isfield(Project,'workDir') && exist(Project.workDir,'dir')
    screensize = get(0,'Screensize');
    infoD = dialog('Name','Saving project...','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
    UC = uicontrol('Parent',infoD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','Saving visualization settings...');
    pause(0.05);
    
    %Save the IA state into the project
    storeIAWindowToProject();
    storeOAWindowToProject();
    
    projToSave = Project;
    %Removing figure handles
    projToSave.figHandles = [];

    if ishandle(UC), set(UC,'String','Removing figure handles from CellProfiler structure...'); end; pause(0.05);
    if isFieldRec(projToSave,{'IAWin','CPHandles'})
       projToSave.IAWin.CPHandles = cleanGUIfromCPHandles(projToSave.IAWin.CPHandles); % burnt-in much uglier but way much quicker solution. Also more readable.
       %projToSave.IAWin.CPHandles = iterateOverCellAndStruct(projToSave.IAWin.CPHandles,@removeHandle);
    end
    
    if checkHandleExistance('CPHandle')
        cphandles = guidata(Project.figHandles.CPHandle);
        projToSave.CPWin.imgPath = cphandles.DefaultImageDirectoryEditBox.String;
    end

    if isfield(projToSave,'ECP')
        projToSave.ECP.prepareForSave();
    end

    if ishandle(UC), set(UC,'String','Saving project to disk...'); end; pause(0.05);
    if isfield(projToSave,'workDir')
        save(fullfile(projToSave.workDir,projToSave.projFileName),'projToSave');
    end
    if ishandle(infoD), close(infoD); end
    
end

end

function output = removeHandle(input)
    if ishandle(input)
        output = [];
    elseif any(isgraphics(input))
        output = [];
    else
        output = input;
    end
end

