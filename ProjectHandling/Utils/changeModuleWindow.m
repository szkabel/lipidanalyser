function [constructBool] = changeModuleWindow(hObject,handles)
%If it gives back true then further construction of the gui handle is
%needed.

global Project;
neededTag = hObject.Tag;
tmp = strsplit(neededTag,'_'); neededItem = tmp{end};
figureTag = [neededItem 'Handle'];

constructBool = 0;

state = get(hObject,'Value');
if state == 1
    if checkHandleExistance(figureTag)
        Project.figHandles.(figureTag).Visible = 'on';
    else
        constructBool = 1;
    end
    hideNotNeededModules(neededTag,handles);
elseif state == 0
    if checkHandleExistance(figureTag)
        Project.figHandles.(figureTag).Visible = 'off';
    end
end

end

