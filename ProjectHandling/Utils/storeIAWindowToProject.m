function storeIAWindowToProject()

global Project;

if checkHandleExistance('IAHandle')
    uData = get(Project.figHandles.IAHandle,'UserData');
    Project.IAWin.up2Date = uData.up2Date;
    Project.IAWin.visualizationSettings = uData.visualizationSettings;
end

end

