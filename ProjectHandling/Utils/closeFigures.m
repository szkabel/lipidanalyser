function closeFigures()
global Project;

    %Closing figures
    if checkHandleExistance('CPHandle')
        CellProfiler('ClosingFunction',Project.figHandles.CPHandle);
    end
    if checkHandleExistance('IAHandle')
        delete(Project.figHandles.IAHandle);
    end
    if checkHandleExistance('OAHandle')
        ObjectAnalyser_GUI('figure1_CloseRequestFcn',Project.figHandles.OAHandle,[],[]);
    end

end

