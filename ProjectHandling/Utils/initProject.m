function initProject(projGUIhandles)
%Initialize the project as such

global Project;

Project.projFileName = '.cellAnalysisProject.mat';
Project.figHandles.ModuleFigHandles = projGUIhandles;

projGUIhandles.button_CP.Value = 0;
projGUIhandles.button_IA.Value = 0;
projGUIhandles.button_OA.Value = 0;

end

