function bool = checkHandleExistance(handleTag)
    global Project;
    bool = isFieldRec(Project,{'figHandles',handleTag}) && ~isempty(Project.figHandles.(handleTag)) && ishandle(Project.figHandles.(handleTag));
end

