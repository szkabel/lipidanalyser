function hideNotNeededModules(neededTag,handles)

buttonTags = {'button_CP','button_IA','button_OA'};

for i=1:length(buttonTags)
    if ~strcmp(neededTag,buttonTags{i})
        handles.(buttonTags{i}).Value = 0;
        eventdata = []; % empty eventdata
        ModuleHandler_GUI([buttonTags{i} '_Callback'],handles.(buttonTags{i}),eventdata,handles);
    end    
end

end

