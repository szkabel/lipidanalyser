function updateModuleHandlerActivity(handles)
% We currently have 3 buttons which should be enabled/disabled according to
% the Project's state.

global Project;

if isfield(Project,'workDir')
    handles.button_CP.Enable = 'on';
else
    handles.button_CP.Enable = 'off';
end

if isfield(Project,'IAWin')
    handles.button_IA.Enable = 'on';
else
    handles.button_IA.Enable = 'off';
end

%A bit difficult way for the OA window open
state = 'off';
if isfield(Project,'ECP')
    if ~isempty(Project.ECP.getImageNames())
        state = 'on';        
    end
end
handles.button_OA.Enable = state;

end

