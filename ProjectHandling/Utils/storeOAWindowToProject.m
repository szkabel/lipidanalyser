function storeOAWindowToProject()

global Project;

if checkHandleExistance('OAHandle')
    uData = get(Project.figHandles.OAHandle,'UserData');
    Project.OAWin.uData = uData;
    
    %Store also GUI state and fields
    Project.OAWin.gui = storeObjectAnalyserGUIstate(guidata(Project.figHandles.OAHandle),'store');    
end

end

