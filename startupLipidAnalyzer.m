%To startup the functionalities of the Lipid analyser (CellProfiler or ObjectAnalyser)

[LIPIDPATH, ~, ~] = fileparts(mfilename('fullpath'));
addpath(genpath(fullfile(LIPIDPATH,'CellProfiler')));
addpath(genpath(fullfile(LIPIDPATH,'ECP')));
addpath(genpath(fullfile(LIPIDPATH,'InteractiveTool')));
addpath(genpath(fullfile(LIPIDPATH,'ObjectAnalyser')));
addpath(genpath(fullfile(LIPIDPATH,'Utils')));
addpath(genpath(fullfile(LIPIDPATH,'third-party')));
addpath(genpath(fullfile(LIPIDPATH,'ProjectHandling')));
addpath(LIPIDPATH);

% OLD OPENING:
%answer = questdlg('Which software you wish to use?','Welcome to the Lipid Analyser','Cell Profiler','Object Analyser','Object Analyser');

%if strcmp(answer,'Cell Profiler')
%    CellProfiler;
%elseif strcmp(answer,'Object Analyser')
%    ObjectAnalyser_GUI;
%end

% NEW OPENING:

ModuleHandler_GUI();