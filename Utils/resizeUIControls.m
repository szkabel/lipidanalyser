function  resizeUIControls(paramTextHandles, paramEditHandles,sizeOfPanel,heightOfItems, margin)
%resizes the handles of text and edit in the generateUI framework to match
%the new panel size

sumHeightItems = 0;

%For all objects
for i=1:length(paramTextHandles)
    currentPos = get(paramTextHandles{i},'Position');
    currentHeight = currentPos(4);
    sumHeightItems = sumHeightItems + currentHeight;
    editPosition = [sizeOfPanel(3)/2, sizeOfPanel(4)-margin(2)-sumHeightItems-(i-1)*heightOfItems*0.5, sizeOfPanel(3)/2-margin(1)*2 currentHeight];
    set(paramEditHandles{i},'Position',editPosition);
    set(paramTextHandles{i},'Position',[margin(1)/2, sizeOfPanel(4)-margin(2)-sumHeightItems-(i-1)*heightOfItems*0.5, sizeOfPanel(3)/2-margin(1)*2 currentHeight]);    
end


end

