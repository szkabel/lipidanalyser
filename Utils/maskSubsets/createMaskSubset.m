% This script creates all possible subsets of masks (i.e. their all
% possible combination of intersections)
% The input dir must contain the subfolders listed in maskList
% WORKS ONLY FOR SINGLE-LAYER MASKS!

inputDir = 'L:\ltdk_ikonen\Abel\Lauri\SRS\2024-07-03_A431DL-D38Chol\OAProjWithFluo\Masks';
tgtDir = 'L:\ltdk_ikonen\Abel\Lauri\SRS\2024-07-03_A431DL-D38Chol\OAProjWithFluo\maskSubsets';
maskList = {'ER','Mitochondria'};
tgtDir = fullfile(tgtDir,strjoin(maskList,'-'));
mkdirIfNotExist(tgtDir);
imgExt = 'png';

N = length(maskList);
% The total number of subsets excluding the non-interesting 0:
nofSubsets = 2^N-1;
d = dir(fullfile(inputDir,maskList{1},['*.' imgExt])); % the mask image names MUST MATCH IN ALL FOLDERS, so we just use the first one

waba(0,numel(d),'Creating mask subsets');
for j=1:numel(d)        
    waba(j,numel(d),'Creating mask subsets');
    currImgName = d(j).name;
    imgSet = cell(1,1,N);
    for k =1:N
        imgSet{k} = imread(fullfile(inputDir,maskList{k},currImgName));
    end
    img = cell2mat(imgSet);
    r = size(img,1);
    c = size(img,2);
    base = max(img(:));
    labelMatrix = sum(repmat(reshape(double(base).^(0:N-1),1,1,N),r,c).*double(img),3);
    labelMatrix  = subsetMask(labelMatrix); % making indexing consecutive % THIS LOOSES OBJECT ID BACKTRACKING!
    logImg = img>0;
    for i = 1:nofSubsets       
        currentSubStr = sprintf(['%0' num2str(N) 's'],dec2bin(i));
        logicRep = reshape(currentSubStr == repmat('1',1,N),1,1,[]);
        currOutDir = fullfile(tgtDir,strjoin(maskList(logicRep),'_'));
        mkdirIfNotExist(currOutDir);
        specificImg = all(logImg == repmat(logicRep,r,c),3);
        resImg = zeros(size(specificImg));
        resImg(specificImg) = labelMatrix(specificImg);
        imwrite(uint16(resImg), fullfile(currOutDir,currImgName));
    end
end
waba(numel(d),numel(d),'Creating mask subsets');