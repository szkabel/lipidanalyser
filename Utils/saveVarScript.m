% AUTHOR:   Abel Szkalisity
% DATE:     January 31, 2017
% NAME:     saveVarScript
%
% Save variables script. The parameters are provided by defining variables
% before calling the script. It handles if the variable is too big and
% saves with the proper version.
% This is a script to overcome data communication overhead with function
% calls.
%
% INPUT:
%   varToSave       The name of the variable to save as a string.
%   SavePath        The path to save to
%   SaveFile        The filename to save to
%
% OUTPUT:
%   outimg      	Contain the table for containing the "icon images".
%   className   	Name of the class.
%   mapping     	Contains PlateName, ImageName, OriginalImageName and
%               	CellNumber for each cell shown.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

dataInfo = whos(varToSave);
if dataInfo.bytes>=2*1024*1024*1024
    save([SavePath SaveFile], varToSave, '-v7.3');
else
    save([SavePath SaveFile], varToSave);
end   