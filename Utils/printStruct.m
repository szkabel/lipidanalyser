function printStruct(str,f,level)
% print struct to f file handle

fn = fieldnames(str);
for i=1:length(fn)
    for j=1:level
        fprintf(f,'\t');
    end
    fprintf(f,'%s: ',fn{i});
    switch class(str.(fn{i}))
        case 'double'
            fprintf(f,'%f ',str.(fn{i}));
        case 'char'
            fprintf(f,'%s',str.(fn{i}));
        case 'struct'
            fprintf(f,'\n');
            printStruct(str.(fn{i}),f,level+1);
    end
    fprintf(f,'\n');
end

end

