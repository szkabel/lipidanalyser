function flushData(matrixToSave,headers,filePath,fileName,extention, headerColumn, append)
%save matrix to file with a given header
%you may ask to include a header column for each row by setting the input
%headerColumn to a non-empty string. The value of the string is going to be
%the header. By setting the append boolean input variable to true you ask
%for not rewriting or creating the file but append to it instead. If the
%file to which you wish to append is already exist then the function does
%not flush the headers row into it. 
    headerColumnBool = ~isempty(headerColumn);
    OSFileName = fullfile(filePath,[fileName extention]);
    includeHeader = 1;
    if exist(OSFileName,'file'), includeHeader = 0; end
    if (append)
        fileToWrite = fopen(OSFileName,'a');
    else
        fileToWrite = fopen(OSFileName,'w');
    end
    columnWidth = length(headers);
    if headerColumnBool, columnWidth = columnWidth + 1; end
    if includeHeader
        if ~isempty(headers)
            singleLine = cell(1,columnWidth);
            k = 1;
            if (headerColumnBool), singleLine{k} = sprintf('ImageName,'); k = k+1; end
            for i=1:length(headers)-1
                singleLine{k} = sprintf('%s,',headers{i});
                k = k + 1;
            end    
            singleLine{k} = sprintf('%s\n',headers{end});
            fwrite(fileToWrite,horzcat(singleLine{:}));
        end
    end
    
    for i=1:size(matrixToSave,1)        
        singleLine = cell(1,columnWidth);
        k = 1;
        if (headerColumnBool), singleLine{k} = sprintf('%s,',headerColumn); k = k+1; end
        for j=1:size(matrixToSave,2)-1
            singleLine{k} = sprintf('%f,',matrixToSave(i,j));
            k = k + 1;
        end
        singleLine{k} = sprintf('%f\n',matrixToSave(i,end));
        fwrite(fileToWrite,horzcat(singleLine{:}));
    end
    
    fclose(fileToWrite);
end

