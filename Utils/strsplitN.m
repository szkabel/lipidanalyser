function res = strsplitN(str,delim,id)
    spltStr = strsplit(str,delim);
    if ischar(id)
        res = eval(['strjoin(spltStr(' id '),delim);']);
    else
        if length(id)>1
            res = strjoin(spltStr(id),delim);
        else
            res = spltStr{id};
        end
    end
end