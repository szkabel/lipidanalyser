function [posInImage,currPosWithinAxes,xStart,xExtent,yStart,yExtent,clickInImageBool] = ...
    calculateCursorPositionInImageAxes(axesPos,currPos,imgXYRatio,xLims,yLims)

    xPixExt = xLims(2) - xLims(1);
    yPixExt = yLims(2) - yLims(1);

    currPosWithinAxes = currPos - axesPos(1:2);
    %Flipping y
    currPosWithinAxes(2) = axesPos(4) - currPosWithinAxes(2);
    
    %Calculating within axes start of the image plot    
    axesXYRatio = axesPos(3)/axesPos(4);
    if axesXYRatio <= imgXYRatio 
        yExtent = axesPos(3)/imgXYRatio;
        yStart = (axesPos(4)-yExtent)/2;
        xStart = 0;
        xExtent = axesPos(3);
    else 
        xExtent = axesPos(4)*imgXYRatio;
        xStart = (axesPos(3)-xExtent)/2;
        yStart = 0;
        yExtent = axesPos(4);
    end
                    
    posWithinImgPlot = currPosWithinAxes - [xStart, yStart];    
    xPixRes = xPixExt / xExtent;
    yPixRes = yPixExt / yExtent;

    xPixRel = posWithinImgPlot(1) * xPixRes;
    yPixRel = posWithinImgPlot(2) * yPixRes;

    posInImage = [xLims(1) + xPixRel yLims(1) + yPixRel];
    
    clickInImageBool = ...
        currPosWithinAxes(1) >= xStart && ...
        currPosWithinAxes(1) <= xStart+xExtent && ...
        currPosWithinAxes(2) >= yStart && ...
        currPosWithinAxes(2) <= yStart+yExtent;
    

end

