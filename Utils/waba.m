function waba(prog,full,text)
global waitBarHandleNotUsedAnywhereElse
%WAit Bar helper
if nargin<3
    text = 'Waiting';
end

if prog == 0
    waitBarHandleNotUsedAnywhereElse = waitbar(0,sprintf('%s... [%d/%d]',text,0,full));
elseif prog < full
    if ishandle(waitBarHandleNotUsedAnywhereElse)
        if mod(prog,1) == 0
            waitbar(prog/full,waitBarHandleNotUsedAnywhereElse,sprintf('%s... [%.1f%% %d/%d]',text,prog/full*100,prog,full));
        else
            waitbar(prog/full,waitBarHandleNotUsedAnywhereElse,sprintf('%s... [%.1f%% %.2f/%d]',text,prog/full*100,prog,full));
        end
    end
elseif prog == full
    if ishandle(waitBarHandleNotUsedAnywhereElse)
        close(waitBarHandleNotUsedAnywhereElse)
    end
end         

end

