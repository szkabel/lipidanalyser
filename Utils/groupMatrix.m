function [ groupedMatrix,nofGroupedObjects ] = groupMatrix( origMatrix,groupby, aggregateFuncs,newOtherIndices,newKeyIndices)
%groupMatrix Based on a selected columns of the matrix it groups together
%the rows.
%   The selected columns operate as unique identifiers for the rows. If
%   the rows are similar on the selected columns they will be grouped
%   together. The result can be acquired in a flattened out version or in
%   bunches without aggregation.
%   NOTE: the input parameters must be consistant especially in the case
%   when aggregateFuns are used.
%       
%   INPUTS:
%       origMatrix      The original matrix to group. Must be numerical.
%       groupby         Array of indices of the selected columns to use as
%                       keys
%       aggregateFuncs  cellarray of aggregating functions such as mean,
%                       std, max, min, median or similar specified as
%                       string (handle). Can be empty in this case the
%                       groups are not flattened but given back separately
%                       as cell arrays.
%       newOtherIndices ONLY IF aggregateFuncs is NOT EMPTY, but then
%                       obligatory. In this case it contains the column
%                       indices in the result matrix of the aggregated
%                       values. 
%       newKeyIndices   ONLY IF aggregateFuncs is NOT EMPTY, but then it is
%                       obligatory. Contains the column indices in the
%                       result matrix of the key columns (those that remain
%                       unchanged)
%
%   OUTPUT:
%       groupedMatrix   The aggregated matrix that may have more columns
%                       than the original if length(aggregateFuncs)>1. If
%                       the aggregateFuncs is empty then this is a
%                       cellarray with all the groups but not aggregated to
%                       any functions. In this case all the cellarray
%                       entries is a matrix of which groupby columns are
%                       the same.
%       nofGroupedObjects An array with the number of aggregated objects
%                       for each group.

    nofFunc = length(aggregateFuncs);    
    if nofFunc
        newWidth = length(newOtherIndices)+length(newKeyIndices);
        groupedMatrix = zeros(size(origMatrix,1),newWidth);
    else
        groupedMatrix = cell(1,size(origMatrix,1)); %reserve enough place
    end
    nofGroupedObjects = zeros(size(origMatrix,1),1);
    i = 1;
    while (1 <= size(origMatrix,1))
        keyFields = origMatrix(1:end,groupby);
        matchMatrix = (keyFields == repmat(keyFields(1,:),size(keyFields,1),1));
        sameIndices = find(sum(matchMatrix,2) == length(groupby));
        if nofFunc
            if ~isempty(sameIndices)
                for j=1:nofFunc                
                    groupedMatrix(i,newOtherIndices(j:nofFunc:end)) = feval(aggregateFuncs{j},origMatrix(sameIndices,setdiff(1:end,groupby))); % by default matlab aggregates on the first dimension
                end
            end
            groupedMatrix(i,newKeyIndices) = origMatrix(sameIndices(1),groupby);
        else
            groupedMatrix{i} = origMatrix(sameIndices,setdiff(1:end,groupby));
            groupedMatrix{i}(:,newKeyIndices) = origMatrix(sameIndices,groupby);
        end        
        nofGroupedObjects(i) = length(sameIndices);
        origMatrix(sameIndices,:) = []; %delete
        i = i+1;
    end
    
    groupedMatrix(i:end,:) = []; %delete both cell arrays or regular arrays
    nofGroupedObjects(i:end) = [];


end

