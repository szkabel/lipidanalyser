function isItField = isFieldRec(S,embeddedFieldList)
%More general implementation of the isFieldRecursive function
%S structure to be checked
%embeddedFieldList: cellarray with the fields

isItField = 1;
currS = S;
for i=1:length(embeddedFieldList)
    if isfield(currS,embeddedFieldList{i})
        currS = currS.(embeddedFieldList{i});
    else     
        isItField = 0;
    end
end

end

