function newPath = correctSlashes(origPath)
% Function that splits the original path at each folder level (\ or /), and
% rejoins it according to the current filesystem.

spltPath = strsplit(origPath,{'\','/'});
newPath = fullfile(spltPath{:});

if strcmp(origPath(1),'/'),
    newPath = ['/' newPath];
end

end