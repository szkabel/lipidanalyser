function currElement = iterateOverCellAndStruct(currElement, funcToExec)
%Iterates over embedded structures, cell arrays recursively and if reaches
%the final level it executes the final level function

if isstruct(currElement)
    fieldNames = fieldnames(currElement);
    for i=1:length(fieldNames)
        currElement.(fieldNames{i}) = iterateOverCellAndStruct(currElement.(fieldNames{i}),funcToExec);
    end
elseif iscell(currElement)
    for i=1:length(currElement)
        currElement{i} = iterateOverCellAndStruct(currElement{i},funcToExec);
    end
else
    currElement = funcToExec(currElement);
end

end