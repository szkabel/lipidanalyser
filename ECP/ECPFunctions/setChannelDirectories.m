function [ ECP ] = setChannelDirectories( ECP, newChannels )
%reset the channel directories in ECP. newChannels must be a cellarray
%equally long as the old RawImages dir in ECP.

if length(newChannels)==length(ECP.RawImages.Directory)
    ECP.RawImages.Directory = newChannels;
else
    warning('The new channel descriptor is not long enough');
end


end

