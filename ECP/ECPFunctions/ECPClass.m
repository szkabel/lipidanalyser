classdef ECPClass < ObjectAnalyserDataStructure
% AUTHOR:   Abel Szkalisity
% DATE:     November 13, 2019
% NAME:     ECPClass
%
% ECP: standing for Embedded Cell Profiler, the first implementation of the
% database as a Matlab Structure stored in memory.
    
    
    properties (Access = 'private')
        ECP
    end
    
    methods
        %% Constructor
        
        function obj = ECPClass(RawImages,primChanID,DirName,ObjectNames)
        % Constructor that complies to the superclass
            obj.ECP.RawImages = RawImages;                        
            obj.ECP.PrimaryChannel = obj.ECP.RawImages.ChannelName{primChanID};
            obj.ECP.DirName = DirName;
            
            %Creating the directories
            for j=1:length(ObjectNames)
                currentObjectMaskFolder = fullfile(obj.ECP.DirName,'Masks',ObjectNames{j});
                %remove the directory, to start in a clean state
                if exist(currentObjectMaskFolder,'dir'), rmdir(currentObjectMaskFolder,'s'); end
                if ~isfieldRecursive(obj.ECP,['Objects.' ObjectNames{j} '.FolderName'])
                    obj.ECP.Objects.(ObjectNames{j}).FolderName = fullfile('Masks',ObjectNames{j});
                end
                if ~exist(currentObjectMaskFolder,'dir')
                    mkdir(currentObjectMaskFolder);
                end
            end
            
        end
        
        %% Data handling                
        
        function oneColumnData = loadRequestedMeasurement(obj,measModule,imageID,objectName,selectedColumn)
        %For native access to the data. If some store measurement was called before
        %then it can be retrieved by this.
            imageIdx = obj.getImageIndexByName(imageID);
            uniqueID = measModule.uniqueID();

            currFields = obj.ECP.Images{imageIdx}.(objectName).Measurements;
            for i=1:length(currFields)
                if strcmp(currFields{i}.uniqueID,uniqueID)
                    oneColumnData = obj.ECP.Images{imageIdx}.(objectName).Measurements{i}.Data(:,selectedColumn);
                end
            end
        end
        
        function storeRequestedMeasurement( obj,featureStr)
        %store the requestedMeasurement in ECP.
        %featureStr contains the measurement already

            imageIdx = obj.getImageIndexByName(featureStr.imageID);
            objectName = featureStr.ObjectName;
            currentVersion = featureStr.Version;
            uniqueID = featureStr.uniqueID;

            if isfield(obj.ECP.Images{imageIdx},objectName)
                currFields = obj.ECP.Images{imageIdx}.(objectName).Measurements;
            else
                currFields = {};
            end
            boolNewField = true; %new
            for i=1:length(currFields)
                if iscell(currFields) && (currFields{i}.Version == currentVersion && strcmp(currFields{i}.uniqueID,uniqueID))
                    boolNewField = false; %old
                    break;
                end    
            end

            if boolNewField
                if ~iscell(currFields)
                    currFields = {featureStr};
                else
                    currFields{end+1} = featureStr;
                end        
            else
                currFields{i} = featureStr;
            end

            obj.ECP.Images{imageIdx}.(objectName).Measurements = currFields;
        end
        
        %% Meta data querying functions
        
        function [ ok ] = checkMaskFolders( obj, workFolder )
        %checks if the workFolder given in the argument contains the proper folders
        %for the object masks
            ok = 1;
            fields = fieldnames(obj.ECP.Objects);
            for i=1:length(fields)
                if ~isdir([workFolder filesep obj.ECP.Objects.(fields{i}).FolderName]) %#ok<ISDIR> this is also used in older Matlabs
                    ok = 0;
                    return;
                end
            end
        end
        
        function [ output_args ] = getCellProfilerRawImageNames( obj )
            output_args = obj.ECP.RawImages.CPNames;
        end
        
        function [ output_args ] = getChannelDirectories( obj )
        %returns a cellarray which contains the folders of the raw images
            output_args = obj.ECP.RawImages.Directory;
        end

        function [ imageID ] = getImageID( obj,index )       
            notEmptyList = find(not(cellfun(@isempty,obj.ECP.Images)));
            imageID = obj.ECP.Images{notEmptyList(index)}.ImageName;
        end
        
        function imageNameCell = getImageNames( obj )
        %GETIMAGENAMES
        %   Make a list of available images in ECP.
            n = length(obj.ECP.Images);
            imageNameCell = cell(1,n);
            k = 1;
            for i=1:n
                if ~isempty(obj.ECP.Images{i})
                    imageNameCell{k} = obj.ECP.Images{i}.ImageName;
                    k = k+1;
                end
            end

            imageNameCell(k:end) = [];
        end

        function [ objectNameList ] = getObjectNames(obj)
            %To get all possible object types in this experiment
            objectNameList = fieldnames(obj.ECP.Objects);
        end                

        function [ workPath ] = getWorkFolder( obj )
        %returns the working folder of ECP (where the anal files were saved)

        workPath = obj.ECP.DirName;

        end

        function [ bool ] = isUpdateNeeded( obj,measurementModule,imageID,objectName)
        %Is there a measurement already in the data structure? This is checked by
        %the current version number and the uniqueID

        imageIdx = obj.getImageIndexByName(imageID);
        uniqueID = measurementModule.uniqueID();
        currentVersion = measurementModule.getCurrentVersion();
               
        if isfield(obj.ECP.Images{imageIdx},objectName)
            currFields = obj.ECP.Images{imageIdx}.(objectName).Measurements;
        else
            currFields = {};
        end

        bool = true;
        for i=1:length(currFields)
            %If the current field is not cell, then it is not in proper format yet.
            if iscell(currFields) && (currFields{i}.Version == currentVersion && strcmp(currFields{i}.uniqueID,uniqueID))
                bool = false;
                break;
            end
        end

        end

        function [ imageChannelList ] = listImageChannels(obj)
        %listImageChannels: to remove ambigiuty concatenate the channel name and
        %the CP name

        imageChannelList = obj.ECP.RawImages.ChannelName;

        for i=1:length(imageChannelList)
            imageChannelList{i} = [imageChannelList{i} '::' obj.ECP.RawImages.CPNames{i}];
        end

        end       

        function [ maskImage ] = retrieveMaskImage( obj, objectName, imageID)
            %retrieve the mask image based on the data stored in ECP.
            %The mask images are usually on the disc.

            if strcmp(objectName,'Image')
                imgChanList = obj.listImageChannels();
                %retrieve the first channel just to see the image size
                img = obj.retrieveOriginalImage(imageID,imgChanList{1});
                maskImage = uint16(ones(size(img,1),size(img,2)));
            else
                dirName = fullfile(obj.ECP.DirName,obj.ECP.Objects.(objectName).FolderName);
                %imageID contains also the extention
                [~,fileExex,~] = fileparts(imageID);
                maskList = dir(fullfile(dirName,[fileExex '*']));

                nofLayers = length(maskList);

                if nofLayers>0
                    if nofLayers == 1
                        maskImage = imread(fullfile(dirName,maskList.name));
                    else
                        firstLayer = imread(fullfile(dirName,maskList(1).name));    
                        maskImage = zeros([size(firstLayer) nofLayers]);
                        maskImage(:,:,1) = firstLayer;
                        for i=2:nofLayers
                            maskImage(:,:,i) = imread(fullfile(dirName,maskList(i).name));
                        end
                    end
                else
                    error('OA:ECP_maskNotFoundError',['The mask image ' imageID ' for object: ' objectName ' is not found.']);
                end
            end
        end
        
        function storeMaskImage(obj,maskImage,imageID,objectName)
            currentObjectMaskFolder = fullfile(obj.ECP.DirName,obj.ECP.Objects.(objectName).FolderName);            
            ObjectAnalyserDataStructure.saveMaskImage(maskImage,currentObjectMaskFolder,imageID);            
        end

        function [ origImg ] = retrieveOriginalImage(obj,imageID,imageChannel,channelIndex)
        %retieveOriginalImage
            %   ECP, imageID is the ACC image name, imageChannel is the ID of the given
            %   channel (struct, string, string)

            if nargin<4
                for i=1:length(obj.ECP.RawImages.ChannelName)
                    if strcmp(imageChannel,[obj.ECP.RawImages.ChannelName{i} '::' obj.ECP.RawImages.CPNames{i}])
                        channelIndex = i;
                        break;
                    end
                end
            end

            if ~exist('channelIndex','var')
                error('LipidAnalyser:ObjectAnalyserError','(ECP) Image channel not found in data structure');
            end

            onlyTextMatchInfo = strsplit(imageChannel,'::');
            imageChannel = onlyTextMatchInfo{1};

            splitted = strsplit(imageID,obj.ECP.PrimaryChannel);
            if length(splitted)>1
                imageRightChannelID = [splitted{1} imageChannel splitted{2}];
            else
                if strfind(imageID,obj.ECP.PrimaryChannel) == 1
                    imageRightChannelID = [imageChannel splitted{1}];
                else
                    imageRightChannelID = [splitted{1} imageChannel];
                end

            end
            %find the original file.
            currentPath =  obj.ECP.RawImages.Directory{channelIndex};
            possibleFiles = dir(fullfile(currentPath,[imageRightChannelID '*']));

            if ~isempty(possibleFiles)
                origImg = imread(fullfile(currentPath,possibleFiles(1).name));
            else
                error('LipidAnalyser:ObjectAnalyserError','(ECP) Image: %s is not found in folder %s' ,imageRightChannelID,currentPath );
            end
        end

        function setWorkFolder( obj, newWorkFolder)
        %to set the working folder (anal files root) in ECP

        obj.ECP.DirName = newWorkFolder;
        end
        
        function setChannelDirectories(obj,newChannelDirs)
            obj.ECP.RawImages.Directory = newChannelDirs;
        end
        
        function initImage(obj,imgID,idx)
            obj.ECP.Images{idx}.ImageName = imgID;
        end
       
    end
    
    methods (Access = 'private')
        
        function [ idx ] = getImageIndexByName(obj, imageID )
        %getImageIndexByName
        % imageID: ACC image name       
            for i=1:length(obj.ECP.Images)
                if ~isempty(obj.ECP.Images{i}) && strcmp(imageID,obj.ECP.Images{i}.ImageName)
                    idx = i;
                    break;
                end    
            end        
        end
        
    end
    
end

