classdef SQLiteECP < ObjectAnalyserDataStructure
% SQLite implementation of the ECP structure
    
    properties (Access = 'private')
        dbParams;        
        DirName;
        globConn;
        CPreadOption;
        % reserved for further usage. It is intended for being able to read
        % images with "by-order" option. However the current implementation
        % assumes that the image name (file name of a channel) can be
        % figured out from a the primary channel which is not compatible
        % with the by-order option.
        createDataTableSQL = ['CREATE TABLE Data ('...
                'imageID INT,'...
                'objectID INT,'...
                'mmtID INT,'...
                'data TEXT,'...
                'PRIMARY KEY (imageID,objectID,mmtID),'...
                'FOREIGN KEY (imageID) REFERENCES Images(imageID),'...
                'FOREIGN KEY (objectID) REFERENCES Objects(objectID),'...
                'FOREIGN KEY (mmtID) REFERENCES Mmts(mmtID));'];
    end
   
    methods           
        function obj = SQLiteECP(RawImages,PrimaryChannelID,DirName,ObjectNames)            
            driverPath = obj.getDriverPath();
            obj.dbParams.dbName = 'ECP.db';
            dbLocation = fullfile(DirName,obj.dbParams.dbName);
            obj.dbParams.dataBaseURLhead = 'jdbc:sqlite:';
            obj.dbParams.driverPath = driverPath;
            obj.dbParams.driverURL = 'org.sqlite.JDBC';
            obj.dbParams.username = '';
            obj.dbParams.password = '';
            
            javaaddpath(driverPath);
                        
            %Create new database
            if ~exist(dbLocation,'file')                           
                tmpConn = sqlite(dbLocation,'create');
                close(tmpConn);
            end
            
            %Get the dirname also saved
            obj.DirName = DirName;
                       
            obj.createDBConn();
            
            conn = obj.connectToDB();
            
            %Creating database
            conn.execute('DROP TABLE IF EXISTS Objects;');
            conn.execute('DROP TABLE IF EXISTS Mmts;');
            conn.execute('DROP TABLE IF EXISTS Images;');
            conn.execute('DROP TABLE IF EXISTS Data;');
            conn.execute('DROP TABLE IF EXISTS Channels;');
            
            conn.execute(['CREATE TABLE Objects ('...
                'objectID INTEGER PRIMARY KEY,'...
                'name TEXT,',...
                'dirName TEXT);']);
            conn.execute(['CREATE TABLE Mmts ('...
                'mmtID INTEGER PRIMARY KEY,'...
                'uniqueID TEXT UNIQUE,'...
                'version NUMERIC,'...
                'name TEXT);']);
            conn.execute(['CREATE TABLE Images ('...
                'imageID INTEGER PRIMARY KEY,'...
                'imgName TEXT);']);
            conn.execute(['CREATE TABLE Channels ('...
                'channelID INTEGER PRIMARY KEY,'...
                'channelName TEXT,'...
                'dirName TEXT,'...
                'cpName TEXT,'...
                'prim INT);']);
            conn.execute(obj.createDataTableSQL);
            
            %Fill in Channel Table
            for i=1:length(RawImages.ChannelName)
                if i == PrimaryChannelID, primChanBool = 1; else, primChanBool = 0; end
                chanStruct = struct('ChannelName',RawImages.ChannelName(i),...
                                    'Directory',RawImages.Directory(i),...
                                    'CPNames',RawImages.CPNames(i),...
                                    'primChanBool',primChanBool);
                obj.addImageChannel(chanStruct)                
            end
            
            %Creating the mask directories
            for j=1:length(ObjectNames)
                currentObjectMaskFolder = fullfile(DirName,'Masks',ObjectNames{j});
                %remove the directory, to start in a clean state
                if exist(currentObjectMaskFolder,'dir'), rmdir(currentObjectMaskFolder,'s'); end                                     
                if ~exist(currentObjectMaskFolder,'dir')
                    mkdir(currentObjectMaskFolder);
                end
                T = table(ObjectNames(j),{fullfile('Masks',ObjectNames{j})},'VariableNames',{'name','dirName'});
                sqlwrite(conn,'Objects',T);
            end
            
            obj.CPreadOption = RawImages.CPreadOption; % identifies the type of image load in CP (text-exact implemented currently)
                            
            %REPLACE_closeConn_HERE
        end                
        
        function delete(obj)
            close(obj.globConn);
        end
        
        function initObject(obj,objectName)
            conn =  obj.connectToDB();
            currentObjectMaskFolder = fullfile(obj.DirName,'Masks',objectName);            
            if ~exist(currentObjectMaskFolder,'dir')
                mkdir(currentObjectMaskFolder);
            end
            T = table({objectName},{fullfile('Masks',objectName)},'VariableNames',{'name','dirName'});
            sqlwrite(conn,'Objects',T);
        end
        
        function removeObject(obj,objectName)
            conn =  obj.connectToDB();
            currentObjectMaskFolder = fullfile(obj.DirName,'Masks',objectName);
            %remove the directory, to start in a clean state
            if exist(currentObjectMaskFolder,'dir'), rmdir(currentObjectMaskFolder,'s'); end            
            conn.execute(['DELETE FROM Objects WHERE name=''' objectName ''';'])
            % When objects are deleted we must delete also the full DATA
            % table to ensure that produced measurements are according to
            % good masks
            obj.resetData()            
        end
        
        function resetData(obj)
            conn =  obj.connectToDB();
            conn.execute('DROP TABLE IF EXISTS Data;');
            conn.execute(obj.createDataTableSQL);
            % Even though the DATA table has the foreign key objectID but
            % that is only the base of a measurement not ALL of its
            % dependencies (e.g. an object overlap measurement ON a "Spots"
            % object may change also if the nuclei has changed and that is not in the foreign key)
        end
                                
        function oneColumnData = loadRequestedMeasurement(obj,measModule,imageID,objectName,selectedColumn)
            conn = obj.connectToDB();
            imageID = obj.getImageIndexByName(imageID);
            objectID = conn.fetch(['SELECT objectID FROM Objects WHERE name=''' objectName ''' ;']);
            objectID = objectID.objectID(1);
            mmtID = checkMmtInDB(obj,measModule.uniqueID(),measModule.getCurrentVersion());
            
            results = conn.fetch(['SELECT * FROM Data WHERE '...
                'imageID = ' num2str(imageID) ' AND '...
                'objectID = ' num2str(objectID) ' AND ' ...
                'mmtID = ' num2str(mmtID) ';']);
            
            retrievedMatrix = dunzip(uint8(results.data{1}));
            oneColumnData = retrievedMatrix(:,selectedColumn);
            
            %REPLACE_closeConn_HERE
        end                
                
        function storeRequestedMeasurement( obj,featureStr )
            %featureStr should be single
            conn = obj.connectToDB();
            imageID = obj.getImageIndexByName(featureStr.imageID);            
            objectID = conn.fetch(['SELECT objectID FROM Objects WHERE name=''' featureStr.ObjectName ''' ;']);
            objectID = objectID.objectID(1);
            currentVersion = featureStr.Version;
            uniqueID = featureStr.uniqueID;
            
            %Check for Mmt
            mmtID = checkMmtInDB(obj,uniqueID,currentVersion); 
            %If it's not there then create one
            if ~mmtID
                moduleName = strsplit(uniqueID,'_');
                moduleName = moduleName(1);
                T = table({uniqueID},moduleName,currentVersion,'VariableNames',{'uniqueID','name','version'});
                try
                    sqlwrite(conn,'Mmts',T);
                catch err
                    %If another module was faster (which can happen for
                    %sure) than the DB throws a constraint error. Even by
                    %the best planning (i.e. checking mmtID in advance this
                    %can happen). Anyway that means that a mmtID is already
                    %available and we can fetch it.
                    if ~strcmp(err.identifier,'database:database:WriteTableDriverError')                    
                        rethrow(err);
                    end
                end
                %Fetch the new ID
                mmtID = checkMmtInDB(obj,uniqueID,currentVersion); 
            end            
            
            %First cehck if the data is not there already to avoid primary
            %key conflict
            results = conn.fetch(['SELECT * FROM Data WHERE '...
                'imageID = ' num2str(imageID) ' AND '...
                'objectID = ' num2str(objectID) ' AND ' ...
                'mmtID = ' num2str(mmtID) ';']);
            
            %If it's not there yet, then put it in
            if isempty(results)
                %Compressing data and save it down
                D = dzip(featureStr.Data); %Zip it into uint8
                DFlat = D'; %Flat it out
                DCS = char(DFlat);
                T = table(imageID,objectID,mmtID,{DCS},'VariableNames',{'imageID','objectID','mmtID','data'});
                sqlwrite(conn,'Data',T);
            end
            
            %REPLACE_closeConn_HERE
        end
        
        function ok = checkMaskFolders( obj, workFolder )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT DirName FROM Objects;');
            
            ok = true;
            for i=1:size(results,1)
                if ~exist(fullfile(workFolder,results.dirName{i}),'dir')
                    ok = false;
                    break;
                end
            end
            %REPLACE_closeConn_HERE
        end
                
        function rawImgNames = getCellProfilerRawImageNames( obj )            
            conn = obj.connectToDB();
            results = conn.fetch('SELECT cpName FROM Channels;');
            rawImgNames = results.cpName;
           
            %REPLACE_closeConn_HERE
        end
        
  
        function output_args = getChannelDirectories( obj )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT dirName FROM Channels;');
            output_args = results.dirName;
            
            %REPLACE_closeConn_HERE
        end
        
        function imageID = getImageID( obj,index )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT imageID FROM Images ORDER BY imageID');
            if index>size(results,1)
                error('OA:ECP_imageID',['There is no image with relative imageID = ' num2str(index)]);
            end
            realImgID = results.imageID(index);
            results = conn.fetch(['SELECT imgName FROM Images WHERE imageID =' num2str(realImgID) ';']);            
            imageID = results.imgName{1};
            
            %REPLACE_closeConn_HERE
        end
        
        function imageID = getCPImageID( obj,realImgID )
            conn = obj.connectToDB();            
            results = conn.fetch(['SELECT imgName FROM Images WHERE imageID =' num2str(realImgID) ';']);                        
            if isempty(results)                        
                error('OA:ECP_imageID',['There is no image with imageID = ' num2str(realImgID)]);
            end
            imageID = results.imgName{1};
        end
                
        function imageNameCell = getImageNames( obj )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT imgName FROM Images;');
            imageNameCell = results.imgName;
            
        end
                
        function objectNameList = getObjectNames( obj )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT name FROM Objects;');            
            objectNameList = results.name;
            
            %REPLACE_closeConn_HERE
        end
                
        function workPath = getWorkFolder( obj )
            workPath = obj.DirName;
        end
                
        function bool = isUpdateNeeded( obj,measurementModule,imageID,objectName)
            conn = obj.connectToDB();
            %Querying data
            imageID = obj.getImageIndexByName(imageID);            
            uniqueID = measurementModule.uniqueID();            
            objectID = conn.fetch(['SELECT objectID FROM Objects WHERE name=''' objectName ''' ;']);
            objectID = objectID.objectID(1);            
            currentVersion = measurementModule.getCurrentVersion();
            mmtID = checkMmtInDB(obj,uniqueID,currentVersion);
                        
            results = conn.fetch(['SELECT * FROM Data WHERE '...
                'imageID = ' num2str(imageID) ' AND '...
                'objectID = ' num2str(objectID) ' AND ' ...
                'mmtID = ' num2str(mmtID) ';']);
            
            if isempty(results), bool = true; else, bool = false; end
            
            %REPLACE_closeConn_HERE
        end

        function addImageChannel(obj, chanStruct)            
            conn = obj.connectToDB();
            T = table({chanStruct.ChannelName},{chanStruct.Directory},{chanStruct.CPNames},chanStruct.primChanBool,'VariableNames',{'channelName','dirName','cpName','prim'});
            sqlwrite(conn,'Channels',T);
        end

        function removeImageChannel(obj, chanID)                        
            conn =  obj.connectToDB();
            primChanID = conn.fetch('SELECT channelID FROM Channels WHERE prim = 1;');
            primChanID = primChanID.channelID(1);
            
            if (chanID ~= primChanID)
                conn.execute(['DELETE FROM Channels WHERE channelID=''' chanID ''';'])
                % When objects are deleted we must delete also the full DATA
                % table to ensure that produced measurements are according to
                % good masks
                obj.resetData();
            end
        end
                        
        function [ imageChannelList,chanIDs, prim] = listImageChannels(obj)
            conn = obj.connectToDB();
            results = conn.fetch('SELECT channelID, channelName,cpName,prim FROM Channels;');
            imageChannelList = results.channelName;
            imageCPList = results.cpName;
            chanIDs = results.channelID;
            prim = results.prim;

            for i=1:length(imageChannelList)
                imageChannelList{i} = [imageChannelList{i} '::' imageCPList{i}];
            end            
            
            %REPLACE_closeConn_HERE
        end       
                
        function [ maskImage ] = retrieveMaskImage( obj, objectName, imageID)
            if strcmp(objectName,'Image')
                imgChanList = obj.listImageChannels();
                %retrieve the first channel just to see the image size
                img = obj.retrieveOriginalImage(imageID,imgChanList{1});
                maskImage = uint16(ones(size(img,1),size(img,2)));
            else
                conn = obj.connectToDB();
                objDirs = conn.fetch(['SELECT dirName FROM Objects WHERE name = ''' objectName ''';']);
                dirName = fullfile(obj.DirName,objDirs.dirName{1});
                %imageID contains also the extention
                % No, it's not
                %[~,fileExex,~] = fileparts(imageID);
                % maskList = dir(fullfile(dirName,[fileExex '*']));
                maskList = dir(fullfile(dirName,[imageID '*']));

                nofLayers = length(maskList);

                if nofLayers>0
                    if nofLayers == 1
                        maskImage = imread(fullfile(dirName,maskList.name));
                    else
                        firstLayer = imread(fullfile(dirName,maskList(1).name));    
                        maskImage = zeros([size(firstLayer) nofLayers]);
                        maskImage(:,:,1) = firstLayer;
                        for i=2:nofLayers
                            maskImage(:,:,i) = imread(fullfile(dirName,maskList(i).name));
                        end
                    end
                else
                    error('OA:ECP_maskNotFoundError',['The mask image ' imageID ' for object: ' objectName ' is not found.']);
                end
                
                
            end
        end
                
        function storeMaskImage(obj,maskImage,imageID,objectName)
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT dirName FROM Objects WHERE name = ''' objectName ''';']);            
            
            currentObjectMaskFolder = fullfile(obj.DirName,results.dirName{1});
            ObjectAnalyserDataStructure.saveMaskImage(maskImage,currentObjectMaskFolder,imageID);
            
            %REPLACE_closeConn_HERE
        end
        
        function deleteMaskImage(obj,imageID,objectName)
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT dirName FROM Objects WHERE name = ''' objectName ''';']);            
            
            currentObjectMaskFolder = fullfile(obj.DirName,results.dirName{1});
            ObjectAnalyserDataStructure.deleteMaskImageDisk(currentObjectMaskFolder,imageID);
            
            %REPLACE_closeConn_HERE
        end
                
        function [ origImg ] = retrieveOriginalImage(obj,imageID,imageChannel,~)
            %retieveOriginalImage
            %   ECP, imageID is the ACC image name, imageChannel is the ID of the given
            %   channel (struct, string, string)

            sepImgNames = strsplit(imageChannel,'::');
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT dirName FROM Channels WHERE channelName = ''' sepImgNames{1} ''' AND cpName = ''' sepImgNames{2} ''';']);
            currentPath = results.dirName{1};                       
            
            onlyTextMatchInfo = strsplit(imageChannel,'::');
            imageChannel = onlyTextMatchInfo{1};
            
            primChan = conn.fetch('SELECT channelName FROM Channels WHERE prim = 1;');
            primChan = primChan.channelName{1};
            
            %[~,imageIDexex,~] = fileparts(imageID);
            %splitted = strsplit(imageIDexex,primChan);
            % splitted = strsplit(imageID,primChan);            
            % if length(splitted)>1
            %     imageRightChannelID = [splitted{1} imageChannel splitted{2}];
            % else
            %     if strfind(imageID,primChan) == 1
            %         imageRightChannelID = [imageChannel splitted{1}];
            %     else
            %         imageRightChannelID = [splitted{1} imageChannel];
            %     end
            % 
            % end
            imageRightChannelID = strrep(imageID,primChan,imageChannel);
            
            %find the original file.            
            possibleFiles = dir(fullfile(currentPath,[imageRightChannelID '*']));

            if ~isempty(possibleFiles)
                origImg = imread(fullfile(currentPath,possibleFiles(1).name));
            else
                error('LipidAnalyser:ObjectAnalyserError','(ECP) Image: %s is not found in folder %s' ,imageRightChannelID,currentPath );
            end
            
            %REPLACE_closeConn_HERE
        end
                
        function setWorkFolder( obj, newWorkFolder)
            obj.DirName = newWorkFolder;
        end
                
        function setChannelDirectories(obj,newChannelDir)
            conn = obj.connectToDB();
            chanIDs = conn.fetch('SELECT channelID FROM Channels;');
            if (length(newChannelDir)~=size(chanIDs,1))
                error('LipidAnalyser:ObjectAnalyserError','The number of old and new channel does not match.');
            end
            for i=1:length(newChannelDir)
                conn.execute(['UPDATE Channels SET '...
                    ' dirName = ''' newChannelDir{i} ''' ' ...
                    ' WHERE channelID=' num2str(chanIDs.channelID(i)) ';']);
            end
            
            %REPLACE_closeConn_HERE
        end                
                
        function initImage(obj,imageID,idx)
            if ~obj.getImageIndexByName(imageID)
                conn = obj.connectToDB();            
                T = table(idx,{imageID},'VariableNames',{'imageID','imgName'});
                sqlwrite(conn,'Images',T);
                
            end
        end       
        
        function prepareForSave(obj)
            close(obj.globConn);
        end
        
        function afterReOpen(obj)
            if ~exist(obj.dbParams.driverPath,'file')                
                driverPath = obj.getDriverPath();
                obj.dbParams.driverPath = driverPath;            
            end
            %Do a check here for an existing connection (so that an existing one is not perturbed)            
            if ~isvalid(obj.connectToDB()) || ~strcmp(obj.connectToDB().Type,'JDBC Connection Object')                
                obj.createDBConn();
            end
        end
        
    end
    
    methods (Access = 'private')
        function driverPath = getDriverPath(~)
            [callPath,~,~] = fileparts(mfilename('fullpath'));            
            spltPath = strsplit(callPath,filesep);
            driverPath = strjoin([spltPath(1:end-2),'third-party','sqlite-jdbc-3.27.2.1.jar'],filesep);
        end
        
        function createDBConn(obj)
            if ~ismember(obj.dbParams.driverPath,javaclasspath('-dynamic'))
                javaaddpath(obj.dbParams.driverPath);
            end
            dbLocation = fullfile(obj.DirName,obj.dbParams.dbName);
            dataBaseURL = [obj.dbParams.dataBaseURLhead dbLocation];
            obj.globConn = database(obj.dbParams.dbName,obj.dbParams.username,obj.dbParams.password,obj.dbParams.driverURL,dataBaseURL);
        end
        
        function conn = connectToDB(obj)
            conn = obj.globConn;
        end
        
        function mmtID = checkMmtInDB(obj,uniqueID,queryVersion)
            %Gives back mmtID or 0 if it's not there yet
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT mmtID FROM Mmts WHERE uniqueID = ''' uniqueID ''' AND version=' num2str(queryVersion) ' ;']);
            if isempty(results)
                mmtID = 0;
            else
                mmtID = results.mmtID(1);
            end
            
            %REPLACE_closeConn_HERE
        end
        
        function [ idx ] = getImageIndexByName(obj, imageID )
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT imageID FROM Images WHERE imgName = ''' imageID ''';']);
            if isempty(results)
                idx = 0;
            else
                idx = results.imageID(1);
            end            
            
            %REPLACE_closeConn_HERE
        end
    end
    
    methods (Static, Access = 'protected')
            
        function saveMaskImage(maskImage,folder,imageName)
        %Saves the maskImage to the folder. If the mask image has multiple
        %layers (i.e. its 3rd dimension is greater than 1) then it saves
        %multiple images
        
            %single layer
            if size(maskImage,3)==1
                imwrite(uint16(maskImage),fullfile(folder,imageName));
            else
                [~,base,~] = fileparts(imageName);
                for i=1:size(maskImage,3)
                    imwrite(uint16(maskImage(:,:,i)),fullfile(folder,[base sprintf('_l%02d',i) '.tif']));
                end        
            end
        end
    end
end

