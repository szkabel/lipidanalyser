function [ imageNameCell] = getImageNames( ECP )
%GETIMAGENAMES
%   Make a list of available images in ECP.

n = length(ECP.Images);
imageNameCell = cell(1,n);
k = 1;
for i=1:n
    if ~isempty(ECP.Images{i})
        imageNameCell{k} = ECP.Images{i}.ImageName;
        k = k+1;
    end
end

imageNameCell(k:end) = [];

end

