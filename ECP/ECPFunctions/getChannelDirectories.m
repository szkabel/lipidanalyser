function [ output_args ] = getChannelDirectories( ECP )
%returns a cellarray which contains the folders of the raw images

output_args = ECP.RawImages.Directory;


end

