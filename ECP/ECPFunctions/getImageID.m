function [ imageID ] = getImageID( ECP,index )
%ECP is the data field, index is the index given back by getImageNames
%function.

notEmptyList = find(not(cellfun(@isempty,ECP.Images)));
imageID = ECP.Images{notEmptyList(index)}.ImageName;

end

