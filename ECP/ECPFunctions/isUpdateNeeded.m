function [ bool ] = isUpdateNeeded( ECP,measurementModule,imageID,objectName)
%Is there a measurement already in the data structure? This is checked by
%the current version number and the uniqueID

imageIdx = getImageIndexByName(ECP,imageID);
uniqueID = measurementModule.uniqueID();
currentVersion = measurementModule.getCurrentVersion();

currFields = ECP.Images{imageIdx}.(objectName).Measurements;

bool = true;
for i=1:length(currFields)
    %If the current field is not cell, then it is not in proper format yet.
    if iscell(currFields) && (currFields{i}.Version == currentVersion && strcmp(currFields{i}.uniqueID,uniqueID))
        bool = false;
        break;
    end
end

end

