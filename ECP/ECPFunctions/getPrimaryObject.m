function [ primObj ] = getPrimaryObject( ECP )
%getPrimaryObject Return the name of the primary object.

primObj = ECP.PrimaryObject;

end

