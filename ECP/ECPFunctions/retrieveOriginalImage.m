function [ origImg ] = retrieveOriginalImage(ECP,imageID,imageChannel,channelIndex)
%retieveOriginalImage
%   ECP, imageID is the ACC image name, imageChannel is the ID of the given
%   channel (struct, string, string)

if nargin<4
    for i=1:length(ECP.RawImages.ChannelName)
        if strcmp(imageChannel,[ECP.RawImages.ChannelName{i} '::' ECP.RawImages.CPNames{i}])
            channelIndex = i;
            break;
        end
    end
end

if ~exist('channelIndex','var')
    error('LipidAnalyser:ObjectAnalyserError','(ECP) Image channel not found in data structure');
end

onlyTextMatchInfo = strsplit(imageChannel,'::');
imageChannel = onlyTextMatchInfo{1};

splitted = strsplit(imageID,ECP.PrimaryChannel);
if length(splitted)>1
    imageRightChannelID = [splitted{1} imageChannel splitted{2}];
else
    if strfind(imageID,ECP.PrimaryChannel) == 1
        imageRightChannelID = [imageChannel splitted{1}];
    else
        imageRightChannelID = [splitted{1} imageChannel];
    end
    
end
%find the original file.
currentPath =  ECP.RawImages.Directory{channelIndex};
possibleFiles = dir(fullfile(currentPath,[imageRightChannelID '*']));

if ~isempty(possibleFiles)
    origImg = imread(fullfile(currentPath,possibleFiles(1).name));
else
    error('LipidAnalyser:ObjectAnalyserError','(ECP) Image: %s is not found in folder %s' ,imageRightChannelID,currentPath );
end


end

