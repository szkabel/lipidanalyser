function [ ECP ] = storeRequestedMeasurement( ECP,featureStr)
%store the requestedMeasurement in ECP.
%featureStr contains the measurement already

imageIdx = getImageIndexByName(ECP,featureStr.imageID);
objectName = featureStr.ObjectName;
currentVersion = featureStr.Version;
uniqueID = featureStr.uniqueID;

currFields = ECP.Images{imageIdx}.(objectName).Measurements;
boolNewField = true; %new
for i=1:length(currFields)
    if iscell(currFields) && (currFields{i}.Version == currentVersion && strcmp(currFields{i}.uniqueID,uniqueID))
        boolNewField = false; %old
        break;
    end    
end

if boolNewField
    if ~iscell(currFields)
        currFields = {featureStr};
    else
        currFields{end+1} = featureStr;
    end        
else
    currFields{i} = featureStr;
end

ECP.Images{imageIdx}.(objectName).Measurements = currFields;


end

