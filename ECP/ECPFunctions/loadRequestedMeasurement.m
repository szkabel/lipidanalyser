function oneColumnData = loadRequestedMeasurement(ECP,measModule,imageID,objectName,selectedColumn)
%For native access to the data. If some store measurement was called before
%then it can be retrieved by this.

imageIdx = getImageIndexByName(ECP,imageID);
uniqueID = measModule.uniqueID();

currFields = ECP.Images{imageIdx}.(objectName).Measurements;
for i=1:length(currFields)
    if strcmp(currFields{i}.uniqueID,uniqueID)
        oneColumnData = ECP.Images{imageIdx}.(objectName).Measurements{i}.Data(:,selectedColumn);
    end
end
