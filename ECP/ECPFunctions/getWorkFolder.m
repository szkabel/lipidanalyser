function [ workPath ] = getWorkFolder( ECP )
%returns the working folder of ECP (where the anal files were saved)

workPath = ECP.DirName;

end

