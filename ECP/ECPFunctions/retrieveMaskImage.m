function [ maskImage ] = retrieveMaskImage( ECP, objectName, imageID)
%retrieve the mask image based on the data stored in ECP.
%The mask images are usually on the disc.

if strcmp(objectName,'Image')
    imgChanList = listImageChannels(ECP);
    %retrieve the first channel just to see the image size
    img = retrieveOriginalImage(ECP,imageID,imgChanList{1});
    maskImage = uint16(ones(size(img,1),size(img,2)));
else
    dirName = correctSlashes(fullfile(ECP.DirName,ECP.Objects.(objectName).FolderName));
    %imageID contains also the extention
    [~,fileExex,~] = fileparts(imageID);
    maskList = dir(fullfile(dirName,[fileExex '*']));

    nofLayers = length(maskList);

    if nofLayers>0
        if nofLayers == 1
            maskImage = imread(fullfile(dirName,maskList.name));
        else
            firstLayer = imread(fullfile(dirName,maskList(1).name));    
            maskImage = zeros([size(firstLayer) nofLayers]);
            maskImage(:,:,1) = firstLayer;
            for i=2:nofLayers
                maskImage(:,:,i) = imread(fullfile(dirName,maskList(i).name));
            end
        end
    else
        error('ACC:ECP_maskNotFoundError',['The mask image ' imageID ' for object: ' objectName ' is not found.']);
    end
end
end

