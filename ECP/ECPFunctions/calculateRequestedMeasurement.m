function [featureStruct] = calculateRequestedMeasurement(data,measModule,imageID,objectName)
%Calculates the measModule for the given imageID and objectName if needed.
%Otherwise it gives back an empty result.
%NOTE: Gives back cellarray
    if isUpdateNeeded(data,measModule,imageID,objectName)                
        featureStruct = measModule.measure(data,imageID,objectName);            
    else
        featureStruct = [];
    end
end

