function resultArray = uint8ToPrintChar(msg,codeType)
%msg should be an array with uint8 values
%codeType is string with 'encode'/'decode'
    n = length(msg);
    msgMaxValue = 255;
    %The following 2 values correspond to the ASCII range of 33-126 which
    %was correctly retrieved from the database
    baseOfTheSystem = 94; 
    offset = 33;
    nofDigits = floor(log(msgMaxValue)/log(baseOfTheSystem))+1; %N is the length of the numbers needed
    
    switch codeType
        case 'encode'                        
            resultArray = zeros(1,nofDigits*n);
            for i=0:nofDigits-1
                resultArray((1:nofDigits:nofDigits*n)+i) = mod(msg,baseOfTheSystem);
                msg = floor(double(msg)./baseOfTheSystem);
            end
            resultArray = resultArray + offset;
        case 'decode'
            if mod(n,nofDigits) ~= 0
                error('The message length does not match with the encoding system');
            end
            msg = msg - offset;
            resultArray = uint8(zeros(1,n/nofDigits));
            base = 1;
            for i=0:nofDigits-1
                resultArray = resultArray + msg((1:nofDigits:n)+i)*base;
                base = base * baseOfTheSystem;
            end
            resultArray = resultArray';
    end
end