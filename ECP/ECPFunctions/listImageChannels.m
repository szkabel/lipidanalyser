function [ imageChannelList ] = listImageChannels(ECP)
%listImageChannels: to remove ambigiuty concatenate the channel name and
%the CP name

imageChannelList = ECP.RawImages.ChannelName;

for i=1:length(imageChannelList)
    imageChannelList{i} = [imageChannelList{i} '::' ECP.RawImages.CPNames{i}];
end

end

