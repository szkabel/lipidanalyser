function [ parents ] = retrieveParents( ECP,imageID,objectName,parentObjectName)

    childMask = retrieveMaskImage(ECP,objectName,imageID);
if ~strcmp(objectName,parentObjectName)    
    parentMask = retrieveMaskImage(ECP,parentObjectName,imageID);

    if strcmp(objectName,parentObjectName)
        handles = struct('Measurements',struct(objectName,[]));
    else
        handles = struct('Measurements',struct(objectName,[],parentObjectName,[]));
    end
    handles.Current.SetBeingAnalyzed = 1;
    %parent retrieval is pushed back to on the fly calculations (to maintain flexibility)
    [~,~,parents] = CPrelateobjects(handles,objectName,parentObjectName,double(childMask),double(parentMask),'Retrieve parents');
else
    indices = unique(childMask(:));
    if indices(1) == 0
        parents = indices(2:end);        
    else
        parents = indices;
    end
end

end

