function [ idx ] = getImageIndexByName(ECP, imageID )
%getImageIndexByName
% imageID: ACC image name

% THIS IS A PRIVATE FUNCTION!!!! FORBIDDEN TO USE OUTSIDE OF ECP!

for i=1:length(ECP.Images)
    if ~isempty(ECP.Images{i}) && strcmp(imageID,ECP.Images{i}.ImageName)
        idx = i;
        break;
    end    
end


end

