function [ ok ] = checkMaskFolders( ECP, workFolder )
%checks if the workFolder given in the argument contains the proper folders
%for the object masks
ok = 1;
fields = fieldnames(ECP.Objects);
for i=1:length(fields)
    if ~isdir([workFolder filesep ECP.Objects.(fields{i}).FolderName])
        ok = 0;
        return;
    end
end

end

