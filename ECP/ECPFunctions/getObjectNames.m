function [ objectNameList ] = getObjectNames(ECP)
    %To get all possible object types in this experiment
    objectNameList = fieldnames(ECP.Objects);

end

