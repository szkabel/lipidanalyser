classdef SQLiteECP_interface < ObjectAnalyserDataStructure
% With lines for the Matlab SQLite interface
% Check SQLiteECP for connection with JDBC driver.
% SQLite implementation of the ECP structure
    
    properties %(Access = 'private')
        dbParams;        
        DirName;
        globConn;
    end
   
    methods           
        function obj = SQLiteECP_interface(RawImages,PrimaryChannelID,DirName,ObjectNames)
            driverPath = obj.getDriverPath();
            obj.dbParams.dbName = 'ECP.db';
            dbLocation = fullfile(DirName,obj.dbParams.dbName);
            obj.dbParams.dataBaseURLhead = 'jdbc:sqlite:';
            obj.dbParams.driverPath = driverPath;
            obj.dbParams.driverURL = 'org.sqlite.JDBC';
            obj.dbParams.username = '';
            obj.dbParams.password = '';
            obj.DirName = DirName;
            
            javaaddpath(driverPath);
                        
            %Create new database
            if ~exist(dbLocation,'file')                           
                tmpConn = sqlite(dbLocation,'create');
                close(tmpConn);
            end
            
            obj.createDBConn();
            
            conn = obj.connectToDB();
            
            %Creating database
            conn.exec('DROP TABLE IF EXISTS Objects;');
            conn.exec('DROP TABLE IF EXISTS Mmts;');
            conn.exec('DROP TABLE IF EXISTS Images;');
            conn.exec('DROP TABLE IF EXISTS Data;');
            conn.exec('DROP TABLE IF EXISTS Channels;');
            conn.exec('DROP TABLE IF EXISTS MaskImages;');
            
            conn.exec(['CREATE TABLE Objects ('...
                'objectID INTEGER PRIMARY KEY,'...
                'name TEXT,',...
                'dirName TEXT);']);
            conn.exec(['CREATE TABLE Mmts ('...
                'mmtID INTEGER PRIMARY KEY,'...
                'uniqueID TEXT UNIQUE,'...
                'version NUMERIC,'...
                'name TEXT);']);
            conn.exec(['CREATE TABLE Images ('...
                'imageID INTEGER PRIMARY KEY,'...
                'imgName TEXT);']);
            conn.exec(['CREATE TABLE Channels ('...
                'channelID INTEGER PRIMARY KEY,'...
                'channelName TEXT,'...
                'dirName TEXT,'...
                'cpName TEXT,'...
                'prim INT);']);
            conn.exec(['CREATE TABLE Data ('...
                'imageID INT,'...
                'objectID INT,'...
                'mmtID INT,'...
                'data TEXT,'...
                'PRIMARY KEY (imageID,objectID,mmtID),'...
                'FOREIGN KEY (imageID) REFERENCES Images(imageID),'...
                'FOREIGN KEY (objectID) REFERENCES Objects(objectID),'...
                'FOREIGN KEY (mmtID) REFERENCES Mmts(mmtID));']);
            conn.exec(['CREATE TABLE MaskImages ('...
                'imageID INT,'...
                'objectID INT,'...               
                'maskImgList TEXT,'...                
                'FOREIGN KEY (imageID) REFERENCES Images(imageID),'...
                'FOREIGN KEY (objectID) REFERENCES Objects(objectID));']);
            
            %Fill in Channel Table
            for i=1:length(RawImages.ChannelName)
                if i == PrimaryChannelID, primChanBool = 1; else, primChanBool = 0; end                
                T = table(RawImages.ChannelName(i),RawImages.Directory(i),RawImages.CPNames(i),primChanBool,'VariableNames',{'channelName','dirName','cpName','prim'});
                SQLiteECP_interface.writeToDB(conn,'Channels',T.Properties.VariableNames,T);
            end                       
            
            %Creating the mask directories
            for j=1:length(ObjectNames)
                currentObjectMaskFolder = fullfile(DirName,'Masks',ObjectNames{j});
                %remove the directory, to start in a clean state
                if exist(currentObjectMaskFolder,'dir'), rmdir(currentObjectMaskFolder,'s'); end                                     
                if ~exist(currentObjectMaskFolder,'dir')
                    mkdir(currentObjectMaskFolder);
                end
                T = table(ObjectNames(j),{fullfile('Masks',ObjectNames{j})},'VariableNames',{'name','dirName'});
                SQLiteECP_interface.writeToDB(conn,'Objects',T.Properties.VariableNames,T);
            end                                    
                
            %REPLACE_closeConn_HERE
        end
        
        function delete(obj)
            close(obj.globConn);
        end
                                
        function oneColumnData = loadRequestedMeasurement(obj,measModule,imageID,objectName,selectedColumn)
            conn = obj.connectToDB();
            imageID = obj.getImageIndexByName(imageID);
            objectID = conn.fetch(['SELECT objectID FROM Objects WHERE name=''' objectName ''' ;']);
            objectID = objectID{1};%objectID.objectID(1);
            mmtID = checkMmtInDB(obj,measModule.uniqueID(),measModule.getCurrentVersion());
            
            results = conn.fetch(['SELECT * FROM Data WHERE '...
                'imageID = ' num2str(imageID) ' AND '...
                'objectID = ' num2str(objectID) ' AND ' ...
                'mmtID = ' num2str(mmtID) ';']);
            
            retrievedMatrix = dunzip(uint8ToPrintChar(uint8(results{1,4}),'decode'));%dunzip(uint8(results.data{1}));
            oneColumnData = retrievedMatrix(:,selectedColumn);
            
            %REPLACE_closeConn_HERE
        end                
                
        function storeRequestedMeasurement( obj,featureStr )
            %featureStr should be single
            conn = obj.connectToDB();
            imageID = obj.getImageIndexByName(featureStr.imageID);            
            objectID = conn.fetch(['SELECT objectID FROM Objects WHERE name=''' featureStr.ObjectName ''' ;']);
            objectID = objectID{1};%objectID.objectID(1);
            currentVersion = featureStr.Version;
            uniqueID = featureStr.uniqueID;
            
            %Check for Mmt
            mmtID = checkMmtInDB(obj,uniqueID,currentVersion); 
            %If it's not there then create one
            if ~mmtID
                moduleName = strsplit(uniqueID,'_');
                moduleName = moduleName(1);
                T = table({uniqueID},moduleName,currentVersion,'VariableNames',{'uniqueID','name','version'});
                try
                    SQLiteECP_interface.writeToDB(conn,'Mmts',T.Properties.VariableNames,T);
                catch err
                    %If another module was faster (which can happen for
                    %sure) than the DB throws a constraint error. Even by
                    %the best planning (i.e. checking mmtID in advance this
                    %can happen). Anyway that means that a mmtID is already
                    %available and we can fetch it.
                    if ~strcmp(err.identifier,'database:database:WriteTableDriverError')
                        disp(err.identifier)
                        rethrow(err);
                    end
                end
                %Fetch the new ID
                mmtID = checkMmtInDB(obj,uniqueID,currentVersion); 
            end            
            
            %First cehck if the data is not there already to avoid primary
            %key conflict
            results = conn.fetch(['SELECT * FROM Data WHERE '...
                'imageID = ' num2str(imageID) ' AND '...
                'objectID = ' num2str(objectID) ' AND ' ...
                'mmtID = ' num2str(mmtID) ';']);
            
            %If it's not there yet, then put it in
            if isempty(results)
                %Compressing data and save it down
                D = dzip(featureStr.Data); %Zip it into uint8
                DFlat = D'; %Flat it out
                DFlatPrint = uint8ToPrintChar(DFlat,'encode');
                DCS = char(DFlatPrint);
                T = table(imageID,objectID,mmtID,{DCS},'VariableNames',{'imageID','objectID','mmtID','data'});
                SQLiteECP_interface.writeToDB(conn,'Data',T.Properties.VariableNames,T);
            end
            
            %REPLACE_closeConn_HERE
        end
        
        function ok = checkMaskFolders( obj, workFolder )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT DirName FROM Objects;');
            
            ok = true;
            for i=1:size(results,1)
                if ~exist(fullfile(workFolder,results{i,1}),'dir') %if ~exist(fullfile(workFolder,results.dirName{i}),'dir')
                    ok = false;
                    break;
                end
            end
            %REPLACE_closeConn_HERE
        end
                
        function rawImgNames = getCellProfilerRawImageNames( obj )            
            conn = obj.connectToDB();
            results = conn.fetch('SELECT cpName FROM Channels;');
            rawImgNames = results;%results.cpName;
           
            %REPLACE_closeConn_HERE
        end
        
  
        function output_args = getChannelDirectories( obj )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT dirName FROM Channels;');
            output_args = results;%results.dirName;
            
            %REPLACE_closeConn_HERE
        end
        
        function imageID = getImageID( obj,index )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT imageID FROM Images ORDER BY imageID');
            if index>size(results,1)
                error('OA:ECP_imageID',['There is no image with relative imageID = ' num2str(index)]);
            end
            realImgID = results{index};%results.imageID(index);
            results = conn.fetch(['SELECT imgName FROM Images WHERE imageID =' num2str(realImgID) ';']);            
            imageID = results{1};%results.imgName{1};
            
            %REPLACE_closeConn_HERE
        end
                
        function imageNameCell = getImageNames( obj )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT imgName FROM Images;');
            imageNameCell = results;%results.imgName;
            
        end
                
        function objectNameList = getObjectNames( obj )
            conn = obj.connectToDB();
            results = conn.fetch('SELECT name FROM Objects;');            
            objectNameList = results;%results.name;
            
            %REPLACE_closeConn_HERE
        end
                
        function workPath = getWorkFolder( obj )
            workPath = obj.DirName;
        end
                
        function bool = isUpdateNeeded( obj,measurementModule,imageID,objectName)
            conn = obj.connectToDB();
            %Querying data
            imageID = obj.getImageIndexByName(imageID);            
            uniqueID = measurementModule.uniqueID();            
            objectID = conn.fetch(['SELECT objectID FROM Objects WHERE name=''' objectName ''' ;']);
            objectID = objectID{1};%objectID.objectID(1);            
            currentVersion = measurementModule.getCurrentVersion();
            mmtID = checkMmtInDB(obj,uniqueID,currentVersion);
                        
            results = conn.fetch(['SELECT * FROM Data WHERE '...
                'imageID = ' num2str(imageID) ' AND '...
                'objectID = ' num2str(objectID) ' AND ' ...
                'mmtID = ' num2str(mmtID) ';']);
            
            if isempty(results), bool = true; else, bool = false; end
            
            %REPLACE_closeConn_HERE
        end
                        
        function [ imageChannelList ] = listImageChannels(obj)
            conn = obj.connectToDB();
            results = conn.fetch('SELECT channelName,cpName FROM Channels;');
            imageChannelList = results(:,1);%results.channelName;
            imageCPList = results(:,2);%results.cpName;
            

            for i=1:length(imageChannelList)
                imageChannelList{i} = [imageChannelList{i} '::' imageCPList{i}];
            end
            
            %REPLACE_closeConn_HERE
        end
                
        function [ maskImage ] = retrieveMaskImage( obj, objectName, imageName)
            if strcmp(objectName,'Image')
                imgChanList = obj.listImageChannels();
                %retrieve the first channel just to see the image size
                img = obj.retrieveOriginalImage(imageName,imgChanList{1});
                maskImage = uint16(ones(size(img,1),size(img,2)));
            else
                conn = obj.connectToDB();
                objDirs = conn.fetch(['SELECT dirName,objectID FROM Objects WHERE name = ''' objectName ''';']);
                %Maka sure to split and rejoin the pathnames accordingly:
                
                dirName = correctSlashes(fullfile(obj.DirName,objDirs{1,1}));%fullfile(obj.DirName,objDirs.dirName{1});
                objectID = objDirs{1,2};
                [~,imageNameExex,~] = fileparts(imageName);
                imgResults = conn.fetch(['SELECT imageID FROM Images WHERE imgName = ''' imageNameExex ''';']);
                maskImgList = conn.fetch(['SELECT maskImgList FROM MaskImages WHERE objectID = ' num2str(objectID) ' AND imageID = ' num2str(imgResults{1,1}) ';']);
                
                %imageID contains also the extention
                %[~,fileExex,~] = fileparts(imageName);
                maskList = strsplit(maskImgList{1},';');%dir(fullfile(dirName,[fileExex '*']));

                nofLayers = length(maskList);

                if nofLayers>0
                    if nofLayers == 1
                        maskImage = imread(fullfile(dirName,maskList{1}));
                    else
                        firstLayer = imread(fullfile(dirName,maskList{1}));    
                        maskImage = zeros([size(firstLayer) nofLayers]);
                        maskImage(:,:,1) = firstLayer;
                        for i=2:nofLayers
                            maskImage(:,:,i) = imread(fullfile(dirName,maskList{i}));
                        end
                    end
                else
                    error('OA:ECP_maskNotFoundError',['The mask image ' imageName ' for object: ' objectName ' is not found.']);
                end
                
                
            end
        end
                
        function storeMaskImage(obj,maskImage,imageName,objectName)
            conn = obj.connectToDB();
            objResults = conn.fetch(['SELECT dirName,objectID FROM Objects WHERE name = ''' objectName ''';']);
            imgResults = conn.fetch(['SELECT imageID FROM Images WHERE imgName = ''' imageName ''';']);
            
            currentObjectMaskFolder = fullfile(obj.DirName,objResults{1,1});%fullfile(obj.DirName,results.dirName{1});
            
           maskExt = '.png';
            
            %Saves the maskImage to the folder. If the mask image has multiple
            %layers (i.e. its 3rd dimension is greater than 1) then it saves
            %multiple images
            
            %single layer
            if size(maskImage,3)==1
                imwrite(uint16(maskImage),fullfile(currentObjectMaskFolder,[imageName maskExt]));
                maskFileNames = {[imageName maskExt]};
            else
                [~,base,~] = fileparts(imageName);
                maskFileNames = cell(1,size(maskImage,3));
                for i=1:size(maskImage,3)
                    maskFileNames{i} = [base sprintf('_l%02d',i) maskExt];
                    imwrite(uint16(maskImage(:,:,i)),fullfile(currentObjectMaskFolder,maskFileNames{i}));
                end        
            end
            
            T = table(objResults{1,2},imgResults{1,1},{strjoin(maskFileNames,';')},'VariableNames',{'objectID','imageID','maskImgList'});
            obj.writeToDB(conn,'MaskImages',T.Properties.VariableNames,T);
                        
        end
                
        function [ origImg ] = retrieveOriginalImage(obj,imageID,imageChannel,~)
            %retieveOriginalImage
            %   ECP, imageID is the ACC image name, imageChannel is the ID of the given
            %   channel (struct, string, string)

            sepImgNames = strsplit(imageChannel,'::');
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT dirName FROM Channels WHERE channelName = ''' sepImgNames{1} ''' AND cpName = ''' sepImgNames{2} ''';']);
            currentPath = results{1};%results.dirName{1};

            onlyTextMatchInfo = strsplit(imageChannel,'::');
            imageChannel = onlyTextMatchInfo{1};
            
            primChan = conn.fetch('SELECT channelName FROM Channels WHERE prim = 1;');
            primChan = primChan{1};%primChan.channelName{1};

            splitted = strsplit(imageID,primChan);
            if length(splitted)>1
                imageRightChannelID = [splitted{1} imageChannel splitted{2}];
            else
                if strfind(imageID,primChan) == 1
                    imageRightChannelID = [imageChannel splitted{1}];
                else
                    imageRightChannelID = [splitted{1} imageChannel];
                end

            end
            %find the original file.            
            possibleFiles = dir(fullfile(currentPath,[imageRightChannelID '*']));

            if ~isempty(possibleFiles)
                origImg = imread(fullfile(currentPath,possibleFiles(1).name));
            else
                error('LipidAnalyser:ObjectAnalyserError','(ECP) Image: %s is not found in folder %s' ,imageRightChannelID,currentPath );
            end
            
            %REPLACE_closeConn_HERE
        end
                
        function setWorkFolder( obj, newWorkFolder)
            obj.DirName = newWorkFolder;
        end
                
        function setChannelDirectories(obj,newChannelDir)
            conn = obj.connectToDB();
            chanIDs = conn.fetch('SELECT channelID FROM Channels;');
            if (length(newChannelDir)~=size(chanIDs,1))
                error('LipidAnalyser:ObjectAnalyserError','The number of old and new channel does not match.');
            end
            for i=1:length(newChannelDir)
                conn.exec(['UPDATE Channels SET '...
                    ' dirName = ''' newChannelDir{i} ''' ' ...
                    ' WHERE channelID=' num2str(chanIDs{i}) ';']);
                %{
                conn.execute(['UPDATE Channels SET '...
                    ' dirName = ''' newChannelDir{i} ''' ' ...
                    ' WHERE channelID=' num2str(chanIDs.channelID(i)) ';']);
                %}
            end
            
            %REPLACE_closeConn_HERE
        end                
                
        function initImage(obj,imageID,idx)
            if ~obj.getImageIndexByName(imageID)
                conn = obj.connectToDB();            
                T = table(idx,{imageID},'VariableNames',{'imageID','imgName'});                
                SQLiteECP_interface.writeToDB(conn,'Images',T.Properties.VariableNames,T);
            end
        end       
        
        function prepareForSave(obj)
            close(obj.globConn);
        end
        
        function afterReOpen(obj)
            if ~exist(obj.dbParams.driverPath,'file')                
                driverPath = obj.getDriverPath();
                obj.dbParams.driverPath = driverPath;            
            end
            %Do a check here for an existing connection (so that an existing one is not perturbed)            
            if ~obj.connectToDB().IsOpen
                obj.createDBConn();
            end
        end
        
    end
    
    methods (Access = 'private')
        function driverPath = getDriverPath(~)
            [callPath,~,~] = fileparts(mfilename('fullpath'));            
            spltPath = strsplit(callPath,filesep);
            driverPath = strjoin([spltPath(1:end-2),'third-party','sqlite-jdbc-3.27.2.1.jar'],filesep);            
        end
        
        function createDBConn(obj)            
            if ~ismember(obj.dbParams.driverPath,javaclasspath('-dynamic'))
                javaaddpath(obj.dbParams.driverPath);
            end
            dbLocation = fullfile(obj.DirName,obj.dbParams.dbName);
            %dataBaseURL = [obj.dbParams.dataBaseURLhead dbLocation];
            %obj.globConn = database(obj.dbParams.dbName,obj.dbParams.username,obj.dbParams.password,obj.dbParams.driverURL,dataBaseURL);
            obj.globConn = sqlite(dbLocation);
        end
        
        function conn = connectToDB(obj)
            conn = obj.globConn;
        end
        
        function mmtID = checkMmtInDB(obj,uniqueID,queryVersion)
            %Gives back mmtID or 0 if it's not there yet
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT mmtID FROM Mmts WHERE uniqueID = ''' uniqueID ''' AND version=' num2str(queryVersion) ' ;']);
            if isempty(results)
                mmtID = 0;
            else
                mmtID = results{1};%results.mmtID(1);
            end
            
            %REPLACE_closeConn_HERE
        end
        
        function [ idx ] = getImageIndexByName(obj, imageID )
            conn = obj.connectToDB();
            results = conn.fetch(['SELECT imageID FROM Images WHERE imgName = ''' imageID ''';']);
            if isempty(results)
                idx = 0;
            else
                idx = results{1};%results.imageID(1);
            end            
            
            %REPLACE_closeConn_HERE
        end                
                
    end
    
    methods (Static, Access = 'protected')                    
        
        function writeToDB(conn,tableName,variableNames,T)
        % Function to write into the db, but in case of lock error it waits
        % for random time and then retries
            successWrite = false;
            while ~successWrite
                try
                    conn.insert(tableName,variableNames,T);
                    successWrite = true;
                catch e
                    if strcmp(e.identifier,'database:sqlite:interfaceError') && contains(e.message,'is locked, or a table in the database is locked. (database is locked)')
                        waitTime = exprnd(2);
                        fprintf('Lock error.\nWaiting for: %f sec\n',waitTime);
                        pause(waitTime);
                    else
                        rethrow(e)
                    end
                end
            end
        end
    end
end

