classdef (Abstract) ObjectAnalyserDataStructure < handle
% AUTHOR:   Abel Szkalisity
% DATE:     November 13, 2019
% NAME:     ObjectAnalyserDataStructure
%
% Abstract class to form interface for the available data structures in
% ObjectAnalyser. This is in principle the database connectivity.
% Implementations of this class may use own parameters to implement the
% required actions. It is important to keep in mind that the measurement
% functions should be callable also on parallel workers. Note that this
% class is handle, to reduce the need of variable passing.
%
% NOTE: Each subclass must implement a constructor with the parameter list
% described below.
    
   
    methods (Abstract)        
        % Constructor arguments:
        % INPUTS:
        %   RawImages       This should be a structure describing the name
        %                   and location of the original image files. It
        %                   must have 3 fields (ChannelName, Directory,
        %                   CPNames), which stand for respectively: the ID
        %                   of the channel that was used to identify it
        %                   from the image set, the full path to the folder
        %                   location and the name given to that channel in
        %                   CellProfiler. Each of these field must be an
        %                   equally long cellarray with strings.
        %   PrimaryChannelID An index specifying which is the primary
        %                   channel (i.e. the image name used for the
        %                   identification of the images within OA)
        %   DirName         The fullpath to the directory of the project
        %   ObjectNames     A cellarray of object names to be examined.                
        
        % TODO
        initObject(obj,objectName)
        
        % TODO
        removeObject(obj,objectName)
        
        % To load the requested measurement from the database
        oneColumnData = loadRequestedMeasurement(obj,measModule,imageID,objectName,selectedColumn)
        
        % To store the requested measurement in the database
        storeRequestedMeasurement( obj,featureStr )
        
        % Checks if the workFolder given in the argument contains the proper folders
        % for the object masks
        %
        % INPUTS: 
        %   workFolder      Working directory of OA
        %
        % OUTPUT:
        %   ok              Bool indicating of ALL the mask folders are
        %                   existing
        ok = checkMaskFolders( obj, workFolder )
        
        % Checks if the workFolder given in the argument contains the proper folders
        % for the object masks        
        %
        % OUTPUT:
        %   rawImgNames     The image names defined in CP (CP referece)
        rawImgNames = getCellProfilerRawImageNames( obj )                
        
        % TODO
        %This is tricky: index is the relative index of the non-empty
        %elements of the image structure.
        imageID = getImageID( obj,index )
        
        % TODO
        %This index is just matching one-to-one with the CellProfiler
        %CurrentSetBeingAnalyzed counter.
        imageID = getCPImageID( obj,realImgID )
        
        %TODO
        imageNameCell = getImageNames( obj )
        
        %TODO
        objectNameList = getObjectNames( obj )                                
        
        %TODO
        workPath = getWorkFolder( obj )
        
        %TODO
        bool = isUpdateNeeded( obj,measurementModule,imageID,objectName )
                
        %TODO
        [ imageChannelList ] = listImageChannels(obj)                    

        % To add a new image channel to the data structure.
        %  INPUTS:
        %   chanStruct      has 4 fields. First 3 are ChannelName, Directory,
        %                   CPNames similar to the constructor's RawImages
        %                   description. 4th entry is primChanBool a boolean
        %                   indicating if the added channel is primary.
        %  OUTPUT: 
        %   ok              bool to indicate if the operation was
        %                   successful
        ok = addImageChannel(obj, chanStruct)
        
        %TODO
        [ maskImage ] = retrieveMaskImage( obj, objectName, imageID)
        
        %TODO
        storeMaskImage(obj,maskImage,imageID,objectName)
        
        %TODO
        deleteMaskImage(obj,imageID,objectName)
        
        %TODO
        [ origImg ] = retrieveOriginalImage(obj,imageID,imageChannel,channelIndex)
        
        %TODO
        setWorkFolder( obj, newWorkFolder)      
        
        %TODO
        setChannelDirectories(obj,newChannelDir)
        
        %TODO
        channelDirs = getChannelDirectories(obj)
        
        %TODO
        initImage(obj,imageID,num)
        
    end
    
    methods
        
        function [featureStruct] = calculateRequestedMeasurement(obj,measModule,imageID,objectName)            
        % This function checks the consistency of the requested data in the
        % database and only if update is needed (or the data is missing) it
        % performs the measurement via the module.
        %
        % INPUTS: 
        %   measModule      The measurement module to be run (should be
        %                   child of the MeasurementModule Abstract Class)
        %   imageID         The ID of the image on which the measurement
        %                   should be run.
        %   objectName      The ID of the object on which the measurement
        %                   is done.
        %
        % OUTPUT:
        %   featureStruct   This is a cellarray of the performed
        %                   measurement feature structures. The structure
        %                   format is written down in the MeasurementModule
        %                   class. It is a cellarray because some meas.
        %                   module has to run multiple modules inside them        
            if obj.isUpdateNeeded(measModule,imageID,objectName)                
                featureStruct = measModule.measure(obj,imageID,objectName);            
            else
                featureStruct = [];
            end
        end              
        
        function [ parents ] = retrieveParents( obj,imageID,objectName,parentObjectName)
            
            childMask = obj.retrieveMaskImage(objectName,imageID);
            if ~strcmp(objectName,parentObjectName)    
                parentMask = obj.retrieveMaskImage(parentObjectName,imageID);

                if strcmp(objectName,parentObjectName)
                    handles = struct('Measurements',struct(objectName,[]));
                else
                    handles = struct('Measurements',struct(objectName,[],parentObjectName,[]));
                end
                handles.Current.SetBeingAnalyzed = 1;
                %parent retrieval is pushed back to on the fly calculations (to maintain flexibility)
                [~,~,parents] = CPrelateobjects(handles,objectName,parentObjectName,double(childMask),double(parentMask),'Retrieve parents');
            else
                indices = unique(childMask(:));
                if indices(1) == 0
                    parents = indices(2:end);        
                else
                    parents = indices;
                end
            end

        end
        
        function prepareForSave(~)    
        end

        function afterReOpen(~)        
        end
        
    end        
    
    methods (Static, Access = 'protected')
            
        function saveMaskImage(maskImage,folder,imageName)
        %Saves the maskImage to the folder. If the mask image has multiple
        %layers (i.e. its 3rd dimension is greater than 1) then it saves
        %multiple images
        
            %single layer
            if size(maskImage,3)==1
                imwrite(uint16(maskImage),fullfile(folder,imageName));
            else
                [~,base,~] = fileparts(imageName);
                for i=1:size(maskImage,3)
                    imwrite(uint16(maskImage(:,:,i)),fullfile(folder,[base sprintf('_l%02d',i) '.tif']));
                end        
            end
        end
        
        function deleteMaskImageDisk(currentObjectMaskFolder,imageID)
            [~,base,~] = fileparts(imageID);
            d = dir(fullfile(currentObjectMaskFolder,[base '*']));
            for i=1:numel(d)
                delete(fullfile(currentObjectMaskFolder,d(i).name));
            end
        end
        
    end
end

