function sameModule = isSameModule(moduleA,moduleB)
% To modules as returned by extractModuleFromPipeline.
% returns true if they are the same BY parameters.
    cellFields = {'ModuleNames','VariableValues','VariableInfoTypes'};
    doubleFields = {'VariableRevisionNumbers','ModuleRevisionNumbers','NumbersOfVariables'};
    
    sameModule = 1;
    for i=1:length(cellFields)
        if any(  size(moduleA.(cellFields{i})) ~= size(moduleB.(cellFields{i})) ) % This essentially checks for equal amount of parameters
            sameModule = 0; return;        
        end
        for j=1:size(moduleA.(cellFields{i}),2) % one loop is enough as this is module to module comparison
            %if NOT (both empty or equal strings) <=> not BOTH empty AND not equal strings. This is needed as strcmp([],[]) gives false.
            if ~(isempty(moduleA.(cellFields{i}){j}) && isempty(moduleB.(cellFields{i}){j}) ) && ...
                    ~strcmp(moduleA.(cellFields{i}){j},moduleB.(cellFields{i}){j})
                sameModule = 0; return;
            end        
        end
    end

    for i=1:length(doubleFields)
        % double fields are always scalar so no need to check for size here        
        if moduleA.(doubleFields{i}) ~= moduleB.(doubleFields{i})
            sameModule = 0; return;
        end            
    end
  
end