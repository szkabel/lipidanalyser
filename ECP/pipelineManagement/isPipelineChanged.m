function different = isPipelineChanged(oldPIPE, newPIPE)
%Checks by all settings if the pipeline was changed


cellFields = {'ModuleNames','VariableValues','VariableInfoTypes'};
doubleFields = {'VariableRevisionNumbers','ModuleRevisionNumbers','NumbersOfVariables'};

different = 0;
for i=1:length(cellFields)
    if any(  size(oldPIPE.(cellFields{i})) ~= size(newPIPE.(cellFields{i})) )
        different = 1; return;        
    end
    for j=1:size(oldPIPE.(cellFields{i}),1)
        for k=1:size(oldPIPE.(cellFields{i}),2)
            %if NOT (both empty or equal strings) <=> not BOTH empty AND not equal strings
            if ~(isempty(oldPIPE.(cellFields{i}){j,k}) && isempty(newPIPE.(cellFields{i}){j,k}) ) && ...
                    ~strcmp(oldPIPE.(cellFields{i}){j,k},newPIPE.(cellFields{i}){j,k})
                different = 1; return;
            end
        end
    end
end

for i=1:length(doubleFields)
    if any(  size(oldPIPE.(doubleFields{i})) ~= size(newPIPE.(doubleFields{i})) )
        different = 1; return;        
    end
    for j=1:size(oldPIPE.(doubleFields{i}),1)
        for k=1:size(oldPIPE.(doubleFields{i}),2)
            %if NOT (both empty or equal strings) <=> not BOTH empty AND not equal strings
            if oldPIPE.(doubleFields{i})(j,k) ~= newPIPE.(doubleFields{i})(j,k)
                different = 1; return;
            end
        end
    end
end

if isfield(oldPIPE,'ObjectExport') && isfield(newPIPE,'ObjectExport')        
    %First one is the paramcell parameter
    objToExportIdx = 1;
    newObjectStr = newPIPE.ObjectExport.paramcell{objToExportIdx};    
    if ~iscell(newObjectStr), newObjectStr = {newObjectStr}; end
    oldObjectStr = oldPIPE.ObjectExport.paramcell{objToExportIdx};    
    if ~iscell(oldObjectStr), oldObjectStr = {oldObjectStr}; end
    if length(newObjectStr) ~= length(oldObjectStr) || ~all(cellfun(@strcmp,newObjectStr,oldObjectStr))
        different = 1;
        return;
    end    
elseif xor(isfield(oldPIPE,'ObjectExport'),isfield(newPIPE,'ObjectExport'))
    different = 1; 
    return;
end


end