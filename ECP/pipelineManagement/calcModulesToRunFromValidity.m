function up2Date = calcModulesToRunFromValidity(newPIPE,validModules)
% newPIPE is a cellprofiler pipeline in struct form
% valid modules is a bool array as long as the number of modules in the
% pipeline. It calculates which modules need to be run to update all
% invalid modules (propagates image dependency)
% Update requirement is dependent on the dependency type:
%       a) If a module is object-group dependent on a valid module,
%       then the parent module need NOT to be re-run (as the object
%       mask is valid and still stored)
%       b) Otherwise (in case of image dependency or dependency on
%       invalid module) the parent needs to be run too.    

    nofModules = length(newPIPE.ModuleNames);
    up2Date = zeros(nofModules,1);
    up2Date(validModules) = 1;
    objectDepends = calcPipeDependency(newPIPE, 'object');
    imageDepends = calcPipeDependency(newPIPE, 'image');
    % A bit custom part to make it work with the FilterObjectMeasurement module
    % The FilterObjectMeasurement is unforunately not specifying that it
    % depends on the output of measurement modules as there the data is not
    % propagated via images or objects but rather as a field. From our
    % point of view this is an "image dependency" type realationship, as
    % the data is not stored anywhere (only calculated objects), hence the
    % pipeline needs to run those modules. So here let's make the
    % FilterObjectMeasurementModule image dependent on all measurement type
    % modules.
    imageDepends(strcmp(newPIPE.ModuleNames,'FilterByObjectMeasurement'),startsWith(newPIPE.ModuleNames,'MeasureObject')) = 1;
    for i=1:nofModules
        for j=1:nofModules
            if ~up2Date(j)
                up2Date(imageDepends(j,:)) = 0;
                up2Date(objectDepends(j,:) & ~validModules) = 0;
            end
        end
    end

end

