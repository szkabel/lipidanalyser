function depends = calcPipeDependency(pipe, type)
    if nargin<2
        type = 'all';
    end
    % We should create a dependency map:    
    % Should go for each module what it depends on
    nofModules = length(pipe.ModuleNames);
    depends = false(nofModules);
    % depends(i,j) means that module i depends on the output of j
    matchingDim = min(size(pipe.VariableValues),size(pipe.VariableInfoTypes));
    for i=nofModules:-1:1    
        for j=1:matchingDim(2) % for all possible variables            
            if ~isempty(pipe.VariableInfoTypes{i,j})                
                if ~contains(pipe.VariableInfoTypes{i,j},'indep') && ... % if it is NOT independent i.e. dependent AND...
                        (strcmp(type,'all') || (strcmp(type,'image') && startsWith(pipe.VariableInfoTypes{i,j},'imagegroup')) || (strcmp(type,'object') && startsWith(pipe.VariableInfoTypes{i,j},'objectgroup')))
                    % AND either the matching type is all (then check in all cases, or check only the specific dependency.
                    % define dependence
                    indepGroups = strcmp(pipe.VariableInfoTypes,[pipe.VariableInfoTypes{i,j} ' indep']);
                    variableAppears = strcmp(pipe.VariableValues,pipe.VariableValues{i,j});
                    parentModule = any(indepGroups(1:matchingDim(1),1:matchingDim(2)) & variableAppears(1:matchingDim(1),1:matchingDim(2)),2);
                    depends(i,parentModule) = 1; 
                end
            end
        end    
    end

end