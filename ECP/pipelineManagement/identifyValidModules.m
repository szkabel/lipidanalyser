function [validModules] = identifyValidModules(newPIPE, oldPIPE)
% Takes 2 pipelines, and identifies the modules in the new pipeline that
% are still valid (meaning that there is a module in the old pipeline with
% which its parameters are completely matching and all predecessor modules
% are also valid).

nofModules = length(newPIPE.ModuleNames);
matchingModules = cell(nofModules,1); % every entry is a vector with the indices of the matching modules

for i=1:nofModules
    tgtModule = extractModuleFromPipeline(newPIPE,i);
    matchingModules{i} = findMatchingModule(tgtModule,oldPIPE);
end

validModules = false(0,nofModules);
depends = calcPipeDependency(newPIPE);
if any(diag(depends))
    badMods = find(diag(depends));
    if length(badMods) == 1
        error('LipidAnalyzer:SelfDependentModule',['Module ' sprintf('%d ',badMods) 'is self-dependent!']);
    else
        error('LipidAnalyzer:SelfDependentModule',['Modules ' sprintf('%d ',badMods) 'are self-dependent!']);
    end    
end
% depends(i,j) means that module i depends on j

for i=1:nofModules
    validModules(i) = computeValidity(i,matchingModules, depends);
end

end

function isValid = computeValidity(currentIdx,matchingModules, depends)
% this will be the recursive call
    isValid = 1;
    if isempty(matchingModules{currentIdx}) % there is no matching module
        isValid = 0;
        return;
    end
    for i=1:size(depends,2)
        if depends(currentIdx,i)
            isValid = isValid & computeValidity(i,matchingModules,depends);
        end
    end

end

