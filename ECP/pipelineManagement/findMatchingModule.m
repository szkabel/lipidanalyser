function findIdx = findMatchingModule(tgtModule, otherPipe)
% This is a simple function that is searching for a list of possible
% modules that are the same exclusively by their settings (not yet
% dependencies). otherPipe is still in a pipeline form.
% returns the indices of other pipe which are matching exactly (by
% parameters) with tgtModule

nofModules = length(otherPipe.ModuleNames);
findIdx = zeros(1,nofModules);

for i=1:nofModules
    otherMod = extractModuleFromPipeline(otherPipe,i);
    if isSameModule(tgtModule,otherMod)
        findIdx(i) = 1;
    end
end

findIdx = find(findIdx);

end