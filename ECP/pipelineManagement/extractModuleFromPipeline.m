function module = extractModuleFromPipeline(pipeline,moduleIdx)
% reduces the pipeline structure only to the module in question

module = [];
vecFields = {'ModuleNames','VariableRevisionNumbers','ModuleRevisionNumbers','NumbersOfVariables'};
matFields = {'VariableValues','VariableInfoTypes'};

for i=1:length(vecFields)
    if length(pipeline.(vecFields{i}))<moduleIdx
        error('There is only %d modules in the pipeline',length(pipeline.(vecFields{i})))
    else
        module.(vecFields{i}) = pipeline.(vecFields{i})(moduleIdx);
    end
end

for i=1:length(matFields)
    if size(pipeline.(matFields{i}),1)<moduleIdx
        error('There is only %d modules in the pipeline',size(pipeline.(matFields{i}),1))
    else
        module.(matFields{i}) = pipeline.(matFields{i})(moduleIdx,:);
    end
end


end