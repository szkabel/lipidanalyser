function up2Date = maskOutputChanged(pipe,objName,ECP)
    objectDep = calcPipeDependency(pipe,'object');
    [objectNames, creatingModule] = collectCPGroup(pipe,'objectgroup indep');
    creatingModuleIdx = creatingModule(strcmp(objectNames,objName));
    dependentModules = objectDep(:,creatingModuleIdx);
    
    validModules = true(1,size(objectDep,1));
    validModules(dependentModules) = 0;
    % We should still invalidate all the stored data (as one cannot say
    % from the database the measurement-object dependencies.
    ECP.resetData();
    up2Date = calcModulesToRunFromValidity(pipe,validModules);
            
end