function varargout = InteractiveWindow(varargin)
% INTERACTIVEWINDOW MATLAB code for InteractiveWindow.fig
%      INTERACTIVEWINDOW, by itself, creates a new INTERACTIVEWINDOW or raises the existing
%      singleton*.
%
%      H = INTERACTIVEWINDOW returns the handle to a new INTERACTIVEWINDOW or the handle to
%      the existing singleton*.
%
%      INTERACTIVEWINDOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INTERACTIVEWINDOW.M with the given input arguments.
%
%      INTERACTIVEWINDOW('Property','Value',...) creates a new INTERACTIVEWINDOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before InteractiveWindow_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to InteractiveWindow_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help InteractiveWindow

% Last Modified by GUIDE v2.5 11-Mar-2020 19:10:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @InteractiveWindow_OpeningFcn, ...
                   'gui_OutputFcn',  @InteractiveWindow_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before InteractiveWindow is made visible.
function InteractiveWindow_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to InteractiveWindow (see VARARGIN)
%
% VARARGIN by mostly name-value pairs
%   imageList: Cellarray of image names that are matching the LoadImages
%              criteria
%       CPhandles: The CellProfiler handles (struct)
%       directoy:  The working directory path
%       pipelineChanged: bool to indicate if the pipeline was changed since the
%       previous one
%       uData: a full uData object containing all neccessary info (optionally we are starting up from this one)

% Choose default command line output for InteractiveWindow
handles.output = hObject;

p = inputParser;
%addRequired(p,'imgList');
%addRequired(p,'CPhandles');
%addRequired(p,'directory');
%addRequired(p,'pipelineChanged');
%addParameter(p,'uData',[]);

parse(p,varargin{:});

global Project;

imageList = Project.IAWin.imageList; %p.Results.imgList;
nofImages = length(imageList);


uData = get(handles.MainFigure,'UserData');
if isFieldRec(Project,{'IAWin','up2Date'})
    uData.up2Date = Project.IAWin.up2Date;
end
if isFieldRec(Project,{'IAWin','visualizationSettings'})    
    uData.visualizationSettings = Project.IAWin.visualizationSettings;
end

CPhandles = Project.IAWin.CPHandles; %p.Results.CPhandles;
uData.CPhandles = CPhandles;
uData.DirName = Project.workDir; %p.Results.directory;
uData.selectedImage = 0;
uData = initNavigation(uData);
uData = initCSMModule(uData);

placeItemsOnGUI(handles);

set(handles.ImageList,'String',imageList);

%load(fullfile(uData.DirName,'ECP.mat'),'ECP'); %Load the ECP
%ECP.afterReOpen();
ECP = Project.ECP;

if ~isfield(uData,'visualizationSettings') %check for existing settings
    uData.visualizationSettings = setupDefaultVisualizationSettings(ECP); %ECP is coming from the load
else % Still, even if it exists, it may not be consistent with ECP's requirement:
    % Both for objects
    toBeSeenObjs = ECP.getObjectNames();
    currObjs = cell(length(uData.visualizationSettings.outlines),1);        
    for j=1:length(currObjs), currObjs{j} = uData.visualizationSettings.outlines{j}.name; end
    % And channels
    tobeSeenChannels = listImageChannels(ECP);
    currChans = cell(length(uData.visualizationSettings.channels),1);        
    for j=1:length(currChans), currChans{j} = uData.visualizationSettings.channels{j}.name; end
    if ~all(cellfun(@ismember,toBeSeenObjs,repmat({currObjs},length(toBeSeenObjs),1))) || ...
       ~all(cellfun(@ismember,tobeSeenChannels,repmat({currChans},length(tobeSeenChannels),1)))    
        oldVisSettings = uData.visualizationSettings;
        % GET NEW SETTINGS HERE
        uData.visualizationSettings = setupDefaultVisualizationSettings(ECP);
        %in case there is some overlap keep at least those settings
        for j=1:length(uData.visualizationSettings.outlines)
            for k=1:length(oldVisSettings.outlines)
                if strcmp(uData.visualizationSettings.outlines{j}.name,oldVisSettings.outlines{k}.name)
                    uData.visualizationSettings.outlines{j} = oldVisSettings.outlines{k};
                end
            end
        end        
        % for channels too
        for j=1:length(uData.visualizationSettings.channels)
            for k=1:length(oldVisSettings.channels)
                if strcmp(uData.visualizationSettings.channels{j}.name,oldVisSettings.channels{k}.name)
                    uData.visualizationSettings.channels{j} = oldVisSettings.channels{k};
                end
            end
        end    
    end
    % It might also happen that some objects have to be deleted
    toDel = ~cellfun(@ismember,currObjs,repmat({toBeSeenObjs},length(currObjs),1));
    uData.visualizationSettings.outlines(toDel) = [];
    % and channels too
    toDel = ~cellfun(@ismember,currChans,repmat({tobeSeenChannels},length(currChans),1));
    uData.visualizationSettings.channels(toDel) = [];    
end

[handles,uData.channelPars,uData.contourPars] = createVisualizationPanel(handles,uData.visualizationSettings);

if ~isfield(uData,'up2Date') || isempty(uData.up2Date)
    uData.up2Date = zeros(length(uData.CPhandles.Settings.ModuleNames),nofImages);    
end

uData.ECP = ECP;

set(handles.MainFigure,'UserData',uData);

[~,chanSet] = displayImage(handles);
uData.visualizationSettings.channels = chanSet;
visualizationSettings2GUI(uData.visualizationSettings,handles);
% Update handles structure
guidata(hObject, handles);

%set the stop to 0.
global GlobalStopCellProfilerIndicator;
GlobalStopCellProfilerIndicator = 0;

% UIWAIT makes InteractiveWindow wait for user response (see UIRESUME)
% uiwait(handles.MainFigure);

end

% --- Outputs from this function are returned to the command line.
function varargout = InteractiveWindow_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

end

% --- Executes during object creation, after setting all properties.
function MainFigure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%
% LIST OF PROPERTY NAMES:
%       + ImageList     - listbox
%       + MainView      - axes

%set everything to be in pixels
set(hObject,'Units','pixels');

%CREATE objects
handles = createInteractiveGUI(hObject);

uData = get(hObject,'UserData');
uData.up2Date = [];
uData.selectedImage = 0;
set(hObject,'UserData',uData);

guidata(hObject, handles);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% REGULAR CALLBACKS %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes when MainFigure is resized.
function MainFigure_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~isempty(handles)
    placeItemsOnGUI(handles);
end
uData = get(handles.MainFigure,'UserData');
try    
    [~,chanSet] = displayImage(handles);
    uData.visualizationSettings.channels = chanSet;
catch    
    uData.selectedImage = 0;    
end
set(handles.MainFigure,'UserData',uData);

end

function BackToSettingsButton_Callback(hObject,eventdata,handles)
%Back to CellProfiler and possibly modify the pipeline
    uiresume(handles.MainFigure);
    
end

function ImageList_Callback(hObject,eventdata,handles)
%Change in the image list selection and request for loading it
    global GlobalStopCellProfilerIndicator;    

    imgNumber = get(handles.ImageList,'Value');
    uData = get(handles.MainFigure,'UserData');
    ECP = uData.ECP;
    
    [succFinished] = conditionalImageAnalysisRunOnImage( imgNumber, handles, ECP);    
    
    if succFinished      
        uData = get(handles.MainFigure,'UserData');
        [imgToShow,chanSet] = displayImage(handles);        
        %handles.MainView.XLim = [0.5 size(imgToShow,2)];
        %handles.MainView.YLim = [0.5 size(imgToShow,1)];
        uData.visualizationSettings.channels = chanSet;        
        set(handles.MainFigure,'UserData',uData);
    end
    
    %Included since project handling
    updateModuleActivityIfProjectExists();
    
    GlobalStopCellProfilerIndicator = 0; %set back to 0;        

end

function VisualizationPanel_Callback(hObject,~,handles)
    uData = get(handles.MainFigure,'UserData');
    uData.visualizationSettings = GUI2visualizationSettings(handles,uData.visualizationSettings);
    set(handles.MainFigure,'UserData',uData);
    [~,chanSet] = displayImage(handles);
    uData.visualizationSettings.channels = chanSet;
    matchUpObjectSelectorColors(handles,uData.visualizationSettings.outlines);
    set(handles.MainFigure,'UserData',uData);    
    
end

function AnalyzeAll_Callback(~,~,handles)    
    
    global GlobalStopCellProfilerIndicator;
    imageList = get(handles.ImageList,'string');
    nofImages = length(imageList);
    figSize = [0 0 500 300];
    analyseAllFigure = figure('Name','Analyse all images','Units','Pixels','Position',figSize);    
    dataListHandle = uitable('Parent',analyseAllFigure,'Units','Pixels','Position',[10 10 figSize(3)-20 figSize(4)-75],'Data',{},'ColumnWidth',{(figSize(3)-20)/2,(figSize(3)-20)/2-35});
    currentText = uicontrol('Parent',analyseAllFigure,'Units','Pixels','Position',[10 figSize(4)-55 figSize(3)-20 50],'Style','text','HorizontalAlignment','center');
    elapsedTimes = zeros(1,nofImages);
    allAnal = tic;
    
    uData = get(handles.MainFigure,'UserData');
    ECP = uData.ECP;
        
    for imgNumber=1:nofImages
        validTimes = elapsedTimes(1:imgNumber-1);
        validTimes = validTimes(validTimes>0.5); %the other images are most probably runned already therefore they ruin the estimation
        set(currentText,'String',sprintf('Analysing image: %s\n Estimated time left: %.2f sec\n [%d/%d DONE]',imageList{imgNumber},(nofImages-imgNumber+1)*mean(validTimes),imgNumber-1,nofImages )  );
        tic 
        [succFinish] = conditionalImageAnalysisRunOnImage( imgNumber, handles, ECP);
        if ~succFinish
            if GlobalStopCellProfilerIndicator
                close(analyseAllFigure);
                GlobalStopCellProfilerIndicator = 0;            
            end    
            break;
        end
        elapsedTimes(imgNumber) = toc;
        prevList = get(dataListHandle,'Data');
        prevList(end+1,:) = {imageList{imgNumber},sprintf('%.4f sec',elapsedTimes(imgNumber))};
        set(dataListHandle,'Data',prevList);
        if GlobalStopCellProfilerIndicator
            close(analyseAllFigure);
            GlobalStopCellProfilerIndicator = 0;
            break;
        end                
    end
    
    %Included since project handling
    updateModuleActivityIfProjectExists();
    
    set(currentText,'String',sprintf('Image analysis is finished.\n Estimated time left: %.0f sec\n [%d/%d DONE] ',0,nofImages,nofImages )  );  
    fprintf('Full analysis time:%f\n',toc(allAnal));
    
end

function AnalyzeAllParralel_Callback(~,~,handles)
    allAnal = tic;
    imageList = get(handles.ImageList,'string');
    nofImages = length(imageList);    
    m = msgbox(sprintf('Parallel analysis is running...\n Time can''t be estimated and the process can''t be stopped.'));    
    up2Date = handles.MainFigure.UserData.up2Date;
    cpHandles = handles.MainFigure.UserData.CPhandles;    
    
    ECP = handles.MainFigure.UserData.ECP;
    
    ECP.prepareForSave();
    up2Date = mat2cell(up2Date,size(up2Date,1),ones(1,size(up2Date,2)));
    parfor imgNumber=1:nofImages
        ECP.afterReOpen(); %#ok<PFBNS>
        [succFinished,up2Date{imgNumber}] = condImgAnalForParal( imgNumber, up2Date{imgNumber}, cpHandles, ECP);
        if succFinished
            fprintf('*******************\n ---  %d/%d READY  ---\n*******************\n',imgNumber,nofImages);
        else
            fprintf(['*******************\n |===  /==\  /==\   \n'...'
                                          ' |     |  |  |  |   \n'...
                                          ' |=    |--/  |--/   \n'...
                                          ' |     |\    |\     \n'...
                                          ' |===  | \   | \    \n'...
                                          '---  %d/%d ERROR  ---\n*******************\n'],imgNumber,nofImages);
        end        
    end
    up2Date = cell2mat(up2Date);
        
    ECP.afterReOpen();
    
    %Included since project handling
    updateModuleActivityIfProjectExists();
    
    handles.MainFigure.UserData.up2Date = up2Date;
    if ishandle(m), close(m); end
    msgbox('Parallel analysis is finished.');
    fprintf('Full analysis time:%f\n',toc(allAnal));

end

function Stop_Callback(~,~,~)
    global GlobalStopCellProfilerIndicator;
    GlobalStopCellProfilerIndicator = 1;    
       
end

function DataExport_Callback(hObject,eventdata,handles)        
    uData = get(handles.MainFigure,'UserData');
    ECP = uData.ECP;
    ECP.prepareForSave();
    save(fullfile(handles.MainFigure.UserData.DirName,'ECP.mat'),'ECP');
    ECP.afterReOpen();
    ObjectAnalyser_GUI({uData.DirName});    
end

% --------------------------------------------------------------------
function pushtool_SaveContourImages_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to pushtool_SaveContourImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.MainFigure,'UserData');
outputFolder = uigetdir(uData.DirName,'Specify the folder for the images');
if outputFolder == 0
    return;
end
imgList = get(handles.ImageList,'String');
h = waitbar(0,sprintf('Saving images %d%%',0));
for i = 1:size(uData.up2Date,2) %[3 84 1376 1459] %[1, 899, 1307, 1737, 10, 46, 1302, 1559, 1628, 1445]%
    uData.selectedImage = i;
    set(handles.MainFigure,'UserData',uData);
    if all(uData.up2Date(:,i))
        %during save we are not updating the images in the smart way any
        %more to maintain consistency
        imgToSave = displayImage(handles,0);
        imwrite(imgToSave,fullfile(outputFolder,imgList{i}));
    end
    if ishandle(h),  waitbar(i/size(uData.up2Date,2),h,sprintf('Saving images... %2.0f%%',i/size(uData.up2Date,2)*100));  end
end

f = fopen(fullfile(outputFolder,'channelParams.txt'),'w');
for i=1:length(handles.MainFigure.UserData.visualizationSettings.channels)
    printStruct(handles.MainFigure.UserData.visualizationSettings.channels{i},f,0);
end
fclose(f);


if ishandle(h), close(h); end
msgbox('Images created successfully');

end

% --------------------------------------------------------------------
function pushtool_saveInteractive_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to pushtool_saveInteractive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% TODO: CURRENTLY NOT WORKING FUNCTIONALITY 

screensize = get(0,'Screensize');
infoD = dialog('Name','Save visualization','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
UC = uicontrol('Parent',infoD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','Getting figure data...');
pause(0.05);

uData = get(handles.MainFigure,'UserData');
set(UC,'String','Removing figure handles from CellProfiler structure...');
pause(0.05);
uData.CPhandles = iterateOverCellAndStruct(uData.CPhandles,@removeHandle);
imgList = get(handles.ImageList,'String');

pathName = uData.DirName;
set(UC,'String','Load ECP');
pause(0.05);
load(fullfile(pathName,'ECP.mat'),'ECP');
VisualizationState.uData = uData;
VisualizationState.imgList = imgList;
set(UC,'String','Save ECP');
pause(0.05);
save(fullfile(pathName,'ECP.mat'),'ECP','VisualizationState');
set(UC,'String','DONE. The visualization metadata is saved for future use');
pause(2);
if ishandle(infoD), close(infoD); end

end

function output = removeHandle(input)
    if ishandle(input)
        output = [];
    elseif any(isgraphics(input))
        output = [];
    else
        output = input;
    end

end

% --------------------------------------------------------------------
function saveContourImageParallel_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to saveContourImageParallel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global myParallelWaitbarHandle;

uData = get(handles.MainFigure,'UserData');
u2d = uData.up2Date;
outputFolder = uigetdir(uData.DirName,'Specify the folder for the images');
if outputFolder == 0
    return;
end
imgList = get(handles.ImageList,'String');

%Trying parfor waitbar
infoStr = 'Saving analyzed images';
clear updateWaitBar; %'0-ing the counter'
myParallelWaitbarHandle = waitbar(0,sprintf('%s %d%% (%d/%d)',infoStr,0,0,sum(all(u2d,1))));
D = parallel.pool.DataQueue;
afterEach(D, @updateWaitBar);

%This is needed for ensuring the parallel run goes smoothly
ECP = handles.MainFigure.UserData.ECP;
ECP.prepareForSave();
parfor i=find(all(u2d,1))
    ECP.afterReOpen(); %#ok<PFBNS> %Fine it is a handle    
    imgToSave = displayImage(handles,0,i);
    imwrite(imgToSave,fullfile(outputFolder,imgList{i}));    
    send(D, struct('str',infoStr,'maxIter',sum(all(u2d,1))));
    pause(0.001);
end
%Open back the connection
ECP.afterReOpen();

if ishandle(myParallelWaitbarHandle), close(myParallelWaitbarHandle); end

msgbox('Images created successfully');   

end

% --- Executes when user attempts to close MainFigure.
function MainFigure_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
% delete(hObject);
global Project;
 
if ~isempty(Project)
    Project.figHandles.ModuleFigHandles.button_IA.Value = 0;
    ModuleHandler_GUI('button_IA_Callback',Project.figHandles.ModuleFigHandles.button_IA,[],Project.figHandles.ModuleFigHandles);
else
    hObject.Visible = 'off';
end

end

% --------------------------------------------------------------------
function toggleObjectEditEraser_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toggleObjectEditEraser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    objectEditToggleCallback(hObject,handles,'delete')
end


% --------------------------------------------------------------------
function toggleObjectEditExtender_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toggleObjectEditExtender (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    objectEditToggleCallback(hObject,handles,'extend')
end


% --- Executes on mouse motion over figure - except title and menu.
function MainFigure_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    currPos = get(handles.MainFigure,'CurrentPoint');    
    winPos = handles.MainView.Position;
    if winPos(1) <= currPos(1) && winPos(2) <= currPos(2) && winPos(1) + winPos(3) >= currPos(1) && winPos(2)+winPos(4) >= currPos(2)
        mainViewButtonMotion(handles,currPos);
    end
end


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function MainFigure_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    uData = get(handles.MainFigure,'UserData');    
    switch hObject.SelectionType
        case 'normal'
            if strcmp(handles.toggleObjectEditExtender.State,'on')
                if ~isempty(uData.CSM.selectedObject) && ishandle(uData.CSM.selectedObject.roi{1})
                    addlistener(uData.CSM.editObject,'MovingROI', @(varargin)editFreehand(uData.CSM.selectedObject.roi{1}, uData.CSM.editObject,'extend'));            
                end
            elseif strcmp(handles.toggleObjectEditEraser.State,'on')
                if ~isempty(uData.CSM.selectedObject) && ishandle(uData.CSM.selectedObject.roi{1})
                    addlistener(uData.CSM.editObject,'MovingROI', @(varargin)editFreehand(uData.CSM.selectedObject.roi{1}, uData.CSM.editObject,'delete'));
                end
            end
            set(handles.MainFigure,'UserData',uData);
        case 'alt'
            currPos = get(handles.MainFigure,'CurrentPoint');
            imgXYRatio = handles.MainView.PlotBoxAspectRatio(1)/handles.MainView.PlotBoxAspectRatio(2);
            [posInImage,~,~,~,~,~,clickInImageBool] = calculateCursorPositionInImageAxes(handles.MainView.Position,currPos,imgXYRatio,handles.MainView.XLim,handles.MainView.YLim);
            if clickInImageBool
                uData.Nav.drag = posInImage;
                set(handles.MainFigure,'Pointer','fleur')
            end
    end
    set(handles.MainFigure,'UserData',uData);
end




% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function MainFigure_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    uData = get(handles.MainFigure,'UserData');
    
    switch hObject.SelectionType
        case 'normal'
            if ~isempty(uData.CSM.eventListener)
                delete(uData.CSM.eventListener);
            end
        case 'alt'                        
            uData.Nav.drag = [];
            set(handles.MainFigure,'Pointer','arrow')
    end    
    
    set(handles.MainFigure,'UserData',uData);
end


% --- Executes on scroll wheel click while the figure is in focus.
function MainFigure_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

    zoomRatio = 0.9;
    
    currPos = get(handles.MainFigure,'CurrentPoint');
    imgXYRatio = handles.MainView.PlotBoxAspectRatio(1)/handles.MainView.PlotBoxAspectRatio(2);
    [editObjPos,currPosWithinAxes,xStart,xExtent,yStart,yExtent] = calculateCursorPositionInImageAxes(handles.MainView.Position,currPos,imgXYRatio,handles.MainView.XLim,handles.MainView.YLim);            
        
    if eventdata.VerticalScrollCount<0         
        z = zoomRatio;
    else                
        z = 1/zoomRatio;
    end
    
    [nx1,nx2,ny1,ny2] = calculateZoomLimits(editObjPos,currPosWithinAxes,xStart,xExtent,yStart,yExtent,z,handles);
    
    handles.MainView.XLim = [nx1 nx2];
    handles.MainView.YLim = [ny1 ny2]; 
end


% --- Executes on key press with focus on MainFigure or any of its controls.
function MainFigure_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

imgObj = findobj(handles.MainView.Children,'flat','-property','CData');
maxSizes = size(imgObj.CData);
mTB = handles.mainToolbar;
uData = get(handles.MainFigure,'UserData');
diffSize = 5;

switch eventdata.Key
    case 'add'
        if ~isempty(uData.CSM.editObject), uData.CSM.editObject.Radius = uData.CSM.editObject.Radius + diffSize; end
    case 'subtract'        
        if ~isempty(uData.CSM.editObject) && uData.CSM.editObject.Radius>diffSize, uData.CSM.editObject.Radius = uData.CSM.editObject.Radius - diffSize; end
    case 'f' % for full
        handles.MainView.XLim = [0 maxSizes(2)];
        handles.MainView.YLim = [0 maxSizes(1)];
    case 'd' % for deletion
        handles.toggleObjectEditEraser.State = 'On';       
        objectEditToggleCallback(handles.toggleObjectEditEraser,handles,'delete');
    case 'e' % for extend        
        handles.toggleObjectEditExtender.State = 'On';
        objectEditToggleCallback(handles.toggleObjectEditExtender,handles,'extend');
    case 'q'
        handles.toggleObjectEditExtender.State = 'Off';
        objectEditToggleCallback(handles.toggleObjectEditEraser,handles,'delete');
        handles.toggleObjectEditExtender.State = 'Off';
        objectEditToggleCallback(handles.toggleObjectEditExtender,handles,'extend');
    case 'm' % for mask
        if ~isempty(uData.CSM.selectedObject) && length(uData.CSM.selectedObject.roi)~=1
            msgbox('You can only update one mask at a time!');
        elseif ~isempty(uData.CSM.selectedObject)
            res = questdlg('Are you sure you would like to update the mask of this image?','Overwrite mask');
            if ~isempty(res) && strcmp(res,'Yes')
                uData = updateMaskCSM(uData);
                set(handles.MainFigure,'UserData',uData); %needs to be done separately not just at the end, as e.g. newMaskCSM is modifying the uData inside
                set(handles.toggleObjectEditExtender,'State','off');
                set(handles.toggleObjectEditEraser,'State','off')
                displayImage(handles);
            end        
        end    
    case 'delete'
        if ~isempty(uData.CSM.selectedObject)
            res = questdlg('Are you sure you would like to delete this object?','Delete mask');
            if ~isempty(res) && strcmp(res,'Yes')
                uData = deleteMaskCSM(uData);
                set(handles.MainFigure,'UserData',uData); %needs to be done separately not for all, as e.g. newMaskCSM is modifying the uData inside
                set(handles.toggleObjectEditExtender,'State','off');
                set(handles.toggleObjectEditEraser,'State','off')
                displayImage(handles);
            end
        end
    case 'r' % for roi
        newMaskCSM(handles);
    case 'u' % for union
        if ~isempty(uData.CSM.selectedObject) && length(uData.CSM.selectedObject.roi)>1                                    
            uniteMasksCSM(handles);                               
        end
    case 'c'
        cutMaskCSM(handles);
        displayImage(handles);
    case 'h' % for help
        CreateStruct.Interpreter = 'tex';
        CreateStruct.WindowStyle = 'non-modal';
        msgbox(["\bf Short manual for keyboard shortcuts";...
                "\rm \fontname{Courier}";
                "h: display this help";...
                "+: increase brush size";...
                "-: decrease brush size";...
                "f: full view";...
                "d: change to eraser brush";...
                "e: change to extender brush";...
                "r: create new object instance";...
                "u: make union of selected objects";...
                "   (use only for nearby objects)";...
                "c: draw a line to cut objects";...
                "delete: delete selected objects";...
                "m: save mask changes to disk";...
                "numbers [0-]: select object type"
                ],"Help",CreateStruct);
    otherwise
        %In case the key is numeric
        possNum = str2double(eventdata.Key);                
        if ~isnan(possNum)
            toggleSelectorPrefix = 'objectSelectorToggle_';
            if possNum == 0 % initally 0 keep this for turning off all selections                
                for i=length(mTB.Children):-1:1
                    if startsWith(mTB.Children(i).Tag,toggleSelectorPrefix) && strcmp(mTB.Children(i).State,'on')
                        mTB.Children(i).State = 'off';
                        objectSelectorToggleCallback(mTB.Children(i));
                        break;
                    end
                end
            else                
                for i=length(mTB.Children):-1:1
                    if startsWith(mTB.Children(i).Tag,toggleSelectorPrefix)
                        possNum = possNum - 1;
                    end
                    if possNum == 0
                        mTB.Children(i).State = 'on';
                        objectSelectorToggleCallback(mTB.Children(i));
                        break;
                    end
                end            
            end
        end
end


        
        

end
