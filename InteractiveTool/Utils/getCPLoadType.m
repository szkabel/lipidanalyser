function CPLoadType = getCPLoadType(moduleNames,variableValues,CPNames)
%moduleNames = handles.Settings.ModuleNames;
%variableValues = handles.Settings.VariableValues;
%CPNames = RawImages.CPNames

% Get load image information
loadImgIdx = strcmp(moduleNames,'LoadImages');
possMatch = variableValues(loadImgIdx,[3,5,7,9]);
CPLoadType = cell(length(CPNames),1);

for i=1:length(CPNames)
    matchM = strcmp(possMatch,CPNames{i});
    [r,~] = ind2sub(size(matchM),find(matchM));
    CPLoadType{i} = variableValues{r,1}; %The matching option is the first parameter
end

end

