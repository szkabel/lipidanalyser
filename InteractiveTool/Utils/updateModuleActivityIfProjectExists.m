function updateModuleActivityIfProjectExists()

global Project;

if ~isempty(Project) && isFieldRec(Project,{'figHandles','ModuleFigHandles','ModuleHandlerFig'}) ...
    && ~isempty(Project.figHandles.ModuleFigHandles.ModuleHandlerFig) && ...
    ishandle(Project.figHandles.ModuleFigHandles.ModuleHandlerFig)
        updateModuleHandlerActivity(Project.figHandles.ModuleFigHandles);
end

end

