function [imageList,handles] = prepareInteractiveOpening(handles)
%prepareInteractiveOpening Function to prepare all the data for the image viewer of CellProfiler
%   INPUTS:
%       handles     CP's handles with the pipeline settings
%       
%   OUTPUTS:
%       imageList   The imageList to display. This should be the same as
%                   the basis of ACC export and the stuff that is going to
%                   be dispayed in the Object Analyser window
%       pipelineChanged Bool to indicate if the pipeline changed since the
%                   previous call
%       handles     The updated handles for furhter processing
%       uData       Optional (either it is there or it is empty). The
%                   previous userData of the Interactive interface

%Fill handles fields to be able to run the LoadImages

global Project;

% Custom parameters:

primChannel = 1;

%search for loadImages modules
for i=1:length(handles.Settings.ModuleNames)
    if strcmp(handles.Settings.ModuleNames{i},'LoadImages')
        handles.Current.CurrentModuleNumber = num2str(i,'%02d');
        handles.Current.SetBeingAnalyzed = 1;
        handles.Current.NumberOfImageSets = 1;
        handles.Current.StartingImageSet = 1;
        moduleStr = ['FigureNumberForModule' num2str(i,'%02d')];
        handles.Current.(moduleStr) = Inf;
        
        handles = LoadImages(handles);
    end
end

if ~objectSturctureExistAndValid(handles.Settings)
    paramcell = getObjectExportSettings(handles);

    handles.Settings.ObjectExport.paramcell = paramcell;                
else %This only means that the currently stored handles.Settings.ObjectExport structure CAN be exported.
    % However, it the object properties may need to get extension.
    paramcell = handles.Settings.ObjectExport.paramcell;
    if checkHandleExistance('IAHandle')
        %Store the existing visualization settings if they exist
        storeIAWindowToProject();
        %NOTE: The up2date fields are 0-d below
    end
end    

%% This is where the objects are constructed and defined
ObjectNames = paramcell{1};        

RawImages.ChannelName = getCPchannels(handles);
RawImages.Directory = handles.Measurements.Image.PathNames{1};
RawImages.CPNames = handles.Measurements.Image.PathNamesText;

RawImages.CPreadOption = getCPLoadType(handles.Settings.ModuleNames,handles.Settings.VariableValues,RawImages.CPNames);

% Add here the ExportImgForAnalysis module image names
extraExportIdx = find(strcmp(handles.Settings.ModuleNames,'ExportImgForAnalysis'));
for i=1:length(extraExportIdx)
    RawImages.ChannelName{end+1} = handles.Settings.VariableValues{extraExportIdx(i),1};
    RawImages.Directory{end+1} = handles.Settings.VariableValues{extraExportIdx(i),2};
    RawImages.CPNames{end+1} = handles.Settings.VariableValues{extraExportIdx(i),1};
    RawImages.CPreadOption{end+1} = 'fromPipeline';
end

% At this point: check if there is an old pipeline and check if some
% modules are still valid in the new pipeline (function: check for this
% validity)
% A module is valid in the new pipeline, if its parameters are the same and all its predecessor modules are
% also valid (i.e. they have not changed). Their order might have changed,
% their parameters and predecessors are the crucial players.
% The output of valid modules (mask images) should be retained (i.e. not
% removed when creating the SQLiteECP class. Instead of a total new
% construction we should just retrieve the already existing one and update
% it accordingly (i.e. delete masks that are not in the pipeline any more
% and in case any channel (image indep creator) module has changed than
% delete all data).
% Why do we need to delete all data? Channels might have changed and
% currently in the database there is no way to know which channels a
% measurement is dependent on. So if channels have changed then the
% measurements need to get invalidated by deleting them.
% -> so here the task is:
if isfieldRecursive(Project,'IAWin.CPHandles.Settings') && isfield(Project,'ECP') % there is a pipeline already stored
    %   1. identify which modules are valid in the pipeline
    newPIPE = handles.Settings;
    oldPIPE = Project.IAWin.CPHandles.Settings;
    validModules = identifyValidModules(newPIPE, oldPIPE);
    
    %   2. identify masks (object indep) that are created by still valid modules
    [allObjects, creatingModules] = collectCPGroup(newPIPE,'objectgroup indep');
    objectsToRetain = allObjects(ismember(creatingModules,find(validModules)));
    %   3. remove the not any more created or to be updated objects from ECP and initialize
    %   those that are new (including those that need update)
    currentObjects = setdiff(Project.ECP.getObjectNames(),'Image'); % Image is always there and fixed
    objectsToDelete = setdiff(currentObjects,objectsToRetain); % we are deleting those that are not present or need update
    objectsToInit = setdiff(ObjectNames,union(intersect(objectsToRetain,currentObjects),'Image')); % we need to init the new and the to be updated objects (everything that was not retained)
    % However, it is possible that an object is created by a valid module
    % but it was not yet exported (THIS SHOULD BE FIXED BY ALWAYS EXPORTING
    % EVERYTHING). Nevertheless we should also initialize those that are to
    % be retained but NOT members of the currentObjects, hence the
    % intersect.
    
    for i=1:length(objectsToDelete)
        Project.ECP.removeObject(objectsToDelete{i});
    end
    for i=1:length(objectsToInit)
        Project.ECP.initObject(objectsToInit{i});
    end
       
    %   4. Check if channels (image indep) creating modules are valid
    %       -> if not then clean ALL in the Data table too (TODO)
    %[CPchanNames,imgCreateModules] = collectCPGroup(newPIPE,'imagegroup indep');    
    if ~all(validModules(strcmp(newPIPE.ModuleNames,'LoadImages')))
        Project.ECP.resetData();
    end
    %chanNames = CPchanNames(ismember(imgCreateModules,loadImgModules) & ~strcmp(CPchanNames,'/')) % keep only the real loaded images (not the ones internally used by CP)
    [currentImgChannels,chanIDs] = Project.ECP.listImageChannels;
    newImgChannels = cellfun(@(x,y)([x '::' y]),RawImages.ChannelName, RawImages.CPNames, 'UniformOutput', false)';
    chansToInit = find(~ismember(newImgChannels,currentImgChannels)); % in relative idx in the RawImages struct 
    chansToDelete = chanIDs(~ismember(currentImgChannels,newImgChannels));
    for i=1:length(chansToInit)
        chanStruct = struct('ChannelName',RawImages.ChannelName{chansToInit(i)},'Directory',RawImages.Directory{chansToInit(i)},'CPNames',RawImages.CPNames{chansToInit(i)},'primChanBool',0);
        Project.ECP.addImageChannel(chanStruct);
    end
    for i=1:length(chansToDelete)
        Project.ECP.addImageChannel(chansToDelete(i));
    end

    %   5. Create an update structure based on the valid modules and apply it
    %   to all images.
    up2Date = calcModulesToRunFromValidity(newPIPE,validModules);
           
    CPNames = Project.ECP.getCellProfilerRawImageNames();
    imageList = handles.Pipeline.(['FileList' CPNames{primChannel}]);
    
    % When possible use the up2Date list from the already run figure.    
    if isfieldRecursive(Project,'figHandles.IAHandle') && isfield(Project.figHandles.IAHandle.UserData,'up2Date')
        Project.IAWin.up2Date = Project.figHandles.IAHandle.UserData.up2Date;
    end
    alreadyRunIdx = all(Project.IAWin.up2Date,1);
    % backward compatibility
    nofModules = length(newPIPE.ModuleNames);
    if size(Project.IAWin.up2Date,1) == 1, Project.IAWin.up2Date = repmat(Project.IAWin.up2Date,nofModules,1); end
    % what if the # of modules have changed meanwhile?
    if size(Project.IAWin.up2Date,1)~=nofModules
        % init again with zeros
        Project.IAWin.up2Date = zeros(nofModules,size(Project.IAWin.up2Date,2));
    end
    % and then refill with the current up2Date structure
    Project.IAWin.up2Date(:,alreadyRunIdx) = repmat(up2Date,1,sum(alreadyRunIdx));    
    
    %   6. Redo the up2Date understanding (i.e. how the pipeline running is
    %   happening)
    
else
    
    % CONSTRUCTING THE DATA CLASS
    % = ECPClass(RawImages,primChannel,handles.Current.DefaultOutputDirectory,ObjectNames);
    ECP = SQLiteECP(RawImages,primChannel,handles.Current.DefaultOutputDirectory,ObjectNames);
    %ECP = SQLiteECP_interface(RawImages,primChannel,handles.Current.DefaultOutputDirectory,ObjectNames);

    Project.ECP = ECP;

    CPNames = ECP.getCellProfilerRawImageNames();
    imageList = handles.Pipeline.(['FileList' CPNames{primChannel}]);
    
    % This should actually never run
    if isFieldRec(Project,{'IAWin','up2Date'})
        disp('This should have never run')
        Project.IAWin.up2Date = zeros(length(handles.Settings.ModuleNames),length(imageList));
    end

end


%Clean out the previous Interactive window
if checkHandleExistance('IAHandle')
    delete(Project.figHandles.IAHandle);
end
Project.figHandles.IAHandle = [];


%Fake figure numbers
for i=1:handles.Current.NumberOfModules
    handles.Current.(['FigureNumberForModule' num2str(i,'%02d')]) = Inf;
end


end
