function bool = objectSturctureExistAndValid(pipeStruct)

    objectToExportIdx = 1; %this is from getObjectExportSettings and a bit ugly coding style
    if isfield(pipeStruct,'ObjectExport')
        indepGroupString = collectCPGroup(pipeStruct,'objectgroup indep');
               
        currObjectStr = pipeStruct.ObjectExport.paramcell{objectToExportIdx}; 
        %Make it cellarray if it is single
        if ~iscell(currObjectStr), currObjectStr = {currObjectStr}; end       
        %Delete Image from the current object string if it is there as that
        %is not fetched from the CP structure.
        currObjectStr(cellfun(@strcmp,currObjectStr,repmat({'Image'},1,length(currObjectStr)))) = [];
        if ~isempty(currObjectStr) && ~all(cellfun(@ismember,currObjectStr,repmat({indepGroupString},1,length(currObjectStr))))
            bool = false;
            return;
        end       
        bool = true;
    else
        bool = false;
    end

end