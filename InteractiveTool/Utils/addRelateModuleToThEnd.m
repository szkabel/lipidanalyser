function handles = addRelateModuleToThEnd(handles,parentObj,childObj)
    %To add a new relate measurement module to the end of the previous
    %settings
    %See how CP's handles.Settings look like and add all neccessary fields.
    
    pipeStruct = handles.Settings; 
    
    pipeStruct.ModuleRevisionNumbers(end+1) = 5025;
    pipeStruct.VariableValues(end+1,:) = cell(1,size(pipeStruct.VariableValues,2));
    pipeStruct.VariableValues{end,1} = childObj;
    pipeStruct.VariableValues{end,2} = parentObj;
    pipeStruct.VariableValues{end,3} = 'None';
    pipeStruct.NumbersOfVariables(end+1) = 3;
    pipeStruct.VariableInfoTypes(end+1,:) = cell(1,size(pipeStruct.VariableInfoTypes,2));
    pipeStruct.VariableInfoTypes(end,1:3) = {'objectgroup','objectgroup','objectgroup'};
    pipeStruct.VariableRevisionNumbers(end+1) = 2;
    pipeStruct.ModuleNames{end+1} = 'RelateMLGOC';     
    
    handles.Settings = pipeStruct;    
    handles.Current.NumberOfModules = handles.Current.NumberOfModules + 1;
end