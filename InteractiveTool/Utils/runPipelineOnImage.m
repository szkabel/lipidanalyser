function [succFinished] = runPipelineOnImage(imageNumber, handles,ECP, modulesToRun)
%Run the pipeline on one selected image
%modules to run is a bool identifying which modules to run

global GlobalStopCellProfilerIndicator;

tic 
handles.Current.SetBeingAnalyzed = imageNumber;
waitHandle = waitbar(0,sprintf('Executing pipeline on image %d',imageNumber));
nofModules = handles.Current.NumberOfModules;
if length(modulesToRun) ~= nofModules, modulesToRun = ones(nofModules,1); end
for SlotNumber = find(modulesToRun)'
    ModuleNumberAsString = sprintf('%02d', SlotNumber);
    ModuleName = char(handles.Settings.ModuleNames(SlotNumber));
    handles.Current.CurrentModuleNumber = ModuleNumberAsString;
    if ishandle(waitHandle), waitbar(SlotNumber/nofModules,waitHandle,sprintf('Executing pipeline on image %d.\n(%s [%d/%d])',imageNumber,ModuleName,SlotNumber,nofModules)); end
    try
        handles = feval(ModuleName,handles);
    catch e
        handles.BatchError = [ModuleName ' ' lasterr];
        disp(['Batch Error: ' ModuleName ' ' lasterr]);
        CPerrordlg(['Error in module: ' ModuleName '. The problem is: ' e.message]);        
        succFinished = 0;        
        disp(['Unsuccesful on image: ' num2str(imageNumber)]);
        if ishandle(waitHandle), close(waitHandle); end                
        return;
    end
    disp(['  -> Running module ' ModuleNumberAsString]);
    if GlobalStopCellProfilerIndicator
        msgbox(sprintf('Analysis stopped on image %d after module %d.',imageNumber,SlotNumber));
        if ishandle(waitHandle), close(waitHandle); end        
        succFinished = 0;
        return;
    end
end
ExportToOA(ECP,handles,imageNumber,modulesToRun);
if ishandle(waitHandle), close(waitHandle); end
fprintf('Set %d done. Elapsed time %f\n', imageNumber, toc);

succFinished = 1;
%cd(folderName);
%handles.Pipeline = [];eval(['save ',sprintf('%s%d_to_%d_OUT', BatchFilePrefix, StartImage, EndImage), ' handles;']);
