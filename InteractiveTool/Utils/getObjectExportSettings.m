function [paramcell] = getObjectExportSettings(handles)
    %   - asks with settings GUI for the proper objects to be exported and
    %   creates ECP   
    
    indepGroupString = collectCPGroup(handles.Settings,'objectgroup indep');
    indepGroupString = ['Image' indepGroupString]; %extend with image
    %Create settings panel:
    infoText = 'Please specify parameters for exporting objects!';
    objectExpIdx = 1;
    paramarray{objectExpIdx}.name = 'Objects to export';
    paramarray{objectExpIdx}.type = 'multiple-enum';
    paramarray{objectExpIdx}.values = indepGroupString;
    
    %To set back old results
    if isfieldRecursive(handles,'Settings.ObjectExport')
        selectedBool = false(1,length(indepGroupString));
        if ~iscell(handles.Settings.ObjectExport.paramcell{objectExpIdx}), handles.Settings.ObjectExport.paramcell{objectExpIdx} = handles.Settings.ObjectExport.paramcell(objectExpIdx); end
        for i=1:length(handles.Settings.ObjectExport.paramcell{objectExpIdx})
            selectedBool = selectedBool | strcmp(indepGroupString,handles.Settings.ObjectExport.paramcell{objectExpIdx}{i});
        end
        if any(selectedBool)
            paramarray{objectExpIdx}.default = find(selectedBool); 
        else
            paramarray{objectExpIdx}.default = [];
        end
    end        
    
    function [bool,msg] = checkObjectExportSettings(paramcellCandidate)
        bool = true;
        msg = 'All ok';                
        if ~isempty(setdiff(indepGroupString,paramcellCandidate{objectExpIdx}))
            answer = questdlg('You haven''t selected all the object to export. You may proceed but it is advised to export all the objects.','WARNING! MISSING OBJECT FROM EXPORT','Proceed','Go back','Proceed');                           
            if strcmp(answer,'Go back')
                bool = false;
                msg = 'User interrupt';
                return;
            end
        end                
    end
    
    paramcell = Settings_GUI(paramarray,'infoText',infoText,'checkFunction',@checkObjectExportSettings);

end


