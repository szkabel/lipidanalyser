function ExportToOA(ECP,handles,imgNumber,modulesToRun)
% modulesToRun can help to identify which objects need resave (i.e. only
% those that are created by a to-be run module

% Save data for ObjectAnalyser
% Category: File Processing
%
% SHORT DESCRIPTION:
% Saves masks for Object analyser. In the huge project this one should not
% be a CP module after a while (as this is always neccessary)
% *************************************************************************
%%
% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Peter Horvath and Abel Szkalisity.
% Copyright 2018.
%
% Please see the AUTHORS file for credits.
%
% Website: http://acc.ethz.ch
%
% $Revision: 5025 $

% The output variable is imageSlice: the part of ECP which corresponds to
% the currently analyzed sequence.    
            
    % for each image sets
    % Note: position of the objects comes from the first measured object
    % type (usually nuclei)
    
    primChannel = 1;
        
    i = handles.Current.SetBeingAnalyzed;             
    
    try            
        ObjectAnalyzerImageName = char(handles.Measurements.Image.FileNames{i}(primChannel));        
    catch e
        % In case the LoadImages module was not run (e.g. because the
        % pipeline update didn't require that), then the field above does
        % not contain the ImageName. However this is only possible if that
        % image was run already so in that case the imageID is already in
        % the ECP.
        if strcmp(e.identifier,'MATLAB:badsubscript')
            [~,~,origExt] = fileparts(handles.Measurements.Image.FileNames{1}{primChannel});
            ObjectAnalyzerImageName = [ECP.getImageID(imgNumber) origExt];
        else
            rethrow(e)
        end
    end
    
    %Finally save the image name too
    [~,ObjectAnalyzerImageName,origFileExt] = fileparts(ObjectAnalyzerImageName);
    ECP.initImage(ObjectAnalyzerImageName,imgNumber);

    % get measured fileds:
    ObjectNames = ECP.getObjectNames();
    [allObjects, creatingModules] = collectCPGroup(handles.Settings,'objectgroup indep');
    for j=1:length(ObjectNames)
                
        % This is a delicate balance! With the continue thingy we are
        % skipping ALL objects that were not run anyways in this round
        % (because they were created already). As the mask operation update
        % (with CSM module) is NOT deleting the image from the disk we MUST
        % use the handles.Pipeline field directly (not the CPretrieve image
        % function as that would just recognize the old mask image)
        if ~ismember(creatingModules(strcmp(allObjects,ObjectNames{j})),find(modulesToRun)), continue; end
        % Saving masks if it is not the image (for saving disk space)
        if ~strcmp(ObjectNames{j},'Image')            
            maskImage = handles.Pipeline.(['Segmented', ObjectNames{j}]);
            ECP.storeMaskImage(maskImage,[ObjectAnalyzerImageName origFileExt],ObjectNames{j})
        end

        %{
        % Image measurements might not have been created during CP analysis
        if isfield(handles.Measurements,ObjectNames{j})
            %Export to Object analyser        
            measurementField = handles.Measurements.(ObjectNames{j}); 
            [~,~,structure] = transformMeasurementField(measurementField,{'Location','Parent','Children'},i,ObjectNames{j});
            imageSlice.(ObjectNames{j}).Measurements = structure;
        else
        end
        %}

    end               
        