function handles = removeLastRelateModules(handles)
%From a given Pipeline structure it removes the last relate modules (which
%information is retrieved from the ObjectExport field if exist implicitely). 

    pipeStruct = handles.Settings;

   if objectSturctureExistAndValid(pipeStruct)
       %we exploit the fact that the number of relate modules is the same
       %as the number of childObjects.
       for i=1:length(pipeStruct.ObjectExport.paramcell{pipeStruct.ObjectExport.childIdx})
            pipeStruct.ModuleRevisionNumbers(end) = [];
            pipeStruct.VariableValues(end,:) = [];            
            pipeStruct.NumbersOfVariables(end) = [];
            pipeStruct.VariableInfoTypes(end,:) = [];            
            pipeStruct.VariableRevisionNumbers(end) = [];
            pipeStruct.ModuleNames(end) = [];
            handles.Current.NumberOfModules = handles.Current.NumberOfModules - 1;
       end
       
       handles.Settings = pipeStruct;
   end
      

end

