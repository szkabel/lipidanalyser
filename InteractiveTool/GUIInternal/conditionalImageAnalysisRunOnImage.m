function [succFinished] = conditionalImageAnalysisRunOnImage( imgNumber, handles, ECP)
%Runs the analysis on the selected image if needed according to the up2Date
%indicator. It also handles the possible stop requests.

global GlobalStopCellProfilerIndicator;
uData = get(handles.MainFigure,'UserData');    
    
if ~all(uData.up2Date(:,imgNumber))
    [succFinished] = runPipelineOnImage(imgNumber,uData.CPhandles,ECP,~uData.up2Date(:,imgNumber));
    if ~GlobalStopCellProfilerIndicator
        if succFinished, uData.up2Date(:,imgNumber) = 1; end                
        uData.selectedImage = imgNumber;
    else
        uData.selectedImage = 0;
    end
else    
    succFinished = 1;
    uData.selectedImage = imgNumber;
end

set(handles.MainFigure,'UserData',uData);

end

