function visSet = GUI2visualizationSettings( handles, visSet)
%visualizationSettings2GUI Puts the visualization settings to the GUI
%controls (via fillOutUIControls)

%channels
uData = get(handles.MainFigure,'UserData');

for i=1:length(visSet.channels)
    try
        paramcell = fetchUIControlValues(uData.channelPars.paramarray{i},uData.channelPars.paramEditHandles{i},@(paramcell)(checkNumber(paramcell{5},0,1,[0,Inf],'Gamma')));
    catch        
        return;
    end
    visSet.channels{i}.on = paramcell{1};
    visSet.channels{i}.smartStrech.on = paramcell{2};
    visSet.channels{i}.lims(1) = paramcell{3};
    visSet.channels{i}.lims(2) = paramcell{4};
    visSet.channels{i}.gamma = paramcell{5};
    visSet.channels{i}.color = paramcell{6};
end

for i=1:length(visSet.outlines)
    try
        paramcell = fetchUIControlValues(uData.contourPars.paramarray{i},uData.contourPars.paramEditHandles{i},@(paramcell)(checkNumber(paramcell{2},1,1,[1,100],'Line width')));
    catch
        return; %the user is notified by the function and we just simply return without changing the settings
    end
    visSet.outlines{i}.on = paramcell{1};
    visSet.outlines{i}.width = paramcell{2};
    visSet.outlines{i}.color = paramcell{3};
    visSet.outlines{i}.objectNumbers = paramcell{4};
    visSet.outlines{i}.objectNumberFontSize = paramcell{5};
end


end

