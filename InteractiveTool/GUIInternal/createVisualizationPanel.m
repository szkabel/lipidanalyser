function [handles,channelPars,contourPars] = createVisualizationPanel(handles,visSet)
% To create the visualization setting panel for the figure

chanSet = visSet.channels;
nofChannels = length(chanSet);
%delete previous guiObjects
if isfield(handles,'ChannelPanels')
    for i=1:length(handles.ChannelPanels)
        if ishandle(handles.ChannelPanels{i})
            delete(handles.ChannelPanels{i});
        end
    end
end
handles.ChannelPanels = cell(1,nofChannels);
channelParamarray = cell(1,nofChannels);
chanParamEditHandles = cell(1,nofChannels); 

contSet = visSet.outlines;
nofContours = length(contSet);
if isfield(handles,'ContourPanels')
    for i=1:length(handles.ContourPanels)
        if ishandle(handles.ContourPanels{i})
            delete(handles.ContourPanels{i});
        end
    end
end
handles.ContourPanels = cell(1,nofContours);
contourParamarray = cell(1,nofContours);
contParamEditHandles = cell(1,nofContours);

for i=1:nofChannels
    handles.ChannelPanels{i} = uipanel(...
        'Parent',handles.InnerVisualizationPanel,...    
        'Units','pixels',...
        'Title',chanSet{i}.name);
    channelParamarray{i} = {...
        struct('name','On','type','checkbox'),...
        struct('name','Smart stretch','type','checkbox'),...
        struct('name','Min','type','slider','lim',[0 1]),...
        struct('name','Max','type','slider','lim',[0 1]),...
        struct('name','Gamma','type','int'),...
        struct('name','Color','type','colorPicker'),...
        };
end
for i=1:nofContours
    handles.ContourPanels{i} = uipanel(...
        'Parent',handles.InnerVisualizationPanel,...    
        'Units','pixels',...
        'Title',contSet{i}.name);
    contourParamarray{i} = {...
        struct('name','On','type','checkbox'),...  
        struct('name','Line width','type','int'),...  
        struct('name','Color','type','colorPicker'),...            
        struct('name','object #','type','checkbox'),...
        struct('name','Font size','type','int'),...
        };
end

panelHeight = placeItemsOnVisualizationPanel( handles );
handles.InnerVisualizationPanel.Position(4) = panelHeight;

%Custom for scroll panel from internet
hPanel = handles.InnerVisualizationPanel;
% Get the panel's underlying JPanel object reference
warning('off','MATLAB:ui:javaframe:PropertyToBeRemoved');
jPanel = hPanel.JavaFrame.getGUIDEView.getParent;
warning('on','MATLAB:ui:javaframe:PropertyToBeRemoved'); 

% Embed the JPanel within a new JScrollPanel object
jScrollPanel = javaObjectEDT(javax.swing.JScrollPane(jPanel));
 
% Remove the JScrollPane border-line
jScrollPanel.setBorder([]);
 
% Place the JScrollPanel in same GUI location as the original panel
warning('off','MATLAB:ui:javacomponent:FunctionToBeRemoved');
pixelpos = getpixelposition(hPanel);
warning('on','MATLAB:ui:javacomponent:FunctionToBeRemoved');
hParent = hPanel.Parent;
[hjScrollPanel, hScrollPanel] = javacomponent(jScrollPanel, pixelpos, hParent);
hScrollPanel.Units = 'norm';
pause(0.01); %this is needed so that the setValue works properly
hjScrollPanel.getVerticalScrollBar().setValue(0);   
hScrollPanel.OuterPosition = [0 0 1 1];

% Ensure that the scroll-panel and contained panel have linked visibility
hLink = linkprop([hPanel,hScrollPanel],'Visible');
setappdata(hPanel,'ScrollPanelVisibilityLink',hLink);

%END of custom scroll panel

for i=1:nofChannels
    [ ~, chanParamEditHandles{i} ] = generateUIControls(channelParamarray{i},handles.ChannelPanels{i},15,[5 30]);
    for j=1:length(chanParamEditHandles{i})
        if isempty(get(chanParamEditHandles{i}{j},'Callback'))
            set(chanParamEditHandles{i}{j},'Callback',@(hObject,eventdata)InteractiveWindow('VisualizationPanel_Callback',hObject,eventdata,guidata(hObject)) );            
        else
            prevCB = get(chanParamEditHandles{i}{j},'Callback');
            set(chanParamEditHandles{i}{j},'Callback',...
                @(hObject,eventdata)(cellfun(@(x)feval(x,hObject,eventdata), ...
                    {...
                        prevCB,...
                        @(hObject,eventdata)InteractiveWindow('VisualizationPanel_Callback',hObject,eventdata,guidata(hObject))...
                    } )  )...
            );
        end
    end
end
for i=1:nofContours
    [ ~, contParamEditHandles{i} ] = generateUIControls(contourParamarray{i},handles.ContourPanels{i},15,[5 30]);
    for j=1:length(contParamEditHandles{i})
        if isempty(get(contParamEditHandles{i}{j},'Callback'))
            set(contParamEditHandles{i}{j},'Callback',@(hObject,eventdata)InteractiveWindow('VisualizationPanel_Callback',hObject,eventdata,guidata(hObject)) );
        else
            prevCB = get(contParamEditHandles{i}{j},'Callback');
            set(contParamEditHandles{i}{j},'Callback',...
                @(hObject,eventdata)(cellfun(@(x)feval(x,hObject,eventdata), ...
                    {...
                        prevCB,...
                        @(hObject,eventdata)InteractiveWindow('VisualizationPanel_Callback',hObject,eventdata,guidata(hObject))...
                    } )  )...
            );
        end
    end
end

channelPars.paramarray = channelParamarray;
contourPars.paramarray = contourParamarray;

channelPars.paramEditHandles = chanParamEditHandles;
contourPars.paramEditHandles = contParamEditHandles;

% Creating object selector toolbar icons
createObjectSelectorToolbarIcons(handles,contSet)
    
end

function createObjectSelectorToolbarIcons(handles,contSet)
    
    %First delete all the previous toggle tools:
    toggleSelectorPrefix = 'objectSelectorToggle_';
    mTB = handles.mainToolbar;
    toDel = false(1,length(mTB.Children));
    for i=1:length(mTB.Children)
        if startsWith(mTB.Children(i).Tag,toggleSelectorPrefix)
            toDel(i) = true;
        end
    end
    for i=find(toDel), delete(mTB.Children(i)); end
    mTB.Children(toDel) = [];       
    
    % Now create the new ones
    for i=1:length(contSet)
        if ~strcmp(contSet{i}.name,'Image')
            uitoggletool(...
                handles.mainToolbar,...
                'ClickedCallback',@objectSelectorToggleCallback,...
                'Tooltip',['Object selector: ' contSet{i}.name ' [' num2str(i-1) ']'],...
                'Tag',[toggleSelectorPrefix contSet{i}.name]);            
        end
    end    
    
end