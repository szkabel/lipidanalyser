function mainViewButtonMotion(handles,currPos)
    uData = get(handles.MainFigure,'UserData');
                
    axesPos = handles.MainView.Position;
    imgXYRatio = handles.MainView.PlotBoxAspectRatio(1)/handles.MainView.PlotBoxAspectRatio(2);
    
    xLims = handles.MainView.XLim;
    yLims = handles.MainView.YLim;
    [editObjPos,~,~,~,~,~,clickInImageBool] = calculateCursorPositionInImageAxes(axesPos,currPos,imgXYRatio,xLims,yLims);

    
    if clickInImageBool        
        switch handles.MainFigure.SelectionType
            case 'normal'                   
                if ~isempty(uData.CSM.editObject) && ishandle(uData.CSM.editObject) && (strcmp(handles.toggleObjectEditEraser.State,'on') || strcmp(handles.toggleObjectEditExtender.State,'on'))
                    uData.CSM.editObject.Visible = 'on';
                    uData.CSM.editObject.Position(1:2) = editObjPos;        
                end    
            case 'alt'                
                if ~isempty(uData.Nav.drag)                    
                    imgObj = findobj(handles.MainView.Children,'flat','-property','CData');
                    maxSizes = size(imgObj.CData);
                    delta = uData.Nav.drag-editObjPos;
                    if min(handles.MainView.XLim + delta(1))>0 && max(handles.MainView.XLim + delta(1))<maxSizes(1)
                        handles.MainView.XLim = handles.MainView.XLim + delta(1);
                    end
                    if min(handles.MainView.YLim + delta(2))>0 && max(handles.MainView.YLim + delta(2))<maxSizes(2)
                        handles.MainView.YLim = handles.MainView.YLim + delta(2);                                      
                    end
                end
        end                        
    end
    
    set(handles.MainFigure,'UserData',uData);
end

