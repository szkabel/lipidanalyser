function [nx1,nx2,ny1,ny2] = calculateZoomLimits(editObjPos,currPosWithinAxes,xStart,xExtent,yStart,yExtent,z,handles)   

    %Handle special (edge) cases:
    if currPosWithinAxes(1)<xStart,         currPosWithinAxes(1) = xStart;          editObjPos(1) = handles.MainView.XLim(1);  end
    if currPosWithinAxes(2)<yStart,         currPosWithinAxes(2) = yStart;          editObjPos(2) = handles.MainView.YLim(1);  end
    if currPosWithinAxes(1)>xStart+xExtent, currPosWithinAxes(1) = xStart+xExtent;  editObjPos(1) = handles.MainView.XLim(2);  end
    if currPosWithinAxes(2)>yStart+yExtent, currPosWithinAxes(2) = yStart+yExtent;  editObjPos(2) = handles.MainView.YLim(2);  end

    %Calculating parameters as defined in my notebook
    px = editObjPos(1);
    py = editObjPos(2);
    Qx = currPosWithinAxes(1);
    Qy = currPosWithinAxes(2);    
    x1 = handles.MainView.XLim(1);
    x2 = handles.MainView.XLim(2);
    y1 = handles.MainView.YLim(1);
    y2 = handles.MainView.YLim(2);
    A = handles.MainView.Position(3);
    B = handles.MainView.Position(4);
    imgObj = findobj(handles.MainView.Children,'flat','-property','CData');
    maxSizes = size(imgObj.CData);
    
    %Falling to 3 different cases:    
    epsLim = 20;
    
    % 1) We are full in both ends
    if xStart<epsLim && yStart<epsLim
        nx1 = (xStart-Qx)*(x2-x1)*z/xExtent+px;    
        nx2 = (x2-x1)*z+nx1;

        ny1 = (yStart-Qy)*(y2-y1)*z/yExtent+py;
        ny2 = (y2-y1)*z+ny1; 
    
    % 2) We are only full in x
    elseif   xStart<epsLim  && ~yStart<epsLim
        %Calc x in a full manner
        nx1 = (xStart-Qx)*(x2-x1)*z/xExtent+px;    
        nx2 = (x2-x1)*z+nx1;
        
        %If we are in the top
        if currPosWithinAxes(2)<yStart+yExtent/2
            ny1 = 0;
            ny2 = (nx2-nx1)*(B-2*Qy)/A+2*py-ny1;
        else %If we are in the bottom half
            ny2 = maxSizes(1);
            ny1 = (nx2-nx1)*(B-2*Qy)/A+2*py-ny2;
        end
        
    % 3) We are only full in y
    elseif ~xStart<epsLim   &&  yStart<epsLim
        %Calc y in a full manner
        ny1 = (yStart-Qy)*(y2-y1)*z/yExtent+py;
        ny2 = (y2-y1)*z+ny1; 
        
        %If we are in the left
        if currPosWithinAxes(1)<xStart+xExtent/2
            nx1 = 0;
            nx2 = (ny2-ny1)*(A-2*Qx)/B+2*px-nx1;
        else %If we are in the right half
            nx2 =  maxSizes(2);
            nx1 = (ny2-ny1)*(A-2*Qx)/B+2*px-nx2;
        end
    end
        
    if nx1<0, nx1 = 0; end
    if ny1<0, ny1 = 0; end    
    if ny2>maxSizes(1), ny2 = maxSizes(1); end
    if nx2>maxSizes(2), nx2 = maxSizes(2); end
        

end

