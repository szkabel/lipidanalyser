function uniteMasksCSM(handles)
    
    uData = handles.MainFigure.UserData;
    [index,colorCode,objName] = identifySelectedObjectCSM(handles);
    nofObjects = length(uData.CSM.selectedObject.roi);
    unitedObjects = cell(1,nofObjects);
    
    %Init the position
    pos = createMask(uData.CSM.selectedObject.roi{1});    
    minimaxDist = -Inf; % we'll be searching for the maximum of the minimum distances
    if index
        if ~isempty(uData.CSM.selectedObject)
            for i=1:nofObjects
                unitedObjects{i} = uData.CSM.selectedObject.objNum{i};
                newMask = createMask(uData.CSM.selectedObject.roi{i});
                distMap = bwdist(newMask);
                minDist = min(distMap(pos),[],'All');
                pos = pos | newMask;
                minimaxDist = max(minDist,minimaxDist);
                delete(uData.CSM.selectedObject.roi{i});                
            end
            uData.CSM.selectedObject = [];
        end
        
        st = strel('disk',double(ceil(minimaxDist)));
        pos = imclose(pos,st);
        
        edgeIdx = bwboundaries(pos,'noholes');
        edgeIdx = fliplr(edgeIdx{1});
        uData.CSM.selectedObject.roi = {drawfreehand(handles.MainView,'Position',edgeIdx,'Color',colorCode)};                
        
        fullImageList = get(handles.ImageList,'String');
        imgNumber = uData.selectedImage;            
        imageName = fullImageList{imgNumber};                
        
        uData.CSM.selectedObject.objNum = unitedObjects;
        uData.CSM.selectedObject.objName = {objName};
        uData.CSM.selectedObject.imageName = {imageName};
    end        
                
    set(handles.MainFigure,'UserData',uData);
end

