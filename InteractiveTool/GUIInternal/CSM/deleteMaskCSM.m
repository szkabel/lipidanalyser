function uData = deleteMaskCSM(uData)
    if isempty(uData.CSM.selectedObject)
        return;
    end
    % It has to be the same image!
    objName = uData.CSM.selectedObject.objName{1};
    imageID = uData.CSM.selectedObject.imageName{1};
    maskImg = retrieveMaskImage(uData.ECP,objName,imageID);
    
    for i=1:length(uData.CSM.selectedObject.objNum)
        objNum = uData.CSM.selectedObject.objNum{i};                
        
        maskLayerIdx = find(sum(maskImg == objNum,[1 2]));
        maskLayer = maskImg(:,:,maskLayerIdx);
        maskLayer(maskLayer == objNum) = 0; % delete
        maskImg(:,:,maskLayerIdx) = maskLayer;        
        
    end
    maskImg = subsetMask(maskImg) ; % making masks consecutive
    storeMaskImage(uData.ECP,maskImg,imageID,objName);
    uData.CSM.selectedObject = [];
    
    uData.up2Date(:,uData.selectedImage) = maskOutputChanged(uData.CPhandles.Settings,objName,uData.ECP);
    
end