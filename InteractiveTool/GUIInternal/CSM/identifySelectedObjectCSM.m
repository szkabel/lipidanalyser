function [index,colorCode,objectName] = identifySelectedObjectCSM(handles)
    index = 0;
    colorCode = [];
    objectName = [];
    mTB = handles.mainToolbar;
    toggleSelectorPrefix = 'objectSelectorToggle_';
    for i=1:length(mTB.Children)
        if startsWith(mTB.Children(i).Tag,toggleSelectorPrefix) && strcmp(mTB.Children(i).State,'on')
            colorCode = mTB.Children(i).CData(1,1,:); colorCode = colorCode(:);
            objectName = mTB.Children(i).Tag(length(toggleSelectorPrefix)+1:end);
            index = i;
        end
    end
end

