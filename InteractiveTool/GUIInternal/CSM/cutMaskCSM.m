function cutMaskCSM(handles)
    
    uData = handles.MainFigure.UserData;
    [~,colorCode,objName] = identifySelectedObjectCSM(handles);
    
    % remove any possible selected object
    if ~isempty(uData.CSM.selectedObject)
        for i=1:length(uData.CSM.selectedObject.roi)
            delete(uData.CSM.selectedObject.roi{i});
        end
        uData.CSM.selectedObject = [];
    end
    
    cutLine = drawpolyline(handles.MainView,'Color',colorCode);
    
    res = questdlg('Are you sure you would like to perform this cut on this image?','Overwrite mask');
    if ~isempty(res) && strcmp(res,'Yes')
        pos = cutLine.createMask();
        
        fullImageList = get(handles.ImageList,'String');
        imgNumber = uData.selectedImage;            
        imageName = fullImageList{imgNumber};    
        
        maskImg = retrieveMaskImage(uData.ECP,objName,imageName);        
        pos = repmat(pos,1,1,size(maskImg,3)); % in case its a deep mask
        pos = imdilate(pos,strel('disk',2));
        
        masksToBeCut = setdiff(unique(maskImg(pos)),0);
        maskImg(pos) = 0; % cut through wherever the line goes
        maxId = max(maskImg,[],'All');
        for i=1:size(maskImg,3)
            maskLayer = maskImg(:,:,i);
            tmpMask = maskLayer;
            tmpMask(~ismember(tmpMask,masksToBeCut)) = 0;
            tmpMask = uint16(bwlabel(tmpMask)) + (tmpMask + maxId).*uint16(tmpMask>0); % add maxIdx and then 0 out those that were initially 0
            maskLayer(tmpMask>0) = tmpMask(tmpMask>0);
            maskImg(:,:,i) = maskLayer;
        end
        maskImg = subsetMask(maskImg,setdiff(unique(maskImg),0));

        storeMaskImage(uData.ECP,maskImg,imageName,objName);
        uData.up2Date(:,uData.selectedImage) = maskOutputChanged(uData.CPhandles.Settings,objName,uData.ECP);        
    end

    if ishandle(cutLine), delete(cutLine); end
    set(handles.MainFigure,'UserData',uData);
end