function newMaskCSM(handles)
    
    uData = handles.MainFigure.UserData;
    [index,colorCode,objName] = identifySelectedObjectCSM(handles);
    if index
        if ~isempty(uData.CSM.selectedObject)
            for i=1:length(uData.CSM.selectedObject.roi)
                delete(uData.CSM.selectedObject.roi{i});
            end
            uData.CSM.selectedObject = [];
        end
        uData.CSM.selectedObject.roi = {drawfreehand(handles.MainView,'Color',colorCode)};
        
        fullImageList = get(handles.ImageList,'String');
        imgNumber = uData.selectedImage;            
        imageName = fullImageList{imgNumber};
        maskImg = retrieveMaskImage(uData.ECP,objName,imageName);
        objNum = max(maskImg(:))+1;
        
        uData.CSM.selectedObject.objNum = {objNum};
        uData.CSM.selectedObject.objName = {objName};
        uData.CSM.selectedObject.imageName = {imageName};
    end        
                
    set(handles.MainFigure,'UserData',uData);
end

