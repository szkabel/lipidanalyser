function uData = updateMaskCSM(uData)
    if isempty(uData.CSM.selectedObject)
        return;
    end
    objNum = uData.CSM.selectedObject.objNum; 
    % objNum is a longer than 1 array when we are performing unification. Still the other fields can only have length of 1 (we are adding 1 object instead of multiple) except for roi when cutting objects (not yet implemented) 
    objName = uData.CSM.selectedObject.objName{1};
    imageID = uData.CSM.selectedObject.imageName{1};

    maskImg = retrieveMaskImage(uData.ECP,objName,imageID);                
    pos = createMask(uData.CSM.selectedObject.roi{1});
    maskLayerIdx = find(sum(maskImg == objNum{1},[1 2]));
    
    maskImg(ismember(maskImg,cell2mat(objNum))) = 0; % delete
    
    if isempty(maskLayerIdx) % in case we are adding a new mask then its index is not yet in the maskImg
        maskLayerIdx = size(maskImg,3);
    end
    maskLayer = maskImg(:,:,maskLayerIdx);    
    maskLayer(pos) = objNum{1}; % and replace with the ROI mask   
    maskImg(:,:,maskLayerIdx) = maskLayer;

    % This line may be contraversial. I think I have assumed during the OA
    % development that masks are consecutive. However this may mess up e.g.
    % the association of nuclei and cell masks.
    maskImg = subsetMask(maskImg) ; % making masks consecutive

    storeMaskImage(uData.ECP,maskImg,imageID,objName);

    uData.up2Date(:,uData.selectedImage) = maskOutputChanged(uData.CPhandles.Settings,objName,uData.ECP);
end