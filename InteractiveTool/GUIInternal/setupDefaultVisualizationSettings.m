function [ visualizationSettings ] = setupDefaultVisualizationSettings( ECP )
%setupDefaultVisualizationSettings Gives initial values for the image
%visualization
%
%   For each channel that was used during the image segmentation there is a
%   color assigned. This color is used to display that channel and it can
%   be arbitrary RGB color. Moreover for each channel there is an interval
%   defined (subset of [0,1]) to strech the image intensities if needed.
%   Gamma is also specified to each channel to change the slope of the
%   rescale if needed (see more imadjust)
%   Each channel can be weighted by a number so that their overall
%   intensity provided to the final image can be adjusted. It can also be
%   just simply switched off which is equivavlent with 0 weight. The
%   weights are normalized so 1-2-7 is the same 0.1-0.2-0.7.
%
%   For each object we display an outline which has a color and a weight to
%   the final merged image. This can also be switched off which means 0
%   weight.

channelNames = listImageChannels(ECP);
nofChannels = length(channelNames);
objectNames = getObjectNames(ECP);
nofObjects = length(objectNames);
visualizationSettings.channels = cell(1,nofChannels);
visualizationSettings.outlines = cell(1,nofObjects);

for i=1:nofChannels
    tmp.color = defaultColors(i);
    tmp.lims = [0 1];
    tmp.gamma = 0.5;
    tmp.weight = 1;
    tmp.on = 1;
    tmp.name = channelNames{i};
    tmp.smartStrech.on = 0;
    tmp.smartStrech.min = Inf;
    tmp.smartStrech.max = 0;
    visualizationSettings.channels{i} = tmp;
end

for i=1:nofObjects
    tmp.color = defaultColors(nofObjects-i+1);
    tmp.weight = 1;
    tmp.on = 1;
    tmp.width = 1;
    tmp.name = objectNames{i};
    tmp.objectNumbers = 0;
    tmp.objectNumberFontSize = 12;
    visualizationSettings.outlines{i} = tmp;
end

end

function col = defaultColors(idx)
    colorArray = getManyColor()./255;
    col = colorArray(mod(idx-1,size(colorArray,1))+1,:);
end

