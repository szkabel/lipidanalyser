function editFreehand(hf, he, editType)
    if ~isempty(hf) && ishandle(hf)
        he.Position = round(he.Position);
        % Create a mask for the target freehand.
        tmask = hf.createMask();
        [m, n,~] = size(tmask);
        % Include the boundary pixel locations
        boundaryInd = sub2ind([m,n], hf.Position(:,2), hf.Position(:,1));
        tmask(boundaryInd) = true;

        % Create a mask from the editor ROI
        emask = he.createMask();
        boundaryInd = sub2ind([m,n], he.Position(:,2), he.Position(:,1));
        emask(boundaryInd) = true;

        % Check if center of the editor ROI is inside the target freehand. If you
        % use a different editor ROI, ensure to update center computation.
        % center = he.Center; %
        % isAdd = hf.inROI(center(1), center(2));
        if strcmp(editType,'extend')
            % Add the editor mask to the freehand mask
            newMask = tmask|emask;
        elseif strcmp(editType,'delete')
            % Delete out the part of the freehand which intersects the editor
            newMask = tmask&~emask;
        end

        % Update the freehand ROI
        perimPos = bwboundaries(newMask, 'noholes');
        hf.Position = [perimPos{1}(:,2), perimPos{1}(:,1)];
    end
end