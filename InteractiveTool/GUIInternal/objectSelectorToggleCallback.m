function objectSelectorToggleCallback(hObject,~)
    
    togglePrefix = strsplitN(hObject.Tag,'_',1);
    listOfTools = hObject.Parent.Children;

    for i=1:length(listOfTools)
        if startsWith(listOfTools(i).Tag,togglePrefix) && ~strcmp(listOfTools(i).Tag,hObject.Tag)
            listOfTools(i).State = 'off';
        end
    end
end