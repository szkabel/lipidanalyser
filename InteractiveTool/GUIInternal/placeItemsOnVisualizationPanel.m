function panelHeight = placeItemsOnVisualizationPanel( handles )

channelPanelHeight = 170;%for the individual panels
contPanelHeight = 150;
innerPadding = 6; %symmetrically distributed on 2 sides and only on the top vertically
topPadding = 0;

panelSize = get(handles.InnerVisualizationPanel,'Position');
panelSize(4) = length(handles.ChannelPanels)*(channelPanelHeight+innerPadding) + length(handles.ContourPanels)*(contPanelHeight+innerPadding) + topPadding;
panelHeight = panelSize(4);

for i=1:length(handles.ChannelPanels)
    set(handles.ChannelPanels{i},'Position',[innerPadding/2 panelSize(4)-i*(channelPanelHeight+innerPadding)-topPadding panelSize(3)-innerPadding channelPanelHeight]);
end
heightOfChannels = length(handles.ChannelPanels)*(channelPanelHeight+innerPadding);

for i=1:length(handles.ContourPanels)
    set(handles.ContourPanels{i},'Position',[innerPadding/2 panelSize(4)-heightOfChannels-i*(contPanelHeight+innerPadding)-topPadding panelSize(3)-innerPadding contPanelHeight]);
end

end

