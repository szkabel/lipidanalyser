function objectEditToggleCallback(hObject,handles,editType)

    uData = get(handles.MainFigure,'UserData');
    
    % edit toggle can only work on a single object instance
    if ~isempty(uData.CSM.selectedObject)
        for i=2:length(uData.CSM.selectedObject.roi)
            if ishandle(uData.CSM.selectedObject.roi{i})
                delete(uData.CSM.selectedObject.roi{i})
            end                        
        end
        uData.CSM.selectedObject.roi(2:end) = [];
        uData.CSM.selectedObject.objNum(2:end) = [];
        uData.CSM.selectedObject.objName(2:end) = [];
        uData.CSM.selectedObject.imageName(2:end) = [];
    end
        
    % Check if we are turning currently on
    if strcmp(hObject.State,'on')
        %Go through on all Edits, check if they are on and if so turn them off
        mTB = handles.mainToolbar;
        childList = mTB.Children;
        for i=1:length(childList)
            if startsWith(childList(i).Tag,'toggleObjectEdit') && strcmp(childList(i).State,'on') && ~strcmp(hObject.Tag,childList(i).Tag)
                childList(i).State = 'off';
                InteractiveWindow([childList(i).Tag '_ClickedCallback'],childList(i),[],handles);
            end
        end
    end
    
    if strcmp(editType,'delete')
        editColor = 'r';
    elseif strcmp(editType,'extend')
        editColor = 'g';
    end
        
    if strcmp(hObject.State,'on')        
        uData.CSM.editObject = images.roi.Circle(...
            'Center', [handles.MainView.XLim(1)+50 handles.MainView.YLim(1)+50],...
            'Radius', 20,...
            'Parent', handles.MainView,...
            'Color',editColor,...
            'Visible','off'...   
                );                
    else
        if ~isempty(uData.CSM.editObject) && ishandle(uData.CSM.editObject)
            delete(uData.CSM.editObject);
        end
    end
    
    set(handles.MainFigure,'UserData',uData);

end

