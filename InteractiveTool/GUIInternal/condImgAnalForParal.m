function [succFinished,up2D] = condImgAnalForParal( imgNumber, up2D, cpHandles, ECP)
%Runs the analysis on the selected image if needed according to the up2Date
%indicator. It also handles the possible stop requests.
    
if ~all(up2D)
    [succFinished] = runPipelineOnImage(imgNumber,cpHandles,ECP,~up2D);
    if succFinished
        up2D(:) = 1;
    end
else
    succFinished = 1;    
end

end

