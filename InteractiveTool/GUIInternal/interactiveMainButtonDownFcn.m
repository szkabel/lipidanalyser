function interactiveMainButtonDownFcn(hObject,~)

    handles = guidata(hObject);
    %This is the main figure
    uData = handles.MainFigure.UserData;
    
    clickedPoint = get(hObject.Parent, 'CurrentPoint');
    approxClickedPix = round(clickedPoint(1,1:2));          

    [index,colorCode,objName] = identifySelectedObjectCSM(handles);
    
    fullImageList = get(handles.ImageList,'String');
    imgNumber = uData.selectedImage;            
    imageName = fullImageList{imgNumber};

    if index
        maskImg = retrieveMaskImage(uData.ECP,objName,imageName);
        objNum = maskImg(approxClickedPix(2),approxClickedPix(1));
        switch handles.MainFigure.SelectionType
            case 'normal'                                                    
                if ~isempty(uData.CSM.selectedObject)
                    for i=1:length(uData.CSM.selectedObject.roi)
                        delete(uData.CSM.selectedObject.roi{i});
                    end
                    uData.CSM.selectedObject = [];
                end

                if objNum
                %Clicked on object
                    edgeIdx = bwboundaries(maskImg == objNum,'noholes');
                    edgeIdx = fliplr(edgeIdx{1});
                    uData.CSM.selectedObject.roi = {drawfreehand(handles.MainView,'Position',edgeIdx,'Color',colorCode)};
                    uData.CSM.selectedObject.objNum = {objNum};
                    uData.CSM.selectedObject.objName = {objName};
                    uData.CSM.selectedObject.imageName = {imageName};
                end                                
            case 'extend'
                if objNum
                %Clicked on object
                    edgeIdx = bwboundaries(maskImg == objNum,'noholes');
                    edgeIdx = fliplr(edgeIdx{1});
                    uData.CSM.selectedObject.roi{end+1} = drawfreehand(handles.MainView,'Position',edgeIdx,'Color',colorCode);
                    uData.CSM.selectedObject.objNum{end+1} = objNum;
                    uData.CSM.selectedObject.objName{end+1} = objName;
                    uData.CSM.selectedObject.imageName{end+1} = imageName;
                end                                

        end
    else
        if ~isempty(uData.CSM.selectedObject)
            for i=1:length(uData.CSM.selectedObject.roi)
                delete(uData.CSM.selectedObject.roi{i});
            end
            uData.CSM.selectedObject = [];
        end
    end
    
    set(handles.MainFigure,'UserData',uData);
    
end