function [imageToDisplay,chanSet] = displayImage(handles, visible, imgNum,varargin)
    %Displays the selected image on the main view        
    
    if nargin<2
        visible = 1;
    end
    uData = get(handles.MainFigure,'UserData');    
    if nargin<3        
        imgNumber = uData.selectedImage;
    else
        imgNumber = imgNum;
    end
    fullImageList = get(handles.ImageList,'String');
    
    actPos = round(get(handles.MainView,'Position'));
    preferredImgSize = actPos([4 3]);

    if imgNumber == 0
        gray = ones(10,10,3).*0.3;
        imageToDisplay = imresize(gray,preferredImgSize);
        chanSet = uData.visualizationSettings.channels;
        handles.MainView.XLim = [0.5 10];
        handles.MainView.YLim = [0.5 10];
    else % so a real image is loaded

        %STEP 1. Channels and raw images
        imageName = fullImageList{imgNumber};
        ECP = uData.ECP;

        %adjust channel images
        channelNames = listImageChannels(ECP);
        nofChannels = length(channelNames);
        chanSet = uData.visualizationSettings.channels;
        separateImages = cell(1,nofChannels);
        weights = colorWeights(chanSet);
        for i=1:nofChannels
            tmp = im2double(retrieveOriginalImage(ECP,imageName,channelNames{i},i));
            %imresize(im2double(retrieveOriginalImage(ECP,imageName,channelNames{i})),preferredImgSize); %scale to 0-1 and resize
            %If it is RGB 3 channel
            if size(tmp,3) == 3                
                %On color images the smart stretch is not defined yet
                for j=1:3
                    separateImages{i}(:,:,j) = adjustImgByChanSet(tmp(:,:,j),chanSet,i);
                end
            else
                if size(tmp,3)>1
                    tmp = rgb2gray(tmp);
                end
                chanSet = updateSmartStrechValues(chanSet,i,tmp);           
                if chanSet{i}.smartStrech.on
                    tmp = imadjust(tmp,[chanSet{i}.smartStrech.min chanSet{i}.smartStrech.max],[0,1]);
                end
                separateImages{i} = adjustImgByChanSet(tmp,chanSet,i);
            end
        end        

        twoDSize = size(separateImages{1});
        twoDSize = twoDSize(1:2);
        imageToDisplay = zeros([twoDSize 3]);
        for i=1:nofChannels
            if size(separateImages{i},3) == 1
                imageToDisplay = imageToDisplay + repmat(separateImages{i},1,1,3).*repmat(reshape(chanSet{i}.color,[1,1,3]),twoDSize(1),twoDSize(2)).*weights(i);
            else
                imageToDisplay = imageToDisplay + separateImages{i}.*repmat(reshape(chanSet{i}.color,[1,1,3]),twoDSize(1),twoDSize(2)).*weights(i);
            end
        end
        
        %STEP 2: Contours   
        objectNames = cellfun(@(x)(x.name),uData.visualizationSettings.outlines,'UniformOutput',false);
        weights = colorWeights(uData.visualizationSettings.outlines);
        contourImage = zeros(size(imageToDisplay));
        sumLogic = false(size(imageToDisplay));
        centerPoints = cell(1,length(weights));
        for i=1:length(weights)
            if weights(i)~=0 || uData.visualizationSettings.outlines{i}.objectNumbers~=0
                tmpMask = retrieveMaskImage( ECP, objectNames{i}, imageName);%imresize(retrieveMaskImage( ECP, objectNames{i}, imageName),preferredImgSize,'nearest');                
                if weights(i)~=0
                    contourLogic = logical(CPlabelperim(double(tmpMask))); %use the wonderful CP function                    
                    st = strel('square',uData.visualizationSettings.outlines{i}.width);
                    contourLogic = imdilate(contourLogic,st); %make it thicker as needed
                    contourLogic = any(contourLogic,3); %flatten out 3rd dimension
                    contourDouble = double(contourLogic);
                    contourImage = contourImage + repmat(contourDouble,1,1,3).*repmat(reshape(uData.visualizationSettings.outlines{i}.color,[1,1,3]),twoDSize(1),twoDSize(2)).*weights(i);
                    sumLogic = sumLogic | repmat(contourLogic,1,1,3);
                end
                if uData.visualizationSettings.outlines{i}.objectNumbers~=0
                    centerPoints{i} = regionprops(tmpMask,'centroid');
                end
            end
        end
        imageToDisplay(sumLogic) = contourImage(sumLogic);
        
        %STEP 3: object numbers
        for i=1:length(centerPoints)
            if ~isempty(centerPoints{i})                
                imageToDisplay = insertText(imageToDisplay,cell2mat({centerPoints{i}.Centroid}'),...
                cellfun(@num2str,num2cell(1:length(centerPoints{i})),repmat({'%d'},1,length(centerPoints{i})),'UniformOutput',false),...
                'FontSize',uData.visualizationSettings.outlines{i}.objectNumberFontSize,'BoxColor',[0 0 0],'BoxOpacity',0.4,'TextColor','white');
            end
        end
                        
    end
    
    %set(handles.MainFigure,'UserData',uData);

    if visible
        %Keep here the original zoom
        oldX = handles.MainView.XLim;
        oldY = handles.MainView.YLim;
        imgObj = imshow(imageToDisplay,'Parent',handles.MainView);
        imgObj.ButtonDownFcn = @interactiveMainButtonDownFcn;
        handles.MainView.XLim = oldX;
        handles.MainView.YLim = oldY;
    end
end

function weights = colorWeights(chanSet)
%chanSet is a cellarray with structures:
%   .on to indicate if they're on
%   .color a 3 element array
%   .weight the weight of that channel
    
    nofChannels = length(chanSet);
    weights = zeros(1,nofChannels);
    colors = zeros(nofChannels,3);
    on = zeros(1,nofChannels);
    for i=1:nofChannels
        on(i) = chanSet{i}.on;
        if on(i)
            weights(i) = chanSet{i}.weight;
        else
            weights(i) = 0;
        end
        colors(i,:) = chanSet{i}.color*weights(i);
    end

    %This was removed because it resulted too dim images and contours.
    %maxSumColor = max(sum(colors,1));
    %if maxSumColor
    %    weights = weights/maxSumColor; %normalization if there is more than 0 sum max color
    %end
end
    
function chanSet = updateSmartStrechValues(chanSet,i,tmp)
    newMin = min(tmp(:));
    newMax = max(tmp(:));
    
    if newMin < chanSet{i}.smartStrech.min
        chanSet{i}.smartStrech.min = newMin;
    end
    
    if newMax > chanSet{i}.smartStrech.max
        chanSet{i}.smartStrech.max = newMax;
    end        
end
    
function tmp = adjustImgByChanSet(tmp,chanSet,i)
    if chanSet{i}.lims(1) >= chanSet{i}.lims(2)
        tmp = imadjust(tmp);
    else
        tmp = imadjust(tmp,chanSet{i}.lims,[0 1],chanSet{i}.gamma);
    end
end
