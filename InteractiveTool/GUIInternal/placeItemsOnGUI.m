function placeItemsOnGUI(handles)
% Sets the positions of the elements of the GUI
% All objects are surrounded with an iP/2 wide band that is the inner
% padding to make distance between objects

figurePos = get(handles.MainFigure,'Position');
fHeight = figurePos(4);
fWidth = figurePos(3);


%if fHeight<=400
%    figurePos(4) = 400;
%    set(handles.MainFigure,'Position',figurePos);
%end

%PARAMETERS
%Margins
mBottom = 5;
mTop = 5;
mLeft = 5;
mRight = 5;
iP = 10; %Inner padding
lB = 200; %The boundary between the image list and the mainview %leftBoundary
rB = 220; %The boundary between mainview and the right panel (from the right side) % the inner view will be the changable %rightBoundary
tB = 10; %The boundary between the top row and the main view %topBoundary
lowButtonHeight = 40;
nofLowButtons = 3; % The number of buttons below the image list on the left
scrollBarSide = 25;

set(handles.ImageList,'Position',[mLeft+iP/2,mBottom+lowButtonHeight*nofLowButtons+iP/2,lB-iP,fHeight-tB-mTop-mBottom-iP-lowButtonHeight*nofLowButtons]);
set(handles.MainView,'Position',[mLeft+lB+iP/2,mBottom+iP/2,fWidth-(rB+lB+iP+mLeft+mRight),fHeight-tB-mTop-iP-mBottom]);
%set(handles.BackToSettingsButton,'Position',[mLeft+iP/2,fHeight-(mTop+tB-iP/2),lB-iP,tB-iP])
%set(handles.DataExportButton,'Position',[mLeft+lB+iP/2, fHeight-(mTop+tB-iP/2) lB-iP, tB-iP]);

%low buttons
set(handles.AnalyzeAllParallelButton,'Position',[mLeft+iP/2,mBottom+lowButtonHeight*2+iP/2,lB-iP,lowButtonHeight-iP])
set(handles.AnalyzeAllButton,'Position',[mLeft+iP/2,mBottom+lowButtonHeight+iP/2,lB-iP,lowButtonHeight-iP]);
set(handles.StopButton,'Position',[mLeft+iP/2,mBottom+iP/2,lB-iP,lowButtonHeight-iP]);
set(handles.VisualizationPanel,'Position',[fWidth-mRight-rB+iP/2,mBottom+iP/2,rB-iP/2,fHeight-tB-mTop-iP-mBottom]);

innerVisPanelPos = get(handles.InnerVisualizationPanel,'Position');
set(handles.InnerVisualizationPanel,'Position',[0 0 rB-iP/2-scrollBarSide innerVisPanelPos(4)]);

%To update the position of the visualization panel elements if possible
%if isfield(handles,'ChannelPanels') && isfield(handles,'ContourPanels')
%    placeItemsOnVisualizationPanel( handles )
%end