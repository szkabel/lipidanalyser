function matchUpObjectSelectorColors(handles,contSet)

%uData = get(handles.MainFigure,'UserData');

mTB = handles.mainToolbar;

for i=1:length(contSet)
    if ~strcmp(contSet{i}.name,'Image')
        for j=1:length(mTB.Children)          
            if startsWith(mTB.Children(j).Tag,'objectSelectorToggle') && strcmp(contSet{i}.name,strsplitN(mTB.Children(j).Tag,'_',2))
                mTB.Children(j).CData = ones(16,16,3).*repmat(reshape(contSet{i}.color,1,1,3),16,16);
            end
        end
    end
end

end

